//
//  ASTremorInterstitialAdProvider.h
//  AerServSDK
//
//  Created by Albert Zhu on 4/12/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <AerServSDK/AerServSDK.h>

#define kTremorVideoAdClassStr @"TremorVideoAd"
#define kTremorParamsAppIDKey @"appId"

@interface ASTremorInterstitialAdProvider : ASCustomInterstitialAdProvider

@end

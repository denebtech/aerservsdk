//
//  ASMoPubSdkBannerAdProvider.h
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ASCustomBannerAdProvider.h"

@interface ASMoPubSdkBannerAdProvider : ASCustomBannerAdProvider

@end

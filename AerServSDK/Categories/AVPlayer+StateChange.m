//
//  AVPlayer+StateChange.m
//  AerServSDK
//
//  Created by Vasyl Savka on 9/18/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "AVPlayer+StateChange.h"
#import "ASCategoryHelper.h"

@implementation AVPlayer (StateChange)

- (void)changeStateToPlay {
    [self play];
    [ASCategoryHelper instance].videoPauseState = NO;
}

- (void)changeStateToPause {
    [self pause];
    [ASCategoryHelper instance].videoPauseState = YES;
}

- (void)playPause {
    if(self.rate == 0.0f) {
        [self changeStateToPlay];
    } else {
        [self changeStateToPause];
    }
}

@end

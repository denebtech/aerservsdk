//
//  ASUnityInterstitialAdProvider.m
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASUnityInterstitialAdProvider.h"
#import "ASUnityInstance.h"
#import "ASUnityDelegateProxy.h"

@interface ASUnityInterstitialAdProvider()

@property (nonatomic, strong) NSString *placementId;

@property (nonatomic, strong) NSString *zoneId;

@end



@implementation ASUnityInterstitialAdProvider

- (instancetype)init {
    if (self = [super initWithAdClassName:kUnityAdsClassStr timeout:kUnityInterstitialTimeout]) {
        // Add init code here
    }
    return self;
}

- (void)initializePartnerAd:(NSDictionary *)properties { }

- (BOOL)hasPartnerAdInitialized {
    return YES;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    [ASUnityInstance startWithUnityGameID:[self valueInProperty:properties forKey:kUnityGameIdKey]];
    self.adReadyForPlacement = NO;
    
    NSString *placement = [self valueInProperty:properties forKey:kUnityZoneIdKey];
    if ([placement length] > 0) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"Seting provider's placement ID to %@", self.placementId]];
        self.placementId = placement;
    }
    [ASUnityInstance addUnityProvider:self forPlacementId:placement];
    
    if ([kTrueStr isEqualToString:[self valueInProperty:properties forKey:kUnityTestModeKey]]) {
        [ASSDKLogger logStatement:@"Setting test mode to YES"];
        [[ASUnityInstance instance].unityAdsClass setDebugMode:YES];
    }
}

- (BOOL)hasPartnerAdLoaded {
    return [self adLoadedForPlacement] || [self adLoaded];
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.failedToReceiveAd;
}

- (void)cancel {
    [self cleanupAd];
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    if([self adLoadedForPlacement]) {
        [[ASUnityInstance instance].unityAdsClass show:rootViewController placementId:self.placementId];
    } else if([self adLoaded]) {
        [[ASUnityInstance instance].unityAdsClass show:rootViewController];
    }
}

- (void)terminatePartnerAd {
    [self cleanupAd];
}

- (void)cleanupAd { }

#pragma mark - Unity Interstitial Ad Provider Helpers

- (BOOL)adLoadedForPlacement {
    BOOL adReady = NO;
    if(!self.adReadyForPlacement) {
        adReady = [[ASUnityInstance instance].unityAdsClass isReady:self.placementId];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInterstitialAdProvider, hasPartnerAdLoaded for placementId: %@ - adLoadedForPlacementId: %d", self.placementId, adReady]];
    } else {
        adReady = self.adReadyForPlacement;
    }
    return adReady;
}

- (BOOL)adLoaded {
    BOOL adReady = [[ASUnityInstance instance].unityAdsClass isReady];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInterstitialAdProvider, hasPartnerAdLoaded - adLoaded: %d", adReady]];
    return adReady;
}

- (NSString *)getPlacement {
    return self.placementId;
}

@end

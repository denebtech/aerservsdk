//
//  ASVideoEndCardInterstitialAdProvider.m
//  AerServSDK
//
//  Created by Albert Zhu on 4/26/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASVideoEndCardInterstitialAdProvider.h"

#import "ASVideoEndCardAd.h"
#import "ASVideoEndCardInterstitialViewController.h"

#import "NSString+ASUtils.h"

@interface ASVideoEndCardInterstitialAdProvider () <ASVideoEndCardAdDelegate>

@property (nonatomic, strong) ASVideoEndCardAd *vecAd;

@end

@implementation ASVideoEndCardInterstitialAdProvider

- (void)dealloc {
    _vecAd = nil;
}

- (BOOL)processData:(NSData*)data withResponseHeaders:(NSDictionary *)headers {
    if([data length] <= 1) return NO;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVideoEndCardInterstitialAdProvider, processData:withResponseHeaders: - data.length: %ld", (long)data.length]];
    
    self.vecAd = [ASVideoEndCardAd adFromJSONData:data withDelegate:self asPreload:self.isPreload];
    
    if(headers[kXAerServKey] != nil) {
        NSString *additionalParamsStr = headers[kXAerServKey];
        NSDictionary *additionalParametersObj = [additionalParamsStr JSONObjectValue];
        [self.vecAd processAdditionalParameters:additionalParametersObj];
    }
    
    return self.vecAd != nil;
}

#pragma mark - ASVideoEndCardAdDelegate

- (void)videoEndCardDidLoad {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialAdProvider, videoEndCardDidLoad"];
    
    __weak ASVideoEndCardInterstitialAdProvider *adProvider = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        adProvider.viewControllerToPresent = [ASVideoEndCardInterstitialViewController viewControllerWithVideoEndCardAd:adProvider.vecAd andDelegate:adProvider];
        
        // BUILD OUT VIDEO END CARD VIEW CONTROLLER
        
    });
}

@end

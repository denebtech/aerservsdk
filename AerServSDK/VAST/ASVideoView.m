//
//  ASVideoView.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/31/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVideoView.h"

@implementation ASVideoView

+ (Class)layerClass {
    return [AVPlayerLayer class];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
    }
    return self;
}

- (AVPlayerLayer *)playerLayer {
    return (AVPlayerLayer *)self.layer;
}

- (void)attachPlayer:(AVPlayer*)player {
    self.playerLayer.player = player;
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
}

- (void)changeFrame:(CGRect)frame {
    self.playerLayer.frame = frame;
}

@end

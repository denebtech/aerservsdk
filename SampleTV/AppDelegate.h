//
//  AppDelegate.h
//  SampleTV
//
//  Created by Vasyl Savka on 10/5/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


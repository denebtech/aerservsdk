//
//  ASTransactionInfo.h
//  AerServSDK
//
//  Created by Vasyl Savka on 4/18/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kTransactionKey @"transaction"
#define kTransactionBuyerNameKey @"buyerName"
#define kTransactionBuyerPriceKey @"buyerPrice"

#define kImpressionUrlKey @"ImpressionURL"

#define kTransactionBuyerQueryParameter @"&buyer="
#define kAmpersandToken @"&"

@interface ASTransactionInfo : NSObject

+ (instancetype)infoWithTransactionObj:(NSDictionary *)transactionObj;
- (NSString *)getBuyer;
- (double)getPrice;
- (NSDictionary *)asTransactionInfoObj;
- (NSString *)asTransactionInfoJSONStr;
- (NSDictionary *)createDictionaryContainingBothTransactionAndVCData:(NSDictionary*)vcData;
- (void)updatedTransactionInfoWithResponseHeadersData:(NSData *)asRespHeaderData;

@end

//
//  ASErrorCodeManager.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/15/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASErrorCodeManager.h"
#import "ASPubSettingUtils.h"

@interface ASErrorCodeManager()

@property (nonatomic, strong) NSDictionary *errParams;

@end

@implementation ASErrorCodeManager

- (instancetype)initWithParams:(NSDictionary *)params {
    
    if(self = [super init]) {
        self.errParams = [NSDictionary dictionaryWithDictionary:params];
    }
    
    return self;
}

- (instancetype)initWithParamString:(NSString *)paramsStr {
    NSDictionary *paramsObj = [paramsStr JSONObjectValue];
    
    return [self initWithParams:paramsObj];
}

- (NSError *)checkForErrorCodeToSwitchError:(NSError *)error {
    // check for error code
    if(self.errParams != nil && self.errParams[kErrorCodeKey] != nil) {
        NSString *errMsg = nil;
        switch ([self.errParams[kErrorCodeKey] intValue]) {
            case kFreqCapReachedErrorCode: // frequency cap reached
                if(self.errParams[kBudgetResetTimeKey] != nil) {
                    errMsg = [ASErrorCodeManager findRemainingTimeTillEpochTime:[self.errParams[kBudgetResetTimeKey] longValue]];
                }
                break;
            default:
                break;
        }
        error = [NSError errorWithDomain:error.domain
                                    code:[self.errParams[kErrorCodeKey] integerValue]
                                userInfo:@{NSLocalizedDescriptionKey : (errMsg != nil ? errMsg : error.localizedDescription)}];
    }
    
    return error;
}

#pragma mark - Freq Cap Helpers

+ (NSString *)findRemainingTimeTillEpochTime:(long)time {
    NSDate *resetTime = [NSDate dateWithTimeIntervalSince1970:time];
    NSTimeInterval timeLeft = [resetTime timeIntervalSinceDate:[NSDate date]];
    
    int hoursLeft;
    if(timeLeft > kMinutesPerHour * kSecondsPerMinute) {
        hoursLeft = (int)(timeLeft/(kMinutesPerHour * kSecondsPerMinute));
        timeLeft = (long long)timeLeft%(hoursLeft*kMinutesPerHour*kSecondsPerMinute);
    } else {
        hoursLeft = 0;
    }
    
    int minLeft;
    if(timeLeft > kSecondsPerMinute) {
        minLeft = (int)(timeLeft/kSecondsPerMinute);
        timeLeft = (long long)timeLeft%(minLeft*kSecondsPerMinute);
    } else {
        minLeft = 0;
    }
    
    if(timeLeft > 0) {
        minLeft++;
        if(minLeft == 60) {
            hoursLeft++;
            minLeft = 0;
        }
    }
    
    return [NSString stringWithFormat:@"%d:%@", hoursLeft, [NSString stringWithFormat:(minLeft < 10 ? @"0%d" : @"%d"), minLeft]];
}

#pragma mark - TESTING

+ (NSDictionary *)injectVCFreqCapErrorInto:(NSDictionary *)aerservHeader {
    
    #if kInjectVCFreqCap
    NSDate *currTime;
    long timeIntervalLong;
    currTime = [NSDate date];
    timeIntervalLong = (long)[currTime timeIntervalSince1970];
//    timeIntervalLong += 0*kMinutesPerHour*kSecondsPerMinute + 0*kSecondsPerMinute + 35; // 0h 0m 35s
//    timeIntervalLong += 5*kMinutesPerHour*kSecondsPerMinute + 59*kSecondsPerMinute + 35; // 5h 59m 35s
    timeIntervalLong += 5*kMinutesPerHour*kSecondsPerMinute + 59*kSecondsPerMinute + 1; // 5h 59m 1s
    NSDictionary *errDic = @{kErrorCodeKey : [NSNumber numberWithInt:1001],
                             kBudgetResetTimeKey : [NSNumber numberWithLong:timeIntervalLong]};
    NSMutableDictionary *testDic = [NSMutableDictionary dictionaryWithDictionary:aerservHeader];
    [testDic setObject:errDic forKey:kErrorParamsKey];
    aerservHeader = [NSDictionary dictionaryWithDictionary:testDic];
    #endif
    
    return aerservHeader;
}

@end

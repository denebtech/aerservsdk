//
//  ASAnalyticsConfigChecker.h
//  AerServSDK
//
//  Created by Vasyl Savka on 10/30/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kAnalyticsConfigEndPoint @"https://ads.aerserv.com/as/sdk/configure/"
#define kAerServAnalyticsConfigSearchTerm @"sybok/config"
#define kASConfigConnTimeout 5.0f

#define kMediationConfigEndPoint
#define kApplicationKey @"application"
#define kVersionKey @"version"
#define kPlacementKey @"placementIds"
#define kSiteIDKey @"siteId"
#define kiPhoneSDKKey @"iPhone SDK"
#define kTVOSSDKKey @"tvOS SDK"

#define kAnalyticsKey @"analytics"
#define kEnabledKey @"enabled"
#define kDisabledEventsKey @"disabledEvents"

#define kAdapterConfigKey @"adapterConfigurations"
#define kAdapterNameKey @"adapterName"

@interface ASAnalyticsConfigChecker : NSObject

@property (nonatomic, assign) BOOL analyticsEnabled;
@property (nonatomic, strong) NSDictionary *configJSONObj;
@property (nonatomic, strong) NSArray *disbaledEventsArr;

+ (ASAnalyticsConfigChecker *)checker;
+ (void)checkAnalyticsConfigWithPlacements:(NSArray *)plcArr orSiteId:(NSString *)siteIdStr;
+ (BOOL)checkDisblaedEventsforCategory:(NSString *)cat andAction:(NSString *)act;

@end

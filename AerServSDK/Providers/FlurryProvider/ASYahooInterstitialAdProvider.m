//
//  ASYahooInterstitialAdProvider.m
//  AerServSDK
//
//  Created by Sung-Ho Tsai on 8/20/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASYahooInterstitialAdProvider.h"
#import "ASCustomInterstitialAdProvider.h"
#import "FlurryAdInterstitial.h"
#import "Flurry.h"

@interface ASYahooInterstitialAdProvider()<FlurryAdInterstitialDelegate>

@property (nonatomic, strong) id flurryAdInterstitial;
@property (nonatomic, assign) BOOL failedToReceiveAd;

@end


@implementation ASYahooInterstitialAdProvider

- (instancetype)init {
    if (self = [super initWithAdClassName:@"FlurryAdInterstitial" timeout:kCustomInterstitialTimeout]) {
        // Add init code here
    }
    return self;
}

- (void)initializePartnerAd:(NSDictionary *)properties {
    Class flurryClass = NSClassFromString(@"Flurry");
    NSString *apiKey = [self valueInProperty:properties forKey:@"YahooApiKey"];
    [flurryClass startSession:apiKey];
}

- (BOOL)hasPartnerAdInitialized {
    return YES;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    NSString *adSpace = [self valueInProperty:properties forKey:@"YahooAdSpace"];
    Class flurryAdInterstitialClass = NSClassFromString(@"FlurryAdInterstitial");
    self.flurryAdInterstitial = [[flurryAdInterstitialClass alloc] initWithSpace:adSpace];
    [self.flurryAdInterstitial setAdDelegate:self];
    [self.flurryAdInterstitial fetchAd];
}

- (BOOL)hasPartnerAdLoaded {
    return [self.flurryAdInterstitial ready];
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.failedToReceiveAd;
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    if ([self.flurryAdInterstitial ready]) {
        [self.flurryAdInterstitial presentWithViewController:rootViewController];
    }
}

#pragma mark - FlurryAdInterstitialDelegate Methods

- (void)adInterstitial:(FlurryAdInterstitial *)interstitialAd adError:(FlurryAdError)adError errorDescription:(NSError *)errorDescription {
    [ASSDKLogger logStatement:@"ASYahooInterstitialAdProvider, adError"];
    self.failedToReceiveAd = YES;
}

- (void)adInterstitialDidDismiss:(FlurryAdInterstitial *)interstitialAd {
    [ASSDKLogger logStatement:@"ASYahooInterstitialAdProvider, adInterstitialDidDismiss"];
    [self asPartnerInterstitialAdDidDisappear];
}

- (void)adInterstitialDidFetchAd:(FlurryAdInterstitial *)interstitialAd {
    [ASSDKLogger logStatement:@"ASYahooInterstitialAdProvider, adInterstitialDidFetchAd"];
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
}

- (void)adInterstitialDidReceiveClick:(FlurryAdInterstitial *)interstitialAd {
    [ASSDKLogger logStatement:@"ASYahooInterstitialAdProvider, adInterstitialDidReceiveClick"];
    [self asPartnerInterstitialAdWasTouched];
}

- (void)adInterstitialDidRender:(FlurryAdInterstitial *)interstitialAd {
    [ASSDKLogger logStatement:@"ASYahooInterstitialAdProvider, adInterstitialDidRender"];
    [self asPartnerInterstitialAdDidAppear];
}

- (void)adInterstitialVideoDidFinish:(FlurryAdInterstitial *)interstitialAd {
    [ASSDKLogger logStatement:@"ASYahooInterstitialAdProvider, adInterstitialVideoDidFinish"];
    [self asPartnerInterstitialVideoCompleted];
}

- (void)adInterstitialWillDismiss:(FlurryAdInterstitial *)interstitialAd {
    [ASSDKLogger logStatement:@"ASYahooInterstitialAdProvider, adInterstitialWillDismiss"];
    [self asPartnerInterstitialAdWillDisappear];
}

- (void)adInterstitialWillLeaveApplication:(FlurryAdInterstitial *)interstitialAd {
    [ASSDKLogger logStatement:@"ASYahooInterstitialAdProvider, adInterstitialWillLeaveApplication"];
}

- (void)adInterstitialWillPresent:(FlurryAdInterstitial *)interstitialAd {
    [ASSDKLogger logStatement:@"ASYahooInterstitialAdProvider, adInterstitialWillPresent"];
    [self asPartnerInterstitialAdWillAppear];
}

@end
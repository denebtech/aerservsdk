//
//  ASWebView.m
//  AerServSDK
//
//  Created by Scott A Andrew on 4/14/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASHTTPAdView.h"
#import "ASAdView+UnityCheck.h"
#import "ASMRAIDView.h"
#import "ASHTMLUtils.h"

@interface ASHTTPAdView()<UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, assign) NSInteger openRequests;
@property (nonatomic, assign) NSInteger contentTestQueuedCount;

@property (nonatomic, assign) BOOL loadingDelegateCalled;
@property (nonatomic, strong) UIGestureRecognizer* tapRecognizer;
@property (nonatomic, assign) BOOL wasTapped;
@property (nonatomic, strong) UIImage *baselineImg;

@end

@implementation ASHTTPAdView

#ifndef NSFoundationVersionNumber_iOS_6_1
#define NSFoundationVersionNumber_iOS_6_1 993.00
#endif

#define MPOffscreenWebViewNeedsRenderingWorkaround() (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTTPAdView, initWithFrame: - frame: %@", NSStringFromCGRect(frame)]];
        self.mediaPlaybackRequiresUserAction = NO;
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        self.delegate = self;
        self.suppressesIncrementalRendering = YES;
        _openRequests = 0;
        _contentTestQueuedCount = 0;
        
        // we are going to put a helper in for our clicks. There are some ads
        // that don't report back a link but we will make sure we know we are
        // clicked or not and ask the delegate for what to do..
        _tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        _tapRecognizer.delegate = self;
        _tapRecognizer.cancelsTouchesInView = NO;
        _tapRecognizer.delaysTouchesEnded = NO;
        self.scrollView.scrollEnabled = NO;
        
        [self addGestureRecognizer:_tapRecognizer];
        
        _baselineImg = [self takeSnapshotImage];
    }
    
    return self;
}

- (void)cleanUp {
    [super cleanUp];
    [ASSDKLogger logStatement:@"ASHTTPAdView, cleanUp"];
    
    [self stopLoading];
    self.delegate = nil;
    [self removeGestureRecognizer:self.tapRecognizer];
    self.tapRecognizer = nil;
    self.baselineImg = nil;
    
    [self loadHTMLString:@"" baseURL:nil];
    [self stopLoading];
    [self removeFromSuperview];
}

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASHTTPAdView, dealloc"];
    [self resetCache];
}

//- (void)rotateToOrientation:(UIInterfaceOrientation)orientation {
//    [self forceRedraw];
//}

- (void)forceRedraw {
    @try {
        
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        int angle = -1;
        switch (orientation)
        {
            case UIInterfaceOrientationPortrait: angle = 0; break;
            case UIInterfaceOrientationLandscapeLeft: angle = 90; break;
            case UIInterfaceOrientationLandscapeRight: angle = -90; break;
            case UIInterfaceOrientationPortraitUpsideDown: angle = 180; break;
            default: break;
        }
        
        if (angle == -1) return;
        
        // UIWebView doesn't seem to fire the 'orientationchange' event upon rotation, so we do it here.
        NSString *orientationEventScript = [NSString stringWithFormat:
                                            @"window.__defineGetter__('orientation',function(){return %d;});"
                                            @"(function(){ var evt = document.createEvent('Events');"
                                            @"evt.initEvent('orientationchange',true,true);window.dispatchEvent(evt);})();",
                                            angle];
        [self stringByEvaluatingJavaScriptFromString:orientationEventScript];
        
        // XXX: If the UIWebView is rotated off-screen (which may happen with interstitials), its
        // content may render off-center upon display. We compensate by setting the viewport meta tag's
        // 'width' attribute to be the size of the webview.
        NSString *viewportUpdateScript = [NSString stringWithFormat:
                                          @"document.querySelector('meta[name=viewport]')"
                                          @".setAttribute('content', 'width=%f;', false);",
                                          self.frame.size.width];
        [self stringByEvaluatingJavaScriptFromString:viewportUpdateScript];
        
        // XXX: In iOS 7, off-screen UIWebViews will fail to render certain image creatives.
        // Specifically, creatives that only contain an <img> tag whose src attribute uses a 302
        // redirect will not be rendered at all. One workaround is to temporarily change the web view's
        // internal contentInset property; this seems to force the web view to re-draw.
        if (MPOffscreenWebViewNeedsRenderingWorkaround()) {
            if ([self respondsToSelector:@selector(scrollView)]) {
                UIScrollView *scrollView = self.scrollView;
                UIEdgeInsets originalInsets = scrollView.contentInset;
                UIEdgeInsets newInsets = UIEdgeInsetsMake(originalInsets.top + 1,
                                                          originalInsets.left,
                                                          originalInsets.bottom,
                                                          originalInsets.right);
                scrollView.contentInset = newInsets;
                scrollView.contentInset = originalInsets;
            }
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (UIImage *)takeSnapshotImage {
    UIGraphicsBeginImageContext(self.bounds.size);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return viewImage;
}


- (BOOL)isContentDifferentThanBaseline {
	//we skip bitmap check all together if unity. @TODO: figure out how to fix bitmap check on unity
	if([ASAdView getIsUnity]) {
        [ASSDKLogger logStatement:@"ASHTTPAdView, isContentDifferentThanBaseLine - IS UNITY"];
		return YES;
    } else {
        [ASSDKLogger logStatement:@"ASHTTPAdView, isContentDifferentThanBaseLine - Comparing content"];
    	// take a screenshot of the webview
	    // compare it to the uninitialized state, return YES if screenshot is different from original
    	UIImage *viewImage = [self takeSnapshotImage];
        
	    NSData *baselineData = UIImagePNGRepresentation(self.baselineImg);
	    NSData *snapshotData = UIImagePNGRepresentation(viewImage);
        
	    return ![snapshotData isEqualToData:baselineData];
	}
}

- (void)scaleAd:(float)scale {
    
    self.transform = CGAffineTransformMakeScale(scale, scale);
}

#pragma mark - NSURLCache Reset

- (void)resetCache {
    [ASSDKLogger logStatement:@"ASHTTPAdView, resetCache"];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
}

#pragma mark - Bit Map Check

- (void)continueHTTPTAdViewLoad {
    [ASSDKLogger logStatement:@"ASHTTPAdView, continueHTTPTAdViewLoad - force redraw of content before finishing ad load"];
    [self forceRedraw];
    
    [self calculateContentSize];
    
    // checking for bpf to fire advertiser event
    if([ASHTMLUtils doesContainBPFInWebView:self]) {
        [self.adDelegate HTTPAdViewDidFireBPF:self];
    }
    
    if ([self.adDelegate respondsToSelector:@selector(HTTPAdViewFinsihedLoading:)]) {
        [self.adDelegate HTTPAdViewFinsihedLoading:self];
        [ASAdResponseLogger endAdResponseAndSendEvent];
    }
}

- (void)failHTTPAdViewLoad {
    [ASSDKLogger logStatement:@"ASHTTPAdView, failHTTPAdViewLoad - There's nothing to render, fail load"];
    NSDictionary *details = @{NSLocalizedDescriptionKey : @"nothing rendered"};
    NSError *error = [NSError errorWithDomain:@"AerServSDK" code:200 userInfo:details];                                                                                                       [self.adDelegate HTTPAdViewFailedLoading:self withError:error];
}

- (void)bitMapCheck {
    
    // create a serial bit map checker queue
    static dispatch_queue_t checkQ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        checkQ = dispatch_queue_create("com.aeserv.AerServSDK.bitmapcheckqueue", DISPATCH_QUEUE_SERIAL);
    });
    
    __weak ASHTTPAdView *httpAdView = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        __block BOOL hasContent = NO;
        
        // queue up 4 attempts for checking for content
        for(int bitMapAttempt = 0; bitMapAttempt < kBitMapCheckMaxAttempts; bitMapAttempt++) {
            dispatch_async(checkQ, ^{
                
                @try {
                    // pause queue for delay
                    dispatch_suspend(checkQ);
                    
                    // if content has not been seen create a delay to check again, else short circuit when content found
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (!hasContent) ? kBitMapCheckDelayInMS * NSEC_PER_MSEC : 0), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                        @try {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                @try {
                                    if(!httpAdView) return;
                                    
                                    // if content hasn't been see, compare with base line
                                    if(!hasContent) {
                                        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTTPAdView, bitMapCheck - bitMapAttempt: %d -- Checking For Content", bitMapAttempt]];
                                        hasContent = [httpAdView isContentDifferentThanBaseline];
                                    } else {
                                        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTTPAdView, bitMapCheck - bitMapAttempt: %d -- Already Found Content", bitMapAttempt]];
                                    }
                                }
                                @catch (NSException *exception) {
                                    [ASSDKLogger onException:exception];
                                }
                                @finally {
                                    // resume serial queue for next attempt
                                    dispatch_resume(checkQ);
                                }
                            });
                        }
                        @catch (NSException *exception) {
                            [ASSDKLogger onException:exception];
                            dispatch_resume(checkQ);
                        }
                    });
                }
                @catch (NSException *exception) {
                    [ASSDKLogger onException:exception];
                    dispatch_resume(checkQ);
                }
            });
        }
        
        // queue up final action dependent on whether content has been found
        dispatch_async(checkQ, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                @try {
                    if(!httpAdView) return;
                    
                    if(hasContent) {
                        [httpAdView continueHTTPTAdViewLoad];
                    } else {
                        [httpAdView failHTTPAdViewLoad];
                    }
                }
                @catch (NSException *exception) {
                    [ASSDKLogger onException:exception];
                    [httpAdView failHTTPAdViewLoad];
                }
                @finally {
                    httpAdView.loadingDelegateCalled = YES;
                }
            });
        });
    });
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTTPAdView, webView:shouldStartLoadwithRequest:navigationType: - request.url: %@", request.URL]];
    
    BOOL shouldStartLoading = YES;
    
    @try {
        if ([self.adDelegate respondsToSelector:@selector(HTTPAdViewWasClicked:request:)]) {
            // we have an url clicked we want to go ahead and launch safari.
            if (navigationType == UIWebViewNavigationTypeLinkClicked){
                shouldStartLoading = [self.adDelegate HTTPAdViewWasClicked:self request:request];
            }
            else if ([self.tapRecognizer state] == UIGestureRecognizerStateEnded || self.wasTapped){
                // we know we are tapped if we either got our flagg that we are tapped or we
                // our gesture recognizer is finished. Which means we need to go somewhere.
                shouldStartLoading = [self.adDelegate HTTPAdViewWasClicked:self request:request];
            }
        }
        
        // if we aren't loading what's coming next then we can do a bit of cleanup.
        if (!shouldStartLoading)
            [self stopLoading];
        
        self.wasTapped = NO;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
    return shouldStartLoading;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    self.openRequests++;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    @try {
        if (self.openRequests > 0) {
            self.openRequests--;
        }
        
        if (self.openRequests == 0) {
            // openRequests count often hits 0 before page is fully loaded, introduce a small delay to compensate
            self.contentTestQueuedCount++;
            
            if (self.openRequests == 0 && --self.contentTestQueuedCount == 0) {
                __weak ASHTTPAdView *adView = self;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, kInitialLoadingDelayInMS * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
                    // delay is set pretty long because VDopia performs a slide animation causing the screenshotting process to capture a blank view (because the contents haven't animated into the frame yet)
                    
                    @try {
                        // we want to force no border or padding on our ads.. Just incase they don't do it..
                        [ASSDKLogger logStatement:@"ASHTTPAdView, webViewDidFinishLoad: - force no border or padding"];
                        NSString *zeroPadding = @"document.body.style.margin='0';document.body.style.padding = '0'";
                        [webView stringByEvaluatingJavaScriptFromString:zeroPadding];
                        if (adView.adDelegate != nil && !adView.loadingDelegateCalled) {
                            [adView bitMapCheck];
                        }
                    }
                    @catch (NSException *exception) {
                        [ASSDKLogger onException:exception];
                    }
                });
            } else {
                // a request is in flight, or another block is scheduled, wait to do test later
                [ASSDKLogger logStatement:@"ASHTTPAdView, webViewDidFinishLoad: - request in flight"];
            }
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    @try {
        if (self.openRequests > 0) {
            self.openRequests--;
        }
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTTPAdView, webView:didFailLoadWithError: - HTTP ad failed to load with error %@", error.localizedDescription]];
        
        if (self.isLoading && !self.loadingDelegateCalled && [self.adDelegate respondsToSelector:@selector(HTTPAdViewFailedLoading:withError:)])
            [self.adDelegate HTTPAdViewFailedLoading:self withError:error];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - UIGestureRecognizer Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    // we are not the only tap gesture on the web view so make sure
    // to allow simulatious gestures.
    return YES;
}

- (void)tapped:(UIGestureRecognizer*)gesture {
    self.wasTapped = YES;
}

@end

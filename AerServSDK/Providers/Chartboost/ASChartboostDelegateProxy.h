//
//  ASChartboostDelegateProxy.h
//  AerServSDK
//
//  Created by Albert Zhu on 11/25/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Chartboost.h"
#import "ASChartboostInterstitialAdProvider.h"

@interface ASChartboostDelegateProxy : NSObject <ChartboostDelegate>

+ (ASChartboostDelegateProxy *)sharedProxy;
+ (void)addChartboostProvider:(ASChartboostInterstitialAdProvider *)cbInterProvider;
+ (void)removeChartboostProvider;

@end

//
//  ASVideoEndCardVideoPlayer.h
//  AerServSDK
//
//  Created by Albert Zhu on 4/27/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kAVPlayerItemKeyStatus @"status"
#define kAVPlayerItemKeyPlaybackLikelyToKeepUp @"playbackLikelyToKeepUp"
#define kAVPlayerItemKeyPlaybackBufferFull @"playbackBufferFull"

#define kVideo0Percentage 0.0f

#define kAnimationTime 0.3f
#define kOpacityNone 0.0f
#define kOpacityFull 1.0f

@class ASVideoEndCardAd;
@class ASVideoEndCardVideoPlayerView;
@protocol ASVideoEndCardVideoPlayerDelegate;

@interface ASVideoEndCardVideoPlayer : NSObject

@property (nonatomic, assign) id<ASVideoEndCardVideoPlayerDelegate> delegate;
@property (readonly, nonatomic, strong) ASVideoEndCardVideoPlayerView *view;

- (instancetype)initWithVideoEndCardAd:(ASVideoEndCardAd *)vecAd andDelegate:(id<ASVideoEndCardVideoPlayerDelegate>)delegate;
+ (instancetype)videoPlayerWithVideoEndCardAd:(ASVideoEndCardAd *)vecAd andDelegate:(id<ASVideoEndCardVideoPlayerDelegate>)delegate;
- (void)playAd;
- (void)setViewFrame:(CGRect)frame;

@end

@protocol ASVideoEndCardVideoPlayerDelegate <NSObject>

- (void)videoEndCardVideoPlayer:(ASVideoEndCardVideoPlayer *)vecVidPlayer didFailWithError:(NSError *)err;
- (void)videoEndCardVideoPlayerReadyToPlay:(ASVideoEndCardVideoPlayer *)vecVidePlayer;
- (void)videoEndCardVideoPlayerWillShowSkip:(ASVideoEndCardVideoPlayer *)vecVidPlayer;
- (void)videoEndCardVideoPlayerDidRecordTap:(ASVideoEndCardVideoPlayer *)vecVidPlayer;
- (void)videoEndCardVideoPlayerAdDidPlayCompletely:(ASVideoEndCardVideoPlayer *)vecVidPlayer;
- (UIView *)videoEndCardVideoPlayerContainingView:(ASVideoEndCardVideoPlayer *)vecVidePlayer;

- (void)videoEndCardVideoPlayer:(ASVideoEndCardVideoPlayer *)vecVidPlayer didFireAdvertiserEventWithMessage:(NSString *)msg;

@end

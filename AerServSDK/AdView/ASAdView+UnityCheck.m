//
//  ASAdView+UnityCheck.m
//  AerServSDK
//
//  Created by Vasyl Savka on 9/12/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASAdView+UnityCheck.h"

static BOOL isUnity = NO;

@implementation ASAdView (UnityCheck)

+ (BOOL)getIsUnity {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView+UnityCheck, getIsUnity - isUnity: %d", isUnity]];
    return isUnity;
}

+ (void)setIsUnity:(BOOL)val {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView+UnityCheck, setIsUnity: - val: %d", val]];
    isUnity = val;
}

@end

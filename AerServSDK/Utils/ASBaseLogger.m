//
//  ASBaseLogger.m
//  AerServSDK
//
//  Created by Vasyl Savka on 10/21/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASBaseLogger.h"
#import "ASAnalyticsConfigChecker.h"
#import "NSData+ASUtils.h"
#import "ASCommManager.h"

@interface ASBaseLogger ()

@end

@implementation ASBaseLogger

+ (ASBaseLogger *)logInstance {
    static ASBaseLogger *logInstance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        logInstance = [ASBaseLogger new];
    });
    
    return logInstance;
}

#pragma mark - Helpers

+ (long long)epochTimeInMS {
    return ([[NSDate date] timeIntervalSince1970] * 1000);
}

+ (NSNumber *)epochTimeAsNSNumberObj {
    return  [NSNumber numberWithLongLong:[self epochTimeInMS]];
}

#pragma mark - Resetting and setting common fields

+ (void)resetFields {
    [self logInstance].plc = kEmptyStr;
    [self logInstance].category = kEmptyStr;
    [self logInstance].action = kEmptyStr;
}

+ (void)setPlacement:(NSString *)plc {
    [self logInstance].plc = plc;
}

+ (void)setCategory:(NSString *)cat {
    [self logInstance].category = cat;
}

+ (void)setAction:(NSString *)action {
    [self logInstance].action = action;
}

+ (BOOL)areStaticFieldsDefined {
    #if kBaseLoggerDebug
    NSLog(@"ASBaseLogger, areStaticFieldsUndefined");
    NSLog(@"\t\t- osv: %@, checkForEmpty: %d", [self logInstance].osv, [[self logInstance].osv checkForEmpty]);
    NSLog(@"\t\t- sdkVersion: %@, checkForEmpty: %d", [self logInstance].sdkVersion, [[self logInstance].sdkVersion checkForEmpty]);
    NSLog(@"\t\t- userAgent: %@, checkForEmpty: %d", [self logInstance].userAgent, [[self logInstance].userAgent checkForEmpty]);
    #endif
    return ([[ASStaticLoggerFields fields].sdkv checkForEmpty] || [[ASStaticLoggerFields fields].ua checkForEmpty]);
}

#pragma mark - wrapping event object

+ (NSData *)createJSONData {
    NSDictionary *eventObj = @{ kCategoryKey : [self logInstance].category,
                                kActionKey : [self logInstance].action,
                                kTimestampKey : [self epochTimeAsNSNumberObj],
                                kPLCKey : [self logInstance].plc,
                                kSDKVersionKey : [ASStaticLoggerFields fields].sdkv ? [ASStaticLoggerFields fields].sdkv : @"N/A",
                                kOSVersionKey : [ASStaticLoggerFields fields].osv,
                                kUserAgentKey : [ASStaticLoggerFields fields].ua ? [ASStaticLoggerFields fields].ua : @"N/A"
                                };
    
    return [self createSendDataWithEventObj:eventObj];
}

+ (NSData *)createSendDataWithEventObj:(NSDictionary *)eventObj {
    NSError *err = nil;
    NSData *sendData;
    NSData *eventData = [NSJSONSerialization dataWithJSONObject:eventObj options:0 error:&err];
    if(!err) {
        NSDictionary *sendObj = @{ kEventsKey : [NSString stringWithFormat:@"%@\\n", [eventData stringEncoded]],
                                   kApplicationKey : kiPhoneSDKKey,
                                   kVersionKey : [ASStaticLoggerFields fields].sdkv
                                   };
        sendData = [NSJSONSerialization dataWithJSONObject:sendObj options:0 error:&err];
        if(err != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBaseLogger, createSendDataWithEventObj: -- JSONSerialization ERROR for send object: %@", err.localizedDescription]];
            sendData = nil;
        }
        
    } else {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBaseLogger, createSendDataWithEventObj: -- JSONSerialization ERROR for individual event: %@", err.localizedDescription]];
        sendData = nil;
    }
    
    #if kBaseLoggerShowPOSTData
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBaseLogger, createSendDataWithEventObj: -- sendData:\n\n%@\n\n", [sendData stringEncoded]]];
    #endif
    
    return sendData;
}

+ (void)attemptToSendAnalyticsEventsWithData:(NSData *)jsonEventData {
    
    // if analytics are endabled and there is data to be sent, then send it
    if([ASAnalyticsConfigChecker checker].analyticsEnabled && jsonEventData != nil) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            [ASCommManager sendPostRequestToEndPoint:kAnalyticsEventLoggingEndPoint
                                        withJSONData:jsonEventData
                                             forTime:kASLoggerConnTimeout
                              endWithCompletionBlock:^(NSURLResponse *resp, NSData *data, NSError *err) {
                                  if(err != nil) {
                                      [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBaseLogger, attemptToSendAnalyticsEventWithData: - ERROR: %@", err.localizedDescription]];
                                  } else {
                                      [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBaseLogger, attemptToSendAnalyticsEventWithData: - SENT jsonEventData: %@", [jsonEventData stringEncoded]]];
                                  }
                              }
            ];
        });
    } else {
        [ASSDKLogger logStatement:@"ASBaseLogger, attemptToSendAnalyticsEventWithData: - Analytics Event Data not sent, because either the analytics was disabled or there was no event data to be sent"];
    }
}

@end

//
//  ASXMLNode.m
//  XMLQueryTester
//
//  Created by Vasyl Savka on 3/27/14.
//  Copyright (c) 2014 New Wave Dgital Media. All rights reserved.
//

#import "ASXMLNode.h"
#import "ASXMLQueryEngine.h"

@interface ASXMLNode()

@property(nonatomic, copy) NSDictionary *backingData;

@end

@implementation ASXMLNode

- (void)dealloc {
    self.backingData = nil;
}

- (instancetype)initWithDictionary:(NSDictionary *)nodeData {
    if (self = [super init])
        _backingData = nodeData;
    
    return self;
}

- (NSString *)value {
    // grab the value from the dictionary.
    NSString* value = self.backingData[ASXMLValueKey];
    
    return value;
}

- (NSString *)name {
    return self.backingData[ASXMLNameKey];
}

- (NSDictionary *)attributes {
    return self.backingData[ASXMLAttributesKey];
}

- (BOOL)validValue {
    return self.value && ![self.value isEqualToString:kEmptyStr];
}

- (ASXMLNode *)childNodeNamed:(NSString *)name {
    __block ASXMLNode *foundNode = nil;
    [self.backingData[ASXMLChildrenKey] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        @try {
            ASXMLNode* node = [[ASXMLNode alloc] initWithDictionary:obj];
            if ([node name] != nil) {
                if ([[node name] isEqualToString:name]) {
//                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASXMLNode, childNodeNamed: - found node: %@", node.name]];
                    foundNode = node;
                    *stop = YES;
                }
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
    
    return foundNode;
}

- (NSArray *)children {
    __block NSMutableArray* children = [NSMutableArray new];
    [self.backingData[ASXMLChildrenKey] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        @try {
            ASXMLNode* node = [[ASXMLNode alloc] initWithDictionary:obj];
            
            if ([node name] != nil) {
                [children addObject:node];
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
    
    return ([children count] != 0) ? children : nil;
}

- (NSArray *)childrenNodesNamed:(NSString *)name {
    __block NSMutableArray* children = [NSMutableArray new];
    [self.backingData[ASXMLChildrenKey] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        @try {
            ASXMLNode* node = [[ASXMLNode alloc] initWithDictionary:obj];
            
            if ([node name] != nil && [[node name] isEqualToString:name]) {
                [children addObject:node];
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
    
    return ([children count] != 0) ? children : nil;
}

- (NSString *)description {
    NSString *description = [NSString stringWithFormat:@"Name: %@ \n Value: %@ \nAttributes: %@ \nChildren: %@", [self name], self.value, self.attributes == nil ? @"none" : [self attributes], self.children == nil ? @"none" : [self children]];
    
    description = [description stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    description = [description stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    
    return description;
}

@end

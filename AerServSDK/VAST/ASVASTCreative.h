//
//  ASVASTVideo.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/28/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASVASTEventError.h"

#define kVASTCreativeTimeout 3.0f

@class ASXMLNode;
@class ASVASTEvent;
@class ASVASTEventProgress;

typedef void (^ASVastCreativeLoadResultHandler)(BOOL);

@protocol ASVASTCreativeDelegate;

@interface ASVASTCreative : NSObject

@property (readonly, nonatomic, strong) NSURL *clickThroughURL;

// attributes for the video
@property (nonatomic, weak) id<ASVASTCreativeDelegate> delegate;
@property (readonly, nonatomic, copy) NSString *type;
@property (readonly, nonatomic, assign) NSInteger bitrate;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, strong) NSURL *url;
@property (readonly, nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, assign) NSInteger playOrder;
@property (readonly, nonatomic, copy) NSString *skipOffset;

@property (readonly, nonatomic, assign) BOOL isVpaid;
@property (readonly, nonatomic, copy) NSString *adParams;

//events
@property (readonly, nonatomic, strong) ASVASTEvent *clickTracking;
@property (readonly, nonatomic, strong) ASVASTEvent *firstQuartile;
@property (readonly, nonatomic, strong) ASVASTEvent *thirdQuartile;
@property (readonly, nonatomic, strong) ASVASTEvent *midpoint;
@property (readonly, nonatomic, strong) ASVASTEvent *start;
@property (readonly, nonatomic, strong) ASVASTEvent *complete;
@property (readonly, nonatomic, strong) ASVASTEvent *skip;
@property (readonly, nonatomic, strong) ASVASTEvent *creativeView;

@property (readonly, nonatomic, strong) ASVASTEvent *acceptInvitation;
@property (readonly, nonatomic, strong) ASVASTEvent *collapse;
@property (readonly, nonatomic, strong) ASVASTEvent *close;
@property (readonly, nonatomic, strong) ASVASTEvent *pause;
@property (readonly, nonatomic, strong) ASVASTEvent *resume;
@property (readonly, nonatomic, strong) ASVASTEventError *error;
@property (readonly, nonatomic, strong) ASVASTEvent *mute;
@property (readonly, nonatomic, strong) ASVASTEvent *unmute;

@property (readonly, nonatomic, strong) NSArray *progress;


- (instancetype)initWithXMLNode:(ASXMLNode *)node withDelegate:(id<ASVASTCreativeDelegate>)delegate;
- (void)removeProgressEvent;
- (void)loadAd:(ASVastCreativeLoadResultHandler)resultHandler;
//- (void)displayInWebView:(UIWebView *)webView;
- (void)mergeIntoCreative:(ASVASTCreative *)creative;

@end

@protocol ASVASTCreativeDelegate <NSObject>

- (void)vastCreative:(ASVASTCreative *)creative encountersVASTError:(ASVASTErrorCode)errCode;

@end

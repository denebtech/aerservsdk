//
//  ASErrorCodeManager.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/15/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kInjectVCFreqCap 0

#define kErrorParamsKey @"error"
#define kErrorCodeKey @"errorCode"
#define kBudgetResetTimeKey @"budgetResetTime"

#define kFreqCapReachedErrorCode 1001
#define kSecondsPerMinute 60
#define kMinutesPerHour 60

@interface ASErrorCodeManager : NSObject

- (instancetype)initWithParams:(NSDictionary *)params;
- (instancetype)initWithParamString:(NSString *)paramsStr;

- (NSError *)checkForErrorCodeToSwitchError:(NSError *)error;

// freq cap
+ (NSString *)findRemainingTimeTillEpochTime:(long)timeLeft;

// testing via injection
+ (NSDictionary *)injectVCFreqCapErrorInto:(NSDictionary *)aerservHeaders;

@end

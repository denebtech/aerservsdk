 //
//  ASHTMLInterstitialViewController.m
//  AerServSDK
//
//  Created by Scott A Andrew on 3/25/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASHTMLInterstitialViewController.h"
#import "ASHTTPAdView.h"
#import "ASInterstitialAdProvider.h"
#import "ASMRAIDView.h"
#import "ASMRAIDParser.h"
#import "ASMRAIDSupportsUtil.h"
#import "ASMRAIDCloseRegion.h"
#import "ASCloseButton.h"

NSString *const AerServURLScheme = @"aerserv";

NSString *const UIMoviePlayerControllerDidEnterFullscreenNotification = @"UIMoviePlayerControllerDidEnterFullscreenNotification";
NSString *const UIMoviePlayerControllerDidExitFullscreenNotification = @"UIMoviePlayerControllerDidExitFullscreenNotification";


@interface ASHTMLInterstitialViewController() <ASHTTPAdViewDelegate, UIGestureRecognizerDelegate, ASMRAIDDelegate>

@property (nonatomic, strong) ASHTTPAdView *webView;
@property (nonatomic, assign) BOOL isPlayingVideoFullScreen;
@property (nonatomic, assign) BOOL ignoreFutherRequests;
@property (nonatomic, assign) NSURLRequest *requestToShow;
@property (nonatomic, assign) BOOL constantlyAdjust;

@property (nonatomic, strong) NSData *inputData;
@property (nonatomic, strong) NSDictionary *inputHeaders;
@property (nonatomic, strong) NSURL *inputBaseURL;

@property (nonatomic, strong) ASMRAIDView *mraidView;

@end



@implementation ASHTMLInterstitialViewController

#pragma mark - UIViewController Methods

- (instancetype)initWithData:(NSData *)data headers:(NSDictionary *)headers baseURL:(NSURL *)baseURL delegate:(id<ASInterstitialAdViewControllerDelegate>)delegate {
    if (self = [super initWithDelegate:delegate]) {
        [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, initWithData:headers:baseURL:delelgate:"];
        self.inputData = data;
        self.inputHeaders = headers;
        self.inputBaseURL = baseURL;
        self.view.backgroundColor = [UIColor clearColor];
        
        [self createWebView];
        if(![self.delegate interstitialAdViewControllerIsPreload:self]) {
            __weak ASHTMLInterstitialViewController *htmlInterVC = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [htmlInterVC startAd];
            });
        } else {
            [self.delegate interstitialAdViewControllerCreatedAdSuccessfully:self];
        }
    }
    
    return self;
}

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, dealloc"];
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMoviePlayerControllerDidEnterFullscreenNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMoviePlayerControllerDidExitFullscreenNotification object:nil];
        self.webView = nil;
        self.requestToShow = nil;
        self.inputData = nil;
        self.inputHeaders = nil;
        self.inputBaseURL = nil;
        self.mraidView = nil;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)createWebView {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, createWebView"];
    // view screen capture doesn't work when frame size is set to 1x1
    self.webView = [[ASHTTPAdView alloc] initWithFrame:self.view.bounds];
    
    // setup our webview.
    self.webView.adDelegate = self;

    [self.view addSubview:self.webView];

    // view screen capture doesn't work w/o this
    [self.webView forceRedraw];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, viewDidLoad - Add Notification Observers"];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieWillEnterFullScreen)
                                                 name:UIMoviePlayerControllerDidEnterFullscreenNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieDidExitFullScreen)
                                                 name:UIMoviePlayerControllerDidExitFullscreenNotification
                                               object:nil];

}

- (void)viewWillDisappear:(BOOL)animated {
    @try {
        if(!self.mraidView.movingBetweenMraidViewAndVC && !self.mraidView.didForceOrientaiton)
            [super viewWillDisappear:animated];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    if (self.isPlayingVideoFullScreen)
        return;

    @try {
        [self cancel];
        
        if(!self.mraidView.movingBetweenMraidViewAndVC && !self.mraidView.didForceOrientaiton)
            [super viewDidDisappear:animated];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    @try {
        if (self.isPlayingVideoFullScreen) {
            // force a layout ahead of time. otherwise there is a jump.
            [self viewDidLayoutSubviews];
            
            // force the webview to redraw underneath. This will clean up transitions
            // from the full screen video.
            [self.webView forceRedraw];
        }
        else {
            // force a layout ahead of time. otherwise there is a jump.
            [self adjustWebView];
            
            if(!self.mraidView.movingBetweenMraidViewAndVC && !self.mraidView.didForceOrientaiton)
                [super viewWillAppear:animated];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    @try {
        if(!self.mraidView.movingBetweenMraidViewAndVC && !self.mraidView.didForceOrientaiton)
            [super viewDidAppear:animated];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    float heightScale = CGRectGetHeight(self.view.bounds)/self.webView.contentSize.height;
    float widthScale = CGRectGetWidth(self.view.bounds)/self.webView.contentSize.width;
    
    float scale = MIN(heightScale, widthScale);
    
    // we don't scale the ad bigger.
    if (scale > 1.0)
        scale = 1.0;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLInterstitialViewController, viewDidLayoutSubviews - before transform, webView.frame: %@", NSStringFromCGRect(self.webView.frame)]];
    self.webView.transform = CGAffineTransformMakeScale(scale, scale);
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLInterstitialViewController, viewDidLayoutSubviews - after transform, webView.frame: %@", NSStringFromCGRect(self.webView.frame)]];
    
    self.webView.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    
    if(self.mraidView) {
        self.mraidView.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    }
    if(self.mraidView.supportUtil.mraidVideoPlayerLayer != nil) {
        self.mraidView.supportUtil.mraidVideoPlayerLayer.frame = self.view.frame;
    }
    
    [self.view layoutIfNeeded];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLInterstitialViewController, viewDidLayoutSubviews - exiting, webView.frame: %@", NSStringFromCGRect(self.webView.frame)]];
}

//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
//    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
//    
//    [self.webView rotateToOrientation:self.interfaceOrientation];
//    
//    // force a layout ahead of time. otherwise there is a jump.
//    if(self.constantlyAdjust) {
//        [self adjustWebView];
//    }
//}

- (BOOL)shouldAutorotate {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, shouldAutorotate"];
    [self.mraidView mraidViewWillAttemptOrientationChange];
    
    return (!self.mraidView) ? YES : (kIS_iOS_6 ? NO : self.mraidView.allowOrienationChange);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return (!self.mraidView) ? (kIS_IPHONE ? UIInterfaceOrientationMaskAllButUpsideDown : UIInterfaceOrientationMaskAll) : self.mraidView.preferredOrientation;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [ASSDKLogger logStatement:@"ASHTMLInterstitialVIewController, didReceiveMemoryWarning"];
    [self.webView stopLoading];
    [self.webView resetCache];
}

#pragma mark - HTML Interstitial Methods

- (void)startAd {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, startAd"];
    [self displayData:self.inputData withHeaders:self.inputHeaders baseURL:self.inputBaseURL];
}

- (void)movieWillEnterFullScreen {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, movieWillEnterFullScreen"];
    self.isPlayingVideoFullScreen = YES;
}

- (void)movieDidExitFullScreen {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, movieDidExitFullScreen"];
    self.isPlayingVideoFullScreen = NO;
}

- (void)interstitialVCDidFindMRAIDWithData:(NSData *)htmlData {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, interstitialVCDidFindMRAIDWithData:"];
    
    // hide original webView & closeButton
    self.webView.hidden = YES;
    self.closeButton.hidden = YES;
    
    // init mraid view and add it to view
    self.mraidView = [[ASMRAIDView alloc] initWithFrame:self.view.frame
                                               withData:htmlData
                                                headers:self.inputHeaders
                                              forRootVC:self
                                              asPlcType:kASMRAIDPlacementInterstitial
                                               delegate:self];
    [self.view addSubview:self.mraidView];
    [self.view bringSubviewToFront:self.mraidView];
    
    // notify delegate of successful creation of ad view controller
    if(![self.delegate interstitialAdViewControllerIsPreload:self])
        [self.delegate interstitialAdViewControllerCreatedAdSuccessfully:self];
    
    // move close button above everything else
    [self.view bringSubviewToFront:self.mraidView.closeRegion];
}

- (BOOL)displayData:(NSData *)data withHeaders:(NSDictionary *)headers baseURL:(NSURL *)baseURL {
    
    if ([data length] <= 1)
        return NO;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLInterstitialViewController, displayData:withHeaders:baseURL: - data.length: %lu", (unsigned long)data.length]];
    NSString *contentType = headers[@"Content-Type"];
    NSArray *contentTypes = [contentType componentsSeparatedByString:@";"];
    
    // get the type and encoding stripping away any leading/trailing spaces.
    NSString *mime = [contentTypes[0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *encoding = (contentTypes.count > 1) ? [contentTypes[1] stringByReplacingOccurrencesOfString:@" " withString:@""] : @"charset=UTF-8";
    
    if([ASMRAIDParser checkForMraidWithData:data]) {
        [self interstitialVCDidFindMRAIDWithData:data];
    } else {
        // send the web view the data. we need the base URL.. Otherwise things don't work right. We need to pass in the URL as the base URL.
        [self.webView loadData:data MIMEType:mime textEncodingName:encoding baseURL:baseURL];
    }
    
     return YES;
}

- (void)setOutlineAd:(BOOL)outlineAd {
    [super setOutlineAd:outlineAd];
    
    self.webView.layer.borderWidth = outlineAd ? 2 : 0;
    self.webView.layer.borderColor = [UIColor redColor].CGColor;
    
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, cancel"];
    @try {
        [self.webView cleanUp];
        [self.webView resetCache];
        self.webView = nil;
        self.inputData = nil;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adjustWebView {
    ASWebView *presentedWebView = (!self.mraidView) ? self.webView : self.mraidView.webView;
    
    // before we adjust we need to make sure that we are at 100%.
    CGFloat height = presentedWebView.contentSize.height;
    CGFloat width = presentedWebView.contentSize.width;
    
    
    // if the web view is bigger than our view or the size didnt' change assume full screen scaled. Other wise
    // turn off scaling.
    if ((width > CGRectGetWidth(self.view.bounds) && height > CGRectGetHeight(self.view.bounds)) || (height == 1 || width ==1 ) || self.constantlyAdjust) {
        
        presentedWebView.transform = CGAffineTransformMakeScale(1, 1);
        height = CGRectGetHeight(self.view.bounds);
        width = CGRectGetWidth(self.view.bounds);
        
        presentedWebView.scalesPageToFit = YES;
        presentedWebView.contentSize = CGSizeMake(width, height);
        self.constantlyAdjust = YES;
    }
    
    presentedWebView.frame = CGRectMake(0,0,width,height);
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLInterstitialViewController, adjustWebView - webView.frame: %@", NSStringFromCGRect(presentedWebView.frame)]];
}

#pragma mark - ASHTTPAdViewDelegate

- (void)HTTPAdViewFinsihedLoading:(ASHTTPAdView *)adView {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, HTTPAdViewFinishedLoading:"];
    [self adjustWebView];
    
    if(![self.delegate interstitialAdViewControllerIsPreload:self]) {
        [self.delegate interstitialAdViewControllerCreatedAdSuccessfully:self];
    }
}

- (void)HTTPAdViewFailedLoading:(ASHTTPAdView *)adView withError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLInterstitialViewController, HTTPAdViewFailedLoading:withError: - error: %@", error.localizedDescription]];
    [self.delegate interstitialAdViewControllerFailed:self withError:error];
}

- (BOOL)HTTPAdViewWasClicked:(ASHTTPAdView *)adView request:(NSURLRequest *)request {
    if (self.isPlayingVideoFullScreen)
        return YES;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLInterstitialViewController, HTTPAdViewWasClicked:request: - request.url: %@", request.URL]];
    if ([[[request URL] scheme] isEqualToString:AerServURLScheme]) { // TODO: what is this code for? -- AZ 3/9/16
        
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.delegate interstitialAdViewControllerAdWasTouched:self];
        [self.delegate interstitialAdViewControllerAdFinished:self];
        
        // we don't want do anyting with it.
        return NO;
    }
    
    // we have an url clicked we want to go ahead and launch safari.
    else {
        [self.delegate interstitialAdViewControllerAdWasTouched:self];
        [[UIApplication sharedApplication] openURL:request.URL];

        
        return NO;
    }

    return YES;
}

- (void)HTTPAdViewDidFireBPF:(ASHTTPAdView *)adView {
    [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"Interstitial View Controller Fires Event Banner Pixel"];
}

#pragma mark - ASMRAIDDDelegate

- (void)repositionMRAIDView:(ASMRAIDView *)mraidView toNewFrame:(CGRect)newFramePos {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLInterstitialViewController, repositionMRAIDView:toNewFrame: - frame: %@", NSStringFromCGRect(newFramePos)]];
    if([self.mraidView isEqual:mraidView]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:kMRAIDAnimationDuration animations:^{
                mraidView.frame = newFramePos;
                mraidView.webView.frame = CGRectMake(0.0f, 0.0f, newFramePos.size.width, newFramePos.size.height);
            }];
        });
    }
}

- (void)expandMRAIDView:(ASMRAIDView *)mraidView withWebView:(UIWebView *)webview {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, expandMRAIDView:withWebView:"];
    
    if([self.mraidView isEqual:mraidView]) {
        __weak ASHTMLInterstitialViewController *htmlInterVC = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:kMRAIDAnimationDuration animations:^{
                CGRect viewFrame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
                mraidView.frame = viewFrame;
                webview.frame = viewFrame;
            } completion:^(BOOL fin){
                [htmlInterVC.view bringSubviewToFront:webview];
            }];
        });
    }
}

- (void)closeMRAIDView:(ASMRAIDView *)mraidView {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, closeMRAIDView:"];
    
    if([self.mraidView isEqual:mraidView]) {
        __weak ASHTMLInterstitialViewController *htmlInterVC = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [htmlInterVC.mraidView cleanup];
            htmlInterVC.mraidView = nil;
            [htmlInterVC onCloseAd];
        });
        self.mraidView = nil;
    }
}

- (void)loadExternalPlayerForMRAIDView:(ASMRAIDView *)mraidView withPlayer:(AVPlayer *)player {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialViewController, loadExternalPlayerForMRAIDView:withPlayer:"];
    
    [mraidView.supportUtil initPlayerLayer];
    mraidView.supportUtil.mraidVideoPlayerLayer.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);

    [self.view.layer addSublayer:mraidView.supportUtil.mraidVideoPlayerLayer];
    [player play];;
}

- (void)removeExternalPlayerForMRAIDView:(ASMRAIDView *)mraidView forPlayer:(AVPlayer *)player {
    [self.mraidView.supportUtil.mraidVideoPlayerLayer removeFromSuperlayer];
}

- (BOOL)preloadCheckForMRAIDView:(ASMRAIDView *)mraidView {
    return [self.delegate interstitialAdViewControllerIsPreload:self];
}

- (void)didReceiveTouchForMRAIDView:(ASMRAIDView *)mraidView {
    [self.delegate interstitialAdViewControllerAdWasTouched:self];
}

- (void)mraidView:(ASMRAIDView *)mraidView didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:msg];
}

- (UIViewController *)viewControllerForMRAIDView:(ASMRAIDView *)mraidView {
    return self;
}

- (void)attemptToRewardVC:(ASMRAIDView *)mraidView {
    [self.delegate interstitialAdViewControllerDidRewardVC:self];
}

@end

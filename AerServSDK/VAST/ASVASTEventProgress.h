//
//  ASVASTEventProgress.h
//  AerServSDK
//
//  Created by Vasyl Savka on 6/19/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASVASTEvent.h"

@interface ASVASTEventProgress : ASVASTEvent

@property (nonatomic, copy) NSString *progressTimeStr;

- (instancetype)initWithVASTEvent:(ASVASTEvent *)event;
- (NSInteger)calculateTimeInSec;

@end

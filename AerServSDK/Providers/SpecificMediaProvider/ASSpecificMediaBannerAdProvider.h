//
//  ASSpecificMediaBannerAdProvider.h
//  SpecificMediaProvider
//
//  Created by Arash Sharif on 9/8/14.
//  Copyright (c) 2014 AerServ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ASBannerAdProvider.h"
#import "VindicoAdView.h"
#import "VindicoAdRequest.h"

@interface ASSpecificMediaBannerAdProvider : ASBannerAdProvider
@property (nonatomic, strong) VindicoAdRequest *request;
@property (nonatomic, strong) VindicoAdView *adView;
@end

//
//  ASSpotXAd.h
//  AerServSDK
//
//  Created by Vasyl Savka on 10/14/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSpotX_SDK_URL_Str @"https://m.spotx.tv/tvos/v2/app.js"

#define kContextLaunchOptionsChannelIdKey @"channelId"
#define kContextLaunchOptionsParamsKey @"params"

@protocol SpotXAdDelegate;

@interface ASSpotXAd : NSObject

@property (nonatomic, copy) NSString *channelId;
@property (nonatomic, strong) NSDictionary *params;
@property (nonatomic, assign) id<SpotXAdDelegate>delelgate;

+ (instancetype)adWithChannelId:(NSString *)channelId andDelegate:(id<SpotXAdDelegate>)delelgate;
- (void)showWith:(UIViewController *)presentingViewController asAnimated:(BOOL)animated;

@end



@protocol SpotXAdDelegate <NSObject>

- (void)spotXAdDidStart:(ASSpotXAd *)ad;
- (void)spotXAdDidFinish:(ASSpotXAd *)ad;
- (void)spotXAdDidFail:(ASSpotXAd *)ad withError:(NSError *)error;

@end


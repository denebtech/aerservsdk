//
//  ASMillennialBannerAdProvider.m
//  AerServSDK
//
//  Created by Albert Zhu on 5/13/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASMillennialBannerAdProvider.h"

#import "MMAdSDK.h"

@interface ASMillennialBannerAdProvider () <MMInlineDelegate>

@property (nonatomic, strong) id mmSdkInstance;
@property (nonatomic, strong) id mmInlineAd;

@property (nonatomic, assign) BOOL adInitialized;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;

@end



@implementation ASMillennialBannerAdProvider

- (void)dealloc {
    [self cleanUp];
}

- (instancetype)init {
    if(self = [super initWithAdClassName:kMMInlineAdClassStr timeout:kCustomBannerAdTimeout]) { }
    
    return self;
}

- (void)cleanUp {
    [ASSDKLogger logStatement:@"ASMillennnialBannerAdProvider, cleanUp"];
    self.mmSdkInstance = nil;
    [[self.mmInlineAd view] removeFromSuperview];
    [self.mmInlineAd setDelegate:nil];
    self.mmInlineAd = nil;
}

#pragma mark - ASCustomBannerAdProvider Overloaded Methods

- (void)initializePartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASMillennialBannerAdProvider, initializePartnetAd:"];
    Class mmSdkClass = NSClassFromString(kMMSDKClassStr);
    self.mmSdkInstance = [mmSdkClass sharedInstance];
    [self.mmSdkInstance initializeWithSettings:nil withUserSettings:nil];
    self.adInitialized = YES;
}

- (BOOL)hasPartnerAdInitialized {
    return self.adInitialized;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return !self.adInitialized;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASMillennialBannerAdProvider, loadPartnerAd:"];
    
    NSDictionary *mediationProperties = properties[ASBannerParameter_Parameters];
    NSString *placementId = [NSString stringWithFormat:@"%@", mediationProperties[kMMPlacementIdKey]];
    NSString *adSize = mediationProperties[kMMAdSizeKey];
    
    NSInteger adSizeInt = MMInlineAdSizeFlexible;
    if([adSize isEqualToString:@"BANNER"]) {
        adSizeInt = MMInlineAdSizeBanner;
    } else if([adSize isEqualToString:@"FULL_BANNER"]) {
        adSizeInt = MMInlineAdSizeFullBanner;
    } else if([adSize isEqualToString:@"LARGE_BANNER"]) {
        adSizeInt = MMInlineAdSizeLargeBanner;
    } else if([adSize isEqualToString:@"LEADERBOARD"]) {
        adSizeInt = MMInlineAdSizeLeaderboard;
    } else if([adSize isEqualToString:@"MEDIUM_RECTANGLE"]) {
        adSizeInt = MMInlineAdSizeMediumRectangle;
    }
    
    Class mmInlineAdClass = NSClassFromString(kMMInlineAdClassStr);
    self.mmInlineAd = [[mmInlineAdClass alloc] initWithPlacementId:placementId adSize:adSizeInt];
    [self.mmInlineAd setDelegate:self];
    [[self.mmInlineAd view] setHidden:YES];
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd {
    [ASSDKLogger logStatement:@"ASMillennnialBannerAdProvider, showPartnerAd"];
    [[self.mmInlineAd view] setHidden:NO];
}

- (void)terminatePartnerAd {
    [ASSDKLogger logStatement:@"ASMillennnialBannerAdProvider, terminatePartnerAd"];
    [self cleanUp];
}

- (UIView *)partnerAdView {
    return [self.mmInlineAd view];
}

//- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation withSize:(CGSize)currSize {
//    UIView *adView = [self.mmInlineAd view];
//    if(adView != nil)
//        NSLog(@"mmInlineAd.frame: %@", NSStringFromCGRect(adView.frame));
//    
//    CGSize adSize = [self.mmInlineAd size];
//    CGSize reqAdSize = [self.mmInlineAd requestedSize];
//    NSLog(@"mmInlineAd.size: %@", NSStringFromCGSize(adSize));
//    NSLog(@"mmInlineAd.requestSize: %@", NSStringFromCGSize(reqAdSize));
//}

#pragma mark - MMInlineDelegateMethods

- (UIViewController *)viewControllerForPresentingModalView {
    return [self asBannerViewController];
}

- (void)inlineAdRequestDidSucceed:(MMInlineAd*)ad {
    [ASSDKLogger logStatement:@"ASMillennnialBannerAdProvider, inlineAdrequestDidSucceed:"];
    self.adLoaded = YES;
    [self asPartnerBannerAdDidLoad:[self.mmInlineAd view]];
}

- (void)inlineAd:(MMInlineAd*)ad requestDidFailWithError:(NSError*)error {
    [ASSDKLogger logStatement:@"ASMillennnialBannerAdProvider, inlineAd:requestDidFailWithError:"];
    self.adFailed = YES;
    [self.delegate bannerProvider:self didFailToLoadAdWithError:error];
}

- (void)inlineAdContentTapped:(MMInlineAd*)ad {
    [ASSDKLogger logStatement:@"ASMillennnialBannerAdProvider, inlineAdContentTapped:"];
    [self asPartnerBannerAdWasClicked];
}

- (void)inlineAd:(MMInlineAd*)ad willResizeTo:(CGRect)frame isClosing:(BOOL)isClosingResize { }

- (void)inlineAd:(MMInlineAd*)ad didResizeTo:(CGRect)frame isClosing:(BOOL)isClosingResize { }

- (void)inlineAdWillPresentModal:(MMInlineAd *)ad {
    [ASSDKLogger logStatement:@"ASMillennnialBannerAdProvider, inlineAdWillPresentModal:"];
    [self asPartnerBannerWillBeginAction];
}

- (void)inlineAdDidPresentModal:(MMInlineAd *)ad { }

- (void)inlineAdWillCloseModal:(MMInlineAd *)ad {
    [ASSDKLogger logStatement:@"ASMillennnialBannerAdProvider, inlineAdWillCloseModal:"];
    [self asPartnerBannerWillEndAction];
}

- (void)inlineAdDidCloseModal:(MMInlineAd *)ad { }

- (void)inlineAdWillLeaveApplication:(MMInlineAd *)ad {
    [ASSDKLogger logStatement:@"ASMillennnialBannerAdProvider, inlineAdWillLeaveApplication:"];
    [self asPartnerBannerWillLeaveApplication];
}

- (void)inlineAdAbortDidSucceed:(MMInlineAd*)ad { }

- (void)inlineAd:(MMInlineAd*)ad abortDidFailWithError:(NSError*)error { }

@end

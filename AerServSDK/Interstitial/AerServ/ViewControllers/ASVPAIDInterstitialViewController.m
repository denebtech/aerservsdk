//
//  ASVPAIDInterstitialViewController.m
//  AerServSDK
//
//  Created by Arash Sharif on 4/18/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASVPAIDInterstitialViewController.h"
#import "ASVASTCreative.h"
#import "ASPubSettingUtils.h"
#import "ASVASTEventProgress.h"
#import "ASCloseButton.h"
#import "ASCountdownLabel.h"
#import "ASCommManager.h"

typedef NS_ENUM(NSInteger, ASVPAIDAdMode) {
    kASVPAIDAdMode_Preload = -1,
    kASVPAIDAdMode_Unloaded = 0,
    kASVPAIDAdMode_Loaded,
    kASVPAIDAdMode_Play,
    kASVPAIDAdMode_Pause
};

@interface ASVPAIDInterstitialViewController ()

@property (nonatomic, copy) NSString *closeOffset;
@property (nonatomic, strong) ASVASTCreative *vpaidCreative;

@property (nonatomic, assign) ASVPAIDAdMode adMode;
@property (nonatomic, assign) BOOL adDidStartOnce;
@property (nonatomic, assign) BOOL didSkipAd;
@property (nonatomic, assign) BOOL canRunVerTwo;
@property (nonatomic, strong) NSMutableArray *opArray;
@property (nonatomic, assign) BOOL getReqIsRunning;

@property (nonatomic, strong) NSTimer *killTimer;
@property (nonatomic, strong) NSTimer *closeTimer;
@property (nonatomic, strong) NSTimer *progressTimer;

@property (nonatomic, strong) dispatch_semaphore_t closeSema;
@property (nonatomic, strong) dispatch_semaphore_t progSema;

// AD PROPERTIES
@property (nonatomic, assign) BOOL adSkippableState;
@property (nonatomic, assign) BOOL adLinear;
@property (nonatomic, assign) CGFloat adWidth;
@property (nonatomic, assign) CGFloat adHeight;
@property (nonatomic, assign) CGFloat adVolume;
@property (nonatomic, assign) CGFloat adDuration;
@property (nonatomic, assign) CGFloat adRemainingTime;
@property (nonatomic, assign) CGFloat currentTime;

@property (nonatomic, strong) ASCountdownLabel *countdownLbl;
@property (nonatomic, strong) NSTimer *countdownTimerUpdater;

@end



@implementation ASVPAIDInterstitialViewController

#pragma mark - Helpers

- (CGRect)calcViewFrameBasedOnScreenDimensions {
    CGFloat xPos = 0.0f, yPos = 0.0f, screenWidth = 0.0f, screenHeight = 0.0f;
    if(!kIS_iOS_8_OR_LATER && kIS_LANDSCAPE) {
        screenWidth = self.view.frame.size.height;
        screenHeight = self.view.frame.size.width;
    } else {
        screenWidth = self.view.frame.size.width;
        screenHeight = self.view.frame.size.height;
    }
    
    if(kIS_iOS_6) {
        screenHeight -= [UIApplication sharedApplication].statusBarFrame.size.height;
    }
    
    CGRect frame = CGRectMake(xPos, yPos, screenWidth, screenHeight);
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, calcViewFrameBasedOnScreenDimensions - frame: %@", NSStringFromCGRect(frame)]];
    
    return frame;
}

#pragma mark - Loading HTML and JS

- (NSString *)jsInitFNVPAIDFrameStr {
    return @"var fn = iframe.contentWindow['getVPAIDAd'];";
}

- (NSString *)jsCheckIfVPAIDFrameIsValidStr {
    return @"if(!fn || typeof fn != 'function') { document.location.href='aerserv://event-aderror/vpaid_error'; return; }";
}

- (NSString *)createAdFromFNStr {
    return @"var ad = fn();";
}

- (ASVASTCreative *)nextCreativeToTry:(NSMutableArray *)creativeArr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, nextCreativeToTry - creativeArr.count: %lu", (unsigned long)creativeArr.count]];
    ASVASTCreative *nextCreative = nil;
    [creativeArr removeObjectAtIndex:0];
    if(creativeArr.count > 0) {
        nextCreative = (ASVASTCreative *)creativeArr[0];
    } else {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, nextCreativeToTry - No more creatives to try."];
    }
    return nextCreative;
}

- (void)loadWebViewWithCreativeURL:(NSString *)urlStr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, loadWebViewWithCreativeURL - urlStr: %@", urlStr]];
    self.webView.hidden = YES;
    NSString * html =                    @"<html>";
    html = [html stringByAppendingString:@"<head>"];
    html = [html stringByAppendingString:@"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">"];
    html = [html stringByAppendingString:@"</head>"];
    html = [html stringByAppendingString:@"<body style=\"margin:0px;\" bgcolor=\"#000000\">"];
    html = [html stringByAppendingString:@"<div id=\"aerservVpaidSlot\">"];
    html = [html stringByAppendingString:@"<video id=\"aerservVpaidVideoSlot\" style=\"width: 100%; height: 100%; position: fixed; top:0;\" autoplay webkit-playsinline playsinline></video>"];
    html = [html stringByAppendingString:@"</div>"];
    html = [html stringByAppendingString:@"<script type=\"text/javascript\">"];
    html = [html stringByAppendingString:@"iframe = document.createElement('iframe');"];
    html = [html stringByAppendingString:@"iframe.id = \"adloaderframe\";"];
    html = [html stringByAppendingString:@"iframe.frameBorder=\"0\";"];
    html = [html stringByAppendingString:@"document.body.appendChild(iframe);"];
    html = [html stringByAppendingString:@"iframe.contentWindow.document.write('<html><head><script type=\"text/javascript\" src=\""];
    html = [html stringByAppendingString:urlStr];
    html = [html stringByAppendingString:@"\"></scr'+'ipt></head><body><script type=\"text/javascript\">window.parent.aerservVpaidReady();</scr'+'ipt></body></html>');"];
    html = [html stringByAppendingString:@"function loadReq(url) {"];
    html = [html stringByAppendingString:@"var iframeTwo = document.createElement('iframe');"];
    html = [html stringByAppendingString:@"iframeTwo.setAttribute('src', url);"];
    html = [html stringByAppendingString:@"document.documentElement.appendChild(iframeTwo);"];
    html = [html stringByAppendingString:@"iframeTwo.parentNode.removeChild(iframeTwo);"];
    html = [html stringByAppendingString:@"iframeTwo = null; }"];
    html = [html stringByAppendingString:@"function aerservVpaidReady() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://handshake/\" + ad.handshakeVersion('"];
    html = [html stringByAppendingString:kVPAIDPalyerVersionStr];
    html = [html stringByAppendingString:@"')); }"];
    html = [html stringByAppendingString:@"function getLinear() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-linear/\" + ad.getAdLinear()); }"];
    html = [html stringByAppendingString:@"function getWidth() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-width/\" + ad.getAdWidth()); }"];
    html = [html stringByAppendingString:@"function getHeight() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-height/\" + ad.getAdHeight()); }"];
    html = [html stringByAppendingString:@"function getExpanded() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-expanded/\" + ad.getAdExpanded()); }"];
    html = [html stringByAppendingString:@"function getSkippableState() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-skippablestate/\" + ad.getAdSkippableState()); }"];
    html = [html stringByAppendingString:@"function getRemainingTime() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-remainingtime/\" + ad.getAdRemainingTime()); }"];
    html = [html stringByAppendingString:@"function getDuration() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-duration/\" + ad.getAdDuration()); }"];
    html = [html stringByAppendingString:@"function getVolume() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-volume/\" + ad.getAdVolume()); }"];
    html = [html stringByAppendingString:@"function setVolume(vol) {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"ad.setAdVolume(vol); }"];
    html = [html stringByAppendingString:@"function getCompanions() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-companions/\" + ad.getAdCompanions()); }"];
    html = [html stringByAppendingString:@"function getIcons() {"];
    html = [html stringByAppendingString:[self jsInitFNVPAIDFrameStr]];
    html = [html stringByAppendingString:[self jsCheckIfVPAIDFrameIsValidStr]];
    html = [html stringByAppendingString:[self createAdFromFNStr]];
    html = [html stringByAppendingString:@"loadReq(\"aerserv://getter-icons/\" + ad.getAdIcons()); }"];
    html = [html stringByAppendingString:@"</script>"];
    html = [html stringByAppendingString:@"</body>"];
    html = [html stringByAppendingString:@"</html>"];
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, loadWebViewWithCreativeURL - webView HTML:\n\n\n%@\n\n\n", html]];
     
    [self.webView loadHTMLString:html baseURL:nil];
}

- (instancetype)initWithVASTAd:(ASVASTAd *)VASTAd delegate:(id<ASInterstitialAdViewControllerDelegate>)delegate closeOffset:(NSString*)offset {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, initWithVASTAd:delegate:closeOffset: - offset: %@", offset]];
    self = [super initWithDelegate:delegate];
    
    self.adToDisplay = VASTAd;
    
    // pull out the first creative from adToDisplay
    self.vpaidCreative = self.adToDisplay.creatives[0];

    self.delegate = delegate;
    self.closeOffset = offset;
    
    // Do any additional setup after loading the view, typically from a nib.
    self.webView = [[UIWebView alloc] initWithFrame:[self calcViewFrameBasedOnScreenDimensions]];
    self.webView.backgroundColor = [UIColor blackColor];
    self.webView.scalesPageToFit = YES;
    self.webView.autoresizingMask = UIViewAutoresizingNone;
    self.webView.allowsInlineMediaPlayback = YES;
    self.webView.mediaPlaybackRequiresUserAction = NO;
    self.webView.scrollView.bounces = NO;
    self.webView.scrollView.scrollEnabled = NO;
    self.webView.delegate = self;
    
    [self.view addSubview:self.webView];
    [self.view sendSubviewToBack:self.webView];
    
    // add countdown timer label
    self.countdownLbl = [ASCountdownLabel new];
    [self.countdownLbl changePosToX:0.0f andY:(self.view.frame.size.height - kCountdownLblH)];
    [self.view addSubview:self.countdownLbl];
    [self.view bringSubviewToFront:self.countdownLbl];
    
    self.opArray = [NSMutableArray array];

    if(!self.adToDisplay.isPreload) {
        [self loadWebViewWithCreativeURL:self.vpaidCreative.url.absoluteString];
    } else {
        // in the case that it's preload, we fire the preload ready notification right away and load after there's a show
        self.adMode = kASVPAIDAdMode_Preload;
        [self.delegate interstitialAdViewControllerCreatedAdSuccessfully:self];
    }
    
    return self;
}

- (void)viewDidLayoutSubviews {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, viewDidLayoutSubviews"];
    
    @try {
        CGRect screenFrame = [self calcViewFrameBasedOnScreenDimensions];
        self.webView.frame = screenFrame;
        [self resizeAdToSize:CGSizeMake(screenFrame.size.width, screenFrame.size.height)];
        [self.countdownLbl changePosToX:0.0f andY:(self.view.frame.size.height - kCountdownLblH)];
        [self.view layoutIfNeeded];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - View Controller Methods

- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(leaveApp)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(returnToApp)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, viewWillAppear"];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, viewDidAppear"];
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, viewWillDisappear"];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, viewDidDisappear"];
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, dealloc"];
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
        
        [self revokeAllTimers];
        
        [self.countdownLbl removeFromSuperview];
        self.countdownLbl = nil;

        [self.webView removeFromSuperview];
        self.webView = nil;

        self.closeOffset = nil;
        self.vpaidCreative = nil;
        [self.opArray removeAllObjects];
        self.opArray = nil;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)leaveApp {
    @try {
        [self revokeAllTimers];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)returnToApp {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, returnToApp"];
    @try {
        if(self.adMode == kASVPAIDAdMode_Pause) {
            [self resumeAd];
        }
        [self getDuration];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (BOOL)shouldAutorotate {
    return !kIS_iOS_6;
}

- (void)didRotate {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, didRotate"];
    [self viewDidLayoutSubviews];
}

- (void)resetFlags {
    self.closeButton.hidden = YES;
    [self.opArray removeAllObjects];
}

#pragma mark - Timeout

- (void)revokeKillTimer {
    if(self.killTimer != nil) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, revokeKillTimer"];
        [self.killTimer invalidate];
        self.killTimer = nil;
    }
}

- (void)endAdAttempt {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, endAdAttempt - TIMED OUT"];
    @try {
        [self stopAd];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)startKillTimer {
    [self revokeKillTimer];
    
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, startKillTimer"];
    self.killTimer = [NSTimer scheduledTimerWithTimeInterval:kVPAIDTimeout target:self selector:@selector(endAdAttempt) userInfo:nil repeats:NO];
}

#pragma mark - Closability

- (void)showCloseBtn {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, showCloseBtn"];
    self.closeButton.hidden = NO;
}

- (void)checkClosabilityWith:(NSString *)closabilityStr {
    if(self.closeOffset != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, checkClosabilityWith - closeOffset: %@, closabilityStr: %@", self.closeOffset, closabilityStr]];
        
        if([self.closeOffset isEqualToString:closabilityStr]) {
            [self showCloseBtn];
        }
    }
}

- (void)setClosabilityTimer {
    NSNumber *closePercentage = [ASPubSettingUtils getOffsetAsPercent:self.closeOffset videoDuration:self.adDuration];
    CGFloat closeTime = self.adDuration * [closePercentage doubleValue];
    CGFloat timeTillClose = closeTime - self.currentTime;
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, setClosabilityTImer - timeTIllClose: %0.2lf", timeTillClose]];
    if(timeTillClose >= 0) {
        __weak ASVPAIDInterstitialViewController *vpaidInterVC = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, setClosabilityTImer - sched close timer"];
            @try {
                vpaidInterVC.closeTimer = [NSTimer scheduledTimerWithTimeInterval:(closeTime - vpaidInterVC.currentTime)
                                                                           target:vpaidInterVC
                                                                         selector:@selector(showCloseBtn)
                                                                         userInfo:nil
                                                                          repeats:NO];
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        });
    } else {
        [self showCloseBtn];
    }
}

- (void)revokeCloseTimer {
    if(self.closeTimer != nil) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, revokeCloseTimer"];
        [self.closeTimer invalidate];
        self.closeTimer = nil;
    }
}

#pragma mark - Progress

- (void)progressAction {
    ASVASTEventProgress *progEvent = self.vpaidCreative.progress[0];
    [ASCommManager sendAsynchronousRequestsForEvent:progEvent];
    [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Progress Event"];
    
    [self.vpaidCreative removeProgressEvent];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, progressAction - progEvent.progressTimeStr: %@", progEvent.progressTimeStr]];
    
    __weak ASVPAIDInterstitialViewController *vpaidInterVC = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [vpaidInterVC checkForProgressEvents];
    });
}

- (void)checkForProgressEvents {
    if(!self.vpaidCreative.progress || self.vpaidCreative.progress.count == 0) return;
    [self revokeProgressTimer];
    
    self.progSema = dispatch_semaphore_create(0);
    [self getRemainingTime];
    
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, checkForProgressEvent - BEFORE PROG SEMAPHORE WAIT"];
    dispatch_wait(self.progSema, DISPATCH_TIME_FOREVER);
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, checkForProgressEvent - AFTER PROG SEMAPHORE WAIT"];
    ASVASTEventProgress *nextEvent = self.vpaidCreative.progress[0];
    NSTimeInterval timeTillNextEvent = [nextEvent calculateTimeInSec] - self.currentTime;
    __weak ASVPAIDInterstitialViewController *vpaidInterVC = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, checkForProgressEvent - progressTimer scheduled for %0.2f secs", timeTillNextEvent]];
        vpaidInterVC.progressTimer = [NSTimer scheduledTimerWithTimeInterval:timeTillNextEvent
                                                              target:vpaidInterVC
                                                            selector:@selector(progressAction)
                                                            userInfo:nil
                                                             repeats:NO];
    });
}

- (void)revokeProgressTimer {
    if(self.progressTimer != nil) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, revokeProgressTimer"];
        [self.progressTimer invalidate];
        self.progressTimer = nil;
    }
}

#pragma mark - Shared Timer Methods

- (void)setCloseAndProgTimer {
    __weak ASVPAIDInterstitialViewController *vpaidInterVC = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        @try {
            [vpaidInterVC revokeCloseTimer];
            vpaidInterVC.closeSema = dispatch_semaphore_create(0);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                @try {
                    [vpaidInterVC getRemainingTime];
                    [vpaidInterVC getDuration];
                }
                @catch (NSException *exception) {
                    [ASSDKLogger onException:exception];
                }
            });
            
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, setCloseAndProgTimer - BEFORE CLOSE SEMAPHORE WAIT"]];
            dispatch_semaphore_wait(vpaidInterVC.closeSema, DISPATCH_TIME_FOREVER);
            if(vpaidInterVC.adDuration >= 0 && vpaidInterVC.adRemainingTime >= 0) {
                [vpaidInterVC updateCurrentTime];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    @try {
                        [vpaidInterVC checkForProgressEvents];
                    }
                    @catch (NSException *exception) {
                        [ASSDKLogger onException:exception];
                    }
                });
                
                if(vpaidInterVC.closeOffset != nil) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                        @try {
                            [vpaidInterVC setClosabilityTimer];
                        }
                        @catch (NSException *exception) {
                            [ASSDKLogger onException:exception];
                        }
                    });
                }
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
}

- (void)revokeAllTimers {
    [self revokeKillTimer];
    [self revokeCloseTimer];
    [self revokeProgressTimer];
    [self revokeCountdownUpdater];
}

#pragma mark - Countdown Timer

- (void)initCountDownLabel:(NSString *)remainingTimeStr {
    __weak ASVPAIDInterstitialViewController *vpaidInterVC = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        vpaidInterVC.countdownLbl.hidden = NO;
        vpaidInterVC.countdownLbl.text = remainingTimeStr;
    });
}

- (void)updateCountdownTimer {
    __weak ASVPAIDInterstitialViewController *vpaidInterVC = self;
    self.adRemainingTime-=1;
    dispatch_async(dispatch_get_main_queue(), ^{
        vpaidInterVC.countdownLbl.text = (self.adRemainingTime >= 0) ? [NSString stringWithFormat:@"%d", (int)self.adRemainingTime] : @"";
    });
}

- (void)revokeCountdownUpdater {
    self.countdownLbl.text = @"";
    self.countdownLbl.hidden = YES;
    [self.countdownTimerUpdater invalidate];
    self.countdownTimerUpdater = nil;
}

#pragma mark - Video Player Methods

- (void)handshake:(NSString*)version {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, handShake: - version: %@", version]];
    
    // check to see if ad unit supports VPAID 2.0 functionality
    self.canRunVerTwo = [version hasPrefix:@"2"];

    // handshake successful, call loadAndInit
    [self loadAndInit];
}

- (void)loadAndInit {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, loadAndInit"];
    
    NSString *js = @"var ad = iframe.contentWindow['getVPAIDAd']();";
    
    // setting up creativeData and adding adParams
    js = [js stringByAppendingString:@"var creativeData=new Object();"];
    js = [js stringByAppendingString:@"creativeData.AdParameters=unescape('"];
    js = [js stringByAppendingString:(self.vpaidCreative.adParams != nil) ? [self.vpaidCreative.adParams stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] : @""];
    js = [js stringByAppendingString:@"');"];
    
    // setting up environmentVars
    js = [js stringByAppendingString:@"var environmentVars=new Object();"];
    js = [js stringByAppendingString:@"environmentVars.slot=document.getElementById('aerservVpaidSlot');"];
    js = [js stringByAppendingString:@"environmentVars.videoSlot=document.getElementById('aerservVpaidVideoSlot');"];
    js = [js stringByAppendingString:@"environmentVars.videoSlotCanAutoPlay=true;"];
    
    // setting up listeners for dispatched events
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adloaded/'); }, 'AdLoaded');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adstarted/'); }, 'AdStarted');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adstopped/'); }, 'AdStopped');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adskipped/'); }, 'AdSkipped');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adskippable/'); }, 'AdSkippableStateChange');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adsize/'); }, 'AdSizeChange');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adlinear/'); }, 'AdLinearChange');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adduration/'); }, 'AdDurationChange');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adepxanded/'); }, 'AdExpandedChange');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adremainingtime/'); }, 'AdRemainingTimeChange');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-advolume/'); }, 'AdVolumeChange');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adimpression/'); }, 'AdImpression');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-advideostart/'); }, 'AdVideoStart');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-advideofirstquartile/'); }, 'AdVideoFirstQuartile');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-advideomidpoint/'); }, 'AdVideoMidpoint');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-advideothirdquartile/'); }, 'AdVideoThirdQuartile');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-advideocomplete/'); }, 'AdVideoComplete');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(url, id, playerHandles){ loadReq('aerserv://event-adclickthru/' + url + '|||' + id + '|||' + playerHandles); }, 'AdClickThru');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adinteraction/'); }, 'AdInteraction');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-aduseracceptinvitation/'); }, 'AdUserAcceptInvitation');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-aduserminimize/'); }, 'AdUserMinimize');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-aduserclose/'); }, 'AdUserClose');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adpaused/'); }, 'AdPaused');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(){ loadReq('aerserv://event-adplaying/'); }, 'AdPlaying');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(id){ loadReq('aerserv://event-adlog/' + id); }, 'AdLog');"];
    js = [js stringByAppendingString:@"ad.subscribe(function(message){ loadReq('aerserv://event-aderror/' + message); }, 'AdError');"];
    
    // call to initAd()
    js = [js stringByAppendingString:@"ad.initAd("];
    js = [js stringByAppendingString:[NSString stringWithFormat:@"%d,%d,", (int)self.webView.frame.size.width, (int)self.webView.frame.size.height]];
    js = [js stringByAppendingString:@"'normal', 1000000, creativeData, environmentVars);"];

    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, loadAndInit - js:\n\n\n%@\n\n\n", js]];
    [self.webView stringByEvaluatingJavaScriptFromString:js];
    [self startKillTimer];
    
    // reset flags
    [self resetFlags];
    [self resetAdProperties];
}

- (void)resizeAdToSize:(CGSize)inputSize {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, resizeAdToSize: %@", NSStringFromCGSize(inputSize)]];
    NSString *jsCmd = [NSString stringWithFormat:@"ad.resizeAd(%d, %d, 'normal')", (int)inputSize.width, (int)inputSize.height];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, resizeAdToSize - jsCmd: %@", jsCmd]];
    NSLog(@"resizeAdToSize: - inputSize: %@", NSStringFromCGSize(inputSize));
    NSLog(@"resizeAdToSize: - jsCmd: %@", jsCmd);
    NSLog(@"webView.frame: %@", NSStringFromCGRect(self.webView.frame));
    [self.webView stringByEvaluatingJavaScriptFromString:jsCmd];
}

- (void)startAd {
    if(self.adMode == kASVPAIDAdMode_Preload) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, startAd -- Called from show after preload, fire ad load"];
        [self loadWebViewWithCreativeURL:self.vpaidCreative.url.absoluteString];
    } else if(self.adMode != kASVPAIDAdMode_Unloaded) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, startAd"];
        [self.webView stringByEvaluatingJavaScriptFromString:@"ad.startAd();"];
        [self startKillTimer];
    }
}

- (void)onCloseAd {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, onClosedAd"];
    [self cleanUpAd];
    [super onCloseAd];
}

- (void)dismissAd {
    if(self != nil && self.presentingViewController != nil) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)cleanUpAd {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, cleanUpAd"];
    [self revokeAllTimers];
    [self resetFlags];
    [self.webView removeFromSuperview];
    self.webView = nil;
}

- (void)tryNextCreative {
    // attempt next creative if there are any more in ad.creatives, else remove webview
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, tryNextCreative"];
    self.vpaidCreative = [self nextCreativeToTry:self.adToDisplay.creatives];
    if(self.vpaidCreative != nil) {
        [self.view sendSubviewToBack:self.webView];
        [self loadWebViewWithCreativeURL:self.vpaidCreative.url.absoluteString];
    } else {
        [self cleanUpAd];
        [self dismissAd];
    }
}

- (void)stopAd {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, stopAd"];
    
    // stop current ad
    [self.webView stringByEvaluatingJavaScriptFromString:@"ad.stopAd();"];
}

- (void)pauseAd {
    if(self.adMode == kASVPAIDAdMode_Unloaded) return;
    
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, pauseAd"];
    [self.webView stringByEvaluatingJavaScriptFromString:@"ad.pauseAd();"];
}

- (void)resumeAd {
    if(self.adMode == kASVPAIDAdMode_Unloaded) return;
    
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, resumeAd"];
    [self.webView stringByEvaluatingJavaScriptFromString:@"ad.resumeAd();"];
}

- (void)skipAd {
    if(!self.canRunVerTwo || self.adMode == kASVPAIDAdMode_Unloaded) return;
    
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, skipAd"];
    
    // attempt to skip will be handle inside the skippable state getter method
    self.didSkipAd = YES;
    [self getSkippableState];
}

#pragma mark - Ad Unit Properties

- (void)resetAdProperties {
    [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, resetAdProperties"];
    self.adMode = kASVPAIDAdMode_Unloaded;
    self.adSkippableState = NO;
    self.adVolume = -1.0f;
    self.adDidStartOnce = NO;
    self.adWidth = -1.0f;
    self.adHeight = -1.0f;
    
    self.adDuration = -1.0f;
    self.adRemainingTime = -1.0f;
    
    self.closeSema = nil;
    self.progSema = nil;
}

- (void)runGetSetJSFromString:(NSString *)jsStr {
    __weak ASVPAIDInterstitialViewController *vpaidInterVC = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        @try {
            if(jsStr != nil) {
                [vpaidInterVC.opArray addObject:jsStr];
            }
            
            if(vpaidInterVC.opArray.count > 0 && !vpaidInterVC.getReqIsRunning) {
                vpaidInterVC.getReqIsRunning = YES;
                NSString *jsCmd = vpaidInterVC.opArray[0];
                [vpaidInterVC.opArray removeObjectAtIndex:0];
                
                if(jsCmd != nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
						@try {
                	        [vpaidInterVC.webView stringByEvaluatingJavaScriptFromString:jsCmd];
                    	    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, runGetSetJSFromString - opArray: %@", vpaidInterVC.opArray]];
						}
        				@catch (NSException *exception) {
				            [ASSDKLogger onException:exception];
				        }
                    });
                }
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
}

- (void)getLinear {
    if(self.adMode == kASVPAIDAdMode_Unloaded) return;
    
    [self runGetSetJSFromString:@"getLinear();"];
}

- (void)getWidth {
    if(self.adMode == kASVPAIDAdMode_Unloaded || !self.canRunVerTwo) return;
    
    [self runGetSetJSFromString:@"getWidth();"];
}

- (void)getHeight {
    if(self.adMode == kASVPAIDAdMode_Unloaded || !self.canRunVerTwo) return;
    
    [self runGetSetJSFromString:@"getHeight();"];
}

- (void)getExpanded {
    if(self.adMode == kASVPAIDAdMode_Unloaded) return;
    
    [self runGetSetJSFromString:@"getExpanded();"];
}

- (void)getSkippableState {
    if(self.adMode == kASVPAIDAdMode_Unloaded || !self.canRunVerTwo) return;
    
    [self runGetSetJSFromString:@"getSkippableState();"];
}

- (void)getRemainingTime {
    if(self.adMode == kASVPAIDAdMode_Unloaded && !self.canRunVerTwo) return;
    
    [self runGetSetJSFromString:@"getRemainingTime();"];
}

- (void)getDuration {
    if(self.adMode == kASVPAIDAdMode_Unloaded || !self.canRunVerTwo) return;
    
    [self runGetSetJSFromString:@"getDuration();"];
}

- (void)getVolume {
    if(self.adMode == kASVPAIDAdMode_Unloaded) return;
    
    [self runGetSetJSFromString:@"getVolume();"];
}

// vol should be between 0 and 1
- (void)setVolume:(CGFloat)vol {
    if(self.adMode == kASVPAIDAdMode_Unloaded) return;
    
    [self runGetSetJSFromString:[NSString stringWithFormat:@"setVolume(%0.2f)", vol]];
}

- (void)getCompanions {
    if(self.adMode == kASVPAIDAdMode_Unloaded || !self.canRunVerTwo) return;
    
    [self runGetSetJSFromString:@"getCompanions();"];
}

- (void)getIcons {
    if(self.adMode == kASVPAIDAdMode_Unloaded || !self.canRunVerTwo) return;
    
    [self runGetSetJSFromString:@"getIcons();"];
}

- (void)updateCurrentTime {
    self.currentTime = (self.adDuration >= 0 && self.adRemainingTime >= 0) ? self.adDuration - self.adRemainingTime : -1.0f;
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, updateCurrentTime - currentTime: %0.2f - %0.2f = %0.2f", self.adDuration, self.adRemainingTime, self.currentTime]];
}

#pragma mark - URL Action Responses

- (void)getterTaskForAction:(NSString *)actionStr withArg:(NSString *)argStr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, getterTaskForAction:withArg: - %@ = %@", actionStr, argStr]];
    if([actionStr isEqualToString:@"linear"]) {
        self.adLinear = [argStr boolValue];
    }
    else if([actionStr isEqualToString:@"width"]) {
        // maybe store width?
        self.adWidth = [argStr floatValue];
    }
    else if([actionStr isEqualToString:@"height"]) {
        // maybe store height?
        self.adHeight = [argStr floatValue];
    }
    else if([actionStr isEqualToString:@"expanded"]) {
        // storing expanded seems useless for interstitial
    }
    else if([actionStr isEqualToString:@"skippablestate"]) {
        self.adSkippableState = [argStr boolValue];
        self.closeButton.hidden = !self.adSkippableState;
        
        // if there was a previous call to skipAd, stopAd.
        if(self.adSkippableState && self.didSkipAd) {
            [self onCloseAd];
        }
    }
    else if([actionStr isEqualToString:@"remainingtime"]) {
        self.adRemainingTime = [argStr floatValue];
        
        if(self.countdownLbl.hidden && self.adRemainingTime >= 0.0f) {
            [self initCountDownLabel:[NSString stringWithFormat:@"%d", (int)self.adRemainingTime]];
            
            self.countdownTimerUpdater = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                                          target:self
                                                                        selector:@selector(updateCountdownTimer)
                                                                        userInfo:nil
                                                                         repeats:YES];
        }
        
        if(self.progSema != nil) {
            [self updateCurrentTime];;
            dispatch_semaphore_signal(self.progSema);
            self.progSema = nil;
            [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, getterTaskForAction:withArg: - AFTER PROG SEMAPHORE SIGNAL"];
        }
    }
    else if([actionStr isEqualToString:@"duration"]) {
        self.adDuration = [argStr floatValue];
        
        if(self.closeSema != nil) {
            dispatch_semaphore_signal(self.closeSema);
            self.closeSema = nil;
            [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, getterTaskForAction:withArg: - AFTER CLOSE SEMPAHORE SIGNAL"];
        }
    }
    else if([actionStr isEqualToString:@"volume"]) {
        CGFloat oldVol = self.adVolume;
        self.adVolume = [argStr floatValue];
        
        if(oldVol >= 0) { // adVolume was initialized before
            if(self.adVolume == 0 && oldVol > 0) {
                [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.mute];
                [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Mute Event"];
            } else if(self.adVolume > 0 && oldVol == 0) {
                [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.unmute];
                [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Unmute Event"];
            }
        }
    }
    else if([actionStr isEqualToString:@"companions"]) {
        
    }
    else if([actionStr isEqualToString:@"icons"]) {
        
    }
    
    self.getReqIsRunning = NO;
    [self runGetSetJSFromString:nil];
}

- (void)startEvent {
    [self revokeKillTimer];
    
    if(!self.adDidStartOnce) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, startEvent"];
        self.adDidStartOnce = YES;
        [self setCloseAndProgTimer];
        self.webView.hidden = NO;
        [self.view bringSubviewToFront:self.webView];
        [self.view bringSubviewToFront:self.countdownLbl];
        [self.view bringSubviewToFront:self.closeButton];
        [self getVolume];
        self.adMode = kASVPAIDAdMode_Play;
    }
}

- (void)eventTaskForAction:(NSString *)actionStr withArg:(NSString *)argStr {
    if ([actionStr isEqualToString:@"adloaded"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD LOADED"];
        [self revokeKillTimer];
        
        self.adMode = kASVPAIDAdMode_Loaded;
        if(![self.delegate interstitialAdViewControllerIsPreload:self]) {
            // when it's not preload, we send an ad loaded event here
            [self.delegate interstitialAdViewControllerCreatedAdSuccessfully:self];
        }
        [self startAd];
        
        [self getLinear];
    }
    else if ([actionStr isEqualToString:@"adstarted"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD STARTED"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.creativeView];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Creative View Event"];
        
        [self startEvent];
    }
    else if ([actionStr isEqualToString:@"adstopped"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD STOPPED"];

		[self cleanUpAd];
        [self dismissAd];
    }
    else if ([actionStr isEqualToString:@"adskipped"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD SKIPPED"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.skip];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Skip Event"];
        
        [self onCloseAd];
    }
    else if ([actionStr isEqualToString:@"adskippable"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD SKIPPABLESTATE"];
        
        [self getSkippableState];
    }
    else if ([actionStr isEqualToString:@"adsize"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD SIZE"];
        
        [self getHeight];
        [self getWidth];
    }
    else if ([actionStr isEqualToString:@"adlinear"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD LINEAR"];
        
        [self getLinear];
    }
    else if ([actionStr isEqualToString:@"adduration"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD DURATION"];
        
        [self getDuration];
    }
    else if ([actionStr isEqualToString:@"adexpanded"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD EXPANDED"];
        
    }
    else if ([actionStr isEqualToString:@"adremainingtime"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD REMAINING TIME"];
    
        [self getRemainingTime];
    }
    else if ([actionStr isEqualToString:@"advolume"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD VOLUME"];
        
        // get volume, and fire mute/unmute event inside the response
        [self getVolume];
    }
    else if([actionStr isEqualToString:@"adimpression"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD IMPRESSION"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.adToDisplay.impressionEvent];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Impression Event"];
        [ASAdResponseLogger endAdResponseAndSendEvent];
        
    }
    else if ([actionStr isEqualToString:@"advideostart"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD VIDEO START"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.start];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Video Start Event"];
        
        [self startEvent];
        if(!self.closeTimer) {
            [self checkClosabilityWith:kVPAIDClosabilityZeroPercentStr];
        }
    }
    else if ([actionStr isEqualToString:@"advideofirstquartile"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD VIDEO FIRST QUARTILE"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.firstQuartile];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Video First Quartile Event"];
        
        if(!self.closeTimer) {
            [self checkClosabilityWith:kVPAIDClosability25PercentStr];
        }
    }
    else if ([actionStr isEqualToString:@"advideomidpoint"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD VIDEO MID POINT"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.midpoint];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Second Quartile Event"];
        
        if(!self.closeTimer) {
            [self checkClosabilityWith:kVPAIDClosability50PercentStr];
        }
    }
    else if ([actionStr isEqualToString:@"advideothirdquartile"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg, advideomidpoint - AD VIDEO THRID QUARTILE"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.thirdQuartile];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Third Quartile Event"];
        
        if(!self.closeTimer) {
            [self checkClosabilityWith:kVPAIDClosability75PercentStr];
        }
    }
    else if ([actionStr isEqualToString:@"advideocomplete"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD VIDEO COMPLETE"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.complete];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Video Complete Event"];
        
        [self.delegate interstitialAdViewControllerAdFinished:self];
        [self tryNextCreative];
    }
    else if ([actionStr isEqualToString:@"adclickthru"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD CLICK THRU"];
        NSArray *argComponents = [argStr componentsSeparatedByString:kVPAIDArgSepStr];
        NSString *url = argComponents[kVPAIDClickThruURLIndex];
        NSString *Id = argComponents[kVPAIDClickThruIdIndex];
        BOOL ph = [argComponents[kVPAIDClickThruPlayerHandlesIndex] boolValue];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - url: %@, Id: %@, ph: %d", url, Id, ph]];
        
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.clickTracking];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Click Tracking Event"];
        [self.delegate interstitialAdViewControllerAdWasTouched:self];
        
        // if the player handles and there's a click through url go to it
        if(ph && ![url isEqualToString:@""]) {
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        }
    }
    else if ([actionStr isEqualToString:@"adinteraction"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD INTERACTION"];

        [self.delegate interstitialAdViewControllerAdWasTouched:self];
        
        // if the ad pauses from interaction, resume ad upon 2nd interaction
        if(self.adMode == kASVPAIDAdMode_Pause) {
            [self resumeAd];
        }
    }
    else if ([actionStr isEqualToString:@"aduseracceptinvitation"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD USER ACCEPT INVITATION"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.acceptInvitation];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Ad User Accept Invitation Event"];
        
        //no event parsed in vast
        
    }
    else if ([actionStr isEqualToString:@"aduserminimize"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD USER MINIMIZE"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.collapse];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Ad User Minimize Event"];
        
        //no event parsed in vast
        
    }
    else if ([actionStr isEqualToString:@"aduserclose"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD USER CLOSE"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.close];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Ad User Close Event"];
        
        //no event parsed in vast
        
    }
    else if ([actionStr isEqualToString:@"adpaused"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD PAUSE"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.pause];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Ad Pause Event"];
        
        self.adMode = kASVPAIDAdMode_Pause;
        [self revokeAllTimers];
    }
    else if ([actionStr isEqualToString:@"adplaying"]) {
        [ASSDKLogger logStatement:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD RESUME PLAYING"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.vpaidCreative.resume];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VPAID Interstitial View Controller Fires Ad Resume Event"];
        
        self.adMode = kASVPAIDAdMode_Play;
        [self setCloseAndProgTimer];
    }
    else if ([actionStr isEqualToString:@"adlog"]) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD LOG: %@", argStr]];
    }
    else if ([actionStr isEqualToString:@"aderror"]) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, eventTaskForAction:withArg: - AD ERROR: %@", argStr]];
        [ASCommManager sendAsynchronousRequestsForEvent:[self.vpaidCreative.error replaceErrorMacroWithCode:kVASTErrorCodeGeneralVPAIDError]];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VPAID Interstitial View Controller Fires Ad Error Event with argStr: %@", argStr]];
        
        [self.delegate interstitialAdViewControllerFailed:self
                                                withError:[NSError errorWithDomain:NSStringFromClass([ASVPAIDInterstitialViewController class])
                                                                                             code:100
                                                                                         userInfo:@{NSLocalizedDescriptionKey : [NSString stringWithFormat:@"VPAID ERROR: %@", argStr]}]];
        [self cleanUpAd];
        [self dismissAd];
    }
}

#pragma mark - UIWebViewDelegate methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    @try {
        // ignore legit webview requests so they load normally
        if (![request.URL.scheme isEqualToString:@"aerserv"]) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, webView:shouldStartLoadWithRequest: - external request to url: %@", request.URL.absoluteString]];
            if(self.adMode >= kASVPAIDAdMode_Play) {
                self.adMode = kASVPAIDAdMode_Pause;
                [[UIApplication sharedApplication] openURL:request.URL];
                return NO;
            } else {
                return YES;
            }
        }
        
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, webView:shouldStartLoadWithRequest: - internal request to url: %@", request.URL.absoluteString]];
        
        __weak ASVPAIDInterstitialViewController *vpaidInterVC = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            @try {
                // get the task-action
                NSString *taskAction = request.URL.host;
                NSString *taskType = nil;
                NSString *actionType = nil;
                if([taskAction rangeOfString:@"-"].location < taskAction.length ) {
                    NSArray *taskActionArr = [taskAction componentsSeparatedByString:@"-"];
                    taskType = taskActionArr[0];
                    actionType = taskActionArr[1];
                } else {
                    actionType = request.URL.host;
                }
                
                //get argument
                NSRange actionRange = [request.URL.absoluteString rangeOfString:actionType];
                NSString *argument = [[request.URL absoluteString] substringFromIndex:(actionRange.location + actionRange.length + kVPAIDSlashCharLength)];
                argument = [argument stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, webView:shouldStartLoadWithRequest: - actionType: %@, argument: %@", actionType, argument]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    @try {
                        if(taskType != nil) {
                            if([taskType isEqualToString:@"getter"]) {
                                [vpaidInterVC getterTaskForAction:actionType withArg:argument];
                            } else if([taskType isEqualToString:@"event"]) {
                                [vpaidInterVC eventTaskForAction:actionType withArg:argument];
                            }
                        } else if ([actionType isEqualToString:@"handshake"]) {
                            [vpaidInterVC handshake:argument];
                        }
                    }
                    @catch (NSException *exception) {
                        [vpaidInterVC.delegate interstitialAdViewControllerFailed:vpaidInterVC
                                                                        withError:[NSError errorWithDomain:NSStringFromClass([ASVPAIDInterstitialViewController class])
                                                                                                      code:100
                                                                                                  userInfo:@{NSLocalizedDescriptionKey : @"Issue handling vpaid getter/event/action"} ]];
                        [ASSDKLogger onException:exception];
                    }
                });
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        });
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
    return NO;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, webViewDidStartLoad - url: %@", webView.request.URL]];
    
    // starting the load, show the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, webViewDidFinishLoad - url: %@", webView.request.URL]];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVPAIDInterstitialViewController, webView:didFailLoadWithError: - url: %@\n\t\t\t\terror: %@", webView.request.URL, error.localizedDescription]];
    
    @try {
        [self.delegate interstitialAdViewControllerFailed:self
                                                withError:[NSError errorWithDomain:NSStringFromClass([ASVPAIDInterstitialViewController class])
                                                                              code:100
                                                                          userInfo:@{NSLocalizedDescriptionKey : [NSString stringWithFormat:@"ASVPAIDInterstitialViewController.webView didFailLoadWIthError - error: %@", error.localizedDescription]}]];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

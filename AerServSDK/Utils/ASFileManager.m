//
//  ASFileManager.m
//  AerServSDK
//
//  Created by Vasyl Savka on 5/20/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASFileManager.h"

@implementation ASFileManager

- (ASFileManager *)manager {
    static ASFileManager *manager;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [ASFileManager new];
    });
    
    return manager;
}

+ (NSString *)readVersionFromBundle {
    NSString *versionStr = @"N/A";
    @try {
        NSError *err = nil;
        versionStr = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kVersionFileName
                                                                                        ofType:@""]
                                               encoding:NSUTF8StringEncoding error:&err];
        
        if(err != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASFileManager, readVersionFromBundle, err: %@", err.description]];
            versionStr = @"Err";
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
    return [NSString stringWithFormat:@"Version: %@", versionStr];
}

@end

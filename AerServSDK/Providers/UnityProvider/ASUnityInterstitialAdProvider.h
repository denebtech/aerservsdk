//
//  ASUnityInterstitialAdProvider.h
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdProvider.h"
#import "ASCustomInterstitialAdProvider.h"
#import <Foundation/Foundation.h>

#define kUnityInterstitialTimeout 1.5f

@interface ASUnityInterstitialAdProvider : ASCustomInterstitialAdProvider

@property (nonatomic, assign) BOOL adReadyForPlacement;
@property (nonatomic, assign) BOOL failedToReceiveAd;

- (NSString *)getPlacement;

@end

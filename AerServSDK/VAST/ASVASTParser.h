//
//  ASVASTParser.h
//  AerServSDK
//
//  Created by Vasyl Savka on 12/22/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASVASTAd.h"

#define kUseStaticTestVASTAd 0
#define kVastStaticTestAdFileName @"vast-test"
#define kVastStaticTestAdFileType @"xml"

@class ASVASTContainer;

@interface ASVASTParser : NSObject

@property (nonatomic, strong) NSData *xmlData;
@property (nonatomic, copy) NSString *vastVer;

+ (ASVASTParser *)parserInstance;
+ (ASVASTContainer *)findAdWithXMLData:(NSData *)data withDelegate:(id<ASVASTAdDelegate>)delegate asPreload:(BOOL)isPreload;

@end

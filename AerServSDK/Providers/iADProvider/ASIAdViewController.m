//
//  ASIAdViewController.m
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/27/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <iAd/iAd.h>
#import "ASIAdViewController.h"

@interface ASIAdViewController ()<ADInterstitialAdDelegate>

@property (nonatomic, strong) ADInterstitialAd* interstitialAd;
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation ASIAdViewController

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASIAdViewController, dealloc"];
    self.interstitialAd = nil;
    [self.scrollView removeFromSuperview];
    self.scrollView = nil;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{ // don't believe this is being utilized...
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
        _interstitialAd = [ADInterstitialAd new];
        _interstitialAd.delegate = self;
    }
    return self;
}

- (instancetype)initWithDelegate:(id<ASInterstitialAdViewControllerDelegate>)delegate {
    if(self = [super initWithDelegate:delegate]) {
        _interstitialAd = [ADInterstitialAd new];
        _interstitialAd.delegate = self;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:[self.view bounds]];
        [self.view addSubview:_scrollView];
    }
    
    return _scrollView;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.scrollView.frame = self.view.bounds;
}

#pragma mark - ADInterstitialAdDelegate Protocol Methods

- (void)interstitialAdDidUnload:(ADInterstitialAd *)interstitialAd {
    [ASSDKLogger logStatement:@"ASIAdViewController, interstitialAdDidUnload:"];
    self.interstitialAd = nil;
}

- (void)interstitialAd:(ADInterstitialAd *)interstitialAd didFailWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASIAdViewController, interstitialAd:didFailWithError: - Error: %@", error.localizedDescription]];
    @try {
        self.interstitialAd = nil;
        [self.delegate interstitialAdViewControllerFailed:self withError:error];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)interstitialAdDidLoad:(ADInterstitialAd *)interstitialAd {
    [ASSDKLogger logStatement:@"ASIAdViewController, interstitialAdDidLoad:"];
    @try {
        [self.interstitialAd presentInView:self.scrollView];
        
        [self.delegate interstitialAdViewControllerCreatedAdSuccessfully:self];
        
        [ASAdResponseLogger endAdResponseAndSendEvent];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (BOOL)interstitialAdActionShouldBegin:(ADInterstitialAd *)interstitialAd willLeaveApplication:(BOOL)willLeave {
    [ASSDKLogger logStatement:@"ASIAdViewController, interstitialAdActionShouldBegin:willLeaveApplication:"];
    @try {
        [self.delegate interstitialAdViewControllerAdWasTouched:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
    return YES;
}

- (void)interstitialAdActionDidFinish:(ADInterstitialAd *)interstitialAd {
    [ASSDKLogger logStatement:@"ASIAdViewController, interstitialAdActionDidFinish:"];
    @try {
        [self.scrollView removeFromSuperview];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        [self.delegate interstitialAdViewControllerAdFinished:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

//
//  ASSIAdBannerProvider.m
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/26/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <iAd/iAd.h>
#import "ASIAdBannerAdProvider.h"


@interface ASIAdBannerAdProvider()<ADBannerViewDelegate>

@property (nonatomic, strong) ADBannerView *adBannerView;

@end


@implementation ASIAdBannerAdProvider

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASIAdBannerAdProvider, dealloc"];
    self.adBannerView = nil;
}

- (void)requestAdWithSize:(CGSize)size withProperties:(NSDictionary *)properties isPreload:(BOOL)preload {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASIAdBannerAdProvider, requestAdWithSize:withProperties: - size: %@", NSStringFromCGSize(size)]];
    self.adBannerView = [ADBannerView new];
    self.adBannerView.delegate = self;
    self.isPreload = preload;
}

//- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation {
//    CGSize size = [self.adBannerView sizeThatFits:self.adBannerView.superview.bounds.size];
//    
//    CGRect frame = self.adBannerView.frame;
//    frame.size = size;
//    
//    self.adBannerView.frame = frame;
//}

#pragma mark - ADBannerViewDelegate Protocol Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    [ASSDKLogger logStatement:@"ASIAdBannerAdProvider, bannerViewDidLoadAd:"];
    @try {
        [self.delegate bannerProvider:self didLoadAd:banner];
        
        [ASAdResponseLogger endAdResponseAndSendEvent];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASIAdBannerAdProvider, bannerView:didFailToReceiveAdWithError: - Error: %@", error.localizedDescription]];
    @try {
        [self.delegate bannerProvider:self didFailToLoadAdWithError:error];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASIAdBannerAdProvider, bannerViewActionShouldBegin:willLeaveApplication: - willLeave: %d", willLeave]];
    @try {
        [self.delegate bannerProviderWillBeginAction:self];
        [self.delegate bannerProviderAdWasClicked:self];
        
        if (willLeave) {
            [self.delegate bannerProviderWillLeaveApplication:self];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner {
    [ASSDKLogger logStatement:@"ASIAdBannerAdProvider, bannerViewActionDidFinish"];
    @try {
        [self.delegate bannerProviderWillEndAction:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

#!/usr/bin/python
#
# ConvertFileToBytes.py
# Used to convert given file into a byte array to be stored inside a framework
#
# Usage: ConvertFilesToBytes.py inputFile outputFile
#
# Albert Zhu | AerServ, LLC


import sys
import os

if len(sys.argv) > 1:
	inputFile = sys.argv[1]
	if len(sys.argv) > 2:
		writeToFile = sys.argv[2]

		if os.path.isfile(inputFile):
			readFile = open(inputFile, 'r')
			readData = readFile.read()
			b = bytearray(readData)
			readFile.close()
			inputDir = os.path.split(inputFile)[0]
			writeToFilePath = os.path.join(inputDir, writeToFile)
			writeFile = open(writeToFilePath, 'w+');
		
			writeFile.write('//\n//  ASMRAIDJS.h\n//  AerServSDK\n//\n//  Created by Albert Zhu on 9/1/15.\n//  Copyright (c) 2015 AerServ, LLC. All rights reserved.\n\n\n')
			writeFile.write('unsigned char mraidJS[] = {\n')
			i = 0	
			while i < len(b)-10:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}, 0x{2:02X}, 0x{3:02X}, 0x{4:02X}, 0x{5:02X}, 0x{6:02X}, 0x{7:02X}, 0x{8:02X}, 0x{9:02X},\n'.format(b[i], b[i+1], b[i+2], b[i+3], b[i+4], b[i+5], b[i+6], b[i+7], b[i+8], b[i+9]))
				i += 10

			j  = len(b) - i
			if j == 10:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}, 0x{2:02X}, 0x{3:02X}, 0x{4:02X}, 0x{5:02X}, 0x{6:02X}, 0x{7:02X}, 0x{8:02X}, 0x{9:02X}\n'.format(b[i], b[i+1], b[i+2], b[i+3], b[i+4], b[i+5], b[i+6], b[i+7], b[i+8], b[i+9]))
			elif j == 9:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}, 0x{2:02X}, 0x{3:02X}, 0x{4:02X}, 0x{5:02X}, 0x{6:02X}, 0x{7:02X}, 0x{8:02X}\n'.format(b[i], b[i+1], b[i+2], b[i+3], b[i+4], b[i+5], b[i+6], b[i+7], b[i+8]))
			elif j == 8:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}, 0x{2:02X}, 0x{3:02X}, 0x{4:02X}, 0x{5:02X}, 0x{6:02X}, 0x{7:02X}\n'.format(b[i], b[i+1], b[i+2], b[i+3], b[i+4], b[i+5], b[i+6], b[i+7]))
			elif j == 7:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}, 0x{2:02X}, 0x{3:02X}, 0x{4:02X}, 0x{5:02X}, 0x{6:02X}\n'.format(b[i], b[i+1], b[i+2], b[i+3], b[i+4], b[i+5], b[i+6]))
			elif j == 6:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}, 0x{2:02X}, 0x{3:02X}, 0x{4:02X}, 0x{5:02X}\n'.format(b[i], b[i+1], b[i+2], b[i+3], b[i+4], b[i+5]))
			elif j == 5:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}, 0x{2:02X}, 0x{3:02X}, 0x{4:02X}\n'.format(b[i], b[i+1], b[i+2], b[i+3], b[i+4]))
			elif j == 4:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}, 0x{2:02X}, 0x{3:02X}\n'.format(b[i], b[i+1], b[i+2], b[i+3]))
			elif j == 3:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}, 0x{2:02x}\n'.format(b[i], b[i+1], b[i+2]))
			elif j == 2:
				writeFile.write('\t0x{0:02X}, 0x{1:02X}\n'.format(b[i], b[i+1]))
			elif j == 1:
				writeFile.write('\t0x{0:02X}\n'.format(b[i]))
			writeFile.write('};\n\n')

			writeFile.write('unsigned int mraidJSLen = {0};\n'.format(len(b)))
			writeFile.close()
	        	print "Converted bytes written to successfully at " + str(writeToFilePath)
		else:
			print "File does not exist at the path supplied"
	else:
		print "No output filename supplied."
else:
	print "No filepath was specified to convert"

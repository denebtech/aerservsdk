//
//  ASVASTParser.m
//  AerServSDK
//
//  Created by Vasyl Savka on 12/22/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASVASTParser.h"
#import "ASVASTContainer.h"
#import "ASXMLQueryEngine.h"
#import "ASXMLNode.h"

@implementation ASVASTParser

+ (ASVASTParser *)parserInstance {
    static ASVASTParser *pInst = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        pInst = [[ASVASTParser alloc] init];
    });
    
    return pInst;
}

+ (ASVASTContainer *)findAdWithXMLData:(NSData *)data withDelegate:(id<ASVASTAdDelegate>)delegate asPreload:(BOOL)isPreload {
    
    #if kUseStaticTestVASTAd
    data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kVastStaticTestAdFileName ofType:kVastStaticTestAdFileType]];
    #endif
    
    if(!data) return nil;

    // initial setup
    [ASVASTParser parserInstance].xmlData = data;
    ASVASTContainer *container = [ASVASTContainer new];
    
    // checking for invalid XML
    ASXMLQueryEngine *xmlQueryEngine = [[ASXMLQueryEngine alloc] initWithXMLData:data];
    if(!xmlQueryEngine) {
        [delegate vastAdDidFailToLoad:nil
                            withError:[NSError errorWithDomain:NSStringFromClass([ASVASTParser class])
                                                          code:kVASTErrorCodeXMLParsingError
                                                      userInfo:@{NSLocalizedDescriptionKey : @"Parsing Error, Invalid XML"}]
                     andVastErrorCode:kVASTErrorCodeXMLParsingError];
        container.didFailoverAlready = YES;
    } else {
        NSArray *vastElement = [xmlQueryEngine queryWithXPath:@"/VAST[1]"];
        NSArray *adElement = [xmlQueryEngine queryWithXPath:@"/VAST[1]/Ad"];
        
        // checking for empty vast
        if(!vastElement || vastElement.count == 0 ) {
            [delegate vastAdDidFailToLoad:nil
                                withError:[NSError errorWithDomain:NSStringFromClass([ASVASTParser class])
                                                              code:kVASTErrorCodeNoInlineVASTAfterWrappers
                                                          userInfo:@{NSLocalizedDescriptionKey : @"Parsing Error, Empty VAST"}]
                         andVastErrorCode:kVASTErrorCodeNoInlineVASTAfterWrappers];
            return nil;
        } else if(vastElement != nil && vastElement.count > 0) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTParser, findAdWithXMLData:withDelegate:asPreload: -- adElement.count: %ld", (unsigned long)adElement.count]];
            
            // grab the vast version number
            NSString *vastVer = nil;
            for(ASXMLNode *node in vastElement) {
                if(!vastVer) {
                    if([[node name] isEqualToString:@"VAST"])
                        vastVer = [node attributes][@"version"];
                } else {
                    continue;
                }
            }
            
            if(adElement.count == 1) {
                
                // loading a single vast
                container.vastAd = [[ASVASTAd alloc] initWithXMLNode:adElement[0] withDelegate:delegate asPreload:isPreload partOfAdPods:NO];
                
            } else if(adElement.count > 1) {
                
                // potentially an ad pod, but could still be a single ad
                NSMutableArray *seqArr = [[NSMutableArray alloc] initWithCapacity:adElement.count];
                NSMutableArray *adArr = [[NSMutableArray alloc] initWithCapacity:adElement.count];
                
                // determine where the ad is an sequence ad that is part of an ad pod or not
                for(ASXMLNode *node in adElement) {
                    NSString *seq = node.attributes[@"sequence"];
                    if(seq != nil && ![seq isEqualToString:@""]) {
                        seqArr[[seq integerValue]-1] = node;
                    } else {
                        [adArr addObject:node];
                    }
                }
                
                if(seqArr.count > 0) {
                    // change all the xml nodes into vast ads
                    for(int i=0; i<seqArr.count; i++) {
                        seqArr[i] = [[ASVASTAd alloc] initWithXMLNode:seqArr[i] withDelegate:delegate asPreload:isPreload partOfAdPods:YES];
                    }
                    
                    // create a linked list of vast ads
                    for(int i=0; i<seqArr.count; i++) {
                        int j=i+1;
                        if(j != seqArr.count) {
                            ASVASTAd *tempAd = seqArr[i];
                            tempAd.seqAd = seqArr[j];
                        }
                    }
                    
                    // set the found ad to be the first of the sequence
                    container.vastAd = seqArr[0];
                    [delegate vastAdDidLoad:container.vastAd];
                } else {
                    // since there were no ad pods found, just link the first ad
                    container.vastAd = [[ASVASTAd alloc] initWithXMLNode:adArr[0] withDelegate:delegate asPreload:isPreload partOfAdPods:NO];
                }
            }
        }
        vastElement = nil;
        adElement = nil;
    }
    
    return container;
}

@end

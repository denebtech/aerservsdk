//
//  ASVastVideoPlayer.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/31/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVASTVideoPlayer.h"
#import "ASVASTCreative.h"
#import "ASVideoView.h"
#import "ASVASTEvent.h"
#import "ASVASTEventError.h"
#import "ASVASTAd.h"
#import "ASVASTVideoPlayerView.h"
#import "ASCountdownLabel.h"

#import "ASCommManager.h"
#import "ASCategoryHelper.h"
#import "ASPubSettingUtils.h"

#import "AVPlayer+StateChange.h"

typedef NS_ENUM(NSUInteger, ASVASTVideoPlayerWebViewTag) {
    ASVASTVideoPlayerWebViewBanner = 1000,
    ASVASTVideoPlayerwebViewAd
};

static void* ASVASTVideoPlayer_StatusObservation = &ASVASTVideoPlayer_StatusObservation;
static void* ASVASTPlayerItem_BufferObservation = &ASVASTPlayerItem_BufferObservation;

@interface ASVASTVideoPlayer ()

@property (nonatomic, strong) ASVASTCreative *video;
@property (nonatomic, strong) ASVASTVideoPlayerView *view;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) id periodicTimeObserver;

@property (nonatomic, assign) BOOL ready;
@property (nonatomic, assign) BOOL notifyUserOfComletion;
@property (nonatomic, assign) CMTime duration;
@property (nonatomic, strong) ASVASTAd *ad;
@property (nonatomic, assign) NSInteger creativeNumber;
@property (nonatomic, assign) BOOL bannerLoaded;
@property (nonatomic, assign) BOOL offsetApplies;
@property (nonatomic, assign) BOOL didBufferStart;
@property (nonatomic, assign) BOOL didSendReady;
@property (nonatomic, assign) BOOL didStart;

@property (nonatomic, strong) ASCountdownLabel *countdownLbl;
@property (nonatomic, strong) UIActivityIndicatorView *actIndView;
@property (nonatomic, assign) BOOL trackerStarted;

@end


@implementation ASVASTVideoPlayer

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, dealloc"];
    
    @try {
        // go through and clean up.
        if (self.playerItem != nil)
            [self finishInlineAd:self.playerItem didComplete:NO];
        
        // deregister on destruct
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
        
        self.video = nil;
        
        // remove labels
        [self.countdownLbl removeFromSuperview];
        self.countdownLbl = nil;
        
        [self.actIndView removeFromSuperview];
        self.actIndView = nil;
        
        self.ad = nil;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)playVASTAd:(ASVASTAd*)ad {
    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, playVASTAd:"];
    self.ad = ad;
    self.creativeNumber = 0;
    
    [self showCurrentCreative];
}

- (void)showCurrentCreative {
    if ([self.ad.creatives count] == 0) {
        [ASSDKLogger logStatement:@"ASVASTVideoPlayer, showCurrentCreative - No Creatives, return."];
        return;
    }
    
    ASVASTCreative *creative = self.ad.creatives[self.creativeNumber];
    self.didSendReady = NO;

    // assuming that we will always have VAST
    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, showCurrentCreative - VAST Creative"];
    [self.delegate VASTVideoPlayer:self willShowSkip:NO];
    self.trackerStarted = NO;
    [self playVASTVideo:creative];
}

- (void)playVASTVideo:(ASVASTCreative *)video {
    if(!video.url) {
        [ASSDKLogger logStatement:@"ASVASTVideoPlayer, playVASTVideo: - creative video url does not exist, thus failover"];
        [self.delegate VASTVideoPlayerFailedToLoad:self withErrorCode:kVASTErrorCodeGeneralLinearError];
        return;
    } else {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTVideoPlayer, playVASTVideo: - Creative URL: %@", video.url]];
    }
	
	// always make the ad full screen
	// @TODO make sure this is done with the right AR
    CGFloat maxLen = MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    CGFloat minLen = MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    if(video.size.width > video.size.height) {
        video.size = CGSizeMake(maxLen, minLen);
    } else {
        video.size = CGSizeMake(minLen, maxLen);
    }
    self.video = video;
    if (self.view == nil) {
        [ASSDKLogger logStatement:@"ASVASTVideoPlayer, playVASTVideo: - view does not exist, create ASVASTVideoPlayerView object as new view"];
        self.view = [[ASVASTVideoPlayerView new] initWithFrame:CGRectMake(0,0,video.size.width, video.size.height)];
    }
    
    if(!self.countdownLbl) {
        self.countdownLbl = [ASCountdownLabel new];
        [self.countdownLbl changePosToX:0.0f andY:(self.view.frame.size.height - kCountdownLblH)];
        [self.view addSubview:self.countdownLbl];
    } else {
        self.countdownLbl.text = nil;
    }
    
    if(!self.actIndView) {
        self.actIndView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.actIndView.hidesWhenStopped = YES;
        self.actIndView.hidden = NO;
//        self.actIndView.frame = CGRectMake(0.0f, 0.0f, self.actIndView.frame.size.width, self.actIndView.frame.size.height);
        self.actIndView.center = self.view.center;
        self.actIndView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        self.actIndView.layer.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.25f].CGColor;
        self.actIndView.layer.cornerRadius = 20.0f;
        self.actIndView.layer.shadowColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f].CGColor;
        self.actIndView.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);

        [self.view addSubview:self.actIndView];
        [self.view bringSubviewToFront:self.actIndView];
    }
    
    self.ready = NO;
    
    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, playVASTVideo: - Create AVPlayerItem with notifications and observations"];
    AVPlayerItem* item = [[AVPlayerItem alloc] initWithURL:video.url];
    
    // registering notifiations
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidGotoBackground)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appBecameActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(inlineAdFailedToPlayUntilEnd:)
                                                 name:AVPlayerItemFailedToPlayToEndTimeNotification
                                               object:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(inlineAdSuccessfullyPlayedUntilEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:item];
    
    // lets add some simple observation...
    [item addObserver:self
           forKeyPath:kAVPlayerItemKeyStatus
              options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
              context:ASVASTVideoPlayer_StatusObservation];
    
    [item addObserver:self
           forKeyPath:kAVPlayerItemKeyPlaybackLikelyToKeepUp
              options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
              context:ASVASTPlayerItem_BufferObservation];
    
    [item addObserver:self
           forKeyPath:kAVPlayerItemKeyPlaybackBufferFull
              options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
              context:ASVASTPlayerItem_BufferObservation];
    
    self.playerItem = item;
    
    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, playVASTVideo: - Create AVPlayer"];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    [self.delegate AVPlayerCreated:self.player];
    
    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, playVASTVideo: - Setup offset percent"];
    NSNumber *offsetPercent = [[NSNumber alloc] initWithInt:-1];
     
    if([self.delegate closeOffset]) {
        offsetPercent = [ASPubSettingUtils getOffsetAsPercent:[self.delegate closeOffset] videoDuration:video.duration];
    }
    
    if([offsetPercent doubleValue] >= 0) {
        self.offsetApplies = YES;
    } else {
        self.offsetApplies = NO;
    }
    
    // we are going to set up a periodic listener.
    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, playVASTVideo: - Setup periodic listener"];
    __weak ASVASTVideoPlayer* videoPlayer = self;
    self.periodicTimeObserver = [self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1, 1) queue:NULL usingBlock:^(CMTime time) {
        @try {
            if (CMTIME_IS_INVALID(videoPlayer.duration)) {
                videoPlayer.countdownLbl.hidden = YES;
                videoPlayer.countdownLbl.text = nil;
                return;
            }
            
            Float64 currentPercentage = CMTimeGetSeconds(time)/CMTimeGetSeconds(videoPlayer.duration);
            videoPlayer.countdownLbl.text = [NSString stringWithFormat:@"%d", (int)(CMTimeGetSeconds(videoPlayer.duration) - CMTimeGetSeconds(time))];
            
            if(videoPlayer.offsetApplies) {
                if(currentPercentage > [offsetPercent doubleValue]) {
                    [videoPlayer.delegate VASTVideoPlayer:videoPlayer willShowSkip:YES];
                    videoPlayer.offsetApplies = NO;
                }
            }
            
            // depending on our duration is when we need to fire a VAST 2.0 event.
            if(currentPercentage >= .00 && videoPlayer.video.start.canBeSent) {
                [ASCommManager sendAsynchronousRequestsForEvent:videoPlayer.video.start];
                [videoPlayer.delegate VASTVideoPlayer:videoPlayer didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Video Player Fires Video Start Event"]];
                [videoPlayer.actIndView stopAnimating];
            }
            
            if (currentPercentage >= .25 && videoPlayer.video.firstQuartile.canBeSent) {
                [ASCommManager sendAsynchronousRequestsForEvent:videoPlayer.video.firstQuartile];
                [videoPlayer.delegate VASTVideoPlayer:videoPlayer didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Video Player Fires First Quartile Event"]];
            }
            
            if (currentPercentage >= .5 && videoPlayer.video.midpoint.canBeSent) {
                [ASCommManager sendAsynchronousRequestsForEvent:videoPlayer.video.midpoint];
                [videoPlayer.delegate VASTVideoPlayer:videoPlayer didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Video Player Fires Second Quartile Event"]];
            }
            
            if (currentPercentage >= .75  && videoPlayer.video.thirdQuartile.canBeSent) {
                [ASCommManager sendAsynchronousRequestsForEvent:videoPlayer.video.thirdQuartile];
                [videoPlayer.delegate VASTVideoPlayer:videoPlayer didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Video Player Fires Third Quartile Event"]];
            }
        }
        @catch (NSException *exception) {
            [videoPlayer.delegate VASTVideoPlayerFailedToLoad:videoPlayer withErrorCode:kVASTErrorCodeGeneralLinearError];
            [ASSDKLogger onException:exception];
        }
    }];
}

- (void)startAd {
    if ([self.delegate shouldShowAd]) {
        [ASSDKLogger logStatement:@"ASVASTVideoPlayer, startAd - Ad should show and play."];
        
        // start video play
        [self.player changeStateToPlay];
        self.didStart = YES;
        
        // fire vast impression
        if(self.ad.impressionEvent.canBeSent) {
            [ASCommManager sendAsynchronousRequestsForEvent:self.ad.impressionEvent];
            [self.delegate VASTVideoPlayer:self didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Video Player Fires Impression Event"]];
        }
        
        // end ad response logging time for vast
        [ASAdResponseLogger endAdResponseAndSendEvent];
    }
}

- (void)stopAd {
    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, stopAd"];
    [self.player changeStateToPause];
    [self finishInlineAd:self.playerItem didComplete:NO];
}

- (CGSize)contentSize {
    return [self.view contentSize];
}

#pragma mark - Notification Handlers

- (void)appDidGotoBackground {
    @try {
        if(self.didStart) {
            [ASSDKLogger logStatement:@"ASVASTVideoPlayer, appDidGotoBackground - PAUSE AD"];
            [self.player changeStateToPause];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)appBecameActive {
    @try {
        if(self.didStart) {
            [ASSDKLogger logStatement:@"ASVASTVideoPlayer, appBecameActive - START AD"];
            [self startAd];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
        [self.delegate VASTVideoPlayerFailedToLoad:self withErrorCode:kVASTErrorCodeGeneralLinearError];
    }
}

- (void)finishInlineAd:(AVPlayerItem*)playerItem didComplete:(BOOL)completed {
    if(!self.player) return;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTVideoPlayer, finishInLineAd:userInvoked: - completed: %d", completed]];
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemFailedToPlayToEndTimeNotification object:playerItem];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        
        if (self.periodicTimeObserver != nil) {
            [self.player removeTimeObserver:self.periodicTimeObserver];
            self.periodicTimeObserver = nil;
            self.duration = kCMTimeInvalid;
        }
        
        [playerItem removeObserver:self forKeyPath:kAVPlayerItemKeyStatus];
        [playerItem removeObserver:self forKeyPath:kAVPlayerItemKeyPlaybackLikelyToKeepUp];
        [playerItem removeObserver:self forKeyPath:kAVPlayerItemKeyPlaybackBufferFull];
        
        if (self.ready && completed) {
            if (self.video.complete.canBeSent) {
                [ASCommManager sendAsynchronousRequestsForEvent:self.video.complete];
                [self.delegate VASTVideoPlayer:self didFireAdvertiserEventWithMessage:@"VAST Video Player Fires Video Complete Event"];
            }
        }
        
        [self.view.videoView attachPlayer:nil];
        [self.view removeFromSuperview];
        self.view = nil;        
        
        self.bannerLoaded = NO;
        self.creativeNumber++;
        self.playerItem = nil;
        self.ready = NO;
        self.player = nil;
        
        [ASSDKLogger logStatement:@"ASVASTVideoPlayer, finishInlineAd:userInvoked: - Successfully removed event listeners from and released all resources"];
    }
    @catch (NSException *exception) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTVideoPlayer, finishInlineAd:userInvoked: - Caught exception while trying to remove listener ex: %@", exception.description]];
        [ASSDKLogger onException:exception];
    }
}

- (void)showNextCreativeOrAd {
    if (self.creativeNumber >= [self.ad.creatives count]) {
        if(self.ad.seqAd != nil) {
            [self.delegate VASTVideoPlayerWillRewardVC:self];
            [self playVASTAd:self.ad.seqAd];
        } else {
            [self.delegate VASTVideoPlayerAdCompleted:self];
        }
    } else {
        [self showCurrentCreative];
    }
}

- (void)inlineAdFailedToPlayUntilEnd:(NSNotification*)notification {
    @try {
        AVPlayerItem* item = [notification object];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTVideoPlayer, inlineAdFailedToPlayUntilEnd: - The current ad %@ did not finish playing until the end", item]];
        
        __weak ASVASTVideoPlayer* videoPlayer = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                [videoPlayer finishInlineAd:item didComplete:NO];
                [videoPlayer showNextCreativeOrAd];
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
                [videoPlayer.delegate cancel];
            }
        });
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
        [self.delegate cancel];
    }
}

- (void)inlineAdSuccessfullyPlayedUntilEnd:(NSNotification*)notification {
    @try {
        AVPlayerItem* item = [notification object];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTVideoPlayer, inlineAdSuccessfullyPlayedUntilEnd: - The current ad %@ did finish playing until the end", item]];
        
        __weak ASVASTVideoPlayer* videoPlayer = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                [videoPlayer finishInlineAd:item didComplete:YES];
                [videoPlayer showNextCreativeOrAd];
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
                [videoPlayer.delegate cancel];
            }
        });
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
        [self.delegate cancel];
    }
}

- (void)observeValueForKeyPath:(NSString*)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    @try {
        if (context == ASVASTVideoPlayer_StatusObservation) {
            // check our status..
            
            // get our status.. Push back on the main thread..
            AVPlayerItemStatus status = [change[NSKeyValueChangeNewKey] integerValue];
            
            __weak ASVASTVideoPlayer* videoPlayer = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                // we need to send messages to our listeners to funnel down trough appropriate UI channels.
                @try {
                    switch (status) {
                        case AVPlayerItemStatusReadyToPlay:
                            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - PlayerItem Status: AVPlayerItemStatusReadyToPlay, videoPlayer.ready: %d", videoPlayer.ready]];
                            if (!videoPlayer.ready) {
                                [videoPlayer.view.videoView attachPlayer:videoPlayer.player];
                                
                                videoPlayer.duration = videoPlayer.playerItem.duration;
                                
                                CGSize size = videoPlayer.video.size;
                                if (videoPlayer.video.size.width == 0 || videoPlayer.video.size.height == 0)
                                    size = videoPlayer.playerItem.presentationSize;
                                
                                // use our adjusted size
                                videoPlayer.video.size = size;
                                
                                videoPlayer.view.originalAdSize = size;
                                videoPlayer.view.frame = CGRectMake(0,0, videoPlayer.video.size.width, videoPlayer.video.size.height);
                                videoPlayer.ready = YES;
                                
                                if (!videoPlayer.ad.isPreload)
                                    [videoPlayer.delegate VASTVideoPlayerVideoIsReady:videoPlayer];
                                
                                videoPlayer.countdownLbl.hidden = NO;
                                
                                [ASCommManager sendAsynchronousRequestsForEvent:videoPlayer.video.creativeView];
                                [videoPlayer.delegate VASTVideoPlayer:videoPlayer didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Video Player Fires Creative View Event"]];
                                
                                [videoPlayer.actIndView startAnimating];
                            }
                            
                            break;
                        case AVPlayerItemStatusFailed:
                            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Status: AVPlayerItemStatusFailed - change: %@", change]];
                            [videoPlayer.delegate VASTVideoPlayerFailedToLoad:videoPlayer withErrorCode:kVASTErrorCodeProblemDisplayingMediaFile];
                            break;
                        default:
                            break;
                    }
                }
                @catch (NSException *exception) {
                    [videoPlayer.delegate VASTVideoPlayerFailedToLoad:videoPlayer withErrorCode:kVASTErrorCodeProblemDisplayingMediaFile];
                    [ASSDKLogger onException:exception];
                }
            });
        } else if(context == ASVASTPlayerItem_BufferObservation) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: playbackLikelyToKeepUp: %d, playbackBufferFull: %d, videoPauseState: %d", self.playerItem.playbackLikelyToKeepUp, self.playerItem.playbackBufferFull, [ASCategoryHelper instance].videoPauseState]];
            
            if(self.playerItem.playbackLikelyToKeepUp || self.playerItem.playbackBufferFull) {
                [ASSDKLogger logStatement:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: playbackLikelyToKeepUp || playbackBufferFull"];
                if(self.didStart && ![ASCategoryHelper instance].videoPauseState) {
                    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: diStart && !videoPauseState - PLAY"];
                    [self.actIndView stopAnimating];
                    [self.player play];
                } else if(!self.didStart) {
                    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: !didStart"];
                    if(!self.didSendReady) {
                        [ASSDKLogger logStatement:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: !didSendReady - READY"];
                        [self.delegate VASTVideoPlayerVideoIsReady:self];
                        self.didSendReady = YES;
                    } else {
                        [ASSDKLogger logStatement:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: isPreload && !didSendReady - ALREADY SENT READY -- NOW WHAT?"];
                    }
                }
            } else if(self.playerItem.playbackBufferEmpty) {
                [ASSDKLogger logStatement:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: playbackBufferEmpty - PAUSE"];
                [self.actIndView startAnimating];
                [self.player pause];
            } else {
                [ASSDKLogger logStatement:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: !playbackLikelyToKeepUp && !playbackBufferFull && !playbackBufferEmpty"];
                if(self.didStart && ![ASCategoryHelper instance].videoPauseState) {
                    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: didStart && !vidoePauseState - PLAY"];
                    [self.actIndView stopAnimating];
                    [self.player play];
                } else {
                    [ASSDKLogger logStatement:@"ASVASTVideoPlayer, observeValueForKeyPath:ofObject:change:context: - AVPlayerItem, Buffer Observation: (!didStart || vidoePauseState) - DO NOTHING | WAIT"];
                }
            }
            
            
            self.didBufferStart = YES;
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

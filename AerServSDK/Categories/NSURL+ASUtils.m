//
//  NSURL+ASUtils.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/8/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "NSURL+ASUtils.h"

#import "ASCategoryHelper.h"
//#import "ASAlertManager.h"

#import <AdSupport/AdSupport.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
//#import <CoreTelephony/CTTelephonyNetworkInfo.h>
//#import <CoreTelephony/CTCarrier.h>
#import <sys/utsname.h>

NSString* const kASSDKVersion = @"1.0.1";
NSString* const kASProductionBaseURL = @"https://ads.aerserv.com/as/";
NSString* const kASStagingBaseURL = @"https://staging.ads.aerserv.com/as/";
NSString* const kASDevBaseURL = @"https://dev.ads.aerserv.com/as/";

NSString* const kDeviceMakeApple = @"Apple";

@implementation NSURL (ASUtils)

#pragma mark = Initializers

+ (instancetype)URLForPlacementID:(NSString *)placementID inEnv:(ASEnvironmentType)env atlocation:(CLLocation *)location usingKeyWords:(NSArray *)keyWordsArr andPubKeys:(NSDictionary *)pubKeys withUserID:(NSString *)userId asPreload:(BOOL)preload onPlatform:(ASPlatformType)platform {
    return [[[self class] alloc] initWithPlacementID:placementID inEnv:env atlocation:location usingKeyWords:keyWordsArr andPubKeys:pubKeys withUserID:userId asPreload:preload onPlatform:(ASPlatformType)platform];
}

- (instancetype)initWithPlacementID:(NSString *)placementID inEnv:(ASEnvironmentType)env atlocation:(CLLocation *)location usingKeyWords:(NSArray *)keyWordsArr andPubKeys:(NSDictionary *)pubKeys withUserID:(NSString *)userId asPreload:(BOOL)preload onPlatform:(ASPlatformType)platform {
    NSString *adId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
//    // if the environment is pointing off production, display a warning -- POP-UP WARNING WILL AFFECT THE VIEW HIERARCHY, NEEDS TO BE DISABLED
//    if(env != kASEnvProduction){
//        [self displayOffProductionWarning];
//    }
    
    // on the off chance that the placement ID is given as an URL instead of a string
    if ([placementID hasPrefix:@"http://"] || [placementID hasPrefix:@"https://"])
        return [NSURL URLWithAddress:placementID];
    
    // TVOS CHANGE: inttype = 3 & vpaid = 0
    NSMutableString* URLString = [NSMutableString stringWithFormat:@"%@?inttype=3&sdkv=%@&key=2&plc=%@&oid=%@&adid=%@&vpaid=0", [self baseURLForEnv:env], kASSDKVersion, placementID, adId, adId];

    [URLString appendString:[self queryParameterForTimeZone]];
    [URLString appendString:[self queryParameterForLocation:location]];
    [URLString appendString:[self queryParameterForDNT]];
//    [URLString appendString:[self queryParameterForCarrierName]];
//    [URLString appendString:[self queryParameterForMobileNetworkCode]];
//    [URLString appendString:[self queryParameterForMobileCountryCode]];
    [URLString appendString:[self queryParameterForDeviceScale]];
    [URLString appendString:[self queryParameterForDeviceSystemName]];
    [URLString appendString:[self queryParameterForDeviceSystemVersion]];
    [URLString appendString:[self queryParameterForDeviceMake]];
    [URLString appendString:[self queryParameterForDeviceModel]];
    [URLString appendString:[self queryParameterForDeviceName]];
    [URLString appendString:[self queryParameterForBundleId]];
    [URLString appendString:[self queryParameterForLandscapeWidth]];
    [URLString appendString:[self queryParameterForLandscapeHeight]];
    [URLString appendString:[self queryParameterForForPreload:preload]];
    [URLString appendString:[self queryParameterForForPlatform:platform]];
    if(keyWordsArr != nil)
        [URLString appendString:[self queryParameterForKeyWords:keyWordsArr]];
    if(pubKeys != nil)
        [URLString appendString:[self queryParameterForPubKeys:pubKeys]];
    if(userId != nil && userId.length > 0)
        [URLString appendString:[self queryParameterForUserID:userId]];
    
    NSString* encodedString = [self encodeURL:URLString];
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"NSURL+ASUtils, initWithPlacementID:location:keyWords:andEnv:andPubKeys: - URL String: %@", encodedString]];
    return [self initWithStringSafely:encodedString];
}

+ (instancetype)URLWithAddress:(NSString *)address {
    return [[[self class] alloc] initWithAddress:address];
}


- (instancetype)initWithAddress:(NSString *)address {
    return [self initWithStringSafely:address];
}

- (instancetype)initWithStringSafely:(NSString*)encodedString{
    @try {
        if(encodedString != nil) {
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s]" options:NSRegularExpressionCaseInsensitive error:nil];
            NSString* url = [regex stringByReplacingMatchesInString:encodedString options:0 range:NSMakeRange(0, [encodedString length]) withTemplate:@""];
            return [self initWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
    }@catch(NSException * ex) { }
    return nil;
}

#pragma mark - Environment Helpers

- (NSString *)baseURLForEnv:(ASEnvironmentType)envType {
    NSString *baseURL = nil;
    switch(envType) {
        case kASEnvProduction:
            baseURL = kASProductionBaseURL;
            break;
        case kASEnvStaging:
            baseURL = kASStagingBaseURL;
            break;
        case kASEnvDevelopment:
            baseURL = kASDevBaseURL;
            break;
    }
    
    return baseURL;
}

//- (void)displayOffProductionWarning{
//    NSString *message = @"WARNING: NOT USING PRODUCTION URL";
//    [ASAlertManager showAlertWithMessage:message forTimeInt:2];
//}

#pragma mark - Query Paramater Helpers

- (NSString *)queryParameterForTimeZone
{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
    });
    
    [formatter setDateFormat:@"Z"];
    NSDate *today = [NSDate date];
    return [NSString stringWithFormat:@"&z=%@", [formatter stringFromDate:today]];
}

- (NSString *)queryParameterForLocation:(CLLocation*)location
{
    NSString *gpsLat, *gpsLong;
    if (location != nil) {
        gpsLat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
        gpsLong = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    } else {
        gpsLat = @"no-lat";
        gpsLong = @"no-long";
    }
    
    #if kUsePredefinedLocationString
    gpsLat = kPredefinedLocationLatString;
    gpsLong = kPredefinedLocationLongString;
    #endif
    
    NSString *locationResult = [NSString stringWithFormat:@"&lat=%@&long=%@", gpsLat, gpsLong];
    [ASSDKLogger logInstance].coordLat = gpsLat;
    [ASSDKLogger logInstance].coordLong = gpsLong;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"NSURL+ASUtils, queryParameterForLocation: - location, lat: %@, long: %@", gpsLat, gpsLong]];
    
    return locationResult;
}

- (NSString *)queryParameterForDeviceMake {
    NSString *deviceMake = [@"&make=" stringByAppendingString:[kDeviceMakeApple urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return deviceMake;
}

- (NSString *)queryParameterForDeviceModel
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    if (deviceModel) { deviceModel = [NSString stringWithFormat:@"&model=%@", deviceModel];}
    else { deviceModel = @"" ;}
    return deviceModel;
}

- (NSString *)queryParameterForDeviceSystemName
{
    NSString *deviceSystemName = [[UIDevice currentDevice].systemName urlEncodeUsingEncoding:NSUTF8StringEncoding];
    if (deviceSystemName) { deviceSystemName = [NSString stringWithFormat:@"&os=%@", deviceSystemName];}
    else { deviceSystemName = @"" ;}
    return deviceSystemName;
}

- (NSString *)queryParameterForDeviceSystemVersion
{
    NSString *deviceSystemVersion = [[UIDevice currentDevice].systemVersion urlEncodeUsingEncoding:NSUTF8StringEncoding];
    if (deviceSystemVersion) { deviceSystemVersion = [NSString stringWithFormat:@"&osv=%@", deviceSystemVersion];}
    else { deviceSystemVersion = @"" ;}
    return deviceSystemVersion;
}

- (NSString *)queryParameterForDNT
{
    [ASSDKLogger logInstance].dnt = [self advertisingTrackingEnabled] ? 0 : 1;
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"NSURL+ASUtils, queryParameterForDNT - DNT: %ld", (long)[ASSDKLogger logInstance].dnt]];
    
    return [self advertisingTrackingEnabled] ? @"&dnt=0" : @"&dnt=1";
}

- (BOOL)advertisingTrackingEnabled
{
    ASIdentifierManager *adIdentManager = [ASIdentifierManager sharedManager];
    return adIdentManager.advertisingTrackingEnabled;
}

//- (NSString *)queryParameterForCarrierName
//{
//    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
//    CTCarrier *carrier = [netinfo subscriberCellularProvider];
//    NSString *carrierName = [carrier carrierName];
//    [ASSDKLogger logStatement:[NSString stringWithFormat:@"NSURL+ASUtils, queryParameterForCarrierName - Carrier Name: %@", [carrier carrierName]]];
//    [ASSDKLogger logInstance].carrier = !carrierName ? @"no carrier" : carrierName;
//    
//    NSString *carrierNameQS = [carrierName urlEncodeUsingEncoding:NSUTF8StringEncoding];
//    carrierNameQS = [NSString stringWithFormat:@"&carrier=%@", !carrierNameQS ? @"no carrier" : carrierNameQS];
//    
//    return carrierNameQS;
//}

//- (NSString *)queryParameterForMobileCountryCode
//{
//    CTTelephonyNetworkInfo *network_Info = [CTTelephonyNetworkInfo new];
//    CTCarrier *carrier = network_Info.subscriberCellularProvider;
//    NSString *mcc = carrier.mobileCountryCode;
//    [ASSDKLogger logStatement:[NSString stringWithFormat:@"NSURL+ASUtils, queryParameterForCountryCode - Mobile Country Code is: %@", carrier.mobileCountryCode]];
//    [ASSDKLogger logInstance].mcc = !mcc ? @"no mcc" : mcc;
//    
//    NSString *code = mcc;
//    if (code) { code = [NSString stringWithFormat:@"&mcc=%@", code];}
//    else { code = @"" ;}
//    
//    return code;
//}

//- (NSString *)queryParameterForMobileNetworkCode
//{
//    CTTelephonyNetworkInfo *network_Info = [CTTelephonyNetworkInfo new];
//    CTCarrier *carrier = network_Info.subscriberCellularProvider;
//    
//    NSString *code = nil;
//    if(kIS_iOS_7_OR_LATER) {
//        code = network_Info.currentRadioAccessTechnology;
//        code = [code substringFromIndex:[code rangeOfString:@"CTRadioAccessTechnology"].length];
//    } else {
//        code = carrier.mobileNetworkCode ? carrier.mobileNetworkCode : @"wifi";
//    }
//    [ASSDKLogger logStatement:[NSString stringWithFormat:@"NSURL+ASUtils, queryParameterForMobileNetworkCode - network/code: %@", code]];
//    [ASSDKLogger logInstance].network = !code ? @"no network code" : code;
//    if (code) {
//        code = [NSString stringWithFormat:@"&network=%@", code];
//    } else {
//        code = @"" ;
//    }
//    
//    return code;
//}

- (NSString *)queryParameterForDeviceName
{
    NSString *deviceName = [[[UIDevice currentDevice] name] urlEncodeUsingEncoding:NSUTF8StringEncoding];

    deviceName = [NSString stringWithFormat:@"&dn=%@", deviceName];
    
    return deviceName;
}

- (NSString *)queryParameterForDeviceScale {
    float scale = 1.0;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        [[UIScreen mainScreen] respondsToSelector:@selector(scale)])
    {
        scale = [[UIScreen mainScreen] scale];
    }
    
    NSString* scaleParamter = [NSString stringWithFormat:@"&sc=%.1f", scale];
    
    return scaleParamter;
}

- (NSString *)queryParameterForKeyWords:(NSArray *)keyWordsArr {
    NSString *keyWordParam = @"";
    
    if(keyWordsArr != nil && [keyWordsArr isKindOfClass:[NSArray class]]) {
        for(NSString *keyWord in keyWordsArr) {
            keyWordParam = [keyWordParam stringByAppendingString:[NSString stringWithFormat:@"%@,",keyWord]];
        }
        keyWordParam = (keyWordParam.length > 1) ? [keyWordParam substringToIndex:(keyWordParam.length - 1)] : @"";
        
        keyWordParam = [NSString stringWithFormat:@"&keywords=%@", [keyWordParam urlencode]];
    }
    
    return keyWordParam;
}

- (NSString *)queryParameterForPubKeys:(NSDictionary *)pubKeysObj {
    NSString *pubKeysParam = @"";
    
    if(pubKeysObj != nil && [pubKeysObj isKindOfClass:[NSDictionary class]]) {
        for(NSString *key in pubKeysObj.allKeys){
            pubKeysParam = [pubKeysParam stringByAppendingString:[NSString stringWithFormat:@"%@=%@,", key, pubKeysObj[key]]];
        }
        pubKeysParam = (pubKeysParam.length > 1) ? [pubKeysParam substringToIndex:(pubKeysParam.length - 1)] : @"";
        
        pubKeysParam = [NSString stringWithFormat:@"&publisher_keys=%@", [pubKeysParam urlencode]];
    }
    
    return pubKeysParam;
}

- (NSString *)queryParameterForBundleId {
    NSString* bundleIdentifier = [NSString stringWithFormat:@"&bundleid=%@", [[NSBundle mainBundle] bundleIdentifier]];
    
    return bundleIdentifier;
}

- (NSString *)queryParameterForUserID:(NSString *)userId {
    return [NSString stringWithFormat:@"&vc_user_id=%@", userId];
}

- (NSString *)queryParameterForLandscapeWidth {
    CGFloat lsWidth = MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    return [NSString stringWithFormat:@"&lw=%ld", (long)lsWidth];
}

- (NSString *)queryParameterForLandscapeHeight {
    CGFloat lsHeight = MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    return [NSString stringWithFormat:@"&lh=%ld", (long)lsHeight];
}

- (NSString *)queryParameterForForPreload:(BOOL)preload {
    return [NSString stringWithFormat:@"&pl=%ld", (long)(preload ? kASLoadTypePrecache : kASLoadTypeRegular)];
}

- (NSString *)queryParameterForForPlatform:(ASPlatformType)platform {
    return [NSString stringWithFormat:@"&pp=%ld", (long)platform];
}

#pragma mark - URL Encoding

// we need to put this here. Tried to put this as an NSString category.. Didn't work.
- (NSString *)encodeURL:(NSString *)urlString
{
    CFStringRef preprocessedString = CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault, (CFStringRef)urlString, CFSTR(""), kCFStringEncodingUTF8);
    CFStringRef fixedURLString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, preprocessedString, NULL, NULL, kCFStringEncodingUTF8);
    
    CFRelease(preprocessedString);
    
    return (NSString *)CFBridgingRelease(fixedURLString);
}

@end

//
//  NSData+ASUtils.m
//  AerServSDK
//
//  Created by Vasyl Savka on 2/23/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "NSData+ASUtils.h"

@implementation NSData (ASUtils)

- (NSString *)stringEncoded {
    return [[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
}

@end

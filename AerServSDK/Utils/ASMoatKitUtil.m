//
//  ASMoatKitUtil.m
//  AerServSDK
//
//  Created by Albert Zhu on 12/8/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASMoatKitUtil.h"
#import <AERMoatMobileAppKit/AERMoatMobileAppKit.h>

@interface ASMoatKitUtil()

@property (nonatomic, assign) BOOL useMoat;

@property (nonatomic, strong) Class moatTrackerClass;
@property (nonatomic, strong) Class moatVideoTrackerClass;
@property (nonatomic, strong) id moatVideoTracker;
@property (nonatomic, strong) id moatAdTracker;

@property (nonatomic, strong) NSDictionary *adIds;
@property (nonatomic, copy) NSString *appName;

@property (nonatomic, strong) dispatch_queue_t mtq;

@end

@implementation ASMoatKitUtil

+ (ASMoatKitUtil *)tracker {
    static ASMoatKitUtil *tracker;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        tracker = [ASMoatKitUtil new];
        tracker.moatTrackerClass = NSClassFromString(kMoatTrackerClass);
        tracker.moatVideoTrackerClass = NSClassFromString(kMoatVideoTrackerClass);
        tracker.appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:kMoatBundleDisplayNameKey];
        tracker.mtq = dispatch_queue_create("com.aerserv.moatTrackerQueue", DISPATCH_QUEUE_SERIAL);
    });
    
    return tracker;
}

#pragma mark - Check MOAT Methods

+ (void)setMoatBitWithASProperties:(NSDictionary *)properties {
    [self tracker].useMoat = NO;
    if(properties[kMoatEnabledKey] != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMoatKitUtil, setMoatBitWithASProperties: %@", properties[kMoatEnabledKey]]];
        [self tracker].useMoat = [properties[kMoatEnabledKey] boolValue];
    }
}

+ (BOOL)checkForUseOfMoat {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMoatKitUtil, checkForUseOfMoat - useMoat: %d", [self tracker].useMoat]];
    return [self tracker].useMoat;
}

#pragma mark - MOAT AD ID helpers

+ (NSString *)findQueryStringValueFrom:(NSString *)inputUrlStr givenQueryStringParameter:(NSString *)qsParamStr {
    
    NSString *temp = @"";
    NSString *qsParamToken = [qsParamStr stringByAppendingString:@"="];
    if([inputUrlStr containsStr:qsParamToken]) {
        NSRange range = [inputUrlStr rangeOfString:qsParamToken];
        temp = [inputUrlStr substringFromIndex:(range.location + range.length)];
        if([temp containsStr:kQueryStringAmpersandDelimTokenStr]) {
            range = [temp rangeOfString:kQueryStringAmpersandDelimTokenStr];
            temp = [temp substringToIndex:range.location];
        }
    }
    
    if(temp.length == 0 && [qsParamStr isEqualToString:kQueryStringAdvertiserKey]) {
        temp = kASDefaultBuyer;
    }
    
    return temp;
}

#pragma mark - MOAT Video Tracking Methods

+ (void)initMoatVideoTrackerWithTrackingEventURL:(NSString *)eventUrlStr {
    if(![self tracker].moatVideoTrackerClass || ![self tracker].useMoat) return;
    
    __weak ASMoatKitUtil *moatKitTracker = [self tracker];
    dispatch_async([self tracker].mtq, ^{
        if(!moatKitTracker.moatVideoTracker) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [ASSDKLogger logStatement:@"ASMoatKitUtil, initMoatVideoTrackerWithAdvertiser - initialization"];
                moatKitTracker.moatVideoTracker = [moatKitTracker.moatVideoTrackerClass trackerWithPartnerCode:kMoatAerServVideoPartnerCode];
            });
        }
        
        moatKitTracker.adIds = @{ kMoatVideoAdIdsAdvertiserKey : [ASMoatKitUtil findQueryStringValueFrom:eventUrlStr
                                                                      givenQueryStringParameter:kQueryStringAdvertiserKey],
                                  kMoatVideoAdIdsCampaignKey : kMoatAerServCampaignID,
                                  kMoatVideoAdIdsLineItemKey : [ASMoatKitUtil findQueryStringValueFrom:eventUrlStr
                                                                    givenQueryStringParameter:kQueryStringLineItemKey],
                                  kMoatVideoAdsIdsCreativeKey : [ASMoatKitUtil findQueryStringValueFrom:eventUrlStr
                                                                     givenQueryStringParameter:kQueryStringCreativeItemKey],
                                  kMoatVideoAdIdsAppKey : moatKitTracker.appName,
                                  kMoatVideoAdIdsPlacmentKey : [ASMoatKitUtil findQueryStringValueFrom:eventUrlStr
                                                                    givenQueryStringParameter:kQueryStringPlacementKey]
                                  };
    });
}

+ (void)startMoatVideoTrackerWithAVPlayer:(AVPlayer *)videoPlayer andPlayerLayer:(AVPlayerLayer *)playerLayer insideView:(UIView *)viewContainer {
    if(![self tracker].moatVideoTrackerClass || ![self tracker].moatVideoTracker) return;
    
    __weak ASMoatKitUtil *moatKitTracker = [self tracker];
    __weak AVPlayer *weakPlayer = videoPlayer;
    __weak AVPlayerLayer *weakPlayerLayer = playerLayer;
    __weak UIView *weakView = viewContainer;
    dispatch_async([self tracker].mtq, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMoatKitUtil, startMoatVideoTrackerWithAVPlayer:andPlayerLayer:insideView: - trackVideoAd invoked with adIds: %@", moatKitTracker.adIds]];
            [moatKitTracker.moatVideoTracker trackVideoAd:moatKitTracker.adIds
                                       usingAVMoviePlayer:weakPlayer
                                                withLayer:weakPlayerLayer
                                       withContainingView:weakView];
        });
    });
}

+ (void)finishMoatVideoTracker {
    if(![self tracker].moatVideoTrackerClass || ![self tracker].moatVideoTracker) return;
    
    __weak ASMoatKitUtil *moatTracker = [self tracker];
    dispatch_async([self tracker].mtq, ^{
        [ASSDKLogger logStatement:@"ASMoatKitUtil, finishMoatVideoTracker"];
        moatTracker.moatVideoTracker = nil;
        moatTracker.adIds = nil;
    });
}

#pragma mark - MOAT Display Tracking Methods

+ (NSString *)findBPFFromHTMLData:(NSData *)htmlData {
    NSString *htmlStr = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    
    NSString *urlStr = nil;
    while([htmlStr containsStr:kEventsAerServDomain] && ![urlStr containsStr:kBPFEventStr]) {
        NSRange range = [htmlStr rangeOfString:kEventsAerServDomain];
        htmlStr = [htmlStr substringFromIndex:range.location];
        NSRange doubleQuoteRange = [htmlStr rangeOfString:@"\""];
        NSRange singleQuoteRange = [htmlStr rangeOfString:@"'"];
        range = (doubleQuoteRange.location < singleQuoteRange.location) ? doubleQuoteRange : singleQuoteRange;
        
        urlStr = [htmlStr substringToIndex:range.location];
        htmlStr = [htmlStr substringFromIndex:range.location];
    }
    
    return [urlStr containsStr:kBPFEventStr] ? [urlStr lowercaseString] : nil;
}

+ (void)startMoatDisplayTrackerWithAdView:(UIView *)adView andBPF:(NSString *)bpfUrlStr {
    if(![self tracker].moatTrackerClass || !bpfUrlStr) return;
    
    __weak ASMoatKitUtil *moatKitTracker = [self tracker];
    __weak UIView *weakView = adView;
    dispatch_async([self tracker].mtq, ^{
        if(!moatKitTracker.moatAdTracker) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [ASSDKLogger logStatement:@"ASMoatKitUtil, initMoatDisplayTrackerWithBPF: - initialization"];
                moatKitTracker.moatAdTracker = [moatKitTracker.moatTrackerClass trackerWithAdView:weakView partnerCode:kMoatAerServDisplayPartnerCode];
            });
        }
        
        moatKitTracker.adIds = @{ kMoatDisplayAdIdsAdvertiserKey : [ASMoatKitUtil findQueryStringValueFrom:bpfUrlStr
                                                                        givenQueryStringParameter:kQueryStringAdvertiserKey],
                                  kMoatDisplayAdIdsCampaignKey : kMoatAerServCampaignID,
                                  kMoatDisplayAdIdsLineItemKey : [ASMoatKitUtil findQueryStringValueFrom:bpfUrlStr
                                                                      givenQueryStringParameter:kQueryStringLineItemKey],
                                  kMoatDisplayAdsIdsCreativeKey : [ASMoatKitUtil findQueryStringValueFrom:bpfUrlStr
                                                                       givenQueryStringParameter:kQueryStringCreativeItemKey],
                                  kMoatDisplayAdIdsAppKey : moatKitTracker.appName,
                                  kMoatDisplayAdIdsPlacmentKey : [ASMoatKitUtil findQueryStringValueFrom:bpfUrlStr
                                                                      givenQueryStringParameter:kQueryStringPlacementKey]
                                  };
        
        __weak ASMoatKitUtil *moatKitTracker = [self tracker];
        dispatch_async(dispatch_get_main_queue(), ^{
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMoatKitUtil, startMoatDisplayTrackerWithAdView: - trackAd inovked with adIds: %@", moatKitTracker.adIds]];
            [moatKitTracker.moatAdTracker trackAd:moatKitTracker.adIds];
        });
    });
}

+ (void)cleanUp {
    if(![self tracker].moatTrackerClass || ![self tracker].moatAdTracker) return;
    
    @try {
        [ASSDKLogger logStatement:@"ASMoatKitUtil, cleanUp - stop tracking"];
        [[self tracker].moatAdTracker stopTracking];
        [self tracker].moatAdTracker = nil;
        [self tracker].adIds = nil;
    } @catch (NSException *exception) {
        [ASSDKLogger logStatement:@"ASMoatKitUtil, cleanUp - EXCEPTION CAUGHT"];
        [ASSDKLogger onException:exception];
    }
}

@end

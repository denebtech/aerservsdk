//
//  ASVASTEvent.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/2/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVASTEvent.h"
#import "ASXMLNode.h"

@implementation ASVASTEvent

- (void)dealloc {
    [self.urls removeAllObjects];
    self.urls = nil;
}

- (void)addURL:(NSURL*)url {
    #if kShowVASTEventDebug
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTEvent, addURL: url - %@", url.absoluteString]];
    #endif
    [self addURLs:@[url]];
}

- (void)addURLs:(NSArray *)urls {
    #if kShowVASTEventDebug
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTEvent, addURLs: urls - %@", urls]];
    #endif
    if (self.urls == nil)
        self.urls = [NSMutableArray new];
    
    [self.urls addObjectsFromArray:urls];
}

- (id)copyWithZone:(NSZone *)zone {
    #if kShowVASTEventDebug
    [ASSDKLogger logStatement:@"ASVASTEvent, copyWithZone"];
    #endif
    ASVASTEvent *copy = [[[self class] alloc] init];
    
    copy.oneTime = self.oneTime;
    copy.canBeSent = self.canBeSent;
    copy.type = self.type;
    
    copy.urls = [self.urls mutableCopy];
    
    return copy;
}


@end

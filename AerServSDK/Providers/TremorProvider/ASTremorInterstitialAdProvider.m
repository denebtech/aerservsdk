//
//  ASTremorInterstitialAdProvider.m
//  AerServSDK
//
//  Created by Albert Zhu on 4/12/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASTremorInterstitialAdProvider.h"

#import "TremorVideoAd.h"

@interface ASTremorInterstitialAdProvider() <TremorVideoAdDelegate>

@property (nonatomic, strong) Class tremorVideoAdClass;
@property (nonatomic, assign) BOOL adInitialized;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;
@property (nonatomic, assign) BOOL hasVideoCompleted;
@property (nonatomic, assign) BOOL hasVideoSkip;

@end

@implementation ASTremorInterstitialAdProvider

- (void)dealloc {
    [self cleanUp];
}

- (instancetype)init {
    if(self = [super initWithAdClassName:kTremorVideoAdClassStr timeout:kCustomInterstitialTimeout]) {
        _tremorVideoAdClass = NSClassFromString(kTremorVideoAdClassStr);
    }
    
    return self;
}

- (void)cleanUp {
    [ASSDKLogger logStatement:@"ASTremorInterstitialAdProvider, cleanUp"];
    [self.tremorVideoAdClass setDelegate:nil];
    [self.tremorVideoAdClass destroy];
    self.tremorVideoAdClass = nil;
}

#pragma mark - ASCustomInterstitialAdProvider Overloaded Methods

- (void)initializePartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASTremorInterstitialAdProvider, initializePartnerAd:"];
    self.adInitialized = YES;
}

- (BOOL)hasPartnerAdInitialized {
    return self.adInitialized;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return !self.adInitialized;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    self.adLoaded = NO;
    self.adFailed = NO;
    
    NSDictionary *mediationProperties = properties[ASInterstitialParameter_Parameters];
    NSString *appID = [NSString stringWithString:mediationProperties[kTremorParamsAppIDKey]];
    [self.tremorVideoAdClass initWithAppID:appID];
    [self.tremorVideoAdClass setDelegate:self];
    [self.tremorVideoAdClass loadAd];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASTremorInterstitialAdProvider, loadPartnerAd: - appId: %@", appID]];
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    [ASSDKLogger logStatement:@"ASTremorInterstitialAdProvider, showPartnerAd:"];
    if([self.tremorVideoAdClass isAdReady])
        [self.tremorVideoAdClass showAd:rootViewController];
}

- (void)terminatePartnerAd {
    [ASSDKLogger logStatement:@"ASTremorInterstitialAdProvider, terminatePartnerAd:"];
    [self.tremorVideoAdClass stop];
    [self cleanUp];
}

#pragma mark - Tremor Video Ad Delegate

- (void)adReady:(BOOL)success {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASTremorInterstitialAdProvider, adReady: - success: %d", success]];
    if(success) {
        self.adLoaded = YES;
        [self asPartnerInterstitialAdDidLoad:[self.tremorVideoAdClass adView]];
    } else {
        self.adFailed = YES;
        NSError *err = [NSError errorWithDomain:NSStringFromClass([ASTremorInterstitialAdProvider class])
                                           code:100
                                       userInfo:@{NSLocalizedDescriptionKey : @"Tremor Ad failed to load" }];
        [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:err];
    }
}

- (void)adStart {
    [ASSDKLogger logStatement:@"ASTremorInterstitialAdProvider, adStart"];
    
    [self asPartnerInterstitialAdWillAppear];
    [self asPartnerInterstitialAdDidAppear];
}

- (void)adComplete:(BOOL)success responseCode:(NSInteger)responseCode {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASTremorInterstitialAdProvider, adComplete:responseCode: - success: %d, responseCode: %ld", success, (long)responseCode]];
    
    if(success) {
    
    }
    
    if(!self.hasVideoSkip && !self.hasVideoCompleted) {
        [self asPartnerInterstitialVideoCompleted];
    }
}

- (void)adSkipped {
    self.hasVideoSkip = YES;
    [ASSDKLogger logStatement:@"ASTremorInterstitialAdProvider, adSkipped"];
}

- (void)adClickThru {
    [ASSDKLogger logStatement:@"ASTremorInterstitialAdProvider, adClickThru"];
    [self asPartnerInterstitialAdWasTouched];
}

- (void)adImpression {
    [ASSDKLogger logStatement:@"ASTremorInterstitialAdProvider, adImpression"];
}

- (void)adVideoStart:(NSString *)videoId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASTremorInterstitialAdProvider, adVideoStart: - videoId: %@", videoId]];
}

- (void)adVideoFirstQuartile:(NSString *)videoId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASTremorInterstitialAdProvider, adVideoFirstQuartile: - videoId: %@", videoId]];
}

- (void)adVideoMidPoint:(NSString *)videoId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASTremorInterstitialAdProvider, adVideoMidPoint: - videoId: %@", videoId]];
}

- (void)adVideoThirdQuartile:(NSString *)videoId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASTremorInterstitialAdProvider, adVideoThridQuartile: - videoId: %@", videoId]];
}

- (void)adVideoComplete:(NSString *)videoId {
    self.hasVideoCompleted = YES;
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASTremorInterstitialAdProvider, adVideoComplete: - videoId: %@", videoId]];
    [self asPartnerInterstitialVideoCompleted];
}

@end

//
//  ASVungleInstance.h
//  AerServSDK
//
//  Created by Albert Zhu on 11/24/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASGlobalProviderSetup.h"

#define kVungleAppIDKey @"appId"

@interface ASVungleInstance : NSObject

@property (nonatomic, strong) Class vungleClass;
@property (nonatomic, assign) ASProviderSetupState state;

+ (ASVungleInstance *)instance;
+ (void)vungleSetupWithConfig:(NSDictionary *)adapterConfig;

@end

//
//  ASPubSettingUtils.h
//  AerServSDK
//
//  Created by Arash Sharif on 3/3/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef AerServSDK_ASPubSettingUtils_h
#define AerServSDK_ASPubSettingUtils_h

/**
 * This is a util class for publisher settings
 *
 */
@interface ASPubSettingUtils : NSObject

+ (NSNumber *)getOffsetAsPercent:(NSString *)offset videoDuration:(double)duration;

@end

#endif






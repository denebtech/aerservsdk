//
//  ASCacheUtil.m
//  AerServSDK
//
//  Created by Vasyl Savka on 6/23/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASCacheUtil.h"

@implementation ASCacheUtil


- (NSCachedURLResponse *)cachedResponseForRequest:(NSURLRequest *)request {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"--- ASCacheUtil, cachedResponseForRequest - request.URL: %@", request.URL.absoluteString]];
    return [super cachedResponseForRequest:request];
}

@end

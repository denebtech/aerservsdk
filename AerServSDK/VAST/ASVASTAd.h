//
//  ASVASTAd.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/28/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASVASTEventError.h"

#define kVastReqMediaFileAttributeDelivery @"delivery"
#define kVastReqMediaFileAttributeType @"type"
#define kVASTWrapperAdTimeout 3.0f

#define kASEventDomain @"events.aerserv.com"

#define kParseErrorMissingMediaFile @"Missing media file element"
#define kParseErrorMissingMediaFileAttributes @"Missing media file attributes delivery or type"
#define kParseErrorMissingVASTAdTagURI @"Missing VASTAdTagURI element"

extern NSString* const ASVASTAdImpressionEvent;
extern NSString* const ASVASTAdErrorEvent;

@class ASVASTEvent;
@class ASXMLNode;
@protocol ASVASTAdDelegate;

@interface ASVASTAd : NSObject

@property (readonly, nonatomic, strong) ASVASTEvent *impressionEvent;
@property (readonly, nonatomic, strong) ASVASTEventError *errorEvent;
@property (readonly, nonatomic, strong) NSMutableArray *creatives;
@property (readonly, nonatomic, assign) BOOL isWrapper;
@property (readonly, nonatomic, assign) BOOL isPreload;
@property (nonatomic, strong) ASVASTAd *seqAd;

- (instancetype)initWithXMLNode:(ASXMLNode *)node withDelegate:(id<ASVASTAdDelegate>)delegate asPreload:(BOOL)preload partOfAdPods:(BOOL)adPods;
- (void)resolveWrappedAd;

@end

@protocol ASVASTAdDelegate<NSObject>

- (void)vastAdDidLoad:(ASVASTAd *)ad;
- (void)vastAdDidFailToLoad:(ASVASTAd *)ad withError:(NSError *)error andVastErrorCode:(ASVASTErrorCode)errCode;
- (void)vastAd:(ASVASTAd *)vastAd didFireAdvertiserEventWithMessage:(NSString *)msg;

@end

//
//  ASChartboostInstance.m
//  AerServSDK
//
//  Created by Albert Zhu on 11/24/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASChartboostInstance.h"
#import "ASChartboostDelegateProxy.h"
#import "Chartboost.h"

@implementation ASChartboostInstance

+ (ASChartboostInstance *)instance {
    static ASChartboostInstance *instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [ASChartboostInstance new];
        instance.state = kASProviderSetupNone;
    });
    
    return instance;
}

+ (void)chartboostSetupWithConfig:(NSDictionary *)adapterConfig {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostInstance, chartboostSetupWithConfig: - adpaterConfig: %@", adapterConfig]];
    
    [ASChartboostInstance instance].state = kASProviderSetupStarted;
    [ASChartboostInstance instance].chartboostClass = NSClassFromString(kChartboostClassStr);
    if([ASChartboostInstance instance].chartboostClass != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                [[ASChartboostInstance instance].chartboostClass startWithAppId:adapterConfig[kChartboostAppIDKey] appSignature:adapterConfig[kChartboostSignatureKey] delegate:[ASChartboostDelegateProxy sharedProxy]];
                [ASChartboostInstance instance].state = kASProviderSetupReady;
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
                [ASChartboostInstance instance].state = kASProviderSetupNone;
            }
        });
    } else {
        [ASSDKLogger logStatement:@"ASChartboostInstance, chartboostSetupWithConfig - Could not create Charboost Class object. Will try again later during request"];
        [ASChartboostInstance instance].state = kASProviderSetupNone;
    }
}

@end

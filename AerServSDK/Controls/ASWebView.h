//
//  ASWebView.h
//  AerServSDK
//
//  Created by Albert Zhu on 3/21/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASWebView : UIWebView

@property (readwrite, nonatomic, assign) CGSize contentSize;

- (void)cleanUp;
- (void)calculateContentSize;

@end

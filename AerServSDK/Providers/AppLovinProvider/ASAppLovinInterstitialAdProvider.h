//
//  ASAppLovinInterstitialAdProvider.h
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASCustomInterstitialAdProvider.h"
#import <Foundation/Foundation.h>

@interface ASAppLovinInterstitialAdProvider : ASCustomInterstitialAdProvider

@property (nonatomic, assign) BOOL failedToReceiveAd;

- (void)cleanupAd;

@end

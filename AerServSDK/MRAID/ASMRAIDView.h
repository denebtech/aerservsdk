//
//  ASMRAIDView.h
//  AerServSDK
//
//  Created by Albert Zhu on 8/26/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kShowMRAIDJS 0
#define kShowNewPos 0

typedef NS_ENUM(NSInteger, ASMRAIDState) {
    kASMRAIDStateLoading = 0,
    kASMRAIDStateDefault,
    kASMRAIDStateExpanded,
    kASMRAIDStateResized,
    kASMRAIDStateHidden
};

typedef NS_ENUM(NSInteger, ASMRAIDPlacementType) {
    kASMRAIDPlacementInline = 0,
    kASMRAIDPlacementInterstitial
};

#define kASMRAIDPrimary @"Primary ASMRAIDView"
#define kASMRAIDFromURL @"MRAID from URL"

#define kASMRAIDGetVersion @"mraid.getVersion();"
#define kASMRAIDGetState @"mraid.getState();"
#define kASMRAIDGetPropertiesExpand @"mraid.getExpandPropertiesForBridge();"
#define kASMRAIDGetPropertiesResize @"mraid.getResizePropertiesForBridge();"
#define kASMRAIDGetPropertiesDefaultPos @"mraid.getDefaultPositionForBridge();"
#define kASMRAIDGetPropertiesOrientation @"mraid.getOrientationPropertiesForBridge();"
#define kASMRAIDGetPropertiesCurrentPos @"mraid.getCurrentPositionForBridge();"
#define kASMRAIDGetPropertiesScreenSize @"mraid.getScreenSizeForBridge();"
#define kASMRAIDGetPropertiesMaxSize @"mraid.getMaxSizeForBridge();"

#define kASMRAIDSetState @"mraid.setState(%ld);"
#define kASMRAIDSetPlacementType @"mraid.setPlacementType(%ld);"
#define kASMRAIDSetPropertiesCurrentPosition @"mraid.setCurrentPosition(%@);"
#define kASMRAIDSetPropertiesDefaultPosition @"mraid.setDefaultPosition(%@);"
#define kASMRAIDSetPropertiesScreenSize @"mraid.setScreenSize(%@);"
#define kASMRAIDSetPropertiesMaxSize @"mraid.setMaxSize(%@);"
#define kASMRAIDUseCustomClose @"mraid.useCustomClose(%@);"

#define kASMRAIDHandleEventCommand @"mraid.handleEvent('%@'"

#define kASMRAIDSetSupportWithTokens @"mraid.setSupport('%@', %@);"
#define kASMRAIDSupportsSMSFeature @"sms"
#define kASMRAIDSupportsTelFeature @"tel"
#define kASMRAIDSupportsCalFeature @"calendar"
#define kASMRAIDSupportsStorePicFeature @"storePicture"
#define kASMRAIDSupportsInlineVideoFeature @"inlineVideo"
#define kASMRAIDSupportsVPAIDFeature @"vpaid"
#define kASMRAIDSupportsTrue @"true"
#define kASMRAIDSupportsFalse @"false"

#define kASMRAIDEventReady @"ready"
#define kASMRAIDEventError @"error"
//#define kASMRAIDEventStateChange @"stateChange"
//#define kASMRAIDEventViewableChange @"viewableChange"
#define kASMRAIDEventSizeChange @"sizeChange"

#define kASMRAIDBridgeTaskScheme @"aerserv.mraid"
#define kASMRAIDLoggerTaskLog @"log"
#define kASMRAIDBridgeFinishReady @"finishready"
#define kASMRAIDBridgeTaskOpen @"open"
#define kASMRAIDBridgeTaskExpand @"expand"
#define kASMRAIDBridgeTaskExpandWithURL @"expandwithurl"
#define kASMRAIDBridgeTaskClose @"close"
#define kASMRAIDBridgeTaskRestoreDefault @"restoredefault"
#define kASMRAIDBridgeTaskUseCustomClose @"usecustomclose"
#define kASMRAIDBridgeTaskResize @"resize"
#define kASMRAIDBridgeTaskForceOrientation @"forceorientation"
#define kASMRAIDBridgeTaskStorePicture @"storepicture"
#define kASMRAIDBridgeTaskCreateCalendarEvent @"createcalendarevent"
#define kASMRAIDBridgeTaskPlayVideo @"playvideo"
#define kASMRAIDBridgeTaskVPAIDEvent @"vpaidevent"

#define kWidthKey @"width"
#define kHeightKey @"height"
#define kUseCustomClose @"useCustomClose"
#define kXKey @"x"
#define kYKey @"y"
#define kOffsetXKey @"offsetX"
#define kOffsetYKey @"offsetY"
#define kCustomClosePositionKey @"customClosePosition"
#define kAllowOffscreenKey @"allowOffscreen"
#define kAllowOrientationChangeKey @"allowOrientationChange"
#define kForceOrientationKey @"forceOrientation"

#define kOrientationPortrait @"portrait"
#define kOrientationLandscape @"landscape"
#define kOrientationNone @"none"

#define kCustomClosePosTopLeft @"top-left"
#define kCustomClosePosTopRight @"top-right"
#define kCustomClosePosCenter @"center"
#define kCustomClosePosBottomLeft @"bottom-left"
#define kCustomClosePosBottomRight @"bottom-right"
#define kCustomClosePosTopCenter @"top-center"
#define kCustomClosePosBottomCenter @"bottom-center"

#define kMRAIDVPAIDEventImpression @"AdImpression"
#define kMRAIDVPAIDEventVideoStart @"AdVideoStart"
#define kMRAIDVPAIDEventVideoComplete @"AdVideoComplete"

#define kMRAIDAnimationDuration 0.25f
#define kStatusBarHeight 20.0f

@class ASAdView;
@class ASWebView;
@class ASMRAIDCloseRegion;
@class ASMRAIDSupportsUtil;
@class ASMRAIDViewController;

@protocol ASMRAIDDelegate;

@interface ASMRAIDView : UIView

@property (nonatomic, weak) id<ASMRAIDDelegate> delegate;
@property (readonly, nonatomic, strong) ASWebView *webView;
@property (nonatomic, strong) UIViewController *rootVC;

@property (readonly, nonatomic, strong) ASMRAIDCloseRegion *closeRegion;
@property (readonly, nonatomic, assign) BOOL allowOrienationChange;
@property (readonly, nonatomic, assign) CGRect currPos;
@property (readonly, nonatomic, assign) CGSize screenSize;

@property (readonly, nonatomic, assign) BOOL didForceOrientaiton;
@property (readonly, nonatomic, assign) UIInterfaceOrientationMask preferredOrientation;
@property (nonatomic, assign) BOOL movingBetweenMraidViewAndVC;

@property (nonatomic, strong) ASMRAIDSupportsUtil *supportUtil;

@property (nonatomic, strong) ASMRAIDView *parentOfExpandedMRAIDView;
@property (nonatomic, strong) ASMRAIDViewController *mraidVC;

@property (nonatomic, strong) NSURL *vastImpressionURL;

- (instancetype)initWithFrame:(CGRect)frame withData:(NSData *)data headers:(NSDictionary *)headers forRootVC:(UIViewController *)rootVC asPlcType:(ASMRAIDPlacementType)plcType delegate:(id<ASMRAIDDelegate>)delegate;
- (instancetype)initWithFrame:(CGRect)frame withURL:(NSURL *)url forRootVC:(UIViewController *)rootVC asPlcType:(ASMRAIDPlacementType)plcType andDelegate:(id<ASMRAIDDelegate>)delegate;

- (void)mraidViewWillAttemptOrientationChange;

- (void)callReady;
- (void)finishReady;
- (void)setViewable:(BOOL)yesOrNo;

- (void)bridgeCommandProcessed;
- (void)setDefaultPositionWithFrame:(CGRect)frame;
- (void)externalVideoPlayerFromMraidBuiltWith:(AVPlayer *)player;
- (void)externalVideoPlayerFromMraidDidComplete:(AVPlayer *)player;
- (void)stopLoad;
- (void)cleanup;
- (void)setPlacementType:(ASMRAIDPlacementType)plcType;
- (void)onClose;

@end

@protocol ASMRAIDDelegate <NSObject>

- (void)repositionMRAIDView:(ASMRAIDView *)mraidView toNewFrame:(CGRect)newFramePos;
- (void)expandMRAIDView:(ASMRAIDView *)mraidView withWebView:(UIWebView *)webview;
- (void)closeMRAIDView:(ASMRAIDView *)mraidView;
- (void)loadExternalPlayerForMRAIDView:(ASMRAIDView *)mraidView withPlayer:(AVPlayer *)player;
- (void)removeExternalPlayerForMRAIDView:(ASMRAIDView *)mraidView forPlayer:(AVPlayer *)player;
- (BOOL)preloadCheckForMRAIDView:(ASMRAIDView *)mraidView;
- (void)didReceiveTouchForMRAIDView:(ASMRAIDView *)mraidView;
- (void)mraidView:(ASMRAIDView *)mraidView didFireAdvertiserEventWithMessage:(NSString *)msg;

@optional

// Interstitial
- (UIViewController *)viewControllerForMRAIDView:(ASMRAIDView *)mraidView;
- (void)attemptToRewardVC:(ASMRAIDView *)mraidView;

// Banner
- (ASAdView *)adViewForMRAIDView:(ASMRAIDView *)mraidView;
- (UIView *)contentViewForMRAIDView:(ASMRAIDView *)mraidView;
- (void)setContentViewToMRAIDView:(ASMRAIDView *)mraidView;
- (void)repositionParentViewOfMRAID:(ASMRAIDView *)mraidView toNewFrame:(CGRect)newFramePos;

@end

//
//  ASMRAIDParser.m
//  AerServSDK
//
//  Created by Albert Zhu on 8/27/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASMRAIDParser.h"

#import "ASHTMLUtils.h"
#import "NSData+ASUtils.h"
#import "NSString+ASUtils.h"

@implementation ASMRAIDParser

+ (NSDictionary *)stringForProcessedRawHTML:(NSString *)htmlStr {
    NSError *err = nil;

    #if kUseStaticTestMRAIDAd
    __block NSString *htmlStaticMRAID = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSError *err = nil;
        htmlStaticMRAID = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kStaticTestAdFileName ofType:@"html"] encoding:NSUTF8StringEncoding error:&err];
        if(err != nil) {
            NSLog(@"--- issue opening test ad");
        }
    });
    if(htmlStaticMRAID != nil) {
        htmlStr = htmlStaticMRAID;
    }
    #endif
    
    // find and remove mraid.js script tag
    NSString *regExPattern = @"<\\s*script(\\s+\\w+\\s*=\\s*(\"|')\\w+\\\\w+(\"|')\\s*)*\\s+src\\s*=\\s*(\"|')\\s*mraid\\.js\\s*(\"|')\\s*(/\\s*)*>(\\s*<\\s*/\\s*\\w*\\s*>)*\\n*";
    NSRegularExpression *regExLookup = [NSRegularExpression regularExpressionWithPattern:regExPattern
                                                                                 options:NSRegularExpressionCaseInsensitive
                                                                                   error:&err];
    if(err != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDParser, stringForProcessedRawHTML: - RegEx, mraid.js lookup, err: %@", err.localizedDescription]];
        return nil;
    }
    htmlStr = [regExLookup stringByReplacingMatchesInString:htmlStr
                                                    options:0
                                                      range:NSMakeRange(0, htmlStr.length)
                                               withTemplate:@""];
    
    // create impression URL
    NSURL *impressionURL = [self createVastImpressionURLWithHTML:htmlStr];
    
    // checking html, head, and body tags
    NSString *lowercaseHTML = [htmlStr lowercaseString];
    #if kShowMRAIDHTML
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDParser, stringForProcessedRawHTML: - htmlStr:\n%@", htmlStr]];
    #endif
    BOOL hasHTMLTag = [lowercaseHTML rangeOfString:@"<html"].location != NSNotFound;
    BOOL hasHEADTag = [lowercaseHTML rangeOfString:@"<head"].location != NSNotFound;
    BOOL hasBODYTag = [lowercaseHTML rangeOfString:@"<body"].location != NSNotFound;
    
    if(!hasHTMLTag) {
        [ASSDKLogger logStatement:@"ASMRAIDParser, stringForProcessedRawHTML: = NO HTML TAG"];
        
        // there are no html, head, or body tags; add them
        htmlStr = [NSString stringWithFormat:@"<html><head></head><body><div>\n%@\n</div></body></html>", htmlStr];
        
    } else if(!hasHEADTag) {
        [ASSDKLogger logStatement:@"ASMRAIDParser, stringForProcessedRawHTML: = NO HEAD TAG"];
        
        // there is no head tag, add it
        regExPattern = @"<\\shtml\\s*>";
        regExLookup = [NSRegularExpression regularExpressionWithPattern:regExPattern
                                                                options:NSRegularExpressionCaseInsensitive
                                                                  error:&err];
        htmlStr = [regExLookup stringByReplacingMatchesInString:htmlStr
                                                        options:0
                                                          range:NSMakeRange(0, htmlStr.length)
                                                   withTemplate:@"$0<head></head>"];
        if(err != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDParser, stringForProcessedRawHTML: - RegEx, <html> lookup, ERROR: %@", err.localizedDescription]];
            return nil;
        }
        
    }
    
    if(hasHTMLTag && !hasBODYTag) {
        [ASSDKLogger logStatement:@"ASMRAIDParser, stringForProcessedRawHTML: = HAS HTML TAG, BUT NO BODY TAG"];
        
        // if there is a html tag but no body tag, add them
        regExPattern = @"<\\s*/\\s*head\\s*>";
        regExLookup = [NSRegularExpression regularExpressionWithPattern:regExPattern
                                                                options:NSRegularExpressionCaseInsensitive
                                                                  error:&err];
        htmlStr = [regExLookup stringByReplacingMatchesInString:htmlStr
                                                        options:0
                                                          range:NSMakeRange(0, htmlStr.length)
                                                   withTemplate:@"$0<body>"];
        
        if(err != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDParser, stringForProcessedRawHTML: - RegEx, <head> lookup, ERROR: %@", err.localizedDescription]];
            return nil;
        }
        
        regExPattern = @"<\\s*/\\s*html\\s*>";
        regExLookup = [NSRegularExpression regularExpressionWithPattern:regExPattern
                                                                options:NSRegularExpressionCaseInsensitive
                                                                  error:&err];
        htmlStr = [regExLookup stringByReplacingMatchesInString:htmlStr
                                                        options:0
                                                          range:NSMakeRange(0, htmlStr.length)
                                                   withTemplate:@"</body>$0"];
        
        if(err != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDParser, stringForProcessedRawHTML: - RegEx, </hhtml> lookup, ERROR: %@", err.localizedDescription]];
            return nil;
        }
    }
    
    // adding meta and style tags
    NSString *metaTag = @"<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' />";
    NSString *styleTag = @"<style>\nbody { margin:0; padding:0; verticalAlign:middle; textAlign:center; background-color:#000000 }\n*:not(input) { -webkit-touch-callout:none; -webkit-user-select:none; -webkit-text-size-adjust:none; }\n</style>";
    
    regExPattern = @"<\\s*head\\s*>";
    regExLookup = [NSRegularExpression regularExpressionWithPattern:regExPattern
                                                            options:NSRegularExpressionCaseInsensitive
                                                              error:&err];
    htmlStr = [regExLookup stringByReplacingMatchesInString:htmlStr
                                                    options:0
                                                      range:NSMakeRange(0, htmlStr.length)
                                               withTemplate:[NSString stringWithFormat:@"$0\n%@\n%@\n%@\n", [self setAttributeOverride], metaTag, styleTag]];
    #if kShowMRAIDHTML
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDParser, stringForProcessedRawHTML - parsed htmlStr: %@", htmlStr]];
    #endif
    
    if(!impressionURL)
        impressionURL = [NSURL URLWithString:@"N/A"];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDParser, stringForProcessingRawHTML: - impressionURL: %@", impressionURL.absoluteString]];
    
    return @{ kMRAIDHTMLKey : htmlStr, kMRAIDVastImpressionURLKey : impressionURL };
}

+ (NSDictionary *)stringForProcessedRawHTMLData:(NSData *)htmlData {
    
    // translate data into html str
    NSString *htmlStr = [htmlData stringEncoded];
    return [ASMRAIDParser stringForProcessedRawHTML:htmlStr];
}

#pragma mark - Checking For MRAID

+ (BOOL)checkLoadedHTMLForMRAIDInWebView:(UIWebView *)webView {
    // class method to check for mraid tag
    NSString *htmlHead = [ASHTMLUtils htmlHeadStrInWebView:webView];
    NSString *htmlBody = [ASHTMLUtils htmlBodyStrInWebView:webView];
    #if kShowMRAIDHTML
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, checkLoadedHTMLForMRAIDInWebView:\n\n- htmlHead: %@\n\n- htmlBody: %@\n\n", htmlHead, htmlBody]];
    #endif
    return [htmlHead containsStr:kASMRAIDJSTag] || [htmlBody containsStr:kASMRAIDJSTag];
}

+ (BOOL)checkForMraidWithData:(NSData *)htmlData {
    NSString *htmlStr = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    return [htmlStr containsStr:kASMRAIDJSTag];
}

#pragma mark - VAST Impression Creation

+ (NSURL *)createVastImpressionURLWithHTML:(NSString *)htmlStr {
    [ASSDKLogger logStatement:@"ASMRAIDParser, createVastImpressionURLWithHTML"];
    
    NSURL *impUrl;
    if([htmlStr containsStr:kAerServEventsDomain]) {
        NSRange range = [htmlStr rangeOfString:kAerServEventsDomain];
        NSString *impUrlStr = [htmlStr substringFromIndex:range.location];
        
        // find end single quote or double quote
        NSRange singleRange = [impUrlStr rangeOfString:@"' "];
        NSRange doubleRange = [impUrlStr rangeOfString:@"\" "];
        if(singleRange.location < doubleRange.location && singleRange.location < impUrlStr.length) {
            range = singleRange;
        } else if(doubleRange.location < singleRange.location && doubleRange.location < impUrlStr.length) {
            range = doubleRange;
        } else {
            // can't find an ending delimiter, so set the location to be equal to the string's length
            range = NSMakeRange(impUrlStr.length,0);
        }
        
        if(range.location < impUrlStr.length) {
            impUrlStr = [impUrlStr substringToIndex:range.location];
            
            // replace banner pixel event with vast impression event
            impUrlStr = [impUrlStr stringByReplacingOccurrencesOfString:@"ev=22" withString:@"ev=18"];
            
            impUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@", impUrlStr]];
        } else {
            impUrlStr = @"N/A";
        }
    }
    
    return impUrl;
}

#pragma mark - oreitation parsing

+ (NSString *)findForceOrientationString:(NSString *)htmlStr {
    
    NSError *err = nil;
    NSString *regExPattern = @"(('|\")forceOrientation('|\")\\s*:\\s*('|\")\\w+('|\"))";
    NSRegularExpression *regExLookup = [NSRegularExpression regularExpressionWithPattern:regExPattern
                                                                                 options:NSRegularExpressionCaseInsensitive
                                                                                   error:&err];
    if(err != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDParser, findForceOrientationString - RegEx lookup, ERROR: %@", err.localizedDescription]];
        return nil;
    }
    
    NSArray *matches = [regExLookup matchesInString:htmlStr options:0 range:NSMakeRange(0, htmlStr.length)];
    
    NSString* (^pullOrientation)(NSTextCheckingResult *) = ^NSString*(NSTextCheckingResult *result) {
        NSString *orientation = [htmlStr substringWithRange:result.range];
        
        // only look at string past the :
        NSRange range = [orientation rangeOfString:@":"];
        orientation = [orientation substringFromIndex:(range.location + range.length)];
        
        // determine quote type and remove initial quote
        NSString *quoteType = @"\"";
        range = [orientation rangeOfString:quoteType];
        if(range.location == NSNotFound) {
            quoteType = @"'";
            range = [orientation rangeOfString:quoteType];
        }
        orientation = [orientation substringFromIndex:(range.location + range.length)];
        
        // remove final quote
        range = [orientation rangeOfString:quoteType];
        orientation = [orientation substringToIndex:range.location];
        
        // remove leading/trailing whtie space
        orientation = [orientation stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        return orientation;
    };
    
    NSString *foundOrienation = nil;
    if(matches.count >= 1 && matches.count <= 2) {
        foundOrienation = pullOrientation([matches firstObject]);
    } else if(matches.count > 2) {
        NSMutableDictionary *orientationObj = [NSMutableDictionary dictionary];
        
        // tally up the orientations seen
        for(NSTextCheckingResult *result in matches) {
            NSString *tempOrientaiton = pullOrientation(result);
            
            if(!orientationObj[tempOrientaiton]) {
                orientationObj[tempOrientaiton] = @1;
            } else {
                orientationObj[tempOrientaiton] = [NSNumber numberWithInt:[orientationObj[tempOrientaiton] intValue]+1];
            }
        }
        
        // determine with orientation is greater in occurrances
        for(NSString *orientationKey in orientationObj.allKeys) {
            if(!foundOrienation) {
                foundOrienation = orientationKey;
            } else {
                if([orientationObj[orientationKey] integerValue] > [orientationObj[foundOrienation] integerValue]) {
                    foundOrienation = orientationKey;
                }
            }
        }
    }
    
    return foundOrienation;
}

+ (NSString *)setAttributeOverride {
    return @"<script>var aerserv_setAttribute = Element.prototype.setAttribute; Element.prototype.setAttribute = function(att, value) {if(att == 'webkit-playsinline') {aerserv_setAttribute.call(this, 'playsinline', 'playsinline');}aerserv_setAttribute.call(this, att, value)};</script>";
}

@end

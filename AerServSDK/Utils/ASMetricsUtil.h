//
//  ASMetricsUtil.h
//  AerServSDK
//
//  Created by Vasyl Savka on 6/20/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kMetricsKey @"metrics"
#define kPreloadUrlKey @"preloadUrl"
#define kShowAttemptUrlKey @"showAttemptUrl"

@protocol ASMetricsDelegate;

@interface ASMetricsUtil : NSObject

@property (nonatomic, weak) id<ASMetricsDelegate> delegate;

- (instancetype)initWithMetricsObj:(NSDictionary *)metrics withDelegate:(id<ASMetricsDelegate>)delegate;
+ (instancetype)utilWithMetricsObj:(NSDictionary *)metrics withDelegate:(id<ASMetricsDelegate>)delegate;
- (void)firePreloadReadyEvent;
- (void)fireShowAttemptEvent;

@end

@protocol ASMetricsDelegate <NSObject>

- (void)metrics:(ASMetricsUtil *)mUtil didFireAdvertiserEventWithMessage:(NSString *)msg;

@end

//
//  ASVirtualCurrency.m
//  AerServSDK
//
//  Created by Eric Koyanagi on 3/20/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASVirtualCurrency.h"
#import "ASCommManager.h"

#import "NSString+ASUtils.h"

@interface ASVirtualCurrency()

@end



@implementation ASVirtualCurrency

+ (id)convertingToJSONObjWith:(NSData *)jsonData {
    id jsonObj;
    
    NSError *error = nil;
    if(!jsonData) {
        error = [NSError errorWithDomain:NSStringFromClass([ASVirtualCurrency class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"No data found for X-AerServ"}];
    } else {
        jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    }
    
    if(error != nil) {
        [ASSDKLogger logStatement:@"ASVirtualCurrency, convertingToJSONObjWith: - X-AerServ header has malformed JSON, could not be parsed"];
        jsonObj = nil;
    }
    
    return jsonObj;
}

+ (NSDictionary *)getVirtualCurrencyData:(NSDictionary *)headers {
    id object = [ASVirtualCurrency convertingToJSONObjWith:[headers[kXAerServKey] dataUsingEncoding:NSUTF8StringEncoding]];
    if([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *aerservHeader = object;
        
        if(aerservHeader[@"vc"] != nil) {
            [ASSDKLogger logStatement:@"ASVirtualCurrency, getVirtualCurrencyData - VC data found"];
			return aerservHeader[@"vc"];
        }
    }
	
	return nil;
}

+ (void)vcServerCallbackWithVCData:(NSDictionary *)vcData {
    NSString *serverCallbackUrlStr = vcData[kServerCallbackEventURLKey];
    
    // replacing escaped-forward-slash, with just a forward slash
    if([serverCallbackUrlStr containsStr:@"\\/"]) {
        serverCallbackUrlStr = [serverCallbackUrlStr stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    }
    
    if(serverCallbackUrlStr != nil && serverCallbackUrlStr.length > 0) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVirtualCurrency, vcServerCallbackWithVCData: - GET Request to serverCallbackUrlStr: %@", serverCallbackUrlStr]];
        [ASCommManager sendGetRequestToEndPoint:serverCallbackUrlStr forTime:kServerCallbackEventTimeout endWithCompletionBlock:nil];
    }
}

@end
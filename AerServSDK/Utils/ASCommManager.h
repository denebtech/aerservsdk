//
//  ASCommManager.h
//  AerServSDK
//
//  Created by Vasyl Savka on 1/29/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kCommShowData 0

#define kHTTPMethodGET @"GET"
#define kHTTPMethodPOST @"POST"

#define kASEventTimeout 3.0f

@class ASVASTEvent;

@interface ASCommManager : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate,
                                     NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

+ (ASCommManager *)manager;

+ (void)sendGetRequestToEndPoint:(id)epStrOrUrl forTime:(NSTimeInterval)timeInt endWithCompletionBlock:(void(^)(NSURLResponse *resp, NSData *data, NSError *err))handler;
+ (void)sendPostRequestToEndPoint:(id)epStrOrUrl withJSONData:(NSData *)jsonData forTime:(NSTimeInterval)timeInt endWithCompletionBlock:(void(^)(NSURLResponse *resp, NSData *data, NSError *err))handler;

// blindly send events to the urls or the event if the event can be called.
+ (void)sendAsynchronousRequestsForEvent:(ASVASTEvent *)event;
+ (void)sendAsynchronousRequestForURL:(NSURL *)url;

@end

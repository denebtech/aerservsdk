//
//  ASHTMLInterstitialViewController.h
//  AerServSDK
//
//  Created by Scott A Andrew on 3/25/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdViewController.h"

@class ASMRAIDView;

@interface ASHTMLInterstitialViewController : ASInterstitialAdViewController

@property (readonly, nonatomic, strong) ASMRAIDView *mraidView;

- (id)initWithData:(NSData*)data headers:(NSDictionary*)headers baseURL:(NSURL*)baseURL delegate:(id<ASInterstitialAdViewControllerDelegate>)delegate;
- (void)cancel;

@end

//
//  YumeProvider.m
//  YumeProvider
//
//  Created by Gauthier on 7/28/14.
//  Copyright (c) 2014 AerServ. All rights reserved.
//

#import "ASYumeInterstitialAdProvider.h"
#import "YuMeViewController.h"

@interface ASYumeInterstitialAdProvider()

@property (nonatomic, strong) YuMeViewController *viewController;
@property (nonatomic, assign) int attempts;

@end


@implementation ASYumeInterstitialAdProvider

- (void)requestInterstitialAdWithPropeties:(NSDictionary *)info {
    
    NSDictionary* params = info[ASInterstitialParameter_Parameters];
    NSError* error;
    
    if (params[@"domain"] == nil) {
        error = [NSError errorWithDomain:NSStringFromClass([self class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Invalid domain"}];
    }
    
    if (params[@"server"] == nil) {
        error = [NSError errorWithDomain:NSStringFromClass([self class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Invalid server"}];
    }
    
    self.viewController = [[YuMeViewController alloc] initWithDelegate:self withDomain:params[@"domain"] withServer:params[@"server"]];
    [self.viewController initYuMeSDK];
    

    [self.delegate interstitialAdProvider:self didLoadAd:nil];
}

- (void)AVPlayerCreated:(AVPlayer *)avPlayer{
    [self.delegate AVPlayerCreated:avPlayer];
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    [rootViewController presentViewController:self.viewController animated:YES completion:nil];
    [self.viewController showAdAction];
    
}

- (void)handleTimer:(NSTimer*)theTimer {
    [self.viewController showAdAction];
}


-(void) interstitialAdViewControllerViewWillAppear:(ASInterstitialAdViewController*)viewController {
    [self.delegate interstitialAdProviderWillAppear:self];
}

-(void) interstitialAdViewControllerViewDidAppear:(ASInterstitialAdViewController*)viewController {
    [self.delegate interstitialAdProviderDidAppear:self];
}

-(void) interstitialAdViewControllerViewWillDisappear:(ASInterstitialAdViewController*)viewController {
    [self.delegate interstitialAdProviderWillDisappear:self];
}
-(void) interstitialAdViewControllerViewDidDisappear:(ASInterstitialAdViewController*)viewController {
    [self.delegate interstitialAdProviderWillDisappear:self];
}
-(void) interstitialAdViewControllerViewReallyDidDisappear {
    [self.delegate interstitialAdProviderDidDisappear:self];
}


-(void) interstitialAdViewControllerCreatedAdSuccessfully:(ASInterstitialAdViewController*)controller {

}

- (void) interstitialAdViewControllerFailed:(ASInterstitialAdViewController *)controller withError:(NSError *)error {
    [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
}

-(void)interstitialAdViewControllerAdLoadingWasCancelled:(ASInterstitialAdViewController*)controller {
    
}

-(void) interstitialAdViewControllerAdFinished:(ASInterstitialAdViewController*)controller {
    self.viewController = nil;
}

-(void) interstitialAdViewControllerAdWasTouched:(ASInterstitialAdViewController *)controller {
    [self.delegate interstitialAdProviderAdWasTouched:self];
}


@end

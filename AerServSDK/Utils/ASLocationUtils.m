//
//  ASLocationUtils.m
//  AerServSDK
//
//  Created by Arash Sharif on 9/11/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASLocationUtils.h"

@interface ASLocationUtils() <CLLocationManagerDelegate>

@end



@implementation ASLocationUtils

/* locks for instanceLocation and hasBeenCalled */
NSCondition *locationLock, *hasBeenCalledLock;
/* the instance of the manager that will be getting the location */
CLLocationManager *locationManager;
/* the location that will be updated on seperate thread */
volatile CLLocation *instanceLocation;
/* the bit that determines if a location has been already found */
volatile BOOL hasBeenCalled;
/* how long to wait for the hardware to respond before giving up */
const NSTimeInterval kWAIT_TIME = 0.25f;


- (id) init {
    
    if( self = [super init]) {
        locationLock = [[NSCondition alloc] init];
        hasBeenCalled = NO;
    }
    
    return self;
}

/* get the singleton instance */
+ (ASLocationUtils *) getInstance {
    static ASLocationUtils *instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.publisherRequestsLocationServicesEnabled = NO;
    });
    
    return instance;
}

/* get the location syncronously */
- (CLLocation *) getLocation {
    [ASSDKLogger logStatement:@"ASLocationUtils, getLocation - ENTER"];
    
    //lock access to this critical section
    [locationLock lock];
    
    //if we already have a location just pass a copy of it back
    if( instanceLocation != nil) {
        
        //copy the location to return and release the lock
        CLLocation *locationCopy = [instanceLocation copy];
        [locationLock unlock];
        return locationCopy;
    }
    
    //spin a new thread to grab location
    NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(buildLocation) object:nil];
    [thread start];

    //use a spin lock to protect against spurious wakeups
    while ( instanceLocation == nil) {
      
        //wait and release lock until notified by signal or timeout
        BOOL isSignaled = [locationLock waitUntilDate:[[NSDate date] dateByAddingTimeInterval:kWAIT_TIME]];
        
        if(!isSignaled)
            break;
    
    }
    
    //copy location to return and release the lock
    CLLocation *locationCopy = [instanceLocation copy];
    
    [locationLock unlock];
        
    return locationCopy;
    

}

/* use a single instance of locaiton manager to get location */
- (void)buildLocation {
    @try {
        // lock during reads and writs to the hasBeenCalled
        [hasBeenCalledLock lock];
        
        if(hasBeenCalled) {
            
            [hasBeenCalledLock unlock];
            return;
            
        }
        
        hasBeenCalled = YES;
        
        [hasBeenCalledLock unlock];
        
        // Note:  This is a hack for AE-2486.  The location service request is popping up even when the user
        // has enabled it, but we don't know why.  Since we need to give Fuse a fix soon,
        // For now we will disable location service for Fuse.  When we have a solution for the location
        // service problem, we need to remove everything except what is in if-statement.
        //
        // Added a check for iOS 8+ since adding the NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription
        // keys in the plist will change the display name to the publisher app during the location request.
        Class fuseClass = NSClassFromString(@"FuseAerServASAdView");
        if (!fuseClass || kIS_iOS_8_OR_LATER) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASLocationUtils, buildLocation - Instantiating location services, authorizationStatus: %d", [CLLocationManager authorizationStatus]]];
            
            // callback will occur on the mainthread, so we want to create the location manager on
            // the main thread so it will be around for callback and not relesed by another thread
            dispatch_sync(dispatch_get_main_queue(), ^{
                // initialize location manager
                locationManager = [[CLLocationManager alloc] init];
                locationManager.delegate = self;
                
                // If the pub requests for location services to be enabled and the pub doesn't request authorization by itself, we will
                if(self.publisherRequestsLocationServicesEnabled &&
                   [CLLocationManager authorizationStatus] <= kCLAuthorizationStatusNotDetermined &&
                   [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                    [locationManager requestWhenInUseAuthorization];
                }
                
                if([CLLocationManager authorizationStatus] >= kCLAuthorizationStatusAuthorizedAlways) {
                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASLocationUtils, buildLocation - authorizationStatus: %d", [CLLocationManager authorizationStatus]]];
                    
                    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                    locationManager.distanceFilter = kCLDistanceFilterNone;
//                    [locationManager startUpdatingLocation];

                } else {
                    [ASSDKLogger logStatement:@"ASLocationUtils, buildLocation - Authorization Status -- kCLAuthorizationStatusDenied"];
                }
            });
        } else {
            [ASSDKLogger logStatement:@"ASLocationUtils, buildLocation - Skipping instantiation of location services for fuse"];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

/* this the locationManagers callback for when it gets a location */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    @try {
        [ASSDKLogger logStatement:@"ASLocationUtils, locationManager:didUpdateLocation: - Attempt to get lock"];
        [locationLock lock];
        
        [ASSDKLogger logStatement:@"ASLocationUtils, locationManager:didUpdateLocation: - Updating location"];
        instanceLocation = locations[0];
        [locationManager stopUpdatingLocation];
        
        [ASSDKLogger logStatement:@"ASLocationUtils, locationManager:didUpdateLocation: - Signal & Unlock"];
        [locationLock signal];
        [locationLock unlock];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASLocationUtils, locationManager:didFaileWithError: - error: %@", error.localizedDescription]];
    
    
}



@end

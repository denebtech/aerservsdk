//
//  ASChartboostInterstitialAdProvider.m
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASChartboostInterstitialAdProvider.h"
#import "Chartboost.h"
#import "ASCustomInterstitialAdProvider.h"
#import "ASChartboostInstance.h"
#import "ASChartboostDelegateProxy.h"

@interface ASChartboostInterstitialAdProvider()

@property (nonatomic, strong) Class chartboost;
@property (nonatomic, copy) NSString *cbLocation;

@end

@implementation ASChartboostInterstitialAdProvider

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASChartboostInterstitialAdProvider, dealloc"];
    _chartboost = nil;
    _cbLocation = nil;
}

- (instancetype)init {
    if (self = [super initWithAdClassName:kChartboostClassStr timeout:kCustomInterstitialTimeout]) {
        self.chartboost = [ASChartboostInstance instance].chartboostClass;
        if(!self.chartboost)
            self.chartboost = NSClassFromString(kChartboostClassStr);
        self.cbLocation = kChartboostDefaultLocation;
    }
    return self;
}

- (void)initializePartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASChartboostInterstitialAdProvider, initializePartnerAd:"];
    if(!self.chartboost || [ASChartboostInstance instance].state == kASProviderSetupNone) {
        self.chartboost = NSClassFromString(kChartboostClassStr);
        NSString *appId = [self valueInProperty:properties forKey:kASParamChartboostAppIDKey];
        NSString *appSignature = [self valueInProperty:properties forKey:kASParamChartboostAppSignatureKey];
        [self.chartboost startWithAppId:appId appSignature:appSignature delegate:[ASChartboostDelegateProxy sharedProxy]];
    }
    NSString *location = [self valueInProperty:properties forKey:kASParamChartboostLocation];
    if(location != nil && location.length > 0) {
        self.cbLocation = location;
    }
}

- (BOOL)hasPartnerAdInitialized {
    return YES;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASChartboostInterstitialAdProvider, loadPartnerAd:"];
    [ASChartboostDelegateProxy addChartboostProvider:self];
    if(!self.vcData) {
        [self.chartboost cacheInterstitial:self.cbLocation];
    } else {
        [self.chartboost cacheRewardedVideo:self.cbLocation];
    }
}

- (BOOL)hasPartnerAdLoaded {
    BOOL availableAd = (!self.vcData) ? [self.chartboost hasInterstitial:self.cbLocation] : [self.chartboost hasRewardedVideo:self.cbLocation];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostInterstitialAdProvider, hasPartnerAdLoaded - location: %@, availableAd: %d", self.cbLocation, availableAd]];
    return availableAd;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.failedToReceiveAd;
}

- (void)cancel {
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    [ASSDKLogger logStatement:@"ASChartboostInterstitialAdProvider, showPartnerAd:"];
    [ASChartboostDelegateProxy addChartboostProvider:self];
    if(!self.vcData && [self.chartboost hasInterstitial:self.cbLocation]) {
        [self.chartboost showInterstitial:self.cbLocation];
    } else if(self.vcData != nil && [self.chartboost hasRewardedVideo:self.cbLocation]) {
        [self.chartboost showRewardedVideo:self.cbLocation];
    }
}

@end

//
//  ASAdColonyDelegateProxy.m
//  AerServSDK
//
//  Created by Scott A Andrew on 4/11/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASAdColonyDelegateProxy.h"
#import "ASAdColonyInterstitialAdProvider.h"
#import "ASAdColonyInstance.h"

@interface ASAdColonyDelegateProxy()

@property (nonatomic, strong) ASAdColonyInterstitialAdProvider *provider;

@end

// Ad Colony is initialized once per application with a single delegate for state..
// We are going to use this object as a standing for our so our interstitial can be
// clared.
@implementation ASAdColonyDelegateProxy

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASAdColonyDelegateProxy, dealloc"];
    self.provider = nil;
}

+ (instancetype)sharedProxy {
    static dispatch_once_t pred;
    static ASAdColonyDelegateProxy *sharedProxy = nil;
    dispatch_once(&pred, ^{
        sharedProxy = [[ASAdColonyDelegateProxy alloc] init];
    });
    
    return sharedProxy;
}


- (instancetype)init {
    self = [super init];
    
    return self;
}

- (void)addProvider:(ASAdColonyInterstitialAdProvider*)provider {
    self.provider = provider;
}

#pragma mark - AdColonyDelegate

- (void)onAdColonyAdAvailabilityChange:(BOOL)available inZone:(NSString *)zoneID {
    @try {
        if(available) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonyDelegateProxy, onAdColonyAdAvailabilityChange:inZone: - AdColony zone %@ just became available", zoneID]];
            
            [ASAdColonyInstance addAvailableZoneID:zoneID];
            
            if(!self.provider.zoneAvailable && [[ASAdColonyInstance instance].adColonyClass zoneStatusForZone:zoneID] == ADCOLONY_ZONE_STATUS_ACTIVE) {
                [ASAdColonyInstance instance].zoneID = zoneID;
                [self.provider zoneBecameActive];
            }
            [ASAdColonyInstance instance].state = kASProviderSetupReady;
        } else {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonyDelegateProxy, onAdColonyAdAvailabilityChange:inZone: - AdColony zone %@ just became unavailable", zoneID]];
            
            [ASAdColonyInstance removeAvailabeZoneID:zoneID];
            
            if(self.provider.zoneAvailable) {
                [ASSDKLogger logStatement:@"ASAdColonyDelegateProxy, onAdColonyAdAvailabilityChange:inZone: - No ad available but there is a zone available, expire the zone and make the zone unavailable"];
                [self.provider zoneDidExpire];
                [self.provider onZoneUnavailable];
            }
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - AdColonyAdDelegate

- (void)onAdColonyAdStartedInZone:(NSString *)zoneID {
    [ASSDKLogger logStatement:@"ASAdColonyDelegateProxy, onAdColonyAdStartedInZone:"];
    @try {
        [self.provider onAdColonyAdStartedInZone:zoneID];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onAdColonyAdAttemptFinished:(BOOL)shown inZone:(NSString *)zoneID {
    if(shown) {
        [ASSDKLogger logStatement:@"ASAdColonyDelegateProxy, onAdColonyAdAttemptFinished:inZone: - AdColony ad shown, now finish"];
        @try {
            [self.provider onAdColonyAdAttemptFinished:shown inZone:zoneID];
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    } else {
        [ASSDKLogger logStatement:@"ASAdColonyDelegateProxy, onAdColonyAdAttemptFinished:inZone: - AdColony ad not shown"];
        [self.provider onAdColonyAdFailure:[NSError errorWithDomain:NSStringFromClass([ASAdColonyInterstitialAdProvider class])
                                                               code:100
                                                           userInfo:@{NSLocalizedDescriptionKey : @"AdColony ad not shown, failing over."}]];

    }
}

- (void)onAdColonyV4VCReward:(BOOL)success currencyName:(NSString *)currencyName currencyAmount:(int)amount inZone:(NSString *)zoneID {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonyDelegateProxy, onAdColonyV4VCReward:currencyName:currencyAmount:inZone: -- success: %d, currencyName: %@, currencyAmount: %d", success, currencyName, amount]];
    
    if(success) {
        @try {
            // do something to pass through the callback amount
            [self.provider onAdColonyV4VCReward:success currencyName:currencyName currencyAmount:amount inZone:zoneID];
            
        } @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }
}

@end

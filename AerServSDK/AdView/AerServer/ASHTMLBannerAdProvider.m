//
//  ASHTTPBanner.m
//  AerServSDK
//
//  Created by Scott A Andrew on 4/14/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASHTMLBannerAdProvider.h"
#import "ASHTTPAdView.h"
#import "NSURL+ASUtils.h"
#import "ASCommManager.h"

#import "ASAdView.h"

#import "ASMRAIDView.h"
#import "ASMRAIDParser.h"
#import "ASMRAIDSupportsUtil.h"
#import "ASMRAIDCloseRegion.h"

extern NSString *const UIMoviePlayerControllerDidEnterFullscreenNotification;
extern NSString *const UIMoviePlayerControllerDidExitFullscreenNotification;

@interface ASHTMLBannerAdProvider() <ASHTTPAdViewDelegate, ASMRAIDDelegate>

@property (nonatomic, strong) ASHTTPAdView* webView;
@property (nonatomic, copy) NSDictionary* properties;
@property (nonatomic, assign) BOOL delegateCalled;
@property (nonatomic, assign) BOOL movieIsPlayingFullscreen;

@property (nonatomic, strong) ASMRAIDView *mraidView;

@property (nonatomic, strong) NSData *bannerData;
@property (nonatomic, copy) NSString *bannerMIMEType;
@property (nonatomic, copy) NSString *bannerEncoding;
@property (nonatomic, strong) NSURL *bannerBaseUrl;

@end



@implementation ASHTMLBannerAdProvider

- (instancetype)init {
    if (self = [super init]) {
        self.automaticallyRefreshAds = YES;
        self.bannerRefreshTimeInterval = -1.0;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onMovieDidEnterFullScreen:)
                                                     name:UIMoviePlayerControllerDidEnterFullscreenNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onMovieDidExitFullScreen:)
                                                     name:UIMoviePlayerControllerDidExitFullscreenNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)dealloc {
    @try {
        [self killRefreshTimer];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMoviePlayerControllerDidEnterFullscreenNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMoviePlayerControllerDidExitFullscreenNotification object:nil];
        
        self.properties = nil;
        self.webView = nil;
        self.mraidView = nil;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - Helper methods for requesting ads

- (BOOL)processData:(NSData*)data headers:(NSDictionary*)headers baseURL:(NSURL*)baseURL {
    if ([data length] <= 1)
        return NO;
    
    NSString* contentType = headers[@"Content-Type"];
    
    NSArray* contentTypes = [contentType componentsSeparatedByString:@";"];
    
    // get the type and encoding stripping away any leading/trailing spaces.
    NSString* mime = [contentTypes[0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString* encoding = (contentTypes.count > 1) ? [contentTypes[1] stringByReplacingOccurrencesOfString:@" " withString:@""] : @"charset=UTF-8";
    
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.opaque = NO;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, processData:headers:baseURL: - Attempting to display HTML data, baseUrl: %@", baseURL]];
    #if kShowHTML
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"-- data:\n\n%@\n\n", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]]];
    #endif
    #if kUsePredefinedHTML
    NSData *predefinedHtmlData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kPredefinedHTML ofType:kPredefinedHTMLFileType]];
    if(predefinedHtmlData != nil) {
        data = predefinedHtmlData;
    }
    #endif
    // send the web view the data. we need the base URL.. Otherwise things don't work right. We need to pass
    // in the URL as the base URL.
    if([self.delegate bannerProviderAdAttemptIsPreload:self]) {
        self.bannerData = data;
        self.bannerMIMEType = mime;
        self.bannerEncoding = encoding;
        self.bannerBaseUrl = baseURL;
        [self.delegate bannerProvider:self didPreloadAd:self.webView];
    } else {
        if([ASMRAIDParser checkForMraidWithData:data]) {
            [self bannerAdProviderDidFindMRAIDWithData:data];
        } else {
            [self.webView loadData:data MIMEType:mime textEncodingName:encoding baseURL:baseURL];
        }
    }
    
    return YES;
}

- (BOOL)processData:(NSData*)data withResponseHeaders:(NSDictionary *)headers {
    if ([data length] <= 1)
        return NO;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, processData:withResponseHeaders: - headers: %@", headers]];
    NSString* urlString = self.properties[ASBannerParameter_Parameters][ASBannerParameter_Parameter_URL];
    NSURL* url = [NSURL URLWithAddress:urlString];
    
    if (![self processData:data headers:headers baseURL:url]) {
        [self.delegate bannerProvider:self
             didFailToLoadAdWithError:[NSError errorWithDomain:NSStringFromClass([ASHTMLBannerAdProvider class])
                                                          code:100
                                                      userInfo:@{NSLocalizedDescriptionKey : @"Failed to load ad because the data could not be processed"}]];
        return NO;
    }
    
    return YES;
}

- (void)fetchAdFromURL:(NSURL*)url {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, fetchAdFromURL: - URL: %@", url]];
    
    __weak ASHTMLBannerAdProvider* adProvider = self;
    [ASCommManager sendGetRequestToEndPoint:url forTime:self.timeInt endWithCompletionBlock:^(NSURLResponse *resp, NSData *data, NSError *err) {
        @try {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)resp;
            if ((err != nil || [httpResponse statusCode] != 200)) {
                [adProvider.delegate bannerProvider:adProvider didFailToLoadAdWithError:err];
            }
            
            NSDictionary *headers = [httpResponse allHeaderFields];
            [adProvider.delegate bannerProvider:adProvider willUpdateForFailoverWithResponseHeaders:headers];
            [adProvider processData:data withResponseHeaders:headers];
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
}

- (void)startPreloadedBannerAd {
    [ASSDKLogger logStatement:@"ASHTMLBannerAdProvider, startPreloadedBannerAd"];
    
    if(self.bannerData != nil) {
        if([ASMRAIDParser checkForMraidWithData:self.bannerData]) {
            [self bannerAdProviderDidFindMRAIDWithData:self.bannerData];
        } else {
            [self.webView loadData:self.bannerData MIMEType:self.bannerMIMEType textEncodingName:self.bannerEncoding baseURL:self.bannerBaseUrl];
        }
        self.bannerData = nil;
        self.bannerMIMEType = nil;
        self.bannerEncoding = nil;
        self.bannerBaseUrl = nil;
    }
}

- (void)bannerAdProviderDidFindMRAIDWithData:(NSData *)htmlData {
    [ASSDKLogger logStatement:@"ASHTMLBannerAdProvider, bannerAdProviderDidFindMRAIDWithData:"];
    
    // hide webview and kill refersh
    self.webView.hidden = YES;
    [self killRefreshTimer];
    
    // init and load mraid view
    self.mraidView = [[ASMRAIDView alloc] initWithFrame:self.webView.frame
                                               withData:htmlData
                                                headers:self.properties[ASBannerParameter_HTMLHeaders]
                                              forRootVC:[self.delegate viewControllerForPresentingModalView]
                                              asPlcType:kASMRAIDPlacementInline
                                               delegate:self];
    if([self.delegate bannerProviderAdAttemptIsPreload:self]) {
        [self.delegate bannerProvider:self didFinishLoadingPreloadAd:self.mraidView];
    } else {
        [self.delegate bannerProvider:self didLoadAd:self.mraidView];
    }
    
    // move close button above everything else
    ASAdView *adView = [self.delegate bannerProviderWillGetAdView:self];
    [adView bringSubviewToFront:self.mraidView.closeRegion];
}


#pragma mark - ASBannerAdProvider Public Methods

//- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation {
//    [ASSDKLogger logStatement:@"ASHTMLBannerAdProvider, rotateToOrientation"];
//    [self.webView rotateToOrientation:newOrientation];
//}

- (void)requestAdWithSize:(CGSize)size withProperties:(NSDictionary *)properties isPreload:(BOOL)preload {
    CGRect frame = CGRectZero;
    
    frame.size = size;
    
    self.webView = [[ASHTTPAdView alloc] initWithFrame:frame];
    self.webView.adDelegate = self;
       
    self.properties = properties;

    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, requestAdWithSize: - size: %@", NSStringFromCGSize(size)]];
    if (properties[ASBannerParameter_HTMLData] != nil && properties[ASBannerParameter_HTMLHeaders]) {
		
        if (![self processData:properties[ASBannerParameter_HTMLData] withResponseHeaders:properties[ASBannerParameter_HTMLHeaders]]) {
            
            NSError* error = [NSError errorWithDomain:NSStringFromClass([ASHTMLBannerAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Unable to create URL to retrieve ad from."}];
            
            [self.delegate bannerProvider:self didFailToLoadAdWithError:error];

            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, requestAdWithSize: - Failed to load ad with error:%@", error]];
		}
    }
    else if (properties[ASBannerParameter_Parameters] != nil) {
        NSString* urlString = properties[ASBannerParameter_Parameters][ASBannerParameter_Parameter_URL];
        
        NSURL* url = [NSURL URLWithAddress:urlString];
        
        if (url == nil) {
            NSError* error = [NSError errorWithDomain:NSStringFromClass([ASHTMLBannerAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Unable to create URL to retrieve ad from."}];
            
            [self.delegate bannerProvider:self didFailToLoadAdWithError:error];
            
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, requestAdWithSize: - Failed to create banner URL: %@.", urlString]];
        }
        else {
            [self fetchAdFromURL:url];
        }
    }
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASHTMLBannerAdProvider, cancel"];
    @try {
        [super cancel];
        [self.webView cleanUp];
        self.webView = nil;
        [self.mraidView cleanup];
        self.mraidView = nil;
        [self killRefreshTimer];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - ASHTTPAdViewDelegate

- (void)HTTPAdViewFinsihedLoading:(ASHTTPAdView *)adView {
    self.webView.frame = CGRectMake(0,0,1,1);
    
    CGSize size = self.webView.contentSize;
    CGFloat height = size.height;
    CGFloat width = size.width;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, HTTPAdViewFinishedLoading: - Web banner's size is (%f, %f)", width, height]];
    
    self.webView.frame = CGRectMake(0,0, width, height);
    
    if([self.delegate bannerProviderAdAttemptIsPreload:self]) {
        [self.delegate bannerProvider:self didFinishLoadingPreloadAd:self.webView];
    } else {
        [self.delegate bannerProvider:self didLoadAd:self.webView];
    }
    
    [self scheduleRefresh];
    
    self.delegateCalled = YES;
}

- (void)HTTPAdViewFailedLoading:(ASHTTPAdView *)adView withError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, HTTPAdViewFailedLoading:withError: - error: %@", error.localizedDescription]];
    if (!self.delegateCalled) {
        [self.delegate bannerProvider:self didFailToLoadAdWithError:error];
        self.delegateCalled = YES;
    }
    else {
        [self scheduleRefresh];
    }
}

- (BOOL)HTTPAdViewWasClicked:(ASHTTPAdView *)adView request:(NSURLRequest *)request {
    if (self.movieIsPlayingFullscreen) {
        [ASSDKLogger logStatement:@"ASHTMLBannerAdProvider, HTTPAdViewWasClicked:request: - Load in current view"];
        return YES;
    }
    [ASSDKLogger logStatement:@"ASHTMLBannerAdProvider, HTTPAdViewWasClicked:request: - Stop load in current view"];
    
    [self.delegate bannerProviderAdWasClicked:self];
    [self resolveURLRequest:request];
    
    return NO;
}

- (void)HTTPAdViewDidFireBPF:(ASHTTPAdView *)adView {
    [self.delegate bannerProvider:self didFireAdvertiserEventWithMessage:@"Banner Provider Fires Event Banner Pixel"];
}

- (void)resolveURLRequest:(NSURLRequest *)request {
    [[UIApplication sharedApplication] openURL:request.URL];
}

#pragma mark - Movie Player Notifcations

- (void)onMovieDidEnterFullScreen:(NSNotification *)notification {
    self.movieIsPlayingFullscreen = YES;
}

- (void)onMovieDidExitFullScreen:(NSNotification *)notification {
    self.movieIsPlayingFullscreen = NO;
}

#pragma mark - ASMRAIDViewDelegate Methods

- (void)repositionMRAIDView:(ASMRAIDView *)mraidView toNewFrame:(CGRect)newFramePos {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, repositionMRAIDView:toNewFrame: - frame: %@", NSStringFromCGRect(newFramePos)]];
    
    self.mraidView.frame = newFramePos;
}

- (void)expandMRAIDView:(ASMRAIDView *)mraidView withWebView:(UIWebView *)webview {
    [ASSDKLogger logStatement:@"ASHTMLBannerAdProvider, expandMRAIDView:withWebView:"];
    
    CGRect parRect = [self.delegate viewControllerForPresentingModalView].view.frame;
    webview.frame = parRect;
    [self.mraidView bringSubviewToFront:webview];
    [self.delegate bannerProvider:self willChangeFrameTo:parRect];
}

- (void)closeMRAIDView:(ASMRAIDView *)mraidView {
    __weak ASHTMLBannerAdProvider *htmlBannerProvider = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [htmlBannerProvider.delegate bannerProviderDidShowAdCompletely:htmlBannerProvider];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            htmlBannerProvider.mraidView = nil;
        });
    });
}

- (void)loadExternalPlayerForMRAIDView:(ASMRAIDView *)mraidView withPlayer:(AVPlayer *)player {
    [mraidView.supportUtil initPlayerLayer];
    mraidView.supportUtil.mraidVideoPlayerLayer.frame = CGRectMake(0.0f, 0.0f, mraidView.screenSize.width, mraidView.screenSize.height);
    [self.delegate bannerProvider:self didLoadPlayerLayer:mraidView.supportUtil.mraidVideoPlayerLayer];
    [player play];
}

- (void)removeExternalPlayerForMRAIDView:(ASMRAIDView *)mraidView forPlayer:(AVPlayer *)player {
    [mraidView.supportUtil.mraidVideoPlayerLayer removeFromSuperlayer];
}

- (BOOL)preloadCheckForMRAIDView:(ASMRAIDView *)mraidView {
    return [self.delegate bannerProviderAdAttemptIsPreload:self];
}

- (void)didReceiveTouchForMRAIDView:(ASMRAIDView *)mraidView {
    [self.delegate bannerProviderAdWasClicked:self];
}

- (void)mraidView:(ASMRAIDView *)mraidView didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate bannerProvider:self didFireAdvertiserEventWithMessage:msg];
}

- (ASAdView *)adViewForMRAIDView:(ASMRAIDView *)mraidView {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, adViewForMRAIDView - banner.frame: %@", NSStringFromCGRect([self.delegate bannerProviderWillGetAdView:self].frame)]];
    return [self.delegate bannerProviderWillGetAdView:self];
}

- (UIView *)contentViewForMRAIDView:(ASMRAIDView *)mraidView {
    return [self.delegate bannerProviderWillGetContentView:self];
}

- (void)setContentViewToMRAIDView:(ASMRAIDView *)mraidView {
    return [self.delegate bannerProvider:self willSetContentViewToMRAIDView:mraidView];
}

- (void)repositionParentViewOfMRAID:(ASMRAIDView *)mraidView toNewFrame:(CGRect)newFramePos {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLBannerAdProvider, repositionParentViewOfMRAID:toNewFrame: - frame: %@", NSStringFromCGRect(newFramePos)]];
    
    [self.delegate bannerProvider:self willChangeFrameTo:newFramePos];
}

@end

//
//  ASFacebookBannerAdProvider.m
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//
#import "ASFacebookBannerAdProvider.h"
#import "FBAdSettings.h"
#import "FBAdSize.h"
#import "FBAdView.h"
#import "ASSDKLogger.h"

@interface ASFacebookBannerAdProvider()<FBAdViewDelegate>

@property (nonatomic, strong) id adView;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;

@end


@implementation ASFacebookBannerAdProvider

- (void)dealloc {
    _adView = nil;
}

- (instancetype)init {
    if(self = [super initWithAdClassName:@"FBAdView" timeout:6.0]) {
        self.automaticallyRefreshAds = YES;
    }
    
    return self;
}

- (void)initializePartnerAd:(NSDictionary *)properties {
}

- (BOOL)hasPartnerAdInitialized {
    return YES;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    self.adLoaded = NO;
    self.adFailed = NO;

    NSString *bannerAdSize = [self valueInProperty:properties forKey:@"FacebookBannerAdSize"];
    FBAdSize adSize;
    adSize.size.width = -1;
    adSize.size.height = 50;
    if ([bannerAdSize isEqualToString:@"BANNER_HEIGHT_50"]) {
        adSize.size.height = 50;
    } else if ([bannerAdSize isEqualToString:@"BANNER_HEIGHT_90"]) {
        adSize.size.height = 90;
    } else if ([bannerAdSize isEqualToString:@"RECTANGLE_HEIGHT_250"]) {
        adSize.size.height = 250;
    }
    
    // Init Facebook ad view
    NSString *placementId = [self valueInProperty:properties forKey:@"FacebookPlacementId"];
    Class adViewClass = NSClassFromString(@"FBAdView");
    self.adView = [[adViewClass alloc] initWithPlacementID:placementId adSize:adSize rootViewController:[self.delegate viewControllerForPresentingModalView]];
    
    // Set test device ID
    NSString *testDeviceId = [self valueInProperty:properties forKey:@"FacebookTestDeviceId"];
    if (testDeviceId) {
        [NSClassFromString(@"FBAdSettings") addTestDevice:testDeviceId];
    }
    
    [self.adView setDelegate:self];
    [self.adView loadAd];
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd { }

- (void)terminatePartnerAd {
    [self.adView disableAutoRefresh];
    self.adView = nil;
}

- (UIView *)partnerAdView {
    return self.adView;
}

#pragma mark - FBAdViewDelegate

- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASFacebookBannerAdProvider, didFailWithError: %@", [error localizedDescription]]];
    self.adFailed = YES;
    [self adFailed:[error localizedDescription]];
}

- (void)adViewDidClick:(FBAdView *)adView {
    [ASSDKLogger logStatement:@"ASFacebookBannerAdProvider, adViewDidClick"];
    [self asPartnerBannerAdWasClicked];
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView {
    [ASSDKLogger logStatement:@"ASFacebookBannerAdProvider, adViewDidFinishHandlingClick"];
}

- (void)adViewDidLoad:(FBAdView *)adView {
    [ASSDKLogger logStatement:@"ASFacebookBannerAdProvider, adViewDidLoad"];
    self.adLoaded = YES;
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
}

- (void)adViewWillLogImpression:(FBAdView *)adView {
    [ASSDKLogger logStatement:@"ASFacebookBannerAdProvider, adViewWillLogImpression"];
    [self scheduleRefresh];
}

@end

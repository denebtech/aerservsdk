//
//  ASUnityDelegateProxy.m
//  AerServSDK
//
//  Created by Albert Zhu on 8/17/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASUnityDelegateProxy.h"
#import "ASUnityInstance.h"
#import "ASUnityInterstitialAdProvider.h"

@interface ASUnityDelegateProxy ()

@end



@implementation ASUnityDelegateProxy

+ (ASUnityDelegateProxy *)sharedDelegate {
    static ASUnityDelegateProxy *sharedDelegate = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedDelegate = [ASUnityDelegateProxy new];
    });
    
    return sharedDelegate;
}

#pragma mark - UnityAdsDelegate Methods

- (void)unityAdsReady:(NSString *)placementId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityDelegateProxy, unityAdsReady: - placementId: %@", placementId]];
    [ASUnityInstance addToReadyListForPlacmentId:placementId];
    
    ASUnityInterstitialAdProvider *provider = [ASUnityInstance getUnityProviderForPlacementId:placementId];
    if(provider != nil) {
        provider.adReadyForPlacement = YES;
    }
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityDelegateProxy, unityAdsDidError:withMessage: - error: %ld, message: %@", (long)error, message]];
    NSArray *placementArray = [ASUnityInstance getProviderPlacementList];
    
    for(NSString *plc in placementArray) {
        NSInteger placementState = [[ASUnityInstance instance].unityAdsClass getPlacementState:plc];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityDelegateProxy, unityAdsDidError:withMessage: - placement: %@, state: %ld", plc, (long)placementState]];
        
        ASUnityInterstitialAdProvider *unityProvider = [ASUnityInstance getUnityProviderForPlacementId:plc];
        unityProvider.failedToReceiveAd = YES;
        [ASUnityInstance removeProviderForPlacementId:plc];
    }
    [ASUnityInstance removeAllFromReadyList];
}

- (void)unityAdsDidStart:(NSString *)placementId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityDelegateProxy, unityAdsDidStart: - placementId: %@", placementId]];
    [ASUnityInstance removeFromReadyListForPlacementId:placementId];
    
    ASUnityInterstitialAdProvider *unityProvider = [ASUnityInstance getUnityProviderForPlacementId:placementId];
    [unityProvider asPartnerInterstitialAdWillAppear];
    [unityProvider asPartnerInterstitialAdDidAppear];
}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityDelegateProxy, unityAdsDidFinish:withFinishState: - placementId: %@", placementId]];
    ASUnityInterstitialAdProvider *unityProvider = [ASUnityInstance getUnityProviderForPlacementId:placementId];
    if(state == kUnityAdsFinishStateCompleted) {
        [unityProvider asPartnerInterstitialVideoCompleted];
    }
    [ASUnityInstance removeProviderForPlacementId:placementId];
    
    [unityProvider asPartnerInterstitialAdWillDisappear];
    [unityProvider asPartnerInterstitialAdDidDisappear];
}

@end

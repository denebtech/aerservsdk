//
//  ASWebView.m
//  AerServSDK
//
//  Created by Albert Zhu on 3/21/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASWebView.h"
#import "ASMoatKitUtil.h"

@interface ASWebView()

@property (nonatomic, assign) BOOL trackerStarted;

@end

@implementation ASWebView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
 */
- (void)cleanUp {
    [ASMoatKitUtil cleanUp];
}

#pragma mark - Calculation Content Size

- (void)calculateContentSize {
    CGRect oldFrame = self.frame;
    
    self.frame = CGRectMake(0,0,1,1);
    
    // find the actual content width & height
    CGFloat width = [[self stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] floatValue];
    CGFloat height = [[self stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollHeight"] floatValue];
    
    CGSize resultingSize = CGSizeMake(width, height);
    
    self.frame = oldFrame;
    
    if (height == 1 || width ==1) {
        resultingSize.width = CGRectGetWidth(self.bounds);
        resultingSize.height = CGRectGetHeight(self.bounds);
    }
    
    self.contentSize = resultingSize;
}

#pragma mark - Overloaded Methods

- (void)loadData:(NSData *)data MIMEType:(NSString *)MIMEType textEncodingName:(NSString *)textEncodingName baseURL:(NSURL *)baseURL {
    
    // start moat tracking if enabled and has not been started
    if(!self.trackerStarted && [ASMoatKitUtil checkForUseOfMoat]) {
        [ASSDKLogger logStatement:@"ASWebView, loadData:MIMEType:textEncodingName:baseURL: - START MOAT TRACKER"];
        
        [ASMoatKitUtil startMoatDisplayTrackerWithAdView:self andBPF:[ASMoatKitUtil findBPFFromHTMLData:data]];
        self.trackerStarted = YES;
    }
    
    // load webview regularly
    [super loadData:data MIMEType:MIMEType textEncodingName:textEncodingName baseURL:baseURL];
}

@end

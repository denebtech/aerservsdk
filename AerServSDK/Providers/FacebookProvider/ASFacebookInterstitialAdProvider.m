//
//  ASFacebookInterstitialAdProvider.m
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//
#import "ASFacebookInterstitialAdProvider.h"
#import "FBAdSettings.h"
#import "FBInterstitialAd.h"

@interface ASFacebookInterstitialAdProvider()<FBInterstitialAdDelegate>

@property (nonatomic, strong) id interstitialAd;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;

@end

@implementation ASFacebookInterstitialAdProvider

- (instancetype)init {
    if (self = [super initWithAdClassName:@"FBInterstitialAd" timeout:kCustomInterstitialTimeout]) {
        // Add init code here
    }
    return self;
}

- (void)initializePartnerAd:(NSDictionary *)properties {
}

- (BOOL)hasPartnerAdInitialized {
    return YES;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    // Set test device ID
    NSString *testDeviceId = [self valueInProperty:properties forKey:@"FacebookTestDeviceId"];
    if (testDeviceId) {
        [NSClassFromString(@"FBAdSettings") addTestDevice:testDeviceId];
    }
    
    Class interstitialAdClass = NSClassFromString(@"FBInterstitialAd");
    NSString *placementId = [self valueInProperty:properties forKey:@"FacebookPlacementId"];
    self.interstitialAd = [[interstitialAdClass alloc] initWithPlacementID:placementId];
    [self.interstitialAd setDelegate:self];
    [self.interstitialAd loadAd];
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    [self.interstitialAd showAdFromRootViewController:rootViewController];
}

- (void)cancel {
    [self cleanupAd];
}

- (void)terminatePartnerAd {
    [self cleanupAd];
}

- (void)cleanupAd {
    [self.interstitialAd setDelegate:nil];
    self.interstitialAd = nil;
}

#pragma mark - FBInterstitialAdDelegate Methods

- (void)interstitialAdDidLoad:(FBInterstitialAd *)interstitialAd {
    [ASSDKLogger logStatement:@"ASFacebookInterstitialAdProvider, interstitialAdDidLoad"];
    self.adLoaded = YES;
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
}

- (void)interstitialAd:(FBInterstitialAd *)interstitialAd didFailWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASFacebookInterstitialAdProvider, didFailWithError: %@", [error localizedDescription]]];
    self.adFailed = YES;
    [self cleanupAd];
}

- (void)interstitialAdDidClick:(FBInterstitialAd *)interstitialAd {
    [ASSDKLogger logStatement:@"ASFacebookInterstitialAdProvider, interstitialAdDidClick"];
    [self asPartnerInterstitialAdWasTouched];
}

- (void)interstitialAdDidClose:(FBInterstitialAd *)interstitialAd {
    [ASSDKLogger logStatement:@"ASFacebookInterstitialAdProvider, interstitialAdDidClose"];
    [self asPartnerInterstitialAdWillDisappear];
    [self asPartnerInterstitialAdDidDisappear];
    [self cleanupAd];
}

- (void)interstitialAdWillLogImpression:(FBInterstitialAd *)interstitialAd {
    [ASSDKLogger logStatement:@"ASFacebookInterstitialAdProvider, interstitialAdWillLogImpression"];
    [self asPartnerInterstitialAdWillAppear];
    [self asPartnerInterstitialAdDidAppear];
}

@end

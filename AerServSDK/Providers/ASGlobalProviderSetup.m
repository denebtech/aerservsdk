//
//  ASGlobalProviderSetup.m
//  AerServSDK
//
//  Created by Albert Zhu on 11/23/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASGlobalProviderSetup.h"
#import "ASAdColonyInstance.h"
#import "ASVungleInstance.h"
#import "ASChartboostInstance.h"
#import "ASAppLovinInstance.h"

@implementation ASGlobalProviderSetup

+ (void)initWithAdapterConfig:(NSArray *)adapterConfigArr {
    for(NSDictionary *adapterConfig in adapterConfigArr) {
        NSString *aName = adapterConfig[kAdapterNameKey];
        
        if([aName isEqualToString:kAdapterNameAdColonyKey]) {
            [ASAdColonyInstance adColonySetupWithConfig:adapterConfig];
        } else if([aName isEqualToString:kAdapterNameVungleKey]) {
            [ASVungleInstance vungleSetupWithConfig:adapterConfig];
        } else if([aName isEqualToString:kAdapterNameChartboostKey]) {
            [ASChartboostInstance chartboostSetupWithConfig:adapterConfig];
        } else if([aName isEqualToString:kAdapterNameAppLovinKey]) {
            [ASAppLovinInstance appLovinSetupWithConfig:adapterConfig];
        }
    }
}

@end

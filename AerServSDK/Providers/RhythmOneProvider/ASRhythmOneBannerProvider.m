//
//  ASRhythmOneBannerProvider.m
//  AerServSDK
//
//  Created by Albert Zhu on 7/1/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASRhythmOneBannerProvider.h"
#import "RhythmOneAd.h"

#import "ASLocationUtils.h"

@interface ASRhythmOneBannerProvider ()

@property (nonatomic, strong) Class rhythmOneAdClass;
@property (nonatomic, strong) id rhythmOneAd;
@property (nonatomic, strong) NSDictionary *rhythmOneAdParams;

@property (nonatomic, assign) BOOL adInitialized;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;

@end

@implementation ASRhythmOneBannerProvider

- (void)dealloc {
    @try {
        [self unregisterRhythmEvents];
        [self cleanUp];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (instancetype)init {
    if(self = [super initWithAdClassName:kRhythmOneAdClassStr timeout:kCustomInterstitialTimeout]) {
        _rhythmOneAdClass = NSClassFromString(kRhythmOneAdClassStr);
        _rhythmOneAd = [_rhythmOneAdClass new];
        [self registerRhythmEvents];
    }
    
    return self;
}

- (void)cleanUp {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, cleanUp"];
    self.rhythmOneAdClass = nil;
    self.rhythmOneAd = nil;
    self.rhythmOneAdParams = nil;
}

#pragma mark - ASCustomBannerAdProvider Overloaded Methods

- (void)initializePartnerAd:(NSDictionary *)properties {
    self.adInitialized = YES;
}

- (BOOL)hasPartnerAdInitialized {
    return self.adInitialized;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return !self.adInitialized;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, loadPartnerAd:"];
    self.adLoaded = NO;
    self.adFailed = NO;
    
    NSDictionary *mediationProperties = properties[ASInterstitialParameter_Parameters];
    NSString *appId = mediationProperties[kRhythmOneAdsAppIdKey];
    NSString *placement = mediationProperties[kRhythmOneAdsPlacementKey];
    BOOL fullscreen = [mediationProperties[kRhythmOneAdsFullscreenKey] boolValue];
    BOOL skipButtonVisible = [mediationProperties[kRhythmOneAdsSkipButtonVisibleKey] boolValue];
    CLLocation *locale = [[ASLocationUtils getInstance] getLocation];
    
    self.rhythmOneAdParams = @{ kRhythmOneAdsAppIdKey : appId,
                                kRhythmOneAdsPlacementKey : placement,
                                kRhythmOneAdsFullscreenKey : [NSNumber numberWithBool:fullscreen],
                                kRhythmOneAdsSkipButtonVisibleKey : [NSNumber numberWithBool:skipButtonVisible],
                                kRhythmOneAdsLatKey : [NSNumber numberWithDouble:locale.coordinate.latitude],
                                kRhythmOneAdsLonKey : [NSNumber numberWithDouble:locale.coordinate.longitude]
                                };
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd {
    [self.rhythmOneAd startAd];
}

- (void)terminatePartnerAd {
    [self.rhythmOneAd stopAd];
    [self cleanUp];
}

- (UIView *)partnerAdView {
    return [self asBannerViewController].view; // probably need to replace this with something else
}

#pragma mark - Rhythm Event Notification Methods

- (void)registerRhythmEvents {
    NSNotificationCenter * noteCenter = [NSNotificationCenter defaultCenter];
    [noteCenter addObserver:self selector:@selector(adLoadedEventReceived:) name:RhythmEventAdLoaded object:self.rhythmOneAd];
    [noteCenter addObserver:self selector:@selector(adErrorEventReceived:) name:RhythmEventAdError object:self.rhythmOneAd];
}

- (void)unregisterRhythmEvents {
    NSNotificationCenter * noteCenter = [NSNotificationCenter defaultCenter];
    [noteCenter removeObserver:self name:RhythmEventAdLoaded object:self.rhythmOneAd];
    [noteCenter removeObserver:self name:RhythmEventAdError object:self.rhythmOneAd];
}

- (void)adLoadedEventReceived:(NSNotification *)notification {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, adLoadedEventReceived:"];
    self.adLoaded = YES;
}

- (void)adErrorEventReceived:(NSNotification *)notification {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, adErrorEventReceived:"];
    self.adFailed = YES;
}

@end

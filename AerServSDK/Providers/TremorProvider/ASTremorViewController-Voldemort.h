//
//  TremorVideoSampleViewController.h
//  TremorVideoSample
//

#import <UIKit/UIKit.h>
#import "TremorVideoAd.h"
#import "ASInterstitialAdViewController.h"
#import <Foundation/Foundation.h>
@interface ASTremorViewController : ASInterstitialAdViewController

- (ASTremorViewController *)initWithDelegate:(id)delegate withAppID:(NSString*)appID;

@end

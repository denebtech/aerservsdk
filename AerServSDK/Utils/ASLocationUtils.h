//
//  ASLocationUtils.h
//  AerServSDK
//
//  Created by Arash Sharif on 9/11/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ASLocationUtils : NSObject

@property (nonatomic, assign) BOOL publisherRequestsLocationServicesEnabled;

/* singleton instance */
+ (ASLocationUtils *) getInstance;

- (CLLocation *) getLocation;

@end

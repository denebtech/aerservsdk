//
//  ASBannerAdManager.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/14/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASBannerAdManager.h"
#import "ASBannerAdProvider.h"
#import "NSURL+ASUtils.h"
#import "NSString+ASUtils.h"
#import "ASLocationUtils.h"
#import "ASPubSettingUtils.h"
#import "ASErrorCodeManager.h"
//#import "ASHTMLBannerAdProvider.h"
#import "ASTransactionInfo.h"
//#import "ASMoatKitUtil.h"
#import "ASMetricsUtil.h"
#import "ASCommManager.h"

const NSString* ASBannerFallBackName = nil;

@interface ASBannerAdManager() <ASBannerAdProviderDelegate, ASMetricsDelegate>

// lets save our size for our fallbacks..
@property (nonatomic, assign) CGSize adSize;

@property (nonatomic, strong) ASBannerAdProvider *providerBeingAttempted;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) NSTimer *timeoutTimer;
@property (nonatomic, assign) BOOL isFallingBack;

@property (nonatomic, strong) NSMutableArray *adTypesToTry;
@property (nonatomic, strong) NSDictionary *headers;
@property (nonatomic, strong) NSMutableDictionary *properties;
@property (nonatomic, copy) NSString *plcId;
@property (nonatomic, assign) ASEnvironmentType env;
@property (nonatomic, strong) NSArray *keyWords;
@property (nonatomic, strong) NSDictionary *pubKeys;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, strong) ASErrorCodeManager *errCodeManager;
@property (nonatomic, strong) ASTransactionInfo *transactionInfo;
@property (nonatomic, assign) ASPlatformType platform;
@property (nonatomic, strong) ASMetricsUtil *metrics;

@end

@implementation ASBannerAdManager

@synthesize closeOffset;

-  (void)dealloc {
    [ASSDKLogger logStatement:@"ASBannerAdManager, dealloc"];
    [_providerBeingAttempted cancel];
    _providerBeingAttempted = nil;
    [_adTypesToTry removeAllObjects];
    _adTypesToTry = nil;
    _headers = nil;
    [_properties removeAllObjects];
    _properties = nil;
    _plcId = nil;
    _keyWords = nil;
    _pubKeys = nil;
    _userId = nil;
    _errCodeManager = nil;
    _transactionInfo = nil;
}

+ (void)initialize {
    [ASSDKLogger logStatement:@"ASBannerAdManager, initialize"];
    
    // store and retrieve events by actual classname, rather than a hardcoded string literal
    NSString *myClassName = NSStringFromClass([ASBannerAdManager class]);
    ASBannerFallBackName = [myClassName stringByReplacingOccurrencesOfString:@"AdManager" withString:@"FallBackName"];
}

- (instancetype)init {
    if (self = [super init]) { }
    
    return self;
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASBannerAdManager, cancel"];
    @try {
        self.isLoading = NO;
        [self.providerBeingAttempted cancel];
        self.providerBeingAttempted = nil;
        [self killTimeoutTimer];
        self.timeoutTimer = nil;
        self.transactionInfo = nil;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

// called from ASAdView within try/catch
- (void)fetchBannerAdForPlacementID:(NSString *)plcId inEnv:(ASEnvironmentType)env usingKeyWords:(NSArray *)keyWords andPubKeys:(NSDictionary *)pubKeys withUserID:(NSString *)userId onPlatform:(ASPlatformType)platform {
	
	static BOOL wasLastRequestPreloaded;
	
	if(self.isLoading && wasLastRequestPreloaded) {
		NSLog(@"PRELOADED AD IS LOADING");
		if (self.adTypesToTry != nil && [self.adTypesToTry count] > 0) {
            [ASSDKLogger logStatement:@"ASBannerAdManager, fetchAd - There's already an ad loading that was preload, but if there's more ad types to try, request for an ad"];
			[self.providerBeingAttempted requestAdWithSize:[self.delegate requestedSize] withProperties:self.properties isPreload:self.isPreload];
		}
		else  {
            // this shouldn't happen in preloading
            [ASSDKLogger logStatement:@"ASBannerAdManager, fetchAd - There's already an ad loading that was preload, and there are more ad types to try"];
			[self reportErrorToDelegate:[NSError errorWithDomain:NSStringFromClass([ASBannerAdManager class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Failed to load ad"}]];
		}
		
		return;
	}
	
    if (self.isLoading && !wasLastRequestPreloaded) {
        [ASSDKLogger logStatement:@"ASBannerAdManager, fetchAd - There's an ad loading which was not preloaded, return here"];
        return;
    }
    
    wasLastRequestPreloaded = self.isPreload;
    self.plcId = plcId;
    self.env = env;
    self.keyWords = keyWords;
    self.pubKeys = pubKeys;
    self.platform = platform;
    CLLocation *location = [[ASLocationUtils getInstance] getLocation];
    
    // lets create our URL.
    NSURL *url = [NSURL URLForPlacementID:self.plcId
                                    inEnv:env
                               atlocation:location
                            usingKeyWords:self.keyWords
                               andPubKeys:self.pubKeys
                               withUserID:userId
                                asPreload:self.isPreload
                               onPlatform:self.platform
                  ];
    
    self.isLoading = YES;
    self.isFallingBack = NO;
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, fetchAd - Fetching banner ad at %@", url]];
    __weak ASBannerAdManager *adManager = self;
    [ASCommManager sendGetRequestToEndPoint:url forTime:self.timeoutInt endWithCompletionBlock:^(NSURLResponse *resp, NSData *data, NSError *err) {
        if(!resp) return;
        
        @try {
            // we getting data from an HTTP server so our response should be a URL response.
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)resp;
            
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, fetchAd - Banner URL response: %d", (int)[httpResponse statusCode]]];
            
            [adManager killTimeoutTimer];
            
            // if we got an OK response we need to go ahead and copy the headers out.
            if ([httpResponse statusCode] == 200) {
                // lets grab our headers..
                adManager.headers = [httpResponse allHeaderFields];
                
                adManager.adTypesToTry = [NSMutableArray new];
                
                if ([[httpResponse MIMEType] isEqualToString:@"text/html"] && [httpResponse expectedContentLength] > 0) {
                    [ASSDKLogger logStatement:@"ASBannerAdManager, fetchAd - Banner is HTML ad"];
                    [adManager.adTypesToTry addObject:@{ASBannerFallBackName : @"ASHTML"}];
                }
                else if ([[httpResponse MIMEType] isEqualToString:@"text/xml"] || [[httpResponse MIMEType] isEqualToString:@"application/xml"]) {
                    [ASSDKLogger logStatement:@"ASBannerAdManager, fetchAd - Banner is VAST"];
                    [adManager.adTypesToTry addObject:@{ASBannerFallBackName : @"ASVAST"}];
                }
                else if ([[httpResponse MIMEType] isEqualToString:@"application/json"]) {
                    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    if (dictionary != nil) {
                        [adManager processMediationJSON:dictionary];
                    }
                }
                
                // there are headers then we have our ad types to try (including the one we have
                // a body fore).
                if (adManager.headers[@"X-AerServ-Handlers"] != nil) {
                    // process our fallbacks if any.
                    [adManager processFallbacks];
                }
                
                if (adManager.headers[@"X-AerServ"] != nil) {
                    // process any additional publihser settings.
                    [adManager processPublisherSettings];
                }
                
                #if kInjectVCFreqCap // TODO: REMOVE THIS
                [adManager.adTypesToTry removeAllObjects];
                #endif
                
                // process this initial response..
                if (adManager.adTypesToTry != nil && [adManager.adTypesToTry count] > 0) {
                    
                    [adManager processEventResponse:httpResponse url:url withData:data];
                    
                } else {
                    // if we failed to create any ads we need to ahead and erport an error.
                    [adManager reportErrorToDelegate:[NSError errorWithDomain:NSStringFromClass([ASBannerAdManager class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Failed to load ad because there are no more ads to try"}]];
                }
                
                // go ahead and process the first event.
            }
            else {
                // we didn't get a sucess result.. report an error.
                [adManager reportErrorToDelegate:[NSError errorWithDomain:NSStringFromClass([ASBannerAdManager class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Failed to load ad with HTTP response"}]];
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
    
    // make to setup to timeout.
    [self scheduleTimeOutTimer];
}

// called from ASAdView within try/catch
- (void)startPreloadedBannerAd {
    [ASSDKLogger logStatement:@"ASBannerAdManager, startPreloadedBnnerAd"];
    [self.providerBeingAttempted startPreloadedBannerAd];
    [self.metrics fireShowAttemptEvent];
}

- (void)killTimeoutTimer {
    if (self.timeoutTimer != nil) {
        // if we have timeout timer, kill it
        [ASSDKLogger logStatement:@"ASBannerAdManager, killTimeoutTimer"];
        [self.timeoutTimer invalidate];
        self.timeoutTimer = nil;
    }
}

- (void)scheduleTimeOutTimer {
    
    // kill an existing timer if any.
    [self killTimeoutTimer];
    
    [ASSDKLogger logStatement:@"ASBannerAdManager, scheduleTimeOutTimer - Scheduling the request timeout"];
    
    // reset it.
    self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:self.timeoutInt
                                                         target:self
                                                       selector:@selector(onTimeOutTimer)
                                                       userInfo:nil
                                                        repeats:NO];
}

// called from ASAdView within try/catch
- (void)forceRefresh {

    [ASSDKLogger logStatement:@"ASBannerAdManager, forceRefresh - Banner is being forced to refresh"];
    
    // to force refresh we need to cancel any outstanding sessions and timers.
    [self cancel];
    
    // then fetch the ad.
    [self fetchBannerAdForPlacementID:self.plcId
                                inEnv:self.env
                        usingKeyWords:self.keyWords
                           andPubKeys:self.pubKeys
                           withUserID:self.userId
                           onPlatform:self.platform];
}

- (void)onTimeOutTimer {
    [ASSDKLogger logStatement:@"ASBannerAdManager, onTimeOutTimer - Fetching the banner timed out."];
    
    @try {
        // if we timed out cancel any outstanding requests.
        [self cancel];
        
        // get the fallback.
        [self getFallbackAd];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)processEventResponse:(NSHTTPURLResponse *)response url:(NSURL *)url withData:(NSData *)data {
    
    // lets check what we need to do. If we have no parameters we have all the data we need
    // in the response body there should be an apporiate constructor that takes data+url. If
    // we have parameters we take the right action. If parmeter is a url then then we need to
    // fire off another sessiona and leave the event on the stack. We will pop it off the stack
    // when we get a reponse back.
    if (response.statusCode == 200) {
        [ASSDKLogger logStatement:@"ASBannerAdManager, processEventResponse:url:withData: - Good response, parse the response and attempt the associated provider"];
        
        // we are creating UI get back to the main thread.. It will be up to each
        // controller to spawn when parsing data..
        __weak ASBannerAdManager *adManager = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                // This class (ASBannerAdManager) and ASBannerFallBackName may both have been modified
                // with a prefix (e.g. "FuseAerServ"), now we need to get complicated in order to turn:
                //     "$(prefix)ASBannerAdManager"
                // into:
                //     "$(prefix)$(eventType)BannerAdProvider"
                
                NSDictionary *event = adManager.adTypesToTry[0];
                NSString *eventType = event[ASBannerFallBackName];
                NSDictionary *eventParameters = event[ASBannerParameter_Parameters];
                
                // first, turn "$(prefix)ASBannerFallBackName" into "$(prefix)ASBannerAdProvider"
                NSString *className = [ASBannerFallBackName stringByReplacingOccurrencesOfString:@"FallBackName" withString:@"AdProvider"];
                
                // then, replace substring "ASBanner" with "$(eventType)Banner"
                className = [className stringByReplacingOccurrencesOfString:@"ASBanner" withString:[eventType stringByAppendingString:@"Banner"]];
                
                // fun with runtime.. Try to cget our class name.
                Class bannerClass = NSClassFromString(className);
                
                NSMutableDictionary *properties = [NSMutableDictionary new];
                properties[ASBannerParameter_HTMLHeaders] = [response allHeaderFields];
                properties[ASBannerParameter_HTMLData] = data;
                
                
                NSMutableDictionary *parameters  = [NSMutableDictionary new];
                parameters[ASBannerParameter_Parameter_URL] = [url absoluteString];
                if (eventParameters != nil)
                    [parameters addEntriesFromDictionary:eventParameters];
                
                // we need to pass in the source URL so make it look like a paramater.
                properties[ASBannerParameter_Parameters] = parameters;
                adManager.properties = properties;
                
                if (bannerClass != nil) {
                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, processEventResponse:url:withData: - Attempting to load banner ad class %@", className]];
                    adManager.providerBeingAttempted = [[bannerClass alloc] init];
                    adManager.providerBeingAttempted.delegate = adManager;
                    adManager.providerBeingAttempted.parameters = parameters;
                    adManager.providerBeingAttempted.timeInt = adManager.timeoutInt;
                    adManager.providerBeingAttempted.bannerRefreshTimeInterval = adManager.bannerRefershTimeInterval;
                    [adManager reportLoadAttemptForProvider];
                    
                    [adManager.providerBeingAttempted requestAdWithSize:[adManager.delegate requestedSize] withProperties:properties isPreload:self.isPreload];
                }
                else {
                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, processEventResponse:url:withData: - Associated provider, %@, could not be created so fallback", bannerClass]];
                    [adManager getFallbackAd];
                }
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        });
        
    }
    else {
        // we failed we need to fallback.
        [ASSDKLogger logStatement:@"ASBannerAdManager, processEventResponse:url:withData: - Bad HTTP Response, fallback"];
        [self getFallbackAd];
    }
    
}

- (void)stopAutomaticallyRefreshingAds {
    [self.providerBeingAttempted stopAutomaticallyRefreshingAds];
}

- (void)startAutomaticallyRefreshingAds {
    [self.providerBeingAttempted startAutomaticallyRefreshingAds];
}


- (void)processMediationJSON:(NSDictionary *)dictionary {
    [ASSDKLogger logStatement:@"ASBannerAdManager, processMediationJSON:"];
    
    // we should have an object with some children. Which means the top level should
    // have one key we assume that.
    NSString* serviceName = [dictionary allKeys][0];
    NSMutableDictionary *adToAttempt = [NSMutableDictionary new];
    
    adToAttempt[ASBannerFallBackName] = serviceName;
    adToAttempt[ASBannerParameter_Parameters] = [dictionary allValues][0];
    
    [self.adTypesToTry addObject:adToAttempt];
}

- (void)processFallbacks {
    if (self.headers[@"X-AerServ-Handlers"] != nil) {
        [ASSDKLogger logStatement:@"ASBannerAdManager, processFallbacks - Processing possible fallbacks"];
        
        // our header is a JSON object. So create a mutable copy.
        NSMutableDictionary* dictionary = [NSMutableDictionary new];
        __block NSArray* fallbacks = nil;
        __weak ASBannerAdManager* adManager = self;
        [self.headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([key isEqualToString:@"X-AerServ-Handlers"]) {
                fallbacks = [adManager.headers[@"X-AerServ-Handlers"] JSONObjectValue];
            }
            else if ([key hasPrefix:@"X-AerServ-"]) {
                
                // our headers are arrays.. So turn it mutable so we can
                // rip the parmeter off as processed.
                dictionary[key] = [[obj JSONObjectValue] mutableCopy];
            }
        }];
        
        // now that we are parsed letc check do our association.
        for (NSString* fallback in fallbacks) {
            NSString* parameterType = [@"X-AerServ-" stringByAppendingString:fallback];
            NSMutableDictionary* fallbackEntry = [NSMutableDictionary new];
            
            fallbackEntry[ASBannerFallBackName] = fallback;
            
            if (dictionary[parameterType] != nil) {
                if ([dictionary[parameterType] count] != 0) {
                    if ([dictionary[parameterType] isKindOfClass:[NSDictionary class]]) {
                        fallbackEntry[ASBannerParameter_Parameters] = dictionary[parameterType];
                    }
                    else {
                        fallbackEntry[ASBannerParameter_Parameters] = dictionary[parameterType][0];
                        [dictionary[parameterType] removeObjectAtIndex:0];
                    }
                }
            }
            
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, processFallbacks - Adding fallback: %@", fallbackEntry]];
            [self.adTypesToTry addObject:fallbackEntry];
        }
    }
}

- (void)processPublisherSettings {
    NSError *err = nil;
    id obj = [NSJSONSerialization JSONObjectWithData:[self.headers[kXAerServHeaderKey] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&err];
    
    if(err != nil) {
        [ASSDKLogger logStatement:@"ASBannerAdManager, processPublisherSettings - X-AerServ header had malformed JSON, could not parse."];
        return;
    }
    
    if([obj isKindOfClass:[NSDictionary class]]) {
        NSDictionary *aerservHeader = obj;
        
        if(aerservHeader[kCloseOffsetKey] != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, processPublisherSettings - closeOffset value is: %@", aerservHeader[kCloseOffsetKey]]];
            self.closeOffset = aerservHeader[kCloseOffsetKey];
        }
        
        if(aerservHeader[kBannerRefreshKey] != nil) {
            id bannerRefreshVal = aerservHeader[kBannerRefreshKey];
            if([bannerRefreshVal isKindOfClass:[NSNumber class]]) {
                double bannerRefreshDouble = [bannerRefreshVal doubleValue];
                if(bannerRefreshDouble == kBannerRefreshNeverValue)
                    self.bannerRefershTimeInterval = kBannerRefreshNeverTokenValue;
                else if(bannerRefreshDouble >= kBannerRefreshMinValue)
                    self.bannerRefershTimeInterval = bannerRefreshDouble;
            }
        }
        
        if(aerservHeader[kErrorParamsKey] != nil) {
            id errorParams = aerservHeader[kErrorParamsKey];
            if([errorParams isKindOfClass:[NSString class]]) {
                self.errCodeManager = [[ASErrorCodeManager alloc] initWithParamString:errorParams];
            } else if([errorParams isKindOfClass:[NSDictionary class]]) {
                self.errCodeManager = [[ASErrorCodeManager alloc] initWithParams:aerservHeader[kErrorParamsKey]];
            }
        }
        
        if(aerservHeader[kTransactionKey] != nil) {
            self.transactionInfo = [ASTransactionInfo infoWithTransactionObj:aerservHeader[kTransactionKey]];                                                                         
        }
        
        if(aerservHeader[kMetricsKey] != nil) {
            self.metrics = [ASMetricsUtil utilWithMetricsObj:aerservHeader[kMetricsKey] withDelegate:self];
        }
        
//        [ASMoatKitUtil setMoatBitWithASProperties:aerservHeader];
    }
}

- (void)reportErrorToDelegate:(NSError*)error {
    self.isLoading = NO;
    
    // check for error code
    if(self.errCodeManager != nil)
        error = [self.errCodeManager checkForErrorCodeToSwitchError:error];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, reportErrorToDelegate - Could not fetch the initial banner ad with error: %@", error.localizedDescription]];
    
    __weak ASBannerAdManager* adManager = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            [adManager.delegate bannerAdManager:adManager didFailToFetchAdwithError:error];
            [adManager cancel];
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
}

//- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation {
//    [self.providerBeingAttempted rotateToOrientation:newOrientation];
//}
//
//
//- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation withSize:(CGSize)currSize {
//    [self.providerBeingAttempted rotateToOrientation:newOrientation withSize:currSize];
//}

// all calls should be in try/catch -- need to test the call from banner provider delegate
- (void)getFallbackAd {
    // if there is something to pop off the front queue do it.
    if ([self.adTypesToTry count] > 0)
        [self.adTypesToTry removeObjectAtIndex:0];
    
    self.isFallingBack = YES;
    
    // if we have anymore attempts try the lets make the attempt.
    if ([self.adTypesToTry count] > 0) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, getFallbackAd - Attemping fallback: %@", self.adTypesToTry[0]]];
        
        // lets try to create a controller.
        __weak ASBannerAdManager *adManager = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            if(adManager.adTypesToTry == nil || adManager.adTypesToTry.count == 0)
                return;

            @try {
                [adManager killTimeoutTimer];
                
                // get the fallback
                NSDictionary *event = adManager.adTypesToTry[0];
                NSString *eventType = event[ASBannerFallBackName];
                NSDictionary *parameters = event[ASBannerParameter_Parameters];
                
                NSMutableDictionary *properties = [NSMutableDictionary new];
                if (parameters != nil) {
                    properties[ASBannerParameter_Parameters] = parameters;
                }
                
                CLLocation *location = [[ASLocationUtils getInstance] getLocation];
                if (location != nil)
                    properties[ASBannerParameter_Location] = location;
                
                properties[ASBannerParameter_HTMLHeaders] = adManager.headers;
                
                NSString *className = [eventType stringByAppendingString:@"BannerAdProvider"];
                Class bannerClass = NSClassFromString(className);
                
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, getFallbackAd - Attempting to load banner ad class %@", className]];
                adManager.properties = properties;
                
                if (bannerClass != nil) {
                    adManager.isLoading = YES;
                    [adManager scheduleTimeOutTimer];
                    adManager.providerBeingAttempted = [[bannerClass alloc] init];
                    adManager.providerBeingAttempted.delegate  = adManager;
                    adManager.providerBeingAttempted.parameters = parameters;
                    adManager.providerBeingAttempted.timeInt = adManager.timeoutInt;
                    adManager.providerBeingAttempted.bannerRefreshTimeInterval = adManager.bannerRefershTimeInterval;

                    [adManager reportLoadAttemptForProvider];
                    [adManager.providerBeingAttempted requestAdWithSize:[adManager.delegate requestedSize] withProperties:properties isPreload:adManager.isPreload];
                }
                else {
                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, getFallbackAd - %@ doesn't exist", className]];
                    [adManager getFallbackAd];
                }
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        });
    }
    else {
        [ASSDKLogger logStatement:@"ASBannerAdManager, getFallbackAd - No ad available for this request."];
        
        // report an error if we failed to load any of our ads.
        NSDictionary *errorInfo = @{NSLocalizedDescriptionKey:@"No ad available for this request."};
        NSError *error = [NSError errorWithDomain:NSStringFromClass([ASBannerAdManager class]) code:100 userInfo:errorInfo];
        [self reportErrorToDelegate:error];
    }
}

#pragma mark - analytics reporting for fallbacks

- (void)reportLoadAttemptForProvider {
    if (self.providerBeingAttempted.parameters != nil) {
        NSString* urlString = self.providerBeingAttempted.parameters[@"AttemptURL"];
        
        if (urlString != nil) {
            NSURL *url = [NSURL URLWithString:urlString];
            
            if (url != nil) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, reportLoadAttemptForProvider - event 12 : %@", url]];
                [ASCommManager sendAsynchronousRequestForURL:url];
                [self.delegate bannerAdManager:self didFireAdvertiserEventWithMessage:@"Banner Ad Manager Fires Event Load Attempt For Provider"];
            }
        }
    }
}

- (void)reportProviderAdLoaded {
    if (self.providerBeingAttempted.parameters != nil) {
        NSString* urlString = self.providerBeingAttempted.parameters[@"ImpressionURL"];
        
        if (urlString != nil) {
            NSURL *url = [NSURL URLWithString:urlString];
            
            if (url != nil) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, reportProviderAdLoaded - event 13 : %@", url]];
                [ASCommManager sendAsynchronousRequestForURL:url];
                [self.delegate bannerAdManager:self didFireAdvertiserEventWithMessage:@"Banner Ad Manager Fires Event Ad Loaded For Provider"];
            }
        }
    }
}

- (void)reportErrorForProvider {
    if (self.providerBeingAttempted.parameters != nil) {
        NSString* urlString = self.providerBeingAttempted.parameters[@"FailedURL"];
        
        if (urlString != nil) {
            NSURL *url = [NSURL URLWithString:urlString];
            
            if (url != nil) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, reportErrorForProvider - event 14 : %@", url]];
                [ASCommManager sendAsynchronousRequestForURL:url];
                [self.delegate bannerAdManager:self didFireAdvertiserEventWithMessage:@"Banner Ad Manager Fires Error Event For Provider"];
            }
        }
    }
}

#pragma mark - ASBannerAdProviderDelegate Methods

- (UIViewController *)viewControllerForPresentingModalView{
    return [self.delegate viewControllerForPresentingModalView];
}

- (void)bannerLoadSuccessfulCleanup {
    self.isLoading = NO;
    [self killTimeoutTimer];
    
    // report ad loaded when not preload
    if(!self.isPreload)
        [self reportProviderAdLoaded];
    
    if(self.transactionInfo != nil) {
        [self.delegate bannerAdManager:self didFindAdTransactionInfo:self.transactionInfo];
    }
}

- (void)bannerProvider:(ASBannerAdProvider *)provider didLoadAd:(UIView *)ad {
    if ([provider isEqual:self.providerBeingAttempted] && self.isLoading) {
        [ASSDKLogger logStatement:@"ASBannerAdManager, bannerProvider:didLoadAd:"];
        [self.delegate bannerAdManager:self didLoadAd:ad];
        [self bannerLoadSuccessfulCleanup];
    }
}

- (void)bannerProvider:(ASBannerAdProvider *)provider didPreloadAd:(UIView *)ad {
    if([provider isEqual:self.providerBeingAttempted] && self.isLoading) {
        [ASSDKLogger logStatement:@"ASBannerAdManager, bannerProvider:didLoadAd: - As Preload"];
        [self.delegate bannerAdManager:self didPreloadAd:ad];
        [self bannerLoadSuccessfulCleanup];
        [self.metrics firePreloadReadyEvent];
    }
}

- (void)bannerProvider:(ASBannerAdProvider *)provider didFinishLoadingPreloadAd:(UIView *)ad {
    [self reportProviderAdLoaded];
    [self.delegate bannerAdManager:self didFinishLoadingPreloadAd:ad];
}

- (void)bannerProvider:(ASBannerAdProvider *)provider didFailToLoadAdWithError:(NSError *)error {
    [ASSDKLogger logStatement:@"ASBannerAdManager, didFailToLoadAdWithError - Failed to load ad. Trying fallbacks."];

    self.isLoading = NO;
    [self killTimeoutTimer];
    [self reportErrorForProvider];
    self.providerBeingAttempted = nil;
    
    if([self.adTypesToTry count] > 0)
        [self getFallbackAd];
    else {
        [self reportErrorToDelegate:error];
    }
}

- (void)bannerProviderWillBeginAction:(ASBannerAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASBannerAdManager, bannerProviderWillBeginAction:"];
    [self stopAutomaticallyRefreshingAds];
    [self.delegate bannerAdManagerWillBeginAction:self];
}

- (void)bannerProviderWillEndAction:(ASBannerAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASBannerAdManager, bannerProviderWillEndAction:"];
    [self startAutomaticallyRefreshingAds];
    [self.delegate bannerAdManagerWillEndAction:self];
}

- (void)bannerProviderWillLeaveApplication:(ASBannerAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASBannerAdManager, bannerProviderWillLeaveApplication:"];
    [self.delegate bannerAdManagerWillLeaveApplication:self];
}

- (void)bannerProviderAdChangedSize:(ASBannerAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASBannerAdManager, bannerProviderAdChangedSize:"];
    [self.delegate bannerAdManagerAdChangedSize:self];
}

- (void)bannerProviderAdWasClicked:(ASBannerAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASBannerAdManager, bannerProvideAdWasClicked:"];
    [self.delegate bannerAdManagerAdWasClicked:self];
}

- (void)bannerProviderDidShowAdCompletely:(ASBannerAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASBannerAdManager, bannerProviderDidShowAdCompletely:"];
    [self.delegate bannerAdManagerDidShowAdCompletely:self];
}

- (NSArray*)getKeyWords {
    return self.keyWords;
}

- (NSDictionary*)getPubKeys {
    return self.pubKeys;
}

- (void)AVPlayerCreated:(AVPlayer *)avPlayer {
    [self.delegate AVPlayerCreated:avPlayer];
}

- (void)bannerProvider:(ASBannerAdProvider *)adProvider willChangeFrameTo:(CGRect)newFrame {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdManager, bannerProvider:willChangeFrameTo: - frame: %@", NSStringFromCGRect(newFrame)]];
    [self.delegate bannerAdManager:self willChangeFrameTo:newFrame];
}

- (ASAdView *)bannerProviderWillGetAdView:(ASBannerAdProvider *)provider {
    return [self.delegate bannerAdManagerWillGetAdView:self];
}

- (UIView *)bannerProviderWillGetContentView:(ASBannerAdProvider *)provider {
    return [self.delegate bannerAdManagerWillGetContentView:self];
}

//- (void)bannerProvider:(ASBannerAdProvider *)provider willSetContentViewToMRAIDView:(ASMRAIDView *)mraidView {
//    [self.delegate bannerAdManager:self willSetContentViewToMRAIDView:mraidView];
//}

- (void)bannerProvider:(ASBannerAdProvider *)provider didLoadPlayerLayer:(AVPlayerLayer *)pl {
    [self.delegate bannerAdManager:self didLoadPlayerLayer:pl];
}

- (BOOL)bannerProviderAdAttemptIsPreload:(ASBannerAdProvider *)provider {
    return self.isPreload;
}

- (void)bannerProvider:(ASBannerAdProvider *)provider willShowSkip:(BOOL)show {
    [self.delegate bannerAdManager:self willShowSkip:show];
}

- (void)bannerProviderWillAttemptRefresh:(ASBannerAdProvider *)provider {
    [self cancel];
    [self.delegate bannerAdManagerWillAttemptRefresh:self];
}

- (void)bannerProvider:(ASBannerAdProvider *)provider didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate bannerAdManager:self didFireAdvertiserEventWithMessage:msg];
}

- (void)bannerProvider:(ASBannerAdProvider *)provider willUpdateForFailoverWithResponseHeaders:(NSDictionary *)headers {
    self.headers = headers;
    if(self.headers[kXAerServHeaderKey] != nil) {
        [self.transactionInfo updatedTransactionInfoWithResponseHeadersData:[self.headers[kXAerServHeaderKey] dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

#pragma mark - Metrics Delegate

- (void)metrics:(ASMetricsUtil *)mUtil didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate bannerAdManager:self didFireAdvertiserEventWithMessage:msg];
}

@end

//
//  ASRhythmOneBannerProvider.h
//  AerServSDK
//
//  Created by Albert Zhu on 7/1/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <AerServSDK/AerServSDK.h>

#define kRhythmOneAdClassStr @"RhtyhmOneAd"

#define kRhythmOneAdsAppIdKey @"appId"
#define kRhythmOneAdsPlacementKey @"placement"
#define kRhythmOneAdsFullscreenKey @"fullscreen"
#define kRhythmOneAdsSkipButtonVisibleKey @"skipButtonVisible"
#define kRhythmOneAdsLatKey @"lat"
#define kRhythmOneAdsLonKey @"lon"

@interface ASRhythmOneBannerProvider : ASCustomBannerAdProvider

@end

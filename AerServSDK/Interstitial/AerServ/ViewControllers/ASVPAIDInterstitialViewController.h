//
//  ASVPAIDInterstitialViewController.h
//  AerServSDK
//
//  Created by Arash Sharif on 4/18/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASVASTInterstitialViewController.h"
#import "ASVASTAd.h"

#define kVPAIDPalyerVersionStr @"2.1.05"
#define kVPAIDTimeout 5.0f
#define kVPAIDSlashCharLength 1
#define kVPAIDArgSepStr @"|||"
#define kVPAIDClickThruURLIndex 0
#define kVPAIDClickThruIdIndex 1
#define kVPAIDClickThruPlayerHandlesIndex 2

#define kVPAIDClosabilityZeroPercentStr @"0.0%"
#define kVPAIDClosability25PercentStr @"25.0%"
#define kVPAIDClosability50PercentStr @"50.0%"
#define kVPAIDClosability75PercentStr @"75.0%"

@interface ASVPAIDInterstitialViewController : ASVASTInterstitialViewController <UIWebViewDelegate>

@property (nonatomic, retain) UIWebView *webView;

- (instancetype)initWithVASTAd:(ASVASTAd*)VASTAd delegate:(id<ASInterstitialAdViewControllerDelegate>)delegate closeOffset:(NSString*)offset;
- (void)startAd;

@end

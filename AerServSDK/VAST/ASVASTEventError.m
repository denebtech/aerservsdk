//
//  ASVASTEventError.m
//  AerServSDK
//
//  Created by Vasyl Savka on 12/17/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASVASTEventError.h"
#import "NSString+ASUtils.h"

@implementation ASVASTEventError

- (ASVASTEventError *)replaceErrorMacroWithCode:(ASVASTErrorCode)errCode {
    for(int i=0; i<self.urls.count; i++) {
        NSURL *tempUrl = self.urls[i];
        NSString *tempStr = [tempUrl.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if([tempStr containsStr:KVASTEventErrorCodeMacro]) {
            tempStr = [tempStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            tempStr = [tempStr stringByReplacingOccurrencesOfString:KVASTEventErrorCodeMacro withString:[NSString stringWithFormat:@"%ld", (long)errCode]];
            self.urls[i] = [NSURL URLWithString:tempStr];
        }
        tempStr = nil;
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTEventError, replaceErrorMacroWithCode: - url[%d] = %@", i, self.urls[i]]];
    }
    
    return self;
}

@end

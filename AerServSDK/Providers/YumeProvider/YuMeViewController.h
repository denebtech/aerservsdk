//
//  YuMeViewController.h
//  YuMeiOSSampleApp
//
//  Created by Bharath Ramakrishnan on 4/5/13.
//  Copyright (c) 2014 YuMe Advertising Pvt. Ltd.,. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASInterstitialAdViewController.h"

@interface YuMeViewController : ASInterstitialAdViewController


@property (nonatomic, retain) UIView *adView;
@property (nonatomic, strong) NSString* domain;
@property (nonatomic, strong) NSString* server;
@property (nonatomic, strong) id delegate;

- (YuMeViewController *)initWithDelegate:(id) delegate withDomain:(NSString*) domain withServer:(NSString*) server;
/*
 * Event handler for showAd button.
 */
- (void)showAdAction;

/*
 * Initializes the YuMe SDK.
 */
- (BOOL)initYuMeSDK;

/*
 * De-Initializes the YuMe SDK.
 */
- (void)deInitYuMeSDK;

/*
 * Displays an Ad that was pre-fetched.
 */
- (void)displayAd;

/*
 * Creates a UIView for displaying ad.
 */
- (void)createAdView;

/*
 * Removes the ad view.
 */
- (void)removeAdView;

/*
 * Displays an alert message.
 * @param msg The message to be displayed.
 * @param caption The 'Cancel' button title.
 */
- (void)displayBox:(NSString *)msg withButtonText:(NSString *)caption;

/*
 * Gets the error message from NSError object.
 * @param pError A pointer to an NSError object.
 */
- (NSString *)getErrDesc:(NSError *)pError;

@end //@interface YuMeViewController

//
//  ASInterstitialAdFetcher.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/24/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdManager.h"
#import "ASInterstitialAdProvider.h"
#import "ASMutableURLRequest.h"
#import "ASLocationUtils.h"
#import "ASPubSettingUtils.h"
#import "ASErrorCodeManager.h"
#import "ASTransactionInfo.h"
#import "ASMetricsUtil.h"
#import "ASCommManager.h"

#import "NSString+ASUtils.h"
#import "NSURL+ASUtils.h"

const NSString* ASInterstitialFallBackName = nil;

@interface ASInterstitialAdManager() <ASInterstitialAdProviderDelegate, ASMetricsDelegate>

@property (nonatomic, strong) NSMutableArray *adTypesToTry;
@property (nonatomic, strong) NSDictionary *headers;
@property (nonatomic, strong) ASInterstitialAdProvider *providerBeingAttempted;
@property (nonatomic, strong) NSTimer *timeoutTimer; 
@property (nonatomic, assign) BOOL fallingBack;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) NSArray *keywords;
@property (nonatomic, strong) NSDictionary *pubKeys;
@property (nonatomic, strong) NSMutableDictionary *properties;

@property (nonatomic, strong) ASErrorCodeManager *errCodeManager;
@property (nonatomic, strong) ASTransactionInfo *transactionInfo;
@property (nonatomic, strong) NSDictionary *vcData;
@property (nonatomic, assign) ASPlatformType platform;
@property (nonatomic, strong) ASMetricsUtil *metrics;

@end

@implementation ASInterstitialAdManager

@synthesize closeOffset;

+ (void)initialize {

    // store and retrieve events by actual classname, rather than a hardcoded string literal

    NSString *fallBackName = NSStringFromClass(self.class);
    ASInterstitialFallBackName = [fallBackName stringByReplacingOccurrencesOfString:@"AdManager" withString:@"FallBackName"];
}

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, dealloc"];
    [self cancel];
    [_adTypesToTry removeAllObjects];
    _adTypesToTry = nil;
    _headers = nil;
    _keywords = nil;
    _pubKeys = nil;
    [_properties removeAllObjects];
    _properties = nil;
    _errCodeManager = nil;
    _transactionInfo = nil;
    _vcData = nil;;
    _metrics = nil;
}

#pragma mark - Timeout Methods

- (void)killTimeoutTimer {
    if (self.timeoutTimer != nil) {
        [self.timeoutTimer invalidate];
        self.timeoutTimer = nil;
    }
}

- (void)startTimeoutTimer {
    [self killTimeoutTimer];
    self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:self.timeoutInt target:self selector:@selector(onTimeoutTimer) userInfo:nil repeats:NO];
}

- (void)onTimeoutTimer {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, onTimeoutTimer - cancel and fallback if possible"];
    @try {
        [self reportErrorForProvider];
        [self reportErrorToDelegate:[NSError errorWithDomain:@"Loading Timeout"
                                                        code:100
                                                    userInfo:@{ NSLocalizedDescriptionKey : [NSString stringWithFormat:@"The ad manager took longer than %.2f to load", self.timeoutInt ] }]];
        
        if (self.fallingBack) {
            [self getFallbackAd];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - Interstitial Methods

// this is our call the first time we fetch an add. For fallbacks the controllers
// will be responsible for doing what they need to display.
- (void)fetchAdForPlacementID:(NSString *)placementID inEnv:(ASEnvironmentType)env usingkeyWords:(NSArray*)keyWords andPubKeys:(NSDictionary*)pubKeys withUserID:(NSString *)userId onPlatform:(ASPlatformType)platform {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, fetchAdForPlacementID:keyWords:andEnv:andPubKeys - plc: %@", placementID]];
	static BOOL wasLastRequestPreloaded;
    
    // if in the process of loading, return so that we can't call ads twice
	if(self.isLoading) {
        if(wasLastRequestPreloaded) {
			[ASSDKLogger logStatement:@"ASInterstitialAdManager, fetchAdForPlacementID:keyWords:andEnv:andPubKeys: - PRELOADED AD IS LOADING"];
			if (self.adTypesToTry != nil && [self.adTypesToTry count] > 0) {
				[self.providerBeingAttempted requestInterstitialAdWithProperties:self.properties isPreload:self.isPreload];
				
				// some providers have async processes (vungle) -- if I request another ad, it will try to hit that old PLC again since isLoading is still true
				wasLastRequestPreloaded = NO;
            }
            else {
                [self reportErrorToDelegate:[NSError errorWithDomain:NSStringFromClass([ASInterstitialAdManager class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Failed to load preloaded ad because there are no ads to try"}]];
            }
        } else {
            // skip if I'm NOT preloading now but WAS preloading in the previous call
            [ASSDKLogger logStatement:@"ASInterstitialAdManager, fetchAdForPlacementID:keyWords:andEnv:andPubKeys: - In the process of loading and last request was NOT preload so skip"];
        }
		
        return;
	}
	
	self.isLoading = YES;
    wasLastRequestPreloaded = self.isPreload;
    self.keywords = keyWords;
    self.pubKeys = pubKeys;
    self.fallingBack = NO;
    self.platform = platform;

    // create our URL.
    NSURL *url = [NSURL URLForPlacementID:placementID
                                    inEnv:env
                               atlocation:[[ASLocationUtils getInstance] getLocation]
                            usingKeyWords:self.keywords
                               andPubKeys:self.pubKeys
                               withUserID:userId
                                asPreload:self.isPreload
                               onPlatform:self.platform
                  ];
    
    // send get request to ad server for ad
    __weak ASInterstitialAdManager* adManager = self;
    [ASCommManager sendGetRequestToEndPoint:url forTime:self.timeoutInt endWithCompletionBlock:^(NSURLResponse *response, NSData *data, NSError *error) {
    
        // we getting data from an HTTP server so our response should be a URL response.
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, fetchAdForPlacementID:keyWords:andEnv:andPubKeys: - GET request has response.code: %ld", (long)httpResponse.statusCode]];
        
        @try {
            dispatch_async(dispatch_get_main_queue(), ^{
                [adManager killTimeoutTimer];
            });
            
            // if we got an OK response we need to go ahead and copy the headers out.
            if ([httpResponse statusCode] == 200) {
                // lets grab our headers..
                adManager.headers = [httpResponse allHeaderFields];
                
                adManager.adTypesToTry = [NSMutableArray new];
                
                // lets get our headers ad process the type.
                if ([[httpResponse MIMEType] isEqualToString:@"text/xml"] || [[httpResponse MIMEType] isEqualToString:@"application/xml"]) {
                    [ASSDKLogger logStatement:@"ASInterstitialAdManager, fetchAdForPlacementID:keyWords:andEnv:andPubKeys: - Adding VAST to adTypesToTry"];
                    [adManager.adTypesToTry addObject:@{ASInterstitialFallBackName : @"ASVAST"}];
                }
                else if ([[httpResponse MIMEType] isEqualToString:@"text/html"] && [httpResponse expectedContentLength] > 0) {
                    [ASSDKLogger logStatement:@"ASInterstitialAdManager, fetchAdForPlacementID:keyWords:andEnv:andPubKeys: - Adding HTML to adTypesToTry"];
                    [adManager.adTypesToTry addObject:@{ASInterstitialFallBackName : @"ASHTML"}];
                }
                else if ([[httpResponse MIMEType] isEqualToString:@"application/json"]) {
                    #if kTestMediationJson
                    NSError *err;
                    NSData *mediationData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kTestMediationFileName ofType:kTestMediationFileType] options:NSDataReadingUncached error:&err];
                    if(!err) {
                        data = mediationData;
                    } else {
                        [ASSDKLogger logStatement:@"ASInterstitialAdManager, fetchAdForPlacementID:inEnv:usingkeyWords:andPubKeys:withUserID: - "];
                    }
                    #endif
                    
                    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    
                    if (dictionary != nil) {
                        if([[dictionary allKeys] containsObject:@"endCardHtml"]) {
                            [ASSDKLogger logStatement:@"ASInterstitialAdManager, fetchAdForPlacementID:keyWords:andEnv:andPubKeys: - Adding VideoEndCard to adTypesToTry"];
                            [adManager.adTypesToTry addObject:@{ASInterstitialFallBackName : @"ASVideoEndCard"}];
                        } else {
                            [adManager processMediationJSON:dictionary];
                        }
                    }
                }
                
                // there are headers then we have our ad types to try (including the one we have
                // a body fore).
                if (adManager.headers[kXAerServHandlersHeaderKey] != nil) {
                    // lets process our callbacks.
                    [adManager processFallbacks];
                }
                
                if (adManager.headers[kXAerServHeaderKey] != nil) {
                    // process any additional publihser settings.
                    [adManager processPublisherSettings];
                }
                
                // process this initial response..
                if (adManager.adTypesToTry != nil && [adManager.adTypesToTry count] > 0) {
                    [adManager processEventResponse:httpResponse url:url withData:data];
                    
                }
                else  {
                    [adManager reportErrorToDelegate:[NSError errorWithDomain:NSStringFromClass([ASInterstitialAdManager class])
                                                                         code:100
                                                                     userInfo:@{NSLocalizedDescriptionKey : @"Failed to load ad because there are no more ads to try"}]];
                }
            }
            else {
                [adManager reportErrorToDelegate:[NSError errorWithDomain:NSStringFromClass([ASInterstitialAdManager class])
                                                                     code:100
                                                                 userInfo:@{NSLocalizedDescriptionKey : @"Failed to load ad from bad HTTP response"}]];
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [adManager startTimeoutTimer];
    });
}

- (void)processPublisherSettings {
    NSError *error = nil;
    id object = [NSJSONSerialization JSONObjectWithData:[self.headers[kXAerServHeaderKey] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    
    if( error ) {
        [ASSDKLogger logStatement:@"ASInterstitialAdManager, processPublisherSettings - X-AerServ header had malformed JSON.  Could not parse"];
        return;
    }
    
    if([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *aerservHeader = object;
        
        if(aerservHeader[kCloseOffsetKey] != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, processPublisherSettings - closeOffset value is: %@", aerservHeader[kCloseOffsetKey]]];
            self.closeOffset = aerservHeader[kCloseOffsetKey];
		}
        
        if(aerservHeader[kErrorParamsKey] != nil) {
            id errorParams = aerservHeader[kErrorParamsKey];
            if([errorParams isKindOfClass:[NSString class]]) {
                self.errCodeManager = [[ASErrorCodeManager alloc] initWithParamString:errorParams];
            } else if([errorParams isKindOfClass:[NSDictionary class]]) {
                self.errCodeManager = [[ASErrorCodeManager alloc] initWithParams:aerservHeader[kErrorParamsKey]];
            }
        }
        
        if(aerservHeader[kTransactionKey] != nil) {
            self.transactionInfo = [ASTransactionInfo infoWithTransactionObj:aerservHeader[kTransactionKey]];
        }
        
        if(aerservHeader[kMetricsKey] != nil) {
            self.metrics = [ASMetricsUtil utilWithMetricsObj:aerservHeader[kMetricsKey] withDelegate:self];
        }
        
//        [ASMoatKitUtil setMoatBitWithASProperties:aerservHeader];
    }
}

- (void)processMediationJSON:(NSDictionary *)dictionary {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, processMediationJSON - Mediation:\n %@\n", dictionary]];
    // we should have an object with some children. Which means the top level should
    // have one key we assume that.
    NSString* serviceName = [dictionary allKeys][0];
    NSMutableDictionary *adToAttempt = [NSMutableDictionary new];
    
    adToAttempt[ASInterstitialFallBackName] = serviceName;
    adToAttempt[ASInterstitialParameter_Parameters] = [dictionary allValues][0];

    [self.adTypesToTry addObject:adToAttempt];
}

- (void)presentInterstitialFromViewController:(UIViewController *)controller {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, presentInterstitialFromViewController:"];
    
    [self.providerBeingAttempted setShouldShowAd:YES];
    
    // if we are already trying to a provider then show this one modally.
    if (self.providerBeingAttempted != nil && self.isLoading) {
        self.isLoading = NO;
        [self reportProviderAdLoaded];
        [self.providerBeingAttempted showInterstitialFromRootViewController:controller];
        
        if(self.isPreload)
            [self.metrics fireShowAttemptEvent];
    }
}

- (void)processFallbacks {
    if (self.headers[kXAerServHandlersHeaderKey] != nil) {
        [ASSDKLogger logStatement:@"ASInterstitialAdManager, processFallbacks"];
        
        // our header is a JSON object. So create a mutable copy.
        NSMutableDictionary *dictionary = [NSMutableDictionary new];
        __block NSArray *fallbacks = nil;
        __weak ASInterstitialAdManager *adManager = self;
        
        // lets walk the headers looking for our fallbacks.
        [self.headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([key isEqualToString:kXAerServHandlersHeaderKey]) {
                fallbacks = [adManager.headers[kXAerServHandlersHeaderKey] JSONObjectValue];
            }
            else if ([key hasPrefix:@"X-AerServ-"]) {
                
                // our headers are arrays.. So turn it mutable so we can
                // rip the parmeter off as processed.
                dictionary[key] = [[obj JSONObjectValue] mutableCopy];
            }
        }];
        
        // now that we are parsed lets do our association of fallbacks to parameters.
        for (NSString *fallback in fallbacks) {
            // create our parameter look up string.
            NSString *parameterType = [@"X-AerServ-" stringByAppendingString:fallback];
            
            // we will store our fallbacks in dictionaries. name is the fallback type and paramters
            // are the paramateres we need to pass in.
            NSMutableDictionary *fallbackEntry = [NSMutableDictionary new];
            
            fallbackEntry[ASInterstitialFallBackName] = fallback;
            
            if (dictionary[parameterType] != nil) {
                if ([dictionary[parameterType] count] != 0) {
                    
                    // if the object we have is a dictionary stash the parameters. If we have an
                    // array then we store the first object and remove it.
                    if ([dictionary[parameterType] isKindOfClass:[NSDictionary class]]) {
                        fallbackEntry[ASInterstitialParameter_Parameters] = dictionary[parameterType];
                    }
                    else {
                        fallbackEntry[ASInterstitialParameter_Parameters] = dictionary[parameterType][0];
                        [dictionary[parameterType] removeObjectAtIndex:0];
                    }
                }

            }
            
            // attach it tour items.
            [self.adTypesToTry addObject:fallbackEntry];
        }
    }
}

- (void)processEventResponse:(NSHTTPURLResponse *)response url:(NSURL *)url withData:(NSData *)data {
    
    // we look at event 0.. if we didn't stash headers then
    // it must be we want the body.
    
    // lets check what we need to do. If we have no parameters we have all the data we need
    // in the response body there should be an apporiate constructor that takes data+url. If
    // we have parameters we take the right action. If parmeter is a url then then we need to
    // fire off another sessiona and leave the event on the stack. We will pop it off the stack
    // when we get a reponse back.
    if (response.statusCode == 200) {
        
        // we are creating UI get back to the main thread.. It will be up to each
        // controller to spawn when parsing data..
        [ASSDKLogger logStatement:@"ASInterstitialAdManager, processEventResponse:url:withData: - Good response, parse the response and attempt the associated provider"];
        __weak ASInterstitialAdManager *adManager = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                // This class (ASInterstitialAdManager) and ASInterstitialFallBackName may both have been modified
                // with a prefix (e.g. "FuseAerServ"), now we need to get complicated in order to turn:
                //     "$(prefix)ASInterstitialAdManager"
                // into:
                //     "$(prefix)$(eventType)InterstitialAdProvider"
                
                NSDictionary *event = adManager.adTypesToTry[0];
                NSString *eventType = event[ASInterstitialFallBackName];
                NSDictionary *eventParameters = event[ASInterstitialParameter_Parameters];
                
                // first, turn "$(prefix)ASInterstitialFallBackName" into "$(prefix)ASInterstitialAdProvider"
                NSString *className = [ASInterstitialFallBackName stringByReplacingOccurrencesOfString:@"FallBackName" withString:@"AdProvider"];
                
                // then, replace substring "ASInterstitial" with "$(eventType)Interstitial"
                className = [className stringByReplacingOccurrencesOfString:@"ASInterstitial" withString:[eventType stringByAppendingString:@"Interstitial"]];
                
                // fun with runtime.. Try to cget our class name.
                Class controllerClass = NSClassFromString(className);
                
                // create the properties we will pass in.
                NSMutableDictionary *properties = [NSMutableDictionary new];
                
                properties[ASInterstitialParameter_HTMLHeaders] = [response allHeaderFields];
                properties[ASInterstitialParameter_HTMLData] = data;
                
                // we need to pass in the source URL so make it look like a paramater.
                properties[ASInterstitialParameter_Parameters] = @{ASInterstitialParameter_Parameter_URL : [url absoluteString]};
                
                NSMutableDictionary *parameters = [NSMutableDictionary new];
                
                parameters[ASInterstitialParameter_Parameter_URL] = [url absoluteString];
                
                if (eventParameters != nil)
                    [parameters addEntriesFromDictionary:eventParameters];
                
                // we need to pass in the source URL so make it look like a paramater.
                properties[ASInterstitialParameter_Parameters] = parameters;
                adManager.properties = properties;
                
                // if we support class we are looking for. Then show the ad (we already requested it).
                if (controllerClass != nil) {
                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialViewController, processEventResponse:url:withData: - providerBeingAttempted is of class: %@", className]];
                    if(adManager.providerBeingAttempted == nil) {
                        adManager.providerBeingAttempted = [[controllerClass alloc] init];
                    }
                    adManager.providerBeingAttempted.delegate = adManager;
                    adManager.providerBeingAttempted.outlineAd = adManager.outlineAd;
                    adManager.providerBeingAttempted.parameters = parameters;
                    adManager.providerBeingAttempted.timeoutInt = adManager.timeoutInt;
                    [adManager reportLoadAttemptForProvider];
                    [adManager.providerBeingAttempted requestInterstitialAdWithProperties:properties isPreload:self.isPreload];
                }
                else {
                    // got here so try falling back.
                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, processEventResponse:url:withData: - Associated provider, %@, could not be created so fallback", controllerClass]];
                    [adManager getFallbackAd];
                }
            }
            @catch (NSException *exception) {
                [ASSDKLogger logStatement:@"ASInterstitialAdManager, processEventResponse:url:withData:, dispatch block - EXCEPTION CAUGHT"];
                [ASSDKLogger onException:exception];
            }
        });
    }
    else {
        // we failed we need to fallback.
        [ASSDKLogger logStatement:@"ASInterstitialAdManager, processEventResponse:url:withData: - Bad HTTP Response, fallback"];
        [self getFallbackAd];
    }
    
}

- (void)reportErrorToDelegate:(NSError *)error {
    self.isLoading = NO;
    
    // check for error code
    if(self.errCodeManager != nil)
        error = [self.errCodeManager checkForErrorCodeToSwitchError:error];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager - Failed to fetch the initial interstitial ad with error: %@", error.localizedDescription]];

    __weak ASInterstitialAdManager* adManager = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            [adManager.delegate interstitialAdManager:adManager didFailToFetchAdwithError:error];
            [adManager cancel];
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
        [adManager cancel];
    });

}

// These methods are listed under the ASInterstitialAdViewControllerDelegate protocol, which isn't active for the manager....don't think they're needed in the manager -- AZ

- (void)interstitialAdViewControllerAdLoadingWasCancelled:(ASInterstitialAdViewController *)controller { // who call this method? -- AZ
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdViewControllerAdLoadingWasCacnelled"];
     self.providerBeingAttempted = nil;
}

- (void)interstitialAdViewControllerAdFinished:(ASInterstitialAdViewController *)controller {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdViewControllerAdFinished:"];
    [self.delegate interstitialAdManagerAdCompleted:self];
}

// end ASInterstitialAdViewControllerDelelgate protocol methods

- (void)getFallbackAd {
    if (!self.adTypesToTry) {
        [ASSDKLogger logStatement:@"ASInterstitialAdManager, getFallbackAd - adTypesToTry == nil."];
        return;
    } else {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, getFallbackAd - adTypesToTry.count: %lu", (unsigned long)self.adTypesToTry.count]];
    }
    
    // if there is something to pop off the front queue do it.
    if ([self.adTypesToTry count] > 0) {
        [self.adTypesToTry removeObjectAtIndex:0];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, getFallbackAd - adTypesToTry after pop: %@", self.adTypesToTry]];
    }
    
    self.fallingBack = YES;
    
    // if we have anymore attempts try the lets make the attempt. If not report an error.
    if ([self.adTypesToTry count] > 0) {
        
        // lets try to create a controller.
        __weak ASInterstitialAdManager* adManager = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (adManager.adTypesToTry == nil || adManager.adTypesToTry.count == 0)
                return;
            
            @try {
                [adManager killTimeoutTimer];
                
                // lets get our event info.
                NSDictionary *event = adManager.adTypesToTry[0];
                NSString *eventType = event[ASInterstitialFallBackName];
                NSDictionary *parameters = event[ASInterstitialParameter_Parameters];
                
                // stash our parmeters away..
                NSString *className = [ASInterstitialFallBackName stringByReplacingOccurrencesOfString:@"ASInterstitialFallBackName" withString:[eventType stringByAppendingString:@"InterstitialAdProvider"]];
                NSMutableDictionary *properties = [NSMutableDictionary new];
                CLLocation *location = [[ASLocationUtils getInstance] getLocation];
                
                // stash the location if there is one.
                if (location != nil)
                    properties[ASInterstitialParameter_Location] = location;
                
                // we should pass the parameters down.
                if (parameters != nil) {
                    properties[ASInterstitialParameter_Parameters] = parameters;
                }
                
                properties[ASInterstitialParameter_HTMLHeaders] = adManager.headers;

                adManager.properties = properties;
                
                Class controllerClass = NSClassFromString(className);
                
                if (controllerClass != nil) {
                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialViewController, getFallbackAd - providerBeingAttempted is of class: %@", className]];
                    adManager.isLoading = YES;
                    [adManager startTimeoutTimer];
                    adManager.providerBeingAttempted = [[controllerClass alloc] init];
                    adManager.providerBeingAttempted.delegate  = adManager;
                    adManager.providerBeingAttempted.outlineAd = adManager.outlineAd;
                    adManager.providerBeingAttempted.parameters = parameters;
                    adManager.providerBeingAttempted.timeoutInt = adManager.timeoutInt;
                    [adManager reportLoadAttemptForProvider];
                    [adManager.providerBeingAttempted requestInterstitialAdWithProperties:properties isPreload:adManager.isPreload];
                }
                else {
                    [adManager getFallbackAd];
                }
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        });
    }
    else {
        // report an error if we failed to load any of our ads.
        [ASSDKLogger logStatement:@"ASInterstitialAdManager, getFallbackAd - No ad available for this request"];
        NSDictionary *errorInfo = @{NSLocalizedDescriptionKey:@"No ad available for this request."};
        NSError *error = [NSError errorWithDomain:NSStringFromClass([ASInterstitialAdManager class]) code:100 userInfo:errorInfo];
        [self reportErrorToDelegate:error];
    }
}

- (void)cancel {
    // if we have past session we must invlidate it.
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, cancel"];
    @try {
        self.isLoading = NO;
        [self killTimeoutTimer];
        [self.providerBeingAttempted cancel];
        self.providerBeingAttempted = nil;
        self.transactionInfo = nil;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - analytics reporting for fallbacks

- (void)reportLoadAttemptForProvider {
    if (self.providerBeingAttempted.parameters != nil) {
        NSString* urlString = self.providerBeingAttempted.parameters[@"AttemptURL"];
        
        if (urlString != nil) {
            NSURL *url = [NSURL URLWithString:urlString];
            
            if (url != nil) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, reportLoadAttemptForProvider - event 12 : %@", url]];
                [ASCommManager sendAsynchronousRequestForURL:url];
                [self.delegate interstitialAdManager:self didFireAdvertiserEventWithMessage:@"Interstital Ad Manager Fires Event Load Attempt For Provider"];
            }
        }
    }
}

- (void)reportProviderAdLoaded {
    if (self.providerBeingAttempted.parameters != nil) {
        NSString* urlString = self.providerBeingAttempted.parameters[@"ImpressionURL"];
        
        if (urlString != nil) {
            NSURL *url = [NSURL URLWithString:urlString];
            
            if (url != nil) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, reportProviderAdLoaded - event 13 : %@", url]];
                [ASCommManager sendAsynchronousRequestForURL:url];
                [self.delegate interstitialAdManager:self didFireAdvertiserEventWithMessage:@"Interstital Ad Manager Fires Event Ad Loaded For Provider"];
            }
        }
    }
}

- (void)reportErrorForProvider {
    if (self.providerBeingAttempted.parameters != nil) {
        NSString* urlString = self.providerBeingAttempted.parameters[@"FailedURL"];
        
        if (urlString != nil) {
            NSURL *url = [NSURL URLWithString:urlString];
            
            if (url != nil) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, reportErrorForProvider - event 14 : %@", url]];
                [ASCommManager sendAsynchronousRequestForURL:url];
                [self.delegate interstitialAdManager:self didFireAdvertiserEventWithMessage:@"Interstital Ad Manager Fires Error Event For Provider"];
            }
        }
    }
}

#pragma mark - Ad Provider Delegate

- (void)interstitialLoadSuccessfulCleanup {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialLoadSuccessfulCleanup"];
    [self killTimeoutTimer];
    self.adTypesToTry = nil;
    
    // if there's transaction info and no vc data then pass the transaction info on
    if(self.transactionInfo != nil) {
        [self.delegate interstitialAdManager:self didFindAdTransactionInfo:self.transactionInfo];
    }
}

- (void)interstitialAdProvider:(ASInterstitialAdProvider*)provider didLoadAd:(id)ad {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProvider:didLoadAd:"];
    if (self.isLoading) {
        [self.delegate interstitialAdManagerCreatedAdController:self];
        [self interstitialLoadSuccessfulCleanup];
    }
}

- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didPreloadAd:(id)ad {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProvider:didPreloadAd:"];
    if (self.isLoading) {
        [self.delegate interstitialAdManagerDidPreload:self];
        [self interstitialLoadSuccessfulCleanup];
        [self.metrics firePreloadReadyEvent];
    }
}

- (void)interstitialAdProviderDidAdComplete:(ASInterstitialAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProviderDidAdComplete:"];
    [self.delegate interstitialAdManagerAdCompleted:self];
}

- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didVirtualCurrencyLoad:(NSDictionary *)vcData {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProvider:didVirtualCurrencyLoad:"];
    
    if(self.transactionInfo != nil) {
        self.vcData = [self.transactionInfo createDictionaryContainingBothTransactionAndVCData:vcData];
    } else {
        self.vcData = vcData;
    }
    
	[self.delegate didVirtualCurrencyLoad:self.vcData];
}

- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didVirtualCurrencyReward:(NSDictionary *)vcData {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProvider:didVirtualCurrencyReward:"];
	[self.delegate didVirtualCurrencyReward:vcData];
    self.vcData = nil;
}

- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didFailToLoadAdWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInterstitialAdManager, interstitialAdProvider:didFailToLoadAdWithError: - error: %@", error.localizedDescription]];

    self.isLoading = NO;
    [self killTimeoutTimer];
    [self reportErrorForProvider];
    self.providerBeingAttempted = nil;
    
    // we failed so we need to remove the controller we attempted.
    if ([self.adTypesToTry count] > 0) {
        [self getFallbackAd];
    } else {
        [self reportErrorToDelegate:error];
    }
}

- (void)interstitialAdProviderDidExpire:(ASInterstitialAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProviderDidExpire:"];
    [self.delegate interstitialAdManagerAdDidExpire:self];
}

- (void)interstitialAdProviderWillAppear:(ASInterstitialAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProviderWillAppear:"];
    [self.delegate interstitialAdManagerAdWillAppear:self];
}

- (void)interstitialAdProviderDidAppear:(ASInterstitialAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProviderDidAppear:"];
    [self.delegate interstitialAdManagerAdDidAppear:self];
}

- (void)interstitialAdProviderWillDisappear:(ASInterstitialAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProviderWillDisappear:"];
    [self.delegate interstitialAdManagerAdWillDisappear:self];
}

- (void)interstitialAdProviderDidDisappear:(ASInterstitialAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProviderDidDisappear:"];
    self.providerBeingAttempted = nil;
    [self.delegate interstitialAdManagerAdDidDisappear:self];
}

- (void)interstitialAdProviderAdWasTouched:(ASInterstitialAdProvider *)provider {
    [ASSDKLogger logStatement:@"ASInterstitialAdManager, interstitialAdProviderAdWasTouched:"];
    [self.delegate inerstititalAdManagerAdWasTouched:self];
}

- (BOOL)interstitialAdProviderIsPreload:(ASInterstitialAdProvider *)provider {
    return self.isPreload;
}

- (NSArray*)getKeyWords {
    return self.keywords;
}

- (NSDictionary*)getPubKeys {
    return self.pubKeys;
}

- (void)AVPlayerCreated:(AVPlayer *)avPlayer {
    [self.delegate AVPlayerCreated:avPlayer];
}

- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate interstitialAdManager:self didFireAdvertiserEventWithMessage:msg];
}

- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider willUpdateForFailoverWithResponseHeaders:(NSDictionary *)headers {
    self.headers = headers;
    if(self.headers[kXAerServHeaderKey] != nil) {
        [self.transactionInfo updatedTransactionInfoWithResponseHeadersData:[self.headers[kXAerServHeaderKey] dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

#pragma mark - Metrics Delegate

- (void)metrics:(ASMetricsUtil *)mUtil didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate interstitialAdManager:self didFireAdvertiserEventWithMessage:msg];
}

@end

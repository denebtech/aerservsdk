//
//  ASVideoEndCardVideoPlayerView.m
//  AerServSDK
//
//  Created by Albert Zhu on 4/27/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASVideoEndCardVideoPlayerView.h"
#import "ASVideoView.h"

@implementation ASVideoEndCardVideoPlayerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _videoView = [[ASVideoView alloc] initWithFrame:frame];
        [self addSubview:_videoView];
    }
    
    return self;
}

- (void)changeFrame:(CGRect)frame {
    self.frame = frame;
    [self.videoView changeFrame:frame];
}

@end

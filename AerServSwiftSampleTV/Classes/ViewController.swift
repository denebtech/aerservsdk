//
//  ViewController.swift
//  AerServSwiftSampleTV
//
//  Created by Hall on 10/6/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

import UIKit

struct Consts {
    static let defaultTimeoutInterval = 15.0
}

class ViewController: UIViewController, ASInterstitialViewControllerDelegate {
    var interstitial: ASInterstitialViewController? = nil;
    @IBOutlet var plcField: UITextField?
    @IBOutlet var loadButton: UIButton?
    @IBOutlet var preloadButton: UIButton?
    @IBOutlet var showButton: UIButton?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        NSLog("View did load")
    
        self.loadButton?.addTarget(self, action: #selector(clickLoadInterstitial(sender:)), for: .primaryActionTriggered)
        self.preloadButton?.addTarget(self, action: #selector(clickPreloadInterstitial(sender:)), for: .primaryActionTriggered)
        self.showButton?.addTarget(self, action: #selector(clickShowInterstitial(sender:)), for: .primaryActionTriggered)
        self.showButton?.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        NSLog("Did Receive Memory Warning")
    }
    
    // MARK: Button Actions
  
    func clickLoadInterstitial(sender: UIButton) {
        let plc = plcField?.text
        if(plc == nil || plc == "") {
            NSLog("Load Interstitial");
        } else {
            NSLog("Load Interstitial: " + plc!)
        }
        
        self.interstitial = ASInterstitialViewController.init(forPlacementID: self.plcField?.text, with: self)
        self.interstitial?.timeoutInterval = Consts.defaultTimeoutInterval
        self.interstitial?.showLogs(true)
        self.interstitial?.loadAd()
    }
    
    func clickPreloadInterstitial(sender: UIButton) {
        let plc = plcField?.text
        NSLog("Preload interstitial: " + plc!)
        
        self.interstitial = ASInterstitialViewController.init(forPlacementID: self.plcField?.text, with: self)
        self.interstitial?.isPreload = true
        self.interstitial?.timeoutInterval = Consts.defaultTimeoutInterval
        self.interstitial?.showLogs(true)
        self.interstitial?.keyWords = ["key","word"]
        self.interstitial?.loadAd()
    }
    
    func clickShowInterstitial(sender: UIButton) {
        NSLog("Show preloaded interstitial")
        self.interstitial?.show(from: self)
    }
    
    // MARK: ASInterstitialViewControllerDelegate Protocol Functions
    
    func interstitialViewControllerAdFailed(toLoad viewController: ASInterstitialViewController!, withError error: Error!) {
        NSLog("Interstitial ad did fail to load with error: " + error.localizedDescription);
        self.interstitial = nil
        self.showButton?.isEnabled = false
    }
  
    func interstitialViewControllerAdLoadedSuccessfully(_ viewController: ASInterstitialViewController) {
        NSLog("Interstitial ad did load")
        self.interstitial?.show(from: self)
    }
    
    func interstitialViewControllerDidPreloadAd(_ viewController: ASInterstitialViewController!) {
        NSLog("Interstitial ad did preload")
        self.showButton?.isEnabled = true
    }

    func interstitialViewControllerWillAppear(_ viewController: ASInterstitialViewController!) {
        NSLog("Interstitial ad will appear")
    }
    
    func interstitialViewControllerDidAppear(_ viewController: ASInterstitialViewController!) {
        NSLog("Interstitial ad did appear")
    }
    
    func interstitialViewControllerAdDidComplete(_ viewController: ASInterstitialViewController!) {
        NSLog("Interstitial ad did complete")
    }
    
    func interstitialViewControllerWillDisappear(_ viewController: ASInterstitialViewController!) {
        NSLog("Interstitial ad will disappear")
    }
    
    func interstitialViewControllerDidDisappear(_ viewController: ASInterstitialViewController!) {
        NSLog("Interstitial ad did disappear")
        self.interstitial = nil
        self.showButton?.isEnabled = false
    }
    
    func interstitialViewController(_ viewController: ASInterstitialViewController!, didShowAdWithTransactionInfo transcationData: [AnyHashable : Any]!) {
        let buyerName = transcationData["buyerName"] as! String
        let price = transcationData["buyerPrice"] as! NSInteger
        let buyerPrice = String(price)
        NSLog("Interstitial ad did show from buyerName: %@ with buyerPrice: %@", buyerName, buyerPrice)
    }
}

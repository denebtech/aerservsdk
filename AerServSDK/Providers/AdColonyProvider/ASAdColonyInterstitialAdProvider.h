//
//  ASAdColonyInterstitialProvider.h
//  AerServSDK
//
//  Created by Scott A Andrew on 4/11/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ASInterstitialAdProvider.h"

#define kAdColonyAppIDParamsKey @"AdColonyAppID"
#define kAdColonyZoneIDsParamsKey @"AdColonyZoneIDs"

@interface ASAdColonyInterstitialAdProvider : ASInterstitialAdProvider

@property (readonly, nonatomic, assign) BOOL zoneAvailable;

- (void)zoneBecameActive;
- (void)zoneDidExpire;
- (void)onZoneUnavailable;
- (void)onAdColonyAdStartedInZone:(NSString *)zoneID;
- (void)onAdColonyAdAttemptFinished:(BOOL)shown inZone:(NSString *)zoneID;
- (void)onAdColonyAdFailure:(NSError *)err;
- (void)onAdColonyV4VCReward:(BOOL)success currencyName:(NSString *)currencyName currencyAmount:(int)amount inZone:(NSString *)zoneID;

@end

//
//  ASAdColonyInstance.h
//  AerServSDK
//
//  Created by Albert Zhu on 11/23/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASGlobalProviderSetup.h"

#define kAdColonyClassStr @"AdColony"
#define kInitAdColonyAppIDKey @"appId"
#define kInitAdColonyZoneIDsKey @"zoneIds"

@interface ASAdColonyInstance : NSObject

@property (nonatomic, strong) Class adColonyClass;
@property (nonatomic, copy) NSString *zoneID;
@property (nonatomic, assign) ASProviderSetupState state;

+ (ASAdColonyInstance *)instance;
+ (void)adColonySetupWithConfig:(NSDictionary *)adapterParams;

+ (void)addAvailableZoneID:(NSString *)zoneID;
+ (void)removeAvailabeZoneID:(NSString *)zoneID;
+ (BOOL)isZoneAvailable:(NSString *)zoneID;
+ (NSString *)findAvailableZoneIDFromZoneIDList:(NSArray *)zoneIDArr;

@end

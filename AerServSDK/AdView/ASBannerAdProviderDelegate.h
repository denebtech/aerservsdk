//
//  ASAdProviderDelegate.h
//  AerServSDK
//
//  Created by Scott A Andrew on 4/14/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

/*! @header
 * Delegate calls that the banner ad provider must call. */

@class ASAdView;
@class ASBannerAdProvider;
@class ASMRAIDView;

@protocol ASBannerAdProviderDelegate <NSObject>

- (UIViewController *)viewControllerForPresentingModalView;

- (void)bannerProvider:(ASBannerAdProvider *)provider didLoadAd:(UIView *)ad;
- (void)bannerProvider:(ASBannerAdProvider *)provider didFailToLoadAdWithError:(NSError *)error;

- (void)bannerProviderWillBeginAction:(ASBannerAdProvider *)provider;
- (void)bannerProviderWillEndAction:(ASBannerAdProvider *)provider;

- (void)bannerProviderWillLeaveApplication:(ASBannerAdProvider *)provider;
- (void)bannerProviderAdChangedSize:(ASBannerAdProvider *)provider;
- (void)bannerProviderAdWasClicked:(ASBannerAdProvider *)provider;
- (void)bannerProviderDidShowAdCompletely:(ASBannerAdProvider *)provider;

- (NSArray*) getKeyWords;
- (NSDictionary*) getPubKeys;

- (void)AVPlayerCreated:(AVPlayer *)avPlayer;

- (void)bannerProvider:(ASBannerAdProvider *)provider willChangeFrameTo:(CGRect)newFrame;
- (ASAdView *)bannerProviderWillGetAdView:(ASBannerAdProvider *)provider;
- (UIView *)bannerProviderWillGetContentView:(ASBannerAdProvider *)provider;
- (void)bannerProvider:(ASBannerAdProvider *)provider willSetContentViewToMRAIDView:(ASMRAIDView *)mraidView;
- (void)bannerProvider:(ASBannerAdProvider *)provider didLoadPlayerLayer:(AVPlayerLayer *)pl;

- (BOOL)bannerProviderAdAttemptIsPreload:(ASBannerAdProvider *)provider;

@end

//
//  ASVASTInterstitialViewController.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/25/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdViewController.h"

@class ASVASTAd;

@interface ASVASTInterstitialViewController : ASInterstitialAdViewController

@property (nonatomic, strong) ASVASTAd *adToDisplay;

- (instancetype)initWithVASTAd:(ASVASTAd*)VASTAd delegate:(id<ASInterstitialAdViewControllerDelegate>)delegate closeOffset:(NSString*)offset;

@end

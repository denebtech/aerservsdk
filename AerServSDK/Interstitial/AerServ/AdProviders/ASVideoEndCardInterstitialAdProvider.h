//
//  ASVideoEndCardInterstitialAdProvider.h
//  AerServSDK
//
//  Created by Albert Zhu on 4/26/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASInternalBaseInterstitialAdProvider.h"

#define kXAerServKey @"X-AerServ"

@interface ASVideoEndCardInterstitialAdProvider : ASInternalBaseInterstitialAdProvider

@end

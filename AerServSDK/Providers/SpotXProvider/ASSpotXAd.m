//
//  ASSpotXAd.m
//  AerServSDK
//
//  Created by Vasyl Savka on 10/14/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASSpotXAd.h"

#import <TVMLKit/TVMLKit.h>

@interface ASSpotXAd () <TVApplicationControllerDelegate>

@property (nonatomic, strong) TVApplicationController *controller;
@property (nonatomic, strong) UIViewController *presentingVC;
@property (nonatomic, assign) BOOL animatePresentation;

@end

@implementation ASSpotXAd

- (void)dealloc {
    _channelId = nil;
    _params = nil;
    _delelgate = nil;
    [_controller stop];
    _controller = nil;
    _presentingVC = nil;
}

- (instancetype)initWithChannelId:(NSString *)channelId andDelegate:(nullable id<SpotXAdDelegate>)delegate {
    if(self = [super init]) {
        self.channelId = channelId;
        self.delelgate = delegate;
    }
    
    return self;
}

+ (instancetype)adWithChannelId:(NSString *)channelId andDelegate:(nullable id<SpotXAdDelegate>)delelgate {
    return [[ASSpotXAd alloc] initWithChannelId:channelId andDelegate:delelgate];
}

#pragma mark - SpotXAd Functionality

- (void)showWith:(UIViewController *)presentingViewController asAnimated:(BOOL)animated {
    [ASSDKLogger logStatement:@"ASSpotXAd, showWith:asAnimated:"];
    if(presentingViewController != nil) {
        TVApplicationControllerContext *context = [TVApplicationControllerContext new];
        context.javaScriptApplicationURL = [NSURL URLWithString:kSpotX_SDK_URL_Str];
        NSDictionary *options = (self.params != nil) ? @{kContextLaunchOptionsChannelIdKey : self.channelId,
                                                         kContextLaunchOptionsParamsKey : self.params }
                                                     : @{kContextLaunchOptionsChannelIdKey : self.channelId};
        context.launchOptions = options;
        
        self.controller = [[TVApplicationController alloc] initWithContext:context
                                                                    window:nil
                                                                  delegate:self];
        
        UIViewController *viewController = self.controller.navigationController;
        self.presentingVC = presentingViewController;
        self.animatePresentation = animated;
        [self.presentingVC presentViewController:viewController animated:self.animatePresentation completion:nil];
    } else {
        [ASSDKLogger logStatement:@"ASSpotXAd, showWith:asAnimated: - No ViewController to present from"];
    }
}

#pragma mark - TVApplicationControllerDelegate Protocol Methods

- (void)appController:(TVApplicationController *)appController evaluateAppJavaScriptInContext:(JSContext *)jsContext {
    [ASSDKLogger logStatement:@"ASSpotXAd, appController:eveluateAppJavaScriptInContext:"];
    @try {
        void (^exit)() = ^{
            [appController stop];
        };
        [jsContext setObject:exit forKeyedSubscript:@"exit"];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)appController:(TVApplicationController *)appController didFinishLaunchingWithOptions:(NSDictionary<NSString *,id> *)options {
    [ASSDKLogger logStatement:@"ASSpotXAd, appController:didFinishLaunchingWithOptions:"];
    @try {
        [self.delelgate spotXAdDidStart:self];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)appController:(TVApplicationController *)appController didStopWithOptions:(NSDictionary<NSString *,id> *)options {
    [ASSDKLogger logStatement:@"ASSpotXAd, appController:didStopWithOptions:"];
    @try {
        [self.delelgate spotXAdDidFinish:self];
        [self.presentingVC dismissViewControllerAnimated:self.animatePresentation completion:nil];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)appController:(TVApplicationController *)appController didFailWithError:(NSError *)error {
    [ASSDKLogger logStatement:@"ASSpotXAd, appController:didFailWithError:"];
    @try {
        [self.delelgate spotXAdDidFail:self withError:error];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

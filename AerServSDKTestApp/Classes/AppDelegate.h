//
//  AppDelegate.h
//  AerServSDKTestApp
//
//  Created by Arash Sharif on 11/5/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kLogRedirects 0

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

//
//  ASMRAIDViewController.m
//  AerServSDK
//
//  Created by Albert Zhu on 10/6/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASMRAIDViewController.h"
#import "ASMRAIDView.h"
#import "ASMRAIDCloseRegion.h"

@interface ASMRAIDViewController ()

@property (nonatomic, strong) ASMRAIDView *mraidView;

@property (nonatomic, assign) BOOL didInvokeCloseFromVC;

@end

@implementation ASMRAIDViewController

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASMRAIDViewController, dealloc"];
    
    _origRootVC = nil;
    _mraidView = nil;
    
}

#pragma mark - initializers

- (instancetype)initWithMRAIDView:(ASMRAIDView *)mv {
    [ASSDKLogger logStatement:@"ASMRAIDViewController, initWithMRAIDView:"];
    
    if(self = [super init]) {
        _mraidView = mv;
        _origRootVC = mv.rootVC;
        _mraidView.rootVC = self;
    }
    
    return self;
}

#pragma mark - helpers

- (void)dismissMRAIDInterVCWithBlock:(void (^)(void))completionBlock {
    self.mraidView.rootVC = self.origRootVC;
    [self.origRootVC dismissViewControllerAnimated:NO completion:completionBlock];
}

- (void)onClose {
    [ASSDKLogger logStatement:@"ASMRAIDViewController, onClose"];
    
    @try {
        
        // stop mraid view from loading
        [self.mraidView stopLoad];
        
        // set flag to notify that this close has occurred before
        self.didInvokeCloseFromVC = YES;
        
        // dismiss this vc
        __weak ASMRAIDViewController *mraidVC = self;
        [self dismissMRAIDInterVCWithBlock:^{
            [ASSDKLogger logStatement:@"ASMRAIDViewController, onClose - dismiss completion block - ENTER"];
            
            @try {
                // remove the mraid view from
                [mraidVC.mraidView removeFromSuperview];
                mraidVC.mraidView = nil;
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
            
            [ASSDKLogger logStatement:@"ASMRAIDViewController, onClose - dismiss completion block - EXIT"];
        }];
        
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - default view controller methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [ASSDKLogger logStatement:@"ASMRAIDViewController, viewDidLoad"];
    
    @try {
        // set background color
        self.view.layer.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f].CGColor;
        
        // setup mraid view here using it's existing current position
        CGRect currPos = self.mraidView.currPos;
        self.mraidView.frame = CGRectMake(currPos.origin.x, currPos.origin.y, currPos.size.width, currPos.size.height);
        [self.view addSubview:self.mraidView];
        [self.view bringSubviewToFront:self.mraidView];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // a hacky way to resize the mraid view to fit the new screen after a forced orientation -- might break something else
    if(self.mraidView.frame.origin.x == 0.0f && self.mraidView.frame.origin.y == 0.0f &&
       (self.mraidView.frame.size.width > self.view.frame.size.width || self.mraidView.frame.size.height > self.view.frame.size.height)) {
        self.mraidView.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotate {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDViewController, shouldAutorotate - allowOrientationChange: %d", self.mraidView.allowOrienationChange]];
    
    return (!self.mraidView) ? YES : (kIS_iOS_6 ? NO : self.mraidView.allowOrienationChange); // turning off rotation for iOS6 devices
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return (!self.mraidView) ? (kIS_IPHONE ? UIInterfaceOrientationMaskAllButUpsideDown : UIInterfaceOrientationMaskAll) : self.mraidView.preferredOrientation;
}

//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
//    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDViewController, didRotateFromInterfaceOrientation:"]];
//    
//    @try {
//        [self.mraidView mraidViewWillAttemptOrientationChange];
//    }
//    @catch (NSException *exception) {
//        [ASSDKLogger onException:exception];
//    }
//}

- (void)viewDidLayoutSubviews {
    [ASSDKLogger logStatement:@"ASMRAIDViewController, viewDidLayoutSubviews"];    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end

// Copyright (C) 2015 by Tapjoy Inc.
//
// This file is part of the Tapjoy SDK.
//
// By using the Tapjoy SDK in your software, you agree to the terms of the Tapjoy SDK License Agreement.
//
// The Tapjoy SDK is bound by the Tapjoy SDK License Agreement and can be found here: https://www.tapjoy.com/sdk/license
//

#import "ASTapjoyInterstitialAdProvider.h"
#import <Tapjoy/TJPlacement.h>
#import <Tapjoy/Tapjoy.h>

@interface ASTapjoyInterstitialAdProvider()<TJPlacementDelegate>
@property (nonatomic, strong) TJPlacement *placement;
@property (nonatomic, assign) BOOL failedToReceiveAd;
@end

@implementation ASTapjoyInterstitialAdProvider
- (id)init {
    if (self = [super initWithAdClassName:@"ASTapjoyInterstitialAdProvider" timeout:6.0f]) {
        // Add init code here
    }
    return self;
}
- (void)initializePartnerAd:(NSDictionary *)properties {
    // Do nothing. Nothing to be initialized.
}
- (BOOL)hasPartnerAdInitialized {
    // There is nothing to be initialized, so always return YES
    return YES;
}
- (BOOL)hasPartnerAdFailedToInitialize {
    // There is nothing to be initialized, so always return NO
    return NO;
}
- (void)loadPartnerAd:(NSDictionary *)properties {
    
    self.failedToReceiveAd = NO;
    // Grab placement name defined in AerServ dashboard
    NSString *adUnitId = [self valueInProperty:properties forKey:@"TapjoyPlacementName"];
    
    _placement = [TJPlacement placementWithName:adUnitId mediationAgent:@"aerserv" mediationId:nil delegate:self];
    _placement.adapterVersion = @"1.0";
    [_placement requestContent];
    
    
}
- (BOOL)hasPartnerAdLoaded {
    return self.placement.contentReady;
}
- (BOOL)hasPartnerAdFailedToLoad {
    // Use self.failedToReceiveAd flag to determine if ad has failed to load
    return self.failedToReceiveAd;
}
- (void)cancel {
    
}
- (void)showPartnerAd:(UIViewController *)rootViewController {
    // Perform one last check, and show the ad
    if (self.placement.isContentAvailable) {
        [self.placement showContentWithViewController:nil];
    }
}

//[self asPartnerInterstitialAdWasTouched];
#pragma mark - TJPlacementtDelegate

- (void)requestDidSucceed:(TJPlacement *)placement {
    if (!placement.isContentAvailable) {
        self.failedToReceiveAd = YES;
    }
}

- (void)requestDidFail:(TJPlacement *)placement error:(NSError *)error {
    self.failedToReceiveAd = YES;
}

- (void)contentDidAppear:(TJPlacement *)placement {
    [self asPartnerInterstitialAdWillAppear];
    [self asPartnerInterstitialAdDidAppear];
}

- (void)contentDidDisappear:(TJPlacement *)placement {
    [self asPartnerInterstitialAdWillDisappear];
    [self asPartnerInterstitialAdDidDisappear];
}
@end
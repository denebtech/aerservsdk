//
//  TremorVideoSampleViewController.m
//  TremorVideoSample

#import <QuartzCore/QuartzCore.h>
#import "ASTremorViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface ASTremorViewController() <TremorVideoAdDelegate>

@property (nonatomic, strong) NSString *appID;
@property (nonatomic, assign) BOOL adShown;

@end

@implementation ASTremorViewController


- (ASTremorViewController *)initWithDelegate:(id)delegate withAppID:(NSString*)appID {
    self.appID = appID;
    self = [self initWithDelegate:delegate];
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	self.view.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    [TremorVideoAd setDelegate:self];
    
    //Pass in optional information
    //TremorVideoSettings *settings = [TremorVideoAd getSettings];
    //settings.userLatitude = 84.54;
    //settings.userLongitude = 42.34;

    
    //Initial TremorVideo SDK
    [TremorVideoAd initWithAppID:self.appID];
    [TremorVideoAd start];
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}


-(void)viewDidAppear:(BOOL)animated {
    // this is called after the tremor ad disappears also, so we need to check
    if (!self.adShown) {
        if ([TremorVideoAd showAd:self]) {
            self.adShown = TRUE;
        }
        else {
            NSError* error = [NSError errorWithDomain:NSStringFromClass([self class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Tremor failed to play."}];
            [self.delegate  interstitialAdViewControllerFailed:self withError:error];
        }
    }
    else {
        self.adShown = FALSE;
        // sometimes we don't receive the callback from Tremor, so do it explicitly
        // TODO: this could double fire the impression event
        [self didAdComplete];
    }
}


#pragma mark TremorVideoAdDelegate
// optional

- (void)didAdComplete {
    [self dismissViewControllerAnimated:NO completion:^{}];
    [self.delegate interstitialAdViewControllerAdFinished:self];
}
@end

//
//  ASAppLovinInstance.m
//  AerServSDK
//
//  Created by Albert Zhu on 11/25/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASAppLovinInstance.h"
#import "ALSdk.h"
#import "ALInterstitialAd.h"
#import "ASAppLovinDelegateProxy.h"

@implementation ASAppLovinInstance

+ (ASAppLovinInstance *)instance {
    static ASAppLovinInstance *instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [ASAppLovinInstance new];
        instance.state = kASProviderSetupNone;
    });
    
    return instance;
}

+ (void)appLovinSetupWithConfig:(NSDictionary *)adapterConifg {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinInstance, appLovinSetupWithConfig - adpaterConfig: %@", adapterConifg]];
    
    BOOL successful = YES;
    
    Class alSdkSettingsClass = NSClassFromString(kALSdkSettingsClassStr);
    id alSdkSettings = nil;
    if(alSdkSettingsClass != nil) {
        alSdkSettings = [alSdkSettingsClass new];
    } else {
        successful = NO;
    }
    
    if(alSdkSettings != nil) {
        BOOL isVerboseLogging = adapterConifg != nil && adapterConifg[kAppLovinVerboseLoggingKey];
        [alSdkSettings setIsVerboseLogging:isVerboseLogging];
        
        NSString *preloadSizesStr = @"";
        NSArray *preloadSizesArr = adapterConifg[kAppLovinPreloadSizesKey];
        if(preloadSizesArr != nil && preloadSizesArr.count > 0) {
            for(NSString *size in preloadSizesArr) {
                preloadSizesStr = [preloadSizesStr stringByAppendingFormat:@",%@", size];
            }
            preloadSizesStr = [preloadSizesStr substringFromIndex:1];
        } else {
            preloadSizesStr = kALDefaultPreloadSizesStr;
        }
        [alSdkSettings setAutoPreloadAdSizes:preloadSizesStr];
        
        NSString *preloadTypesStr = @"";
        NSArray *preloadTypesArr = adapterConifg[kAppLovinPreloadTypesKey];
        if(preloadTypesArr != nil && preloadTypesArr.count > 0) {
            for(NSString *type in preloadTypesArr) {
                preloadTypesStr = [preloadTypesStr stringByAppendingFormat:@",%@", type];
            }
            preloadTypesStr = [preloadTypesStr substringFromIndex:1];
        } else {
            preloadTypesStr = kALDefaultPreloadTypesStr;
        }
        [alSdkSettings setAutoPreloadAdTypes:preloadTypesStr];
    } else {
        successful = NO;
    }
    
    NSString *sdkKey = adapterConifg[kAppLovinSDKKey];
    if(!sdkKey) successful = NO;
    
    id alSdk;
    Class alSdkClass = NSClassFromString(kALSdkClassStr);
    if(alSdkClass != nil) {
        [ASAppLovinInstance instance].state = kASProviderSetupStarted;
        alSdk = [alSdkClass sharedWithKey:sdkKey settings:alSdkSettings];
        [alSdk initializeSdk];
        [ASAppLovinInstance instance].alSdk = alSdk;
    } else {
        successful = NO;
    }
    
    Class alInterstitialAdClass = NSClassFromString(kALInterstitialAdClassStr);
    if(alInterstitialAdClass != nil && alSdk != nil && successful) {
        [ASAppLovinInstance instance].alPreInitInterstitialAd = [[alInterstitialAdClass alloc] initWithSdk:alSdk];
        [ASAppLovinInstance instance].state = kASProviderSetupReady;
        [[ASAppLovinInstance instance].alPreInitInterstitialAd setAdLoadDelegate:[ASAppLovinDelegateProxy sharedProxy]];
        [[ASAppLovinInstance instance].alPreInitInterstitialAd setAdDisplayDelegate:[ASAppLovinDelegateProxy sharedProxy]];
        [[ASAppLovinInstance instance].alPreInitInterstitialAd setAdVideoPlaybackDelegate:[ASAppLovinDelegateProxy sharedProxy]];
    } else {
        [ASSDKLogger logStatement:@"ASAppLovinInstance, appLovinSetupWithConfig - Could not create App Lovin SDK object. Will try again later during request"];
        [ASAppLovinInstance instance].state = kASProviderSetupNone;
    }
}

@end

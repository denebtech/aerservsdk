//
//  MoatMobileAppKit.h
//  MoatMobileAppKit
//
//  Created by Moat on 12/31/14.
//  Copyright © 2016 Moat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AERMoatBootstrap.h"
#import "AERMoatTracker.h"
#import "AERMoatVideoTracker.h"

@interface AERMoatMobileAppKit : NSObject

@end

//
//  main.m
//  SampleTV
//
//  Created by Vasyl Savka on 10/5/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  ASVASTVideoPlayerView.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/30/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVASTVideoPlayerView.h"
#import "ASVideoView.h"

@interface ASVASTVideoPlayerView()

@property (nonatomic, strong) ASVideoView* videoView;

@end

@implementation ASVASTVideoPlayerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTVideoPlayerView, initWithFrame: - frame: %@", NSStringFromCGRect(frame)]];
        _videoView = [[ASVideoView alloc] initWithFrame:frame];
        [self addSubview:_videoView];
    }
    
    return self;
}

- (void)layoutSubviews {
//    [ASSDKLogger logStatement:@"ASVASTVideoPlayerView, layoutSubviews"];
    @try {
        [super layoutSubviews];
        
        self.videoView.frame = self.bounds;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}


- (CGSize)contentSize {
    if (self.videoView.hidden) {
//        [ASSDKLogger logStatement:@"ASVASTVideoPlayerView, contentSize - videoView is hidden, return original HTML ad size"];
        return self.originalHTMLAdSize;
    }
    else {
//        [ASSDKLogger logStatement:@"ASVASTVideoPlayerView, contentSize - videoView is shown, return original ad size"];
        return self.originalAdSize;
    }
}



@end

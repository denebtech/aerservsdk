//
//  ASVASTInterstitialViewController.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/25/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVASTInterstitialViewController.h"
#import "ASVASTAd.h"
#import "ASVASTVideoPlayer.h"
#import "ASVideoView.h"
#import "ASVASTCreative.h"
#import "ASInterstitialAdProvider.h"
#import "ASVASTVideoPlayerView.h"

#import "ASCommManager.h"

#import "ASCategoryHelper.h"
#import "AVPlayer+StateChange.h"

@interface ASVASTInterstitialViewController() <ASVASTVideoPlayerDelegate>

@property (nonatomic, strong) ASVASTVideoPlayer *videoPlayer;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, assign) BOOL firstAdPlayed;

@property (nonatomic, strong) AVPlayer *player;

@end

@implementation ASVASTInterstitialViewController

@synthesize closeOffset;

#pragma mark - Ad Methods

- (void)startAd {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, startAd"];
    [self.videoPlayer startAd];
}

- (void)onCloseAd {
    @try {
        [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, onCloseAd"];
        [ASCommManager sendAsynchronousRequestsForEvent:self.videoPlayer.video.skip];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:@"VAST Intersitital View Controller Fires SKIP EVENT"];
        
        [super onCloseAd];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)showOutline:(BOOL)show {
    if (self.videoPlayer.view != nil) {
        self.videoPlayer.view.layer.borderColor = [UIColor redColor].CGColor;
        self.videoPlayer.view.layer.borderWidth = show ? 2 : 0;
    }
}

- (void)setOutlineAd:(BOOL)outlineAd {
    [super setOutlineAd:outlineAd];
    
    [self showOutline:outlineAd];
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, cancel"];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - View Controller Methods

- (instancetype)initWithVASTAd:(ASVASTAd*)VASTAd delegate:(id<ASInterstitialAdViewControllerDelegate>)delegate closeOffset:(NSString*)offset {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTInterstitialViewController, initWithVASTAd:delegate:closeOffset: - offset: %@", offset]];
    if (self = [super initWithDelegate:delegate]) {
        self.adToDisplay = VASTAd;
        self.videoPlayer = [ASVASTVideoPlayer new];
        self.videoPlayer.delegate = self;
        self.closeOffset = offset;
        
        // grab the first video to play..
        [self.videoPlayer playVASTAd:VASTAd];
    }
    
    return self;
}

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, dealloc"];
    self.adToDisplay = nil;
    self.videoPlayer.delegate = nil;
    self.videoPlayer = nil;
    self.closeOffset = nil;
}

- (void)viewDidLoad {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, viewDidLoad"];
    @try {
        [super viewDidLoad];
        if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, viewDidAppear:"];
    @try {
        [super viewDidAppear:animated];
        
        // when the view is done appearing start the the ad.
        [self.videoPlayer startAd];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, viewWillDisappear:"];
    @try {
        [super viewWillDisappear:animated];
        
        // we are going to disappear.. stop the ad.
        [self.videoPlayer stopAd];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // make sure our view is always the back most view in our parent's heirarchy.
    [self.view sendSubviewToBack:self.videoPlayer.view];
    
    // make sure we are stretched to fit our parent...
    self.view.frame = self.view.superview.bounds;
    
    // lets figure out if we don't fit anymore.
    CGRect viewFrame = self.view.bounds;
    CGSize videoSize = [self.videoPlayer contentSize];
    
    videoSize.height = CGRectGetHeight(viewFrame);
    videoSize.width = CGRectGetWidth(viewFrame);
    
    // set the frame and center the video.
    self.videoPlayer.view.frame = CGRectMake(0,0,videoSize.width, videoSize.height);
    self.videoPlayer.view.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    
    [self.view layoutIfNeeded];
}

#pragma mark - Gesture Recognizer

- (void)onPlayPauseTap:(UITapGestureRecognizer *)gesture {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, onPlayPauseTap:"];
    @try {
        if (self.player != nil) {
            if(![ASCategoryHelper instance].videoPauseState) {
                [self.player changeStateToPause];
            } else {
                [self.player changeStateToPlay];
            }
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - ASVASTVideoPlayerDelegate

- (BOOL)shouldShowAd {
    return [self.delegate shouldShowInterstitialAd];
}

- (void)VASTVideoPlayerVideoIsReady:(ASVASTVideoPlayer *)videoPlayer {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTInterstitialViewController, VASTVideoPlayerVideoIsReady - firstAdPlayed: %d", self.firstAdPlayed]];
    if (!self.firstAdPlayed) {
        [self.delegate interstitialAdViewControllerCreatedAdSuccessfully:self];
    }

    [self showOutline:self.outlineAd];
    
    // add the playback view to our view when playback is ready. -- why do this when you're going to present view controller later? AZ
    [self.view addSubview:videoPlayer.view];
    [self.view sendSubviewToBack:videoPlayer.view];
    
    if (self.firstAdPlayed)
        [self.videoPlayer startAd];
    
    self.firstAdPlayed = YES;
}

- (void)VASTVideoPlayerFailedToLoad:(ASVASTVideoPlayer*)videoPlayer withErrorCode:(ASVASTErrorCode)errCode {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, VASTVideoPlayerFailedToLoad"];
    
    if (self.adToDisplay.errorEvent != nil) {
        [ASCommManager sendAsynchronousRequestsForEvent:[self.adToDisplay.errorEvent replaceErrorMacroWithCode:errCode]];
        [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Interstitial View Controller Fires Error Event from VAST Video Player with errCode: %ld", (long)errCode]];
    }
    
    [self.videoPlayer stopAd];
    if(self.videoPlayer.ad.seqAd != nil) {
       [self.videoPlayer playVASTAd:self.videoPlayer.ad.seqAd];
    } else if (self.delegate != nil) {
        [self cancel];
        [self.delegate interstitialAdViewControllerFailed:self
                                                withError:[NSError
                                                           errorWithDomain:NSStringFromClass([ASVASTInterstitialViewController class])
                                                           code:100
                                                           userInfo:@{NSLocalizedDescriptionKey : @"Unable to load ad for playback"}]];
    }
}

- (void)VASTVideoPlayerAdCompleted:(ASVASTVideoPlayer *)videoPlayer {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, VASTVideoPlayerAdCompleted"];
    [self.delegate interstitialAdViewControllerAdFinished:self];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)VASTVideoPlayer:(ASVASTVideoPlayer *)player wasClicked:(NSURL* )URL {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, VASTVideoPlayer:wasClicked:"];
    [self.delegate interstitialAdViewControllerAdWasTouched:self];
}

- (void)VASTVideoPlayerWillRewardVC:(ASVASTVideoPlayer *)player {
    [self.delegate interstitialAdViewControllerDidRewardVC:self];
}

- (UIView *)VASTVideoPlayerContainingView:(ASVASTVideoPlayer *)videoPlayer {
    return self.view;
}

- (void)AVPlayerCreated:(AVPlayer *)avPlayer {
    self.player = avPlayer;
    [self.delegate AVPlayerCreated:avPlayer];
}

- (void)VASTVideoPlayer:(ASVASTVideoPlayer *)videoPlayer willShowSkip:(BOOL)show {
    [ASSDKLogger logStatement:@"ASVASTInterstitialViewController, VASTVideoPlayer:willShowSkip:"];
}

- (void)VASTVideoPlayer:(ASVASTVideoPlayer *)videoPlayer didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:msg];
}

@end

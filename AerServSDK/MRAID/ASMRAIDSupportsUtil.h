//
//  ASMRAIDSupportsUtil.h
//  AerServSDK
//
//  Created by Albert Zhu on 9/10/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kASMRAIDSupportUtilTimeout 5.0f

#define kASMRAIDDateFormatNoTimeZoneLen 16
#define kASMRAIDDateFormatNoTimeZone @"yyyy-MM-dd'+'HH:mm"
#define kASMRAIDDateFormatNoSecondsLen 22
#define kASMRAIDDateFormatNoSeconds @"yyyy-MM-dd'T'HH:mmXXX"
#define kASMRAIDDateFormatWithSecondsLen 25
#define kASMRAIDDateFormatWithSeconds @"yyyy-MM-dd'T'HH:mm:ssXXX"

#define kASMRAIDCalendarEventIDKey @"id"
#define kASMRAIDCalendarEventDescKey @"description"
#define kASMRAIDCalendarEventLocKey @"loc"
#define kASMRAIDCalendarEventSummaryKey @"summary"
#define kASMRAIDCalendarEventStartKey @"start"
#define KASMRAIDCalendarEventEndKey @"end"
#define kASMRAIDCalendarEventStatusKey @"status" // pending, tentative, confirmed, cancelled
#define kASMRAIDCalendarEventTransparencyKey @"transparency"
#define kASMRAIDCalendarEventRecurrenceKey @"recurrence"
#define kASMRAIDCalendarEventReminderKey @"reminder"

#define kASMRAIDCalendarRecurrenceFreqKey @"frequency"
#define kASMRAIDCalendarRecurrenceIntervalKey @"interval"
#define kASMRAIDCalendarRecurrenceExpiresKey @"expires"
#define kASMRAIDCalendarRecurrenceExceptionDatesKey @"exceptionDates"
#define kASMRAIDCalendarRecurrenceDaysInWeekKey @"daysInWeek"
#define kASMRAIDCalendarRecurrenceDaysInMonthKey @"daysInMonth"
#define kASMRAIDCalendarRecurrenceDaysInYearKey @"daysInYear"
#define kASMRAIDCalendarRecurrenceWeeksInMonthKey @"weeksInMonth"
#define kASMRAIDCalendarRecurrenceMonthsInYearKey @"monthsInYear"

@class ASMRAIDView;

@interface ASMRAIDSupportsUtil : NSObject

@property (nonatomic, strong) ASMRAIDView *mainView;
@property (nonatomic, strong, readonly) AVPlayer *mraidExtPlayer;
@property (nonatomic, strong, readonly) AVPlayerLayer *mraidVideoPlayerLayer;

- (instancetype)initWithMRAID:(ASMRAIDView *)mraidView;

- (BOOL)canMakeTelephoneCalls;
- (BOOL)canSendSMSTexts;
- (void)startPhotoDownloadFrom:(NSString *)uri;
- (void)createCalendarEventWith:(NSString *)jsonProperties;
- (void)playVideoWith:(NSString *)urlStr;

- (void)initPlayerLayer;

@end

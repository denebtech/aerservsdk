//
//  YuMeInterface.m
//  YuMeiOSSampleApp
//
//  Created by Bharath Ramakrishnan on 4/5/13.
//  Copyright (c) 2014 YuMe Advertising Pvt. Ltd.,. All rights reserved.
//

#import "YuMeInterface.h"
#import "YuMeViewController.h"

@implementation YuMeInterface

@synthesize viewController;

- (id)init {
    if (self = [super init]) {
        NSLog(@"Creating YuMe SDK Instance...");
        pYuMeSDK = [YuMeSDKInterface getYuMeSdkHandle];
        viewController = nil;
    }
    return self;
}

- (void)dealloc {
    if (pYuMeSDK != nil) {
        pYuMeSDK = nil;
    }
    viewController = nil;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/*   INTERFACE (YuMeAppDelegate) TO BE IMPLEMENTED BY THE PUBLISHER APPLICATION FOR USE BY YUME SDK     */
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Listener that receives various ad events from YuMe SDK.
 * Used by the SDK to notify the application that the indicated ad event has occurred.
 * @param eAdType Ad type requested by the application that the event is related to.
 * @param eAdEvent The Ad event notified to the application.
 * @param eAdStatus The Ad status notified to the application.
 */
- (void)yumeEventListener:(YuMeAdType)eAdType adEvent:(YuMeAdEvent)eAdEvent adStatus:(YuMeAdStatus)eAdStatus {
    
    NSLog(@"%@, Ad Status: %@ (%@)", [self getAdEventStr:eAdEvent], [self getAdStatusStr:eAdStatus], [self getAdTypeStr:eAdType]);
    
    switch (eAdEvent) {
        case YuMeAdEventInitSuccess:
            NSLog(@"Initialization Successful.");
            break;
        case YuMeAdEventInitFailed:
            NSLog(@"Initialization Failed.");
            break;
        case YuMeAdEventAdReadyToPlay:
            break;
        case YuMeAdEventAdNotReady: {
            break;
        }
        case YuMeAdEventAdPlaying:
            break;
        case YuMeAdEventAdCompleted: {
            //remove the ad view
            [viewController removeAdView];
            break;
        }
        case YuMeAdEventAdClicked:
            break;
        default:
            break;
    }
}

/**
 * Gets the ad view info.
 * Used by the SDK to get the ad view info from the application.
 * @return The ad view info object.
 */
- (YuMeAdViewInfo *) yumeGetAdViewInfo {
    YuMeAdViewInfo *adViewInfo = [[YuMeAdViewInfo alloc] init];
    
    /* Publisher should pass the Ad Display view frame (x, y , width and height) through the YuMeAdViewInfo object */
    CGRect adViewFrame = [[UIScreen mainScreen] bounds];
    adViewInfo.width = adViewFrame.size.width;
    adViewInfo.height = adViewFrame.size.height;
    adViewInfo.left = adViewFrame.origin.x;
    adViewInfo.top = adViewFrame.origin.y;
    
    return adViewInfo;
}

/**
 * Gets the updated QS parameters.
 * Used by SDK to get the recently updated qs params from the application.
 * For eg: App can send the latest lat and lon through this for use by SDK.
 * @return The set of key value pairs delimited by '&'.
 */
-  (NSString *) yumeGetUpdatedQSParams {
    return nil;
}

/** Internal functions that interfaces with YuMe SDK APIs */

/*
 * Initializes the YuMe SDK.
 * @param configId The Config Id based on which YuMe SDK will fetch the congifuration from server.
 * @param pAdParams A pointer to an object with ad parameters.
 * @return TRUE, if operation is successful, else FALSE.
 */
- (BOOL) yumeInit:(NSString *)configId adParams:(YuMeAdParams *)pAdParams {
    if (pYuMeSDK) {
        NSError *pError = nil;
        
        //The configId and adParams are mandatory. If config fetching based on configId fails for some reason, then the adParams is used
        BOOL bResult = [pYuMeSDK yumeSdkInit:configId adParams:pAdParams appDelegate:self errorInfo:&pError];
        if ( (!bResult) && (pError) ) {
            [viewController displayBox:[viewController getErrDesc:pError] withButtonText:@"OK"];
        } else {
            //NSLog(@"Initialization Successful.");
        }
        return bResult;
    }
    return FALSE;
}

/*
 * De-Initializes the YuMe SDK.
 * @return TRUE, if operation is successful, else FALSE.
 */
- (BOOL) yumeDeInit {
    if (pYuMeSDK) {
        NSError *pError = nil;
        BOOL bResult = [pYuMeSDK yumeSdkDeInit:&pError];
        if ( (!bResult) && (pError) )
            [viewController displayBox:[viewController getErrDesc:pError] withButtonText:@"OK"];
        else
            NSLog(@"De-Initialization Successful.");
        return bResult;
    }
    return FALSE;
}

/**
 * Initiates the YuMe SDK to fetch an ad.
 * @return TRUE, if operation is successful, else FALSE.
 * If FALSE is returned, the pError object will contain appropriate error info.
 */
- (BOOL) yumeInitAd {
    if (pYuMeSDK) {
        NSError *pError = nil;
        BOOL bResult = [pYuMeSDK yumeSdkInitAd:&pError];
        if ( (!bResult) && (pError) )
            [viewController displayBox:[viewController getErrDesc:pError] withButtonText:@"OK"];
        return bResult;
    }
    return FALSE;
}

/**
 * Initiates the playback of a YuMe Ad.
 * @param pAdView The UIView inside which the Ad will be played.
 * @param pAdViewController The UIViewController inside which the Ad will be played.
 * @return TRUE, if operation is successful, else FALSE.
 * If FALSE is returned, the pError object will contain appropriate error info.
 */
- (BOOL) yumeShowAd:(UIView *)pAdView viewController:(UIViewController *)pAdViewController {
    if (pYuMeSDK) {
        NSError *pError = nil;
        BOOL bResult = [pYuMeSDK yumeSdkShowAd:pAdView viewController:pAdViewController errorInfo:&pError];
        if ( (!bResult) && (pError) ) {
            [viewController removeAdView];
            [viewController displayBox:[viewController getErrDesc:pError] withButtonText:@"OK"];
        }
        return bResult;
    }
    return FALSE;
}

/**
 * Checks if any ad is available in YuMe SDK for playing.
 * @return YES, if an ad is available for playing, else NO.
 */
- (BOOL) yumeIsAdAvailable {
    if (pYuMeSDK) {
        NSError *pError = nil;
        BOOL bResult = [pYuMeSDK yumeSdkIsAdAvailable:&pError];
        if ( (!bResult) && (pError) ) {
            [viewController displayBox:[viewController getErrDesc:pError] withButtonText:@"OK"];
        }
        return bResult;
    }
    return FALSE;
}

/*
 * Gets the YuMe SDK Version.
 * @return The YuMe SDK Version.
 */
- (NSString *) yumeGetVersion {
    NSString *sdkVersion = [YuMeSDKInterface getYuMeSdkVersion];
    return sdkVersion;
}

/*
 * Gets the YuMe SDK handle.
 * @return The YuMe SDK handle.
 */
- (YuMeSDKInterface *)getYuMeSDKHandle {
    return pYuMeSDK;
}

/**
 * Gets the Ad Type String from Ad Type enum.
 * @param adType The Ad Type enum value.
 * @return The Ad Type String.
 */
- (NSString *)getAdTypeStr:(YuMeAdType)adType {
    switch (adType) {
        case YuMeAdTypePreroll:
            return @"PREROLL";
        case YuMeAdTypeMidroll:
            return @"MIDROLL";
        case YuMeAdTypePostroll:
            return @"POSTROLL";
        case YuMeAdTypeNone:
        default:
            return @"NONE";
    }
}

/**
 * Gets the Ad Event String from Ad Event enum.
 * @param adEvent The Ad Event enum value.
 * @return The Ad Event String.
 */
- (NSString *)getAdEventStr:(YuMeAdEvent)adEvent {
    switch (adEvent) {
        case YuMeAdEventInitSuccess:
            return @"INIT_SUCCESS";
        case YuMeAdEventInitFailed:
            return @"INIT_FAILED";
        case YuMeAdEventAdReadyToPlay:
            return @"AD_READY_TO_PLAY";
        case YuMeAdEventAdNotReady:
            return @"AD_NOT_READY";
        case YuMeAdEventAdPlaying:
            return @"AD_PLAYING";
        case YuMeAdEventAdCompleted:
            return @"AD_COMPLETED";
        case YuMeAdEventAdClicked:
            return @"AD_CLICKED";
        case YuMeAdEventNone:
        default:
            return @"NONE";
    }
}

/**
 * Gets the Ad Status String from Ad Status enum.
 * @param adStatus The Ad Status enum value.
 * @return The Ad Status String.
 */
- (NSString *)getAdStatusStr:(YuMeAdStatus)adStatus {
    switch (adStatus) {
        case YuMeAdStatusRequestFailed:
            return @"REQUEST_FAILED";
        case YuMeAdStatusRequestTimedOut:
            return @"REQUEST_TIMED_OUT";
        case YuMeAdStatusCachingFailed:
            return @"CACHING_FAILED";
        case YuMeAdStatusCachedAdExpired:
            return @"CACHED_AD_EXPIRED";
        case YuMeAdStatusEmptyAdInCache:
            return @"EMPTY_AD_IN_CACHE";
        case YuMeAdStatusCachingInProgress:
            return @"CACHING_IN_PROGRESS";
        case YuMeAdStatusPlaybackSuccess:
            return @"PLAYBACK_SUCCESS";
        case YuMeAdStatusPlaybackTimedOut:
            return @"PLAYBACK_TIMED_OUT";
        case YuMeAdStatusPlaybackFailed:
            return @"PLAYBACK_FAILED";
        case YuMeAdStatusPlaybackInterrupted:
            return @"PLAYBACK_INTERRUPTED";
        case YuMeAdStatusNone:
        default:
            return @"NONE";;
    }
}

@end //implementation YuMeInterface

//
//  ASChartboostInterstitialAdProvider.h
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASCustomInterstitialAdProvider.h"
#import <Foundation/Foundation.h>

@interface ASChartboostInterstitialAdProvider : ASCustomInterstitialAdProvider

@property (nonatomic, assign) BOOL failedToReceiveAd;
@property (nonatomic, assign) BOOL didPlayVideo;

@end

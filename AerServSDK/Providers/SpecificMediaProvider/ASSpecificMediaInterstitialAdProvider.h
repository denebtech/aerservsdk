//
//  SpecificMediaProvider.h
//  SpecificMediaProvider
//
//  Created by Gauthier on 7/28/14.
//  Copyright (c) 2014 AerServ. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ASInterstitialAdProvider.h"

@interface ASSpecificMediaInterstitialAdProvider : ASInterstitialAdProvider

@end

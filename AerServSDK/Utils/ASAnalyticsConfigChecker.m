//
//  ASAnalyticsConfigChecker.m
//  AerServSDK
//
//  Created by Vasyl Savka on 10/30/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASAnalyticsConfigChecker.h"
#import "ASMutableURLRequest.h"
//#import "ASGlobalProviderSetup.h"
#import "ASCommManager.h"

@interface ASAnalyticsConfigChecker()

@property (nonatomic, assign) BOOL didFinishPost;

@end

@implementation ASAnalyticsConfigChecker

+ (ASAnalyticsConfigChecker *)checker {
    static ASAnalyticsConfigChecker *checker;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        checker = [ASAnalyticsConfigChecker new];
        checker.disbaledEventsArr = nil;
        checker.analyticsEnabled = NO;
    });
    
    return checker;
}

#pragma mark - Config Checker Helper Methds

+ (NSData *)createJSONDataWithPlacements:(NSArray *)plcArr orSiteID:(NSString *)siteIdStr {
    NSDictionary *jsonDataObj;
    if(plcArr != nil && plcArr.count > 0) {
        jsonDataObj= @{ kApplicationKey : kTVOSSDKKey,
                        kVersionKey : kAerServSDKVersion,
                        kPlacementKey : plcArr };
    } else if(siteIdStr != nil && ![siteIdStr isEqualToString:@""]) {
        jsonDataObj = @{ kApplicationKey : kTVOSSDKKey,
                         kVersionKey : kAerServSDKVersion,
                         kSiteIDKey : [NSNumber numberWithLongLong:[siteIdStr longLongValue]] };
    } else {
        jsonDataObj = @{ kApplicationKey : kTVOSSDKKey,
                         kVersionKey : kAerServSDKVersion };
    }
    
    NSError *err = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDataObj options:0 error:&err];
    return jsonData;
}

- (void)readConfigObjWithData:(NSData *)postData {
    [ASSDKLogger logStatement:@"ASAnalyticsConfigChecker, readConfigObj"];
    if(!self.configJSONObj &&  postData != nil && postData.length > 0) {
        NSError *err = nil;
        self.configJSONObj = [NSJSONSerialization JSONObjectWithData:postData options:NSJSONReadingMutableLeaves error:&err];
        if(err != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAnalyticsConfigChecker, readConfigObj - JSONSerialization ERROR: %@", err.localizedDescription]];
        }
    }
    
    if(self.configJSONObj != nil) {
        NSDictionary *analyticsObj = self.configJSONObj[kAnalyticsKey];
        self.analyticsEnabled = [analyticsObj[kEnabledKey] boolValue];
        if(analyticsObj[kDisabledEventsKey] != nil && [analyticsObj[kDisabledEventsKey] isKindOfClass:[NSArray class]]) {
            self.disbaledEventsArr = [NSArray arrayWithArray:(NSArray *)analyticsObj[kDisabledEventsKey]];
        }
        
//        // init mediation adapters with configuration data
//        __weak ASAnalyticsConfigChecker *configChecker = self;
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//            @try {
//                [ASGlobalProviderSetup initWithAdapterConfig:[NSArray arrayWithArray:(NSArray *)configChecker.configJSONObj[kAdapterConfigKey]]];
//            }
//            @catch (NSException *exception) {
//                [ASSDKLogger onException:exception];
//            }
//        });
        
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAnalyticsConfigChecker, readConfigObj - analyticsEnabled: %d, disabledEventArr: %@", self.analyticsEnabled, self.disbaledEventsArr]];
    }
}

- (void)finishPOSTWithData:(NSData *)postData {
    __weak ASAnalyticsConfigChecker *checker = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        @try {
            if(!checker.didFinishPost) {
                [checker readConfigObjWithData:postData];
                checker.didFinishPost = YES;
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
}

#pragma mark - Config Check Methods

+ (void)checkAnalyticsConfigWithPlacements:(NSArray *)plcArr orSiteId:(NSString *)siteIdStr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAnalyticsConfigChecker, checkAnalyticsConfigWithPlacements:orSiteId: - plcArr: %@ | siteIdStr: %@", plcArr, siteIdStr]];

    // if we have already have config data, return
    if([ASAnalyticsConfigChecker checker].configJSONObj != nil) return;
    
    __weak ASAnalyticsConfigChecker *checker = [ASAnalyticsConfigChecker checker];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        @try {
            checker.didFinishPost = NO;
            
            // send post and check response
            [ASCommManager sendPostRequestToEndPoint:kAnalyticsConfigEndPoint
                                        withJSONData:[self createJSONDataWithPlacements:plcArr orSiteID:siteIdStr]
                                             forTime:kASConfigConnTimeout
                              endWithCompletionBlock:^(NSURLResponse *resp, NSData *data, NSError *err) {
                                  @try {
                                      if(!err) {
                                          [checker finishPOSTWithData:data];
                                      } else {
                                          [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAnalyticsConfigChecker, checkAnalyticsConfigWithPlacements:orSiteId: - ERROR: %@", err.localizedDescription]];
                                          checker.configJSONObj = nil;
                                      }
                                  }
                                  @catch (NSException *exception) {
                                      [ASSDKLogger onException:exception];
                                      checker.configJSONObj = nil;
                                  }
                              }
            ];
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
}

+ (BOOL)checkDisblaedEventsforCategory:(NSString *)cat andAction:(NSString *)act {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAnalyticsConfigChecker, checkDisblaedEventsforCategory:andAction: - cat: %@, act: %@", cat, act]];
    
    BOOL disabled = NO;
    for(NSDictionary *disbaledEventObj in [self checker].disbaledEventsArr) {
        if([disbaledEventObj[kCategoryKey] isEqualToString:cat] &&
           [disbaledEventObj[kActionKey] isEqualToString:act]) {
            disabled = YES;
        }
    }
    
    return disabled;
}

@end

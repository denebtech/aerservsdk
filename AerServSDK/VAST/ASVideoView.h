//
//  ASVideoView.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/31/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASVideoView : UIView

- (AVPlayerLayer *)playerLayer;
- (void)attachPlayer:(AVPlayer*)player;
- (void)changeFrame:(CGRect)frame;

@end

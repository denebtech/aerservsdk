//
//  ASBannerAdProvider.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/14/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASBannerAdProvider.h"

const NSString *ASBannerParameter_Parameters = @"ASBannerParameter_Parameters";
const NSString *ASBannerParameter_HTMLHeaders = @"ASBannerParameter_HTMLHeaders";
const NSString *ASBannerParameter_HTMLData = @"ASBannerParameter_HTMLData";
const NSString *ASBannerParameter_Location = @"ASBannerParameter_Location";
const NSString *ASBannerParameter_Parameter_URL = @"url";

@implementation ASBannerAdProvider

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASBannerAdProvider, dealloc"];
    [_refreshTimer invalidate];
    _refreshTimer = nil;
}

- (void)requestAdWithSize:(CGSize)size withProperties:(NSDictionary *)properties isPreload:(BOOL)preload {
    [ASSDKLogger logStatement:@"ASBannerAdProvider, requestAdWithSize:withProperties:isPreload: -- does nothing"];
}

- (void)startPreloadedBannerAd {
    [ASSDKLogger logStatement:@"ASBannerAdProvider, startPreloadedBannerAd -- does nothing"];
}

- (BOOL)enableAutomaticImpressionAndClickTracking {
    [ASSDKLogger logStatement:@"ASBannerAdProvider, enableAutomaticImpressionAndClickTracking -- does nothing"];
    return YES;
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASBannerAdProvider, cancel"];
    [self killRefreshTimer];
}

#pragma mark - Refresh Timer Methods

- (void)stopAutomaticallyRefreshingAds {
    [ASSDKLogger logStatement:@"ASBannerAdProvider, stopAutomaticallyRefreshingAds"];
    self.automaticallyRefreshAds = NO;
    [self killRefreshTimer];
}

- (void)startAutomaticallyRefreshingAds {
    [ASSDKLogger logStatement:@"ASBannerAdProvider, startAutomaticallyRefreshingAds"];
    self.automaticallyRefreshAds = YES;
    [self scheduleRefresh];
}

- (void)killRefreshTimer {
    // if we have a refresh timer invalidate and kill it.
    if (self.refreshTimer != nil) {
        [ASSDKLogger logStatement:@"ASBannerAdProvider, killRefreshTimer"];
        [self.refreshTimer invalidate];
        self.refreshTimer = nil;
    }
}

- (void)scheduleRefresh {
    [self killRefreshTimer];
    if (self.automaticallyRefreshAds && self.bannerRefreshTimeInterval != kBannerRefreshNeverTokenValue) {
        // choose between a defined time interval that's greater than 10 seconds, or the default time interval
        NSTimeInterval refreshTimeInt = (self.bannerRefreshTimeInterval >= 10) ? self.bannerRefreshTimeInterval : kBannerRefreshDefaultInterval;
        
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASBannerAdProvider, scheduleRefresh - Scheduling banner refresh: %0.2f", refreshTimeInt]];
        self.refreshTimer = [NSTimer scheduledTimerWithTimeInterval:refreshTimeInt
                                                             target:self
                                                           selector:@selector(refreshTimerDidFire)
                                                           userInfo:nil
                                                            repeats:NO];
    } else {
        [ASSDKLogger logStatement:@"ASBannerAdProvider, scheduleRefresh - No refresh time defined"];
    }
}

- (void)refreshTimerDidFire {
    @try {
        [ASSDKLogger logStatement:@"ASBannerAdProvider, refereshTimerDidFire - Refreshing banner ad"];
        
        if (self.automaticallyRefreshAds)
            [self.delegate bannerProviderWillAttemptRefresh:self];
        else
            [self killRefreshTimer];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

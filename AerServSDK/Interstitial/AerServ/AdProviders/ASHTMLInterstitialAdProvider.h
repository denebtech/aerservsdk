//
//  ASHTMLInterstialAdProvider.h
//  AerServSDK
//
//  Created by Scott A Andrew on 4/9/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInternalBaseInterstitialAdProvider.h"
#import "ASInterstitialAdViewController.h"

@interface ASHTMLInterstitialAdProvider : ASInternalBaseInterstitialAdProvider

- (BOOL)processData:(NSData*)data withResponseHeaders:(NSDictionary*)headers;

@end

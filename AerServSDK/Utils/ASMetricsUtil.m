//
//  ASMetricsUtil.m
//  AerServSDK
//
//  Created by Vasyl Savka on 6/20/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASMetricsUtil.h"
#import "ASVASTEvent.h"
#import "ASCommManager.h"

@interface ASMetricsUtil ()

@property (nonatomic, strong) ASVASTEvent *preloadReadyEvent;
@property (nonatomic, strong) ASVASTEvent *showAdAttemptEvent;

@end

@implementation ASMetricsUtil

- (void)dealloc {
    _delegate = nil;
    _preloadReadyEvent = nil;
    _showAdAttemptEvent = nil;
}

- (instancetype)initWithMetricsObj:(NSDictionary *)metrics withDelegate:(id<ASMetricsDelegate>)delegate {
    if(self = [super init]) {
        if(metrics != nil && [metrics isKindOfClass:[NSDictionary class]]) {
            _delegate = delegate;
            
            if(metrics[kPreloadUrlKey] && [metrics[kPreloadUrlKey] isKindOfClass:[NSString class]]) {
                _preloadReadyEvent = [ASVASTEvent new];
                _preloadReadyEvent.type = ASVASTEventType_PreloadReady;
                _preloadReadyEvent.canBeSent = YES;
                _preloadReadyEvent.oneTime = YES;
                [_preloadReadyEvent addURL:[NSURL URLWithString:metrics[kPreloadUrlKey]]];
            }
            
            if(metrics[kShowAttemptUrlKey] && [metrics[kShowAttemptUrlKey] isKindOfClass:[NSString class]]) {
                _showAdAttemptEvent = [ASVASTEvent new];
                _showAdAttemptEvent.type = ASVASTEventType_ShowAttempt;
                _showAdAttemptEvent.canBeSent = YES;
                _showAdAttemptEvent.oneTime = YES;
                [_showAdAttemptEvent addURL:[NSURL URLWithString:metrics[kShowAttemptUrlKey]]];
            }
        }
    }
    
    return self;
}

+ (instancetype)utilWithMetricsObj:(NSDictionary *)metrics withDelegate:(id<ASMetricsDelegate>)delegate {
    return [[ASMetricsUtil alloc] initWithMetricsObj:metrics withDelegate:delegate];
}

- (void)firePreloadReadyEvent {
    [ASCommManager sendAsynchronousRequestsForEvent:self.preloadReadyEvent];
    [self.delegate metrics:self didFireAdvertiserEventWithMessage:@"Metrics Utility Fires Preload Ready Event"];
}

- (void)fireShowAttemptEvent {
    [ASCommManager sendAsynchronousRequestsForEvent:self.showAdAttemptEvent];
    [self.delegate metrics:self didFireAdvertiserEventWithMessage:@"Metrics Utility Fires Show Attempt Event"];
}

@end

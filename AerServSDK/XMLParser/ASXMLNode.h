//
//  ASXMLNode.h
//  XMLQueryTester
//
//  Created by Vasyl Savka on 3/27/14.
//  Copyright (c) 2014 New Wave Dgital Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASXMLNode : NSObject

@property (nonatomic, readonly, copy) NSString *name;
@property (nonatomic, readonly, copy) NSString *value;
@property (nonatomic, readonly, copy) NSDictionary *attributes;
@property (nonatomic, readonly, copy) NSArray *children;

- (BOOL)validValue;
- (instancetype)initWithDictionary:(NSDictionary *)nodeData;
- (ASXMLNode *)childNodeNamed:(NSString *)name;
- (NSArray *)childrenNodesNamed:(NSString *)name;

@end

//
//  ASWebBrowserController.h
//  AerServSDK
//
//  Created by Scott A Andrew on 4/16/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ASWebBrowserControllerDelegate;

@interface ASWebBrowserController : UIViewController

@property (nonatomic, weak) id<ASWebBrowserControllerDelegate> delegate;

- (void)loadRequest:(NSURLRequest *)request;

@end

@protocol ASWebBrowserControllerDelegate<NSObject>

- (void)webBrowserControllerWasDismissed:(ASWebBrowserController*)browser;

@end
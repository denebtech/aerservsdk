//
//  ASMillennialInterstitialAdProvider.m
//  AerServSDK
//
//  Created by Albert Zhu on 5/13/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASMillennialInterstitialAdProvider.h"

#import "MMAdSdk.h"

@interface ASMillennialInterstitialAdProvider () <MMInterstitialDelegate>

@property (nonatomic, strong) id mmSdkInstance;
@property (nonatomic, strong) id mmInterstitialAd;

@property (nonatomic, assign) BOOL adInitialized;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;

@end



@implementation ASMillennialInterstitialAdProvider

- (void)dealloc {
    [self cleanUp];
}

- (instancetype)init {
    if(self = [super initWithAdClassName:kMMInterstitialAdClassStr timeout:kCustomInterstitialTimeout]) { }
    
    return self;
}

- (void)cleanUp {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, cleanUp"];
    [self.mmInterstitialAd setDelegate:nil];
    self.mmInterstitialAd = nil;
    self.mmSdkInstance = nil;
}

#pragma mark - ASCustomInterstitialAdProvider Overloaded Methods

- (void)initializePartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, initializePartnerAd:"];
    Class mmSdkClass = NSClassFromString(kMMSDKClassStr);
    self.mmSdkInstance = [mmSdkClass sharedInstance];
    [self.mmSdkInstance initializeWithSettings:nil withUserSettings:nil];
    self.adInitialized = YES;
}

- (BOOL)hasPartnerAdInitialized {
    return self.adInitialized;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return !self.adInitialized;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    self.adLoaded = NO;
    self.adFailed = NO;
    
    NSDictionary *mediationProperties = properties[ASInterstitialParameter_Parameters];   
    NSString *placementId = [NSString stringWithString:mediationProperties[kMMPlacementIdKey]];
    Class mmInterstitalAdClass = NSClassFromString(kMMInterstitialAdClassStr);
    self.mmInterstitialAd = [[mmInterstitalAdClass alloc] initWithPlacementId:placementId];
    [self.mmInterstitialAd setDelegate:self];
    [self.mmInterstitialAd load:nil];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMillennialInterstitialAdProvider, loadPartnerAd: - placementId: %@", placementId]];
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, showPartnerAd:"];
    if([self.mmInterstitialAd ready]) {
        [self.mmInterstitialAd showFromViewController:rootViewController];
    }
}

- (void)terminatePartnerAd {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, terminatePartnerAd:"];
    [self cleanUp];
}

#pragma mark - MMInterstitialDelegate Methods

-(void)interstitialAdLoadDidSucceed:(MMInterstitialAd*)ad {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, interstitialAdLoadDidSucceed:"];
    self.adLoaded = YES;
    [self asPartnerInterstitialAdDidLoad:self.mmInterstitialAd];
}

-(void)interstitialAd:(MMInterstitialAd*)ad loadDidFailWithError:(NSError*)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMillennialInterstitialAdProvider, interstitialAd:loadDidFailWithError: - error: %@", error.localizedDescription]];
    self.adFailed = YES;
    [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
}

-(void)interstitialAdWillDisplay:(MMInterstitialAd*)ad {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, interstitialAdWillDisplay:"];
    [self asPartnerInterstitialAdWillAppear];
}

-(void)interstitialAdDidDisplay:(MMInterstitialAd*)ad {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, interstitialAdDidDisplay:"];
    [self asPartnerInterstitialAdDidAppear]; 
}

-(void)interstitialAd:(MMInterstitialAd*)ad showDidFailWithError:(NSError*)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMillennialInterstitialAdProvider, interstitialAd:showDidFailWithError: - error: %@", error.localizedDescription]];
    self.adFailed = YES;
    [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
}

-(void)interstitialAdWillDismiss:(MMInterstitialAd*)ad {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, interstitialAdWillDismiss:"];
    [self asPartnerInterstitialAdWillDisappear];
}

-(void)interstitialAdDidDismiss:(MMInterstitialAd*)ad {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, interstitialAdWillDismiss:"];
    [self asPartnerInterstitialAdDidDisappear];
}

-(void)interstitialAdDidExpire:(MMInterstitialAd*)ad {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, interstitialAdDidExpire:"];
    [self.mmInterstitialAd setDelegate:nil];
    self.mmInterstitialAd = nil;
    [self loadPartnerAd:self.properties];
}

-(void)interstitialAdTapped:(MMInterstitialAd*)ad {
    [ASSDKLogger logStatement:@"ASMillennialInterstitialAdProvider, interstitialAdWasTouched:"];
    [self asPartnerInterstitialAdWasTouched];
}

-(void)interstitialAdWillLeaveApplication:(MMInterstitialAd*)ad { }

@end

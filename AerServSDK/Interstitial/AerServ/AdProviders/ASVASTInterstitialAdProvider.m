//
//  ASVastInsterstitalAdProvider.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/9/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVASTInterstitialAdProvider.h"
#import "ASVASTInterstitialViewController.h"
//#import "ASVPAIDInterstitialViewController.h"
#import "ASVASTAd.h"
#import "ASVASTContainer.h"
#import "ASVASTCreative.h"
#import "ASVASTParser.h"

#import "ASCommManager.h"

@interface ASVASTInterstitialAdProvider()<ASVASTAdDelegate>

@property (nonatomic, strong) ASVASTAd* ad;

@end



@implementation ASVASTInterstitialAdProvider

- (void)dealloc {
    self.ad = nil;
}

- (BOOL)processData:(NSData*)data withResponseHeaders:(NSDictionary *)headers {
    if ([data length] <= 1)
        return NO;
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTInterstitialAdProvider, processData:withResponseHeaders: - data.length: %lu", (unsigned long)data.length]];

    ASVASTContainer *container = [ASVASTParser findAdWithXMLData:data withDelegate:self asPreload:self.isPreload];
    
    // if there is no ad then return NO.. Other wise lets try to show the video.
    if(container.vastAd == nil && !container.didFailoverAlready)
        return NO;
    
    if(container.vastAd.isWrapper) {
        [ASSDKLogger logStatement:@"ASVASTInterstitialAdProvider, processData:withResponseHeaders: - VAST Ad is wrapper"];
        [container.vastAd resolveWrappedAd];
    }
    
    self.ad = container.vastAd;
    
    return YES;
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASVASTInterstitialAdProvider, cancel"];
    
    [self.viewControllerToPresent cancel];
}

#pragma mark - ASVASTAdDelegate Methods

- (void)vastAdDidLoad:(ASVASTAd *)ad {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTInterstitialAdProvider, vastAdDidLoad - ad.creatives.count: %lu", (unsigned long)ad.creatives.count]];
    
    // checking for vpaid creative
    BOOL isVpaidAd = NO;
    for(ASVASTCreative *creative in ad.creatives) {
        if(creative.isVpaid) {
            [ASSDKLogger logStatement:@"ASVASTInterstitialAdProvider, vastAdDidLoad - Found VPAID Ad!"];
            isVpaidAd = YES;
        }
    }
    
     __weak ASVASTInterstitialAdProvider* adProvider = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if(ad != nil) {
                if(isVpaidAd) {
//                    [ASSDKLogger logStatement:@"ASVASTInterstitialAdProvider, vastAdDidLoad - Init ASVPAIDInterstitialViewController"];
//                    adProvider.viewControllerToPresent = [[ASVPAIDInterstitialViewController alloc] initWithVASTAd:ad delegate:adProvider closeOffset:[adProvider.delegate closeOffset]];
                } else {
                    [ASSDKLogger logStatement:@"ASVASTInterstitialAdProvider, vastAdDidLoad - Init ASVASTInterstitialViewController"];
                    adProvider.viewControllerToPresent = [[ASVASTInterstitialViewController alloc] initWithVASTAd:ad delegate:adProvider closeOffset:[adProvider.delegate closeOffset]];
                }
                
                // if the provider has no view controller to present report an error.
                if(adProvider != nil && !adProvider.viewControllerToPresent) {
                    [ASSDKLogger logStatement:@"ASVASTInterstitialAdProvider, vastAdDidLoad - Failed to init viewControllerToPresent"];
                    [adProvider.delegate interstitialAdProvider:adProvider
                                       didFailToLoadAdWithError:[NSError errorWithDomain:NSStringFromClass([ASVASTInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Failed to create view controller for vast ad."}]];
                }
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
}

- (void)vastAdDidFailToLoad:(ASVASTAd *)ad withError:(NSError *)error andVastErrorCode:(ASVASTErrorCode)errCode {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTInterstitialAdProvider, vastAdDidFailToLoad:withError:andVastErrorCode: - error: %@ & VASTErrorCode: %ld", error.localizedDescription, (long)errCode]];
    
    if(errCode != kVASTErrorCodeNoERROR) {
        [ASCommManager sendAsynchronousRequestsForEvent:[self.ad.errorEvent replaceErrorMacroWithCode:errCode]];
        [self.delegate interstitialAdProvider:self didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Interstitial Provider Fires Error Event from VAST Ad with errCode: %ld", (long)errCode]];
    }
    
    [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
}

- (void)vastAd:(ASVASTAd *)vastAd didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate interstitialAdProvider:self didFireAdvertiserEventWithMessage:msg];
}

@end

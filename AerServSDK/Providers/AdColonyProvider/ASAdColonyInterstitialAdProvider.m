//
//  ASAdColonyInterstitialProvider.m
//  AerServSDK
//
//  Created by Scott A Andrew on 4/11/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASAdColonyInterstitialAdProvider.h"
#import "ASAdColonyDelegateProxy.h"
#import "ASAdColonyInstance.h"

#import "AdColony.h"
#import "ASVirtualCurrency.h"

@interface ASAdColonyInterstitialAdProvider()<AdColonyDelegate, AdColonyAdDelegate>

@property (nonatomic, assign) BOOL zoneAvailable;
@property (nonatomic, assign) BOOL preload;
@property (nonatomic, assign) BOOL adShown;

@property (nonatomic, strong) NSTimer* timeoutTimer;
@property (nonatomic, assign) NSTimeInterval adcTimeoutInt;
@property (nonatomic, strong) NSDictionary *properties;
@property (nonatomic, strong) NSDictionary *vcObj;

@end



@implementation ASAdColonyInterstitialAdProvider

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, dealloc"];
    [self.timeoutTimer invalidate];
    self.timeoutTimer = nil;
}

#pragma mark - Interstitial Ad Provider Methods

- (void)requestInterstitialAdWithProperties:(NSDictionary*)info isPreload:(BOOL)preload {
    self.properties = info;
    self.preload = preload;
    
    if(![ASAdColonyInstance instance].adColonyClass || [ASAdColonyInstance instance].state == kASProviderSetupNone) {
        [ASAdColonyInstance instance].adColonyClass = NSClassFromString(kAdColonyClassStr);
        
        if( ![ASAdColonyInstance instance].adColonyClass ) {
            [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, requestInterstitialAdProviderWithProperties:isPreload: - Could not create AdColony class object, failing over"];
            NSError* error = [NSError errorWithDomain:NSStringFromClass([ASAdColonyInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Could not link against AdColony.  Failing over"}];
            
            [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
            
            return;
        }
    }
    
    // in our dictionary should be our appID and our zoneIDs.
    NSDictionary* params = info[ASInterstitialParameter_Parameters];
    NSError* error;
    
    if (params[kAdColonyAppIDParamsKey] == nil) {
        error = [NSError errorWithDomain:NSStringFromClass([ASAdColonyInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"AdColony, Invalid AppID"}];
        [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, requestInterstitailAdWithProperties:isPreload: - Invalid AppID Error"];
    }
    
   
    if (params[kAdColonyZoneIDsParamsKey] == nil) {
        error = [NSError errorWithDomain:NSStringFromClass([ASAdColonyInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"AdColony, No Zones"}];
        [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, requestInterstitailAdWithProperties:isPreload: - No Zone Error"];
    }
    if (error != nil) {
        [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
        return;
    }
    
    // instantiate and configure the adcolony sdk once, add this provider to the shared proxy
    [[ASAdColonyInstance instance].adColonyClass configureWithAppID:params[kAdColonyAppIDParamsKey]
                                                            zoneIDs:params[kAdColonyZoneIDsParamsKey]
                                                           delegate:[ASAdColonyDelegateProxy sharedProxy]
                                                            logging:YES];
    [[ASAdColonyDelegateProxy sharedProxy] addProvider:self];
    
    // grab vcObj, if it exists fire VC loaded
    self.vcObj = [ASVirtualCurrency getVirtualCurrencyData:info[ASInterstitialParameter_HTMLHeaders]];
    if(self.vcObj != nil)
        [self.delegate interstitialAdProvider:self didVirtualCurrencyLoad:self.vcObj];
    
    // check if there are any available zone ids
    [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, requestInterstitailAdWithProperties:isPreload: - Checking for available zone ids"];
    [ASAdColonyInstance instance].zoneID = [ASAdColonyInstance findAvailableZoneIDFromZoneIDList:params[kAdColonyZoneIDsParamsKey]];
    self.zoneAvailable = NO;
    if([[ASAdColonyInstance instance].adColonyClass zoneStatusForZone:[ASAdColonyInstance instance].zoneID] == ADCOLONY_ZONE_STATUS_ACTIVE)
    {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonyInterstitialAdProvider, requestInterstitialAdWithProperties - AdColony zone %@ available", [ASAdColonyInstance instance].zoneID]];
        [self zoneBecameActive];
    } else {
        [self createAdColonyTimeoutTimer];
    }
}

- (void)showInterstitialFromRootViewController:(UIViewController*)rootViewController {
    [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, showInterstitialFromRootViewController"];
    
    [[ASAdColonyInstance instance].adColonyClass playVideoAdForZone:[ASAdColonyInstance instance].zoneID withDelegate:[ASAdColonyDelegateProxy sharedProxy]];
    [self.delegate interstitialAdProviderWillAppear:self];
}

#pragma mark - AdColony Timeout Methods

- (void)createAdColonyTimeoutTimer {
    self.adcTimeoutInt = self.timeoutInt/2;
    self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:self.adcTimeoutInt target:self selector:@selector(adTimedOut) userInfo:nil repeats:NO];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonyInterstitialAdProvider, createAdColonyTimeoutTimer - Timeout Timer started with adcTimeoutInt: %0.2f", self.adcTimeoutInt]];
}

- (void)removeTimer {
    if (self.timeoutTimer != nil) {
        [self.timeoutTimer invalidate];
        self.timeoutTimer = nil;
    }
}

- (void)adTimedOut {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonyInterstitialAdProvider, adTimedOut - adcTimeoutInt: %0.2f", self.adcTimeoutInt]];
    @try {
        [self invalidate];
        NSError* error = [NSError errorWithDomain:NSStringFromClass([ASAdColonyInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"AdColony timed out."}];
        [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)invalidate {
    // cleanup the timer if needed
    [self removeTimer];
}

#pragma mark - AdColony Zone Avability Methods

- (void)zoneBecameActive {
    // remove the timeout timer.. We still need the proxy to listen on our behalf.
    [self removeTimer];
    
    if(self.preload) {
        [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, zoneBecameActive - Preload"];
        [self.delegate interstitialAdProvider:self didPreloadAd:nil];
    } else {
        [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, zoneBecameActive"];
        [self.delegate interstitialAdProvider:self didLoadAd:nil];
    }
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
    
    self.zoneAvailable = YES;
}

- (void)onZoneUnavailable {
    [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, onZoneUnavailable"];
    [self invalidate];
    
    if(!self.adShown) {
        NSError* error = [NSError errorWithDomain:NSStringFromClass([ASAdColonyInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Adcolony zone unavailable.  Failing over"}];
        [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
    }
}

- (void)zoneDidExpire {
    [ASSDKLogger logStatement:@"ASAdColonyInterstitialAdProvider, zoneDidExpire"];
    self.zoneAvailable = NO;
    [self.delegate interstitialAdProviderDidExpire:self];
    [self invalidate];
}

#pragma mark - AdColonyAdDelegateProxy Methods

- (void)onAdColonyAdStartedInZone:(NSString *)zoneID {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonlyInterstitialAdProvider, onAdColonyAdStartedInZone - AdColony zone %@ started", zoneID]];
    @try {
        [self.delegate interstitialAdProviderDidAppear:self];
        self.adShown = YES;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onAdColonyAdAttemptFinished:(BOOL)shown inZone:(NSString *)zoneID {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonlyInterstitialAdProvider, onAdColonyAdAttemptFinished:inZone: - AdColony zone %@ finished", zoneID]];
    @try {
        if (shown) {
            [self.delegate interstitialAdProviderDidAdComplete:self];
        }
        
        [self.delegate interstitialAdProviderWillDisappear:self];
        [self.delegate interstitialAdProviderDidDisappear:self];
        
        [self invalidate];
        self.adShown = NO;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onAdColonyAdFailure:(NSError *)err {
    [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:err];
}

- (void)onAdColonyV4VCReward:(BOOL)success currencyName:(NSString *)currencyName currencyAmount:(int)amount inZone:(NSString *)zoneID {
    if(success && self.vcObj != nil) {
        [self.delegate interstitialAdProvider:self didVirtualCurrencyReward:self.vcObj];
        [ASVirtualCurrency vcServerCallbackWithVCData:self.vcObj];
    }
}


@end

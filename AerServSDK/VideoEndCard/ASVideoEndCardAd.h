//
//  ASVideoEndCardAd.h
//  AerServSDK
//
//  Created by Albert Zhu on 4/26/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASInternalBaseInterstitialAdProvider.h"

#define kASVideoEndCardMediaFileURLKey @"mediaFile"
#define kASVideoEndCardHTMLKey @"endCardHtml"
#define kASVideoEndCardTrackingEventsKey @"trackingEvents"
#define kASVideoEndCardTrackingTypeKey @"type"
#define kASVideoEndCardTrackingURLsKey @"trackingUrls"
#define kASVideoEndCardTrackingVideoStartKey @"video0"
#define kASVideoEndCardTrackingVideoCompleteKey @"video100"
#define kASVideoEndCardTrackingVideoClickKey @"click"
#define kASVideoEndCardTrackingVideoImpressionKey @"tracking-video-impression"
#define kASVideoEndCardRedirectURLKey @"redirectUrl"

#define kCloseOffsetKey @"closeOffset"

@class ASVASTEvent;
@protocol ASVideoEndCardAdDelegate;

@interface ASVideoEndCardAd : NSObject

@property (nonatomic, assign) id<ASVideoEndCardAdDelegate> delegate;
@property (readonly, nonatomic, strong) NSURL *mediaFileUrl;
@property (readonly, nonatomic, strong) NSURL *redirectUrl;
@property (readonly, nonatomic, strong) ASVASTEvent *impression;
@property (readonly, nonatomic, strong) ASVASTEvent *videoStart;
@property (readonly, nonatomic, strong) ASVASTEvent *videoComplete;
@property (readonly, nonatomic, strong) ASVASTEvent *videoClick;
@property (readonly, nonatomic, copy) NSString *closeOffset;
@property (readonly, nonatomic, assign) BOOL isPreload;

- (instancetype)initWithJSONData:(NSData *)jsonData withDelegate:(id<ASVideoEndCardAdDelegate>)delegate asPreload:(BOOL)preload;
+ (instancetype)adFromJSONData:(NSData *)jsonData withDelegate:(id<ASVideoEndCardAdDelegate>)delegate asPreload:(BOOL)preload;

- (void)processAdditionalParameters:(NSDictionary *)parameters;

- (NSData *)getEndCardHTMLData;

@end

@protocol ASVideoEndCardAdDelegate <NSObject>

- (void)videoEndCardDidLoad;

@end
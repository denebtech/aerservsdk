//
//  ASVideoEndCardInterstitialViewController.m
//  AerServSDK
//
//  Created by Albert Zhu on 4/28/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASVideoEndCardInterstitialViewController.h"

#import "ASVideoEndCardAd.h"
#import "ASVideoEndCardVideoPlayerView.h"
#import "ASVideoEndCardVideoPlayer.h"
#import "ASVideoEndCardWebView.h"

#import "ASCloseButton.h"

@interface ASVideoEndCardInterstitialViewController () <ASVideoEndCardVideoPlayerDelegate, ASVideoEndCardWebViewDelegate>

@property (nonatomic, strong) ASVideoEndCardAd *vecAd;
@property (nonatomic, strong) ASVideoEndCardVideoPlayer *vecVidPlayer;
@property (nonatomic, strong) ASVideoEndCardWebView *vecWebView;
@property (nonatomic, assign) ASVideoEndCardPhase vecPhase;
@property (nonatomic, assign) BOOL endCardDidLoad;
@property (nonatomic, strong) id<ASInterstitialAdViewControllerDelegate> delegateCopy;

@end

@implementation ASVideoEndCardInterstitialViewController

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, dealloc"];
    [self removeNotifcations];
    _vecAd = nil;
    _vecVidPlayer = nil;
    _delegateCopy = nil;
}

#pragma mark - Public Methods

- (instancetype)initWithVideoEndCardAd:(ASVideoEndCardAd *)vecAd andDelegate:(id<ASInterstitialAdViewControllerDelegate>)delegate {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, initWithVideoEndCardAd:andDelegate:"];
    if(self = [super initWithDelegate:delegate]) {
        _vecAd = vecAd;
        _delegateCopy = delegate;
        _vecVidPlayer = [ASVideoEndCardVideoPlayer videoPlayerWithVideoEndCardAd:_vecAd andDelegate:self];
        _vecPhase = kVideoEndCardInitialPhase;
        [self addNotifications];
        
        self.view.backgroundColor = [UIColor blackColor];
    }
    
    return self;
}

+ (instancetype)viewControllerWithVideoEndCardAd:(ASVideoEndCardAd *)vecAd andDelegate:(id<ASInterstitialAdViewControllerDelegate>)delegate {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, viewControllerWithVideoEndCardAd:andDelegate:"];
    return [[ASVideoEndCardInterstitialViewController alloc] initWithVideoEndCardAd:vecAd andDelegate:delegate];
}

- (void)playAd {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, playAd"];
    [self.vecVidPlayer playAd];
    self.vecPhase = kVideoEndCardVideoAdPhase;
}

- (void)startAd {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, startAd"];
    [self playAd];
}

- (void)onCloseAd {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, onCloseAd"];
    if(self.endCardDidLoad) {
        self.vecPhase = kVideoEndCardCompletedPhase;
        [self.delegate interstitialAdViewControllerAdFinished:self];
    }
    
    [super onCloseAd];
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, cancel"];
    [super cancel];
}

#pragma mark - View Controller Default Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, viewDidLoad"];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, viewWillAppear:"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, viewDidAppear:"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, viewWillDisappear:"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, viewDidDisappear:"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, didReceiveMemoryWarning"];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    UIInterfaceOrientationMask allowedOrientaiton;
    switch (self.vecPhase) {
        case kVideoEndCardInitialPhase:
        case kVideoEndCardVideoAdPhase:
            allowedOrientaiton = UIInterfaceOrientationMaskAll;
            break;
        case kVideoEndCardFullscreenEndCardPhase:
        case kVideoEndCardCompletedPhase:
            allowedOrientaiton = UIInterfaceOrientationMaskLandscape;
            break;
        default:
            break;
    }
    return allowedOrientaiton;
}

- (void)viewDidLayoutSubviews {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, viewDidLayoutSubviews"];
    
    #if kShowUIFrames
    NSLog(@"----- self.view.frame: %@, self.vecVidPlayer.frame: %@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.vecVidPlayer.view.frame));
    #endif
    
    CGFloat width, height;
    if(self.vecPhase == kVideoEndCardInitialPhase || self.vecPhase == kVideoEndCardVideoAdPhase) {
        width = self.view.frame.size.width;
        height = self.view.frame.size.height;
    } else if(self.vecPhase == kVideoEndCardFullscreenEndCardPhase || self.vecPhase == kVideoEndCardCompletedPhase) {
        width = MAX(self.view.frame.size.width, self.view.frame.size.height);
        height = MIN(self.view.frame.size.width, self.view.frame.size.height);
    }
    
    [self.vecVidPlayer setViewFrame:CGRectMake(0.0f, 0.0f, width, height)];
    
    #if kShowUIFrames
    NSLog(@"----- self.view.frame: %@, self.vecVidPlayer.frame: %@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.vecVidPlayer.view.frame));
    #endif
}

//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
//    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, didRotateFromInterfaceOrientation:"];
//    [self ensureOrientation];
//}

#pragma mark - Helpers

//- (void)ensureOrientation {
//    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, ensureOrientation"];
//    switch(self.vecPhase) {
//        case kVideoEndCardFullscreenEndCardPhase:
//            if(self.view.frame.size.width != self.vecWebView.frame.size.width || self.view.frame.size.height != self.vecWebView.frame.size.height)
//                [self forceLandscapoeOrientationAndDisplayEndCardWebView];
//            break;
//        default:
//            break;
//    }
//}

#pragma mark - Notifications & Methods

- (void)addNotifications {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, addNotificaitons"];
    NSNotificationCenter *noteCenter = [NSNotificationCenter defaultCenter];
    
    [noteCenter addObserver:self selector:@selector(appDidGotoBackground)
                       name:UIApplicationWillResignActiveNotification object:nil];
    [noteCenter addObserver:self selector:@selector(appDidBecomeActive)
                       name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)appDidGotoBackground {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, appDidGotoBackground"];
    self.delegate = nil;
}

- (void)appDidBecomeActive {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, appDidBecomeActive"];
    self.delegate = self.delegateCopy;
}

- (void)removeNotifcations {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, removeNotificaitons"];
    NSNotificationCenter *noteCenter = [NSNotificationCenter defaultCenter];
    
    [noteCenter removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [noteCenter removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

#pragma mark - ASVideoEndCardVideoPlayerDelegate

- (void)videoEndCardVideoPlayer:(ASVideoEndCardVideoPlayer *)vecVidPlayer didFailWithError:(NSError *)err {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, videoEndCardVideoPlayer:didFailWithError:"];
    [self.delegate interstitialAdViewControllerFailed:self withError:err];
}

- (void)videoEndCardVideoPlayerReadyToPlay:(ASVideoEndCardVideoPlayer *)vecVidePlayer {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, videoEndCardVideoPlayerReadyToPlay:"];
    if(!self.vecAd || !self.vecVidPlayer) return;
    
    [self.delegate interstitialAdViewControllerCreatedAdSuccessfully:self];
    [self.view addSubview:self.vecVidPlayer.view];
    [self.view bringSubviewToFront:self.vecVidPlayer.view];
    [self.view bringSubviewToFront:self.closeButton];
    
    if(!self.vecAd.isPreload)
        [self.vecVidPlayer playAd];
    
    #if kShowUIFrames
    NSLog(@"----- self.view.frame: %@, self.vecVidPlayer.frame: %@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.vecVidPlayer.view.frame));
    #endif
}

- (void)videoEndCardVideoPlayerWillShowSkip:(ASVideoEndCardVideoPlayer *)vecVidPlayer {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, videoEndCardVideoPlayerWillShowSkip:"];
    self.closeButton.hidden = NO;
}

- (void)videoEndCardVideoPlayerDidRecordTap:(ASVideoEndCardVideoPlayer *)vecVidPlayer {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, videoEndCardVideoPlayerDidRecordTap:"];
    [self.delegate interstitialAdViewControllerAdWasTouched:self];
}

- (void)videoEndCardVideoPlayerAdDidPlayCompletely:(ASVideoEndCardVideoPlayer *)vecVidPlayer {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, videoEndCardVideoPlayerAdDidPlayCompletely:"];
    [self.vecVidPlayer.view removeFromSuperview];
    self.vecVidPlayer = nil;
    
    [self loadEndCardDisplayIfExists];
}

- (UIView *)videoEndCardVideoPlayerContainingView:(ASVideoEndCardVideoPlayer *)vecVidePlayer {
    return self.view;
}

- (void)videoEndCardVideoPlayer:(ASVideoEndCardVideoPlayer *)vecVidPlayer didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate interstitialAdViewController:self didFireAdvertiserEventWithMessage:msg];
}

#pragma mark - ASVideoEndCardWebView Methods

- (void)createAndLoadWebView {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, createAndLoadWebView"];
    self.vecPhase = kVideoEndCardFullscreenEndCardPhase;
    self.vecWebView = [ASVideoEndCardWebView webViewForVideoEndCardAd:self.vecAd andWebViewDelegate:self];
    [self.vecWebView loadEndCard];
    [self.view addSubview:self.vecWebView];
    [self.view bringSubviewToFront:self.vecWebView];
    self.closeButton.hidden = NO;
    [self.view bringSubviewToFront:self.closeButton];
}

- (void)forceLandscapoeOrientationAndDisplayEndCardWebView {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, forceLandscapoeOrientationAndDisplayEndCardWebView"];
    UIViewController *presentingVC = self.presentingViewController;
    if(presentingVC != nil) {
        __weak ASVideoEndCardInterstitialViewController *vecInterVC = self;
        vecInterVC.delegate = nil;
        [presentingVC dismissViewControllerAnimated:NO completion:^{
            @try {
                if(!vecInterVC.vecWebView)
                    [vecInterVC createAndLoadWebView];
                [presentingVC presentViewController:vecInterVC animated:NO completion:^{
                    vecInterVC.delegate = vecInterVC.delegateCopy;
                }];
            }
            @catch (NSException *exception) {
                [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, forceLandscapOrientaitonAndDisplayEndCardWebView, dismissViewController completion block - EXCEPTION CAUGHT"];
                [ASSDKLogger onException:exception];
            }
        }];
    }
}

- (void)loadEndCardDisplayIfExists {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, loadEndCardDisplayIfExists"];
    NSData *endCardHTML = [self.vecAd getEndCardHTMLData];
    if(!endCardHTML) {
        [self.delegate interstitialAdViewControllerAdFinished:self];
        [self cancel];
    } else {
        [self forceLandscapoeOrientationAndDisplayEndCardWebView];
    }
}

#pragma mark - ASVideoEndCardWebViewDelegate

- (void)videoEndCardWebViewWasClicked:(ASVideoEndCardWebView *)vecWebView {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, videoEndCardWebViewWasClicked:"];
    [self.delegate interstitialAdViewControllerAdWasTouched:self];
}

- (void)videoEndCardWebView:(ASVideoEndCardWebView *)vecWebView didFailWithError:(NSError *)err {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, videoEndCardWebView:didFailWithError:"];
    [self.delegate interstitialAdViewControllerFailed:self withError:err];
}

- (void)videoEndCadWebViewDidFinishLoad:(ASVideoEndCardWebView *)vecWebView {
    [ASSDKLogger logStatement:@"ASVideoEndCardInterstitialViewController, videoEndCadWebViewDidFinishLoad:"];
    self.endCardDidLoad = YES;
    
//    [self ensureOrientation];
}

@end

//
//  ASRhythmOneInterstitialProvider.m
//  AerServSDK
//
//  Created by Albert Zhu on 7/1/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASRhythmOneSdkInterstitialAdProvider.h"
#import "RhythmOneAd.h"

#import "ASLocationUtils.h"

@interface ASRhythmOneSdkInterstitialAdProvider ()

@property (nonatomic, strong) Class rhythmOneAdClass;
@property (nonatomic, strong) id rhythmOneAd;

@property (nonatomic, assign) BOOL adInitialized;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;

@end



@implementation ASRhythmOneSdkInterstitialAdProvider

NSString *const RhythmEventAdLoaded;
NSString *const RhythmEventAdError;
NSString *const RhythmEventAdImpression;
NSString *const RhythmEventAdClickThru;
NSString *const RhythmEventAdSkipped;
NSString *const RhythmEventAdVideoComplete;
NSString *const RhythmEventAdUserClose;
NSString *const RhythmEventAdClosed;

- (void)dealloc {
    [self unregisterRhythmEvents];
    [self cleanUp];
}

- (instancetype)init {
    if(self = [super initWithAdClassName:kRhythmOneAdClassStr timeout:kCustomInterstitialTimeout]) {
        _rhythmOneAdClass = NSClassFromString(kRhythmOneAdClassStr);
        _rhythmOneAd = [_rhythmOneAdClass new];
        [self registerRhythmEvents];
    }
    
    return self;
}

- (void)cleanUp {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, cleanUp"];
    self.rhythmOneAdClass = nil;
    self.rhythmOneAd = nil;
}

#pragma mark - ASCustomInterstitialAdProvider Overloaded Methods

- (void)initializePartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, initializePartnerAd:"];
    self.adInitialized = YES;
}

- (BOOL)hasPartnerAdInitialized {
    return self.adInitialized;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return !self.adInitialized;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, loadPartnerAd:"];
    self.adLoaded = NO;
    self.adFailed = NO;
    
    NSDictionary *mediationProperties = properties[ASInterstitialParameter_Parameters];
    NSString *appId = mediationProperties[kRhythmOneAppIdKey];
    NSString *placement = mediationProperties[kRhythmOnePlacementKey];
    NSString *mediaType = (mediationProperties[kRhythmOneMediaTypeKey] != nil) ? mediationProperties[kRhythmOneMediaTypeKey] : @"all";
    NSString *fullscreen = (mediationProperties[kRhythmOneFullscreenKey] != nil) ? mediationProperties[kRhythmOneFullscreenKey] : @"yes";
    NSString *skipButtonVisible = (mediationProperties[kRhythmOneSkipButtonVisibleKey] != nil) ? mediationProperties[kRhythmOneSkipButtonVisibleKey] : @"true";
    CLLocation *locale = [[ASLocationUtils getInstance] getLocation];
    
    NSDictionary *rhythmAdParams = @{ kRhythmOneAdParamsAppIdKey : appId,
                                         kRhythmOneAdParamsPlacementKey : placement,
                                         kRhythmOneAdParamsMediaTypeKey : mediaType,
                                         kRhythmOneAdParamsFullscreenKey : fullscreen,
                                         kRhythmOneAdParamsSkipButtonVisibleKey : skipButtonVisible,
                                         kRhythmOneAdParamsLatKey : [NSString stringWithFormat:@"%f", locale.coordinate.latitude],
                                         kRhythmOneAdParamsLonKey : [NSString stringWithFormat:@"%f", locale.coordinate.longitude]
                                         };
    
    [self.rhythmOneAd initAd:rhythmAdParams];
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, showPartnerAd:"];
    [self.rhythmOneAd startAd];
}

- (void)terminatePartnerAd {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, terminatePartnerAd"];
    [self.rhythmOneAd stopAd];
    [self cleanUp];
}

#pragma mark - Rhythm Event Notification Methods

- (void)registerRhythmEvents {
    NSNotificationCenter * noteCenter = [NSNotificationCenter defaultCenter];
    [noteCenter addObserver:self selector:@selector(adLoadedEventReceived:) name:RhythmEventAdLoaded object:self.rhythmOneAd];
    [noteCenter addObserver:self selector:@selector(adErrorEventReceived:) name:RhythmEventAdError object:self.rhythmOneAd];
    [noteCenter addObserver:self selector:@selector(adImpressionEventReceived:) name:RhythmEventAdImpression object:self.rhythmOneAd];
    [noteCenter addObserver:self selector:@selector(adClickThruEventReceived:) name:RhythmEventAdClickThru object:self.rhythmOneAd];
    [noteCenter addObserver:self selector:@selector(adClosedEventReceived:) name:RhythmEventAdSkipped object:self.rhythmOneAd];
    [noteCenter addObserver:self selector:@selector(adVideoCompleteEventReceived:) name:RhythmEventAdVideoComplete object:self.rhythmOneAd];
    [noteCenter addObserver:self selector:@selector(adClosedEventReceived:) name:RhythmEventAdUserClose object:self.rhythmOneAd];
    [noteCenter addObserver:self selector:@selector(adClosedEventReceived:) name:RhythmEventAdClosed object:self.rhythmOneAd];
}

- (void)unregisterRhythmEvents {
    NSNotificationCenter * noteCenter = [NSNotificationCenter defaultCenter];
    [noteCenter removeObserver:self name:RhythmEventAdLoaded object:self.rhythmOneAd];
    [noteCenter removeObserver:self name:RhythmEventAdError object:self.rhythmOneAd];
    [noteCenter removeObserver:self name:RhythmEventAdImpression object:self.rhythmOneAd];
    [noteCenter removeObserver:self name:RhythmEventAdSkipped object:self.rhythmOneAd];
    [noteCenter removeObserver:self name:RhythmEventAdVideoComplete object:self.rhythmOneAd];
    [noteCenter removeObserver:self name:RhythmEventAdUserClose object:self.rhythmOneAd];
    [noteCenter removeObserver:self name:RhythmEventAdClosed object:self.rhythmOneAd];
}

- (void)adLoadedEventReceived:(NSNotification *)notification {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, adLoadedEventReceived:"];
    @try {
        [self asPartnerInterstitialAdDidLoad:self.rhythmOneAd];
        self.adLoaded = YES;
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adErrorEventReceived:(NSNotification *)notification {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, adErrorEventReceived:"];
    @try {
        [self.delegate interstitialAdProvider:self
                     didFailToLoadAdWithError:[NSError errorWithDomain:NSStringFromClass([self class])
                                                                  code:100
                                                              userInfo:@{NSLocalizedDescriptionKey : notification.userInfo[kRhythmOneErrorEventErrorCodeKey]}]];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adImpressionEventReceived:(NSNotification *)notification {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, adImpressionEventReceived:"];
    @try {
        [self asPartnerInterstitialAdWillAppear];
        [self asPartnerInterstitialAdDidAppear];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adClickThruEventReceived:(NSNotification *)notification {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, adClickThruEventReceived:"];
    @try {
        [self asPartnerInterstitialAdWasTouched];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adVideoCompleteEventReceived:(NSNotification *)notification {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, adVideoCompleteEventReceived:"];
    
    @try {
        [self asPartnerInterstitialVideoCompleted];
        [self adClosedEventReceived:nil];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adClosedEventReceived:(NSNotification *)notification {
    [ASSDKLogger logStatement:@"ASRhythmOneInterstitialAdProvider, adClosedEventReceived:"];
    @try {
        [self asPartnerInterstitialAdWillDisappear];
        [self asPartnerInterstitialAdDidDisappear];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

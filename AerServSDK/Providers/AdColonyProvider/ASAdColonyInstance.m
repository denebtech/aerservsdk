//
//  ASAdColonyInstance.m
//  AerServSDK
//
//  Created by Albert Zhu on 11/23/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASAdColonyInstance.h"
#import "ASAdColonyDelegateProxy.h"
#import "AdColony.h"

@interface ASAdColonyInstance ()

@property (nonatomic, strong) NSMutableArray *availableZoneIDs;

@end

@implementation ASAdColonyInstance

+ (ASAdColonyInstance *)instance {
    static ASAdColonyInstance *instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [ASAdColonyInstance new];
        instance.state = kASProviderSetupNone;
        instance.availableZoneIDs = [NSMutableArray array];
    });
    
    return instance;
}

+ (void)adColonySetupWithConfig:(NSDictionary *)adapterConfig {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonyInstance, adColonySetupWithConfig: %@", adapterConfig]];
    
    [ASAdColonyInstance instance].adColonyClass = NSClassFromString(kAdColonyClassStr);
    
    NSError *err = nil;
    if(!adapterConfig[kInitAdColonyAppIDKey]) {
        [ASSDKLogger logStatement:@"ASAdColonyInstance, adColonySetupWithConfig - Invalid AppID Error"];
        err = [NSError errorWithDomain:NSStringFromClass([ASAdColonyInstance class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"AdColony Instance, Invalid AppID"}];
    }
    
    if(!adapterConfig[kInitAdColonyZoneIDsKey]) {
        [ASSDKLogger logStatement:@"ASAdColonyInstance, adColonySetupWithConfig - Invalid ZoneID Error"];
        err = [NSError errorWithDomain:NSStringFromClass([ASAdColonyInstance class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"AdColony Instance, Invalid ZoneID"}];
    }
    
    if(![ASAdColonyInstance instance].adColonyClass || err != nil) {
        [ASSDKLogger logStatement:@"ASAdColonyInstance, adColonySetupWithConfig - Could not create AdColony class object. Will try again later during request"];
        [ASAdColonyInstance instance].adColonyClass = nil;
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [ASAdColonyInstance instance].state = kASProviderSetupStarted;
        [[ASAdColonyInstance instance].adColonyClass configureWithAppID:adapterConfig[kInitAdColonyAppIDKey]
                                                                zoneIDs:adapterConfig[kInitAdColonyZoneIDsKey]
                                                               delegate:[ASAdColonyDelegateProxy sharedProxy]
                                                                logging:YES];
    });
}

#pragma mark - Zone Availability Methods

+ (void)addAvailableZoneID:(NSString *)zoneID {
    zoneID = [zoneID stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    NSMutableArray *zoneIDs = [ASAdColonyInstance instance].availableZoneIDs;
    if(![zoneIDs containsObject:zoneID]) {
        [zoneIDs addObject:zoneID];
    }
}

+ (void)removeAvailabeZoneID:(NSString *)zoneID {
    NSMutableArray *zoneIDs = [ASAdColonyInstance instance].availableZoneIDs;
    if([zoneIDs containsObject:zoneID]) {
        [zoneIDs removeObject:zoneID];
    }
}

+ (BOOL)isZoneAvailable:(NSString *)zoneID {
    zoneID = [zoneID stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    NSArray *zoneIDs = [ASAdColonyInstance instance].availableZoneIDs;
    return [zoneIDs containsObject:zoneID];
}

+ (NSString *)findAvailableZoneIDFromZoneIDList:(NSArray *)zoneIDArr {
    NSArray *zoneIDs = [ASAdColonyInstance instance].availableZoneIDs;
    NSString *availZoneID = nil;
    
    for(NSString *zoneID in zoneIDArr) {
        NSString *trimmedZoneID = [zoneID stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
        if([zoneIDs containsObject:trimmedZoneID]) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonyInstance, findAvailableZoneIDFromZoneIDList: - available zone ID: %@", zoneID]];
            availZoneID = trimmedZoneID;
            break;
        } else {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdColonyInstance, findAvailableZoneIDFromZoneIDList: - unavailable zone ID: %@", zoneID]];
        }
    }
    
    return availZoneID;
}

@end

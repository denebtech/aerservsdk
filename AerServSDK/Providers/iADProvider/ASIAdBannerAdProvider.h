//
//  ASSIAdBannerProvider.h
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/26/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASBannerAdProvider.h"
#import <Foundation/Foundation.h>

@interface ASIAdBannerAdProvider : ASBannerAdProvider

@end

//
//  ASChartboostInstance.h
//  AerServSDK
//
//  Created by Albert Zhu on 11/24/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASGlobalProviderSetup.h"

#define kChartboostClassStr @"Chartboost"
#define kChartboostSignatureKey @"signature"
#define kASParamChartboostAppSignatureKey @"ChartboostAppSignature"
#define kChartboostAppIDKey @"appId"
#define kASParamChartboostAppIDKey @"ChartboostAppId"
#define kChartboostLocationKey @"location"
#define kASParamChartboostLocation @"ChartboostLocation"
#define kChartboostDefaultLocation @"Home Screen"

@interface ASChartboostInstance : NSObject

@property (nonatomic, strong) Class chartboostClass;
@property (nonatomic, assign) ASProviderSetupState state;

+ (ASChartboostInstance *)instance;
+ (void)chartboostSetupWithConfig:(NSDictionary *)adapterConfig;

@end

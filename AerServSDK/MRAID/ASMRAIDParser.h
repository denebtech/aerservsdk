//
//  ASMRAIDParser.h
//  AerServSDK
//
//  Created by Albert Zhu on 8/27/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kShowMRAIDHTML 0
#define kUseStaticTestMRAIDAd 0
#define kStaticTestAdFileName @"mraid_iab_twopartexpand"//@"mraid_iab_resize"//@"mraid2_test2"//

#define kASMRAIDJSTag @"mraid.js"
#define kAerServEventsDomain @"events.aerserv.com"

#define kMRAIDHTMLKey @"MRAIDHTML"
#define kMRAIDVastImpressionURLKey @"MRAIDVastImpressionURL"

@interface ASMRAIDParser : NSObject

+ (NSDictionary *)stringForProcessedRawHTML:(NSString *)htmlStr;
+ (NSDictionary *)stringForProcessedRawHTMLData:(NSData *)htmlData;

+ (BOOL)checkLoadedHTMLForMRAIDInWebView:(UIWebView *)webView;
+ (BOOL)checkForMraidWithData:(NSData *)htmlData;

+ (NSURL *)createVastImpressionURLWithHTML:(NSString *)htmlStr;

+ (NSString *)findForceOrientationString:(NSString *)htmlStr;

@end

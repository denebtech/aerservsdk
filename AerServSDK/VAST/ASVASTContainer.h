//
//  ASVASTContainer.h
//  AerServSDK
//
//  Created by Vasyl Savka on 2/29/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ASVASTAd;

@interface ASVASTContainer : NSObject

@property (nonatomic, strong) ASVASTAd *vastAd;
@property (nonatomic, assign) BOOL didFailoverAlready;

@end

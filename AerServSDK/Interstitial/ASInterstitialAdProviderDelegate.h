//
//  ASAdProviderDelegate.h
//  AerServSDK
//
//  Created by Scott A Andrew on 4/9/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//


@class ASInterstitialAdProvider;


/*! @header
 * Delegate calls that the interstitial ad provider must call. */

@protocol ASInterstitialAdProviderDelegate <NSObject>

@property (nonatomic, copy) NSString *closeOffset;

- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didLoadAd:(id)ad;
- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didFailToLoadAdWithError:(NSError *)error;
- (void)interstitialAdProviderDidExpire:(ASInterstitialAdProvider *)provider;
- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didPreloadAd:(id)ad;
- (void)interstitialAdProviderDidAdComplete:(ASInterstitialAdProvider *)provider;

- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didVirtualCurrencyLoad:(NSDictionary *)vcData;
- (void)interstitialAdProvider:(ASInterstitialAdProvider *)provider didVirtualCurrencyReward:(NSDictionary *)vcData;

- (void)interstitialAdProviderWillAppear:(ASInterstitialAdProvider *)provider;
- (void)interstitialAdProviderDidAppear:(ASInterstitialAdProvider *)provider;

- (void)interstitialAdProviderWillDisappear:(ASInterstitialAdProvider *)provider;
- (void)interstitialAdProviderDidDisappear:(ASInterstitialAdProvider *)provider;

- (void)interstitialAdProviderAdWasTouched:(ASInterstitialAdProvider *)provider;

- (BOOL)interstitialAdProviderIsPreload:(ASInterstitialAdProvider *)provider;

- (NSArray*)getKeyWords;
- (NSDictionary*)getPubKeys;
- (void)AVPlayerCreated:(AVPlayer *)avPlayer;


@end

//
//  YuMeAppDelegate.h
//  YuMeiOSSampleApp
//
//  Created by Bharath Ramakrishnan on 4/5/13.
//  Copyright (c) 2014 YuMe Advertising Pvt. Ltd.,. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YuMeViewController;

@interface YuMeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) YuMeViewController *viewController;

@end //@interface YuMeAppDelegate

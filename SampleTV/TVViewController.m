//
//  ViewController.m
//  SampleTV
//
//  Created by Vasyl Savka on 10/5/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "TVViewController.h"
#import <AerServSDK/AerServSDK.h>

@interface TVViewController () <ASInterstitialViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UIButton *vastButton;
@property (nonatomic, strong) ASInterstitialViewController *vastVC;

@end

@implementation TVViewController

- (void)dealloc {
    self.vastButton = nil;
    
    self.vastVC = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.vastButton addTarget:self action:@selector(loadVast) forControlEvents:UIControlEventPrimaryActionTriggered];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButton Methods

- (void)loadVast {
    NSLog(@"------- loading VAST");
    if(!self.vastVC) {
        self.vastVC = [ASInterstitialViewController viewControllerForPlacementID:@"1000741" withDelegate:self];
        [self.vastVC loadAd];
    }
}

#pragma mark - ASInterstitialViewControllerDelegate Protocol Methods

- (void)interstitialViewControllerAdLoadedSuccessfully:(ASInterstitialViewController *)viewController {
    [self.vastVC showFromViewController:self];
}

- (void)interstitialViewControllerAdFailedToLoad:(ASInterstitialViewController *)viewController withError:(NSError *)error {
    NSLog(@"------ failed to load - error: %@", error.localizedDescription);
    self.vastVC = nil;
}

- (void)interstitialViewControllerAdDidComplete:(ASInterstitialViewController *)viewController {
    NSLog(@"------ ad did complete: VAST");
}

- (void)interstitialViewControllerDidDisappear:(ASInterstitialViewController *)viewController {
    self.vastVC = nil;
}

@end

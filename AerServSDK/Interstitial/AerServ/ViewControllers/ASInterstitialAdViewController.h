//
//  ASInterstitialAdViewController.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/25/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @header
 * Base class for institial ads that need to supply their own view controller. */

@protocol ASInterstitialAdViewControllerDelegate;

@interface ASInterstitialAdViewController : UIViewController

@property (nonatomic, weak) id<ASInterstitialAdViewControllerDelegate>delegate;
@property (nonatomic, assign) BOOL outlineAd;

- (instancetype)initWithDelegate:(id<ASInterstitialAdViewControllerDelegate>)delegate;
- (void)startAd;
- (void)cancel;

- (void)onCloseAd;
- (void)onPlayPauseTap:(UITapGestureRecognizer *)gesture;
- (void)onMenuTap:(UITapGestureRecognizer *)gesture;

@end


@protocol ASInterstitialAdViewControllerDelegate

- (BOOL)shouldShowInterstitialAd;

- (void)interstitialAdViewControllerViewWillAppear:(ASInterstitialAdViewController *)viewController;
- (void)interstitialAdViewControllerViewDidAppear:(ASInterstitialAdViewController *)viewController;

- (void)interstitialAdViewControllerViewWillDisappear:(ASInterstitialAdViewController *)viewController;
- (void)interstitialAdViewControllerViewDidDisappear:(ASInterstitialAdViewController *)viewController;

- (void)interstitialAdViewControllerCreatedAdSuccessfully:(ASInterstitialAdViewController *)controller;
- (void)interstitialAdViewControllerFailed:(ASInterstitialAdViewController *)controller withError:(NSError *)error;

- (void)interstitialAdViewControllerAdLoadingWasCancelled:(ASInterstitialAdViewController *)controller;
- (void)interstitialAdViewControllerAdFinished:(ASInterstitialAdViewController *)controller;
- (void)interstitialAdViewControllerAdWasTouched:(ASInterstitialAdViewController *)controller;

- (void)interstitialAdViewControllerDidRewardVC:(ASInterstitialAdViewController *)controller;

- (void)AVPlayerCreated:(AVPlayer *)avPlayer;

- (BOOL)interstitialAdViewControllerIsPreload:(ASInterstitialAdViewController *)controller;
- (void)interstitialAdViewController:(ASInterstitialAdViewController *)controller didFireAdvertiserEventWithMessage:(NSString *)msg;

@end

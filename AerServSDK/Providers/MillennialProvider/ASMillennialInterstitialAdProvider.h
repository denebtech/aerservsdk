//
//  ASMillennialInterstitialAdProvider.h
//  AerServSDK
//
//  Created by Albert Zhu on 5/13/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <AerServSDK/AerServSDK.h>

#define kMMSDKClassStr @"MMSDK"
#define kMMInterstitialAdClassStr @"MMInterstitialAd"
#define kMMPlacementIdKey @"placementId"

@interface ASMillennialInterstitialAdProvider : ASCustomInterstitialAdProvider


@end

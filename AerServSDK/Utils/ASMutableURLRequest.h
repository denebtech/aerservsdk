//
//  ASMutableURLRequest.h
//  AerServSDK
//
//  Created by Gauthier on 8/1/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kAerServSDKName @"AerServSDK"
#define kAerServSDKVersion @"1.0.1"
#define kNavigatorUserAgentKey @"navigator.userAgent"
#define kHTTPHeaderUserAgentFieldKey @"User-Agent"

@interface ASMutableURLRequest : NSMutableURLRequest

@end

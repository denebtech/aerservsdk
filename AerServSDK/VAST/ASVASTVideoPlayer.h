//
//  ASVastVideoPlayer.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/31/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASVASTEventError.h"

#define kAVPlayerItemKeyStatus @"status"
#define kAVPlayerItemKeyPlaybackLikelyToKeepUp @"playbackLikelyToKeepUp"
#define kAVPlayerItemKeyPlaybackBufferFull @"playbackBufferFull"

@class ASVASTCreative;
@class ASVideoView;
@class ASVASTAd;
@class ASVASTVideoPlayerView;
@protocol ASVASTVideoPlayerDelegate;

@interface ASVASTVideoPlayer : NSObject

@property (nonatomic, weak) id<ASVASTVideoPlayerDelegate> delegate;
@property (readonly, nonatomic, strong) ASVASTVideoPlayerView *view;
@property (readonly, nonatomic, strong) ASVASTAd* ad;
@property (readonly, nonatomic, strong) ASVASTCreative *video;
@property (nonatomic, assign) BOOL isPreload;

- (void)playVASTAd:(ASVASTAd*)ad;
- (void)startAd;
- (void)stopAd;

- (CGSize)contentSize;

@end



@protocol ASVASTVideoPlayerDelegate <NSObject>

@property (nonatomic, copy) NSString *closeOffset;

// notice these are not optional!!
- (BOOL)shouldShowAd;
- (void)VASTVideoPlayerVideoIsReady:(ASVASTVideoPlayer *)videoPlayer;
- (void)VASTVideoPlayerFailedToLoad:(ASVASTVideoPlayer *)videoPlayer withErrorCode:(ASVASTErrorCode)errCode;
- (void)VASTVideoPlayerAdCompleted:(ASVASTVideoPlayer *)videoPlayer;
- (void)VASTVideoPlayer:(ASVASTVideoPlayer *)player wasClicked:(NSURL *)URL;
- (void)VASTVideoPlayerWillRewardVC:(ASVASTVideoPlayer *)player;
- (void)AVPlayerCreated:(AVPlayer *)avPlayer;
- (void)cancel;
- (void)VASTVideoPlayer:(ASVASTVideoPlayer *)videoPlayer willShowSkip:(BOOL)show;
- (void)VASTVideoPlayer:(ASVASTVideoPlayer *)videoPlayer didFireAdvertiserEventWithMessage:(NSString *)msg;
- (UIView *)VASTVideoPlayerContainingView:(ASVASTVideoPlayer *)videoPlayer;

@optional

- (void)VASTVideoPlayerSizeChanged:(ASVASTVideoPlayer *)player;

@end

//
//  ASCustomInterstitialAdProvider.m
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASCustomInterstitialAdProvider.h"
#import "ASVirtualCurrency.h"

/**
 * This is the abstract class for adapters.  Unlike traditional abstract classes, which usually
 * only implement common methods for subclasses, it also fully implements logic of how AerServ loads
 * our partners' ads and acts as interface between AerServ SDK and our partners' SDKs.
 * In effect it hides our internal implementation from adapters, thus making adapters
 * easier to write.  The use of this class is completely optional--you can still implement
 * an adapter the old way, especially if the partner's ad serving mechanism does not fit
 * with how this class works.  But should you choose to use it, please read this
 * documentation carefully, as there is a bit of learning curve.
 *
 * To use this class, you just need to properly instantiate this class, then define how to
 * handle ad requests and events.  You should NOT have to look at the implementation details
 * of this abstract class, nor know anything about AerServ SDK, e.g. you should
 * not have to worry about when to call
 * [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error].
 * The following sections contain more detailed instructions.
 *
 * INSTANTIATION
 *
 * Your adapter class must have a init method that calls
 * [super initWithAdClassName:(NSString *) timeout:(NSTimeInterval)], where adClassName
 * is the name of the class that the adapter uses to load ads (e.g. for Unity this is
 * "UnityAds"), and timeout is request timeout and is explained below, but a value of 6 seconds
 * should suffice.
 *
 * AD REQUEST HANDLING
 *
 * To define ad request handling, implement the following methods:
 *
 *     - (void)initializePartnerAd:(NSDictionary *)properties:  This initializes the partner's
 *       ad, and it is called once when the adapter is instantiated but not again (unless
 *       you create another instance).  Because of how SDK loads partners' ads, this method's
 *       implementation is usually empty.
 *     - (void)loadPartnerAd:(NSDictionary *)properties:  Loads the partner's ad but does
 *       not show it.
 *     - (void)showPartnerAd:(UIViewController *)rootViewController:  Shows the partner's ad.
 *
 * In some cases it is okay to leave a method empty, e.g. some partners load ads automatically,
 * in which case loadPartnerAd would not need to do anything.
 *
 * All requests run in tasks that time out after a certain duration to prevent them from running
 * forever (this value is specified when calling init).  It is your responsibility
 * to let these tasks know that you have an outcome so that they do not time out and fail the ad.
 * To do so implement the following methods.  These methods are polled, so you do not need
 * to--nor do you have a mechanism to--fire an event.  In other words, if the partner's ad
 * is ready, just return YES for hasPartnerAdLoaded and do not fire any "ad ready" event.
 *
 *     - (BOOL)hasPartnerAdInitialized
 *     - (BOOL)hasPartnerAdFailedToInitialize
 *     - (BOOL)hasPartnerAdLoaded
 *     - (BOOL)hasPartnerAdFailedToLoad
 *
 * Implement the method exactly as instructed by the method name, i.e. if an ad has not loaded,
 * but you do not yet know if it would load, then both hasPartnerAdLoaded and
 * hasPartnerAdFailedToLoad should return false.  Remember that these methods are polled
 * until a request times out...  Returning false to hasPartnerAdLoaded
 * does not mean it has failed; it simply means it has not loaded yet.
 *
 * When a request times out, the method terminatePartnerAd will be called.  At this
 * point an ad failed event is propagated to our SDK so that we can proceeds with failover
 * strategies, and the ad is NOT expected to appear.  You therefore need to
 * override this method and add logic to stop the ad from displaying, and add
 * any cleanup code that needs to be executed.
 *
 * EVENT HANDLING
 *
 * For event handling, this class provides the following methods that you can invoke:
 *
 *     - (void)asPartnerInterstitialAdWillDisappear
 *     - (void)asPartnerInterstitialAdDidDisappear
 *     - (void)asPartnerInterstitialAdWillAppear
 *     - (void)asPartnerInterstitialAdDidAppear
 *     - (void)asPartnerInterstitialAdWasTouched
 *
 * When you get an event from our partner, call the method that most closely corresponds
 * to that event.  Do not worry about firing anything else, e.g.
 * [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error], as they are
 * all handled by this class.  If a one-to-one mapping is not possible, you may need to
 * call more than one methods.  For example if our partner only has a "ad hidden" event,
 * you may want to call interstitialAdWillDisappear and interstitialAdDidDisappear
 * when that event is fired.
 *
 * Notice that you there are no event methods for "ad loaded" and "ad failed" events.
 * This was done intentionally.  The idea is that these two events are communicated by
 * hasPartnerAdLoaded and hasPartnerAdFailedToLoad methods when they are repeatedly
 * polled.
 *
 * OTHER INFORMATION
 *
 * You most likely will need these utility methods:
 *
 *     - (NSString *)valueInProperty:(NSDictionary *)properties forKey:(NSString *)key:
 *       Returns the network attribute value, as configured on SSUI
 */

@interface ASCustomInterstitialAdProvider()

@property (nonatomic, copy) NSString *adClassName;
@property (nonatomic, assign) NSTimeInterval timeout;
@property (nonatomic, strong) NSDate *adInitializationDate;
@property (nonatomic, strong) NSTimer *adInitializationTimer;
@property (nonatomic, strong) NSDate *adLoadingDate;
@property (nonatomic, strong) NSTimer *adLoadingTimer;
@property (nonatomic, assign) BOOL isPreload;
@property (nonatomic, strong) NSDictionary *properties;
@property (nonatomic, assign) NSTimeInterval pollingInterval;

@end


@implementation ASCustomInterstitialAdProvider

static NSMutableArray *initializedPartners;

- (instancetype)initWithAdClassName:(NSString *)adClassName timeout:(NSTimeInterval)timeout {
    if (self = [super init]) {
        self.adClassName = adClassName;
        self.timeout = timeout;
        self.pollingInterval = 0.25f;
        
        if (!initializedPartners) {
            initializedPartners = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (void)requestInterstitialAdWithProperties:(NSDictionary *)properties isPreload:(BOOL)preload {
    if (NSClassFromString(self.adClassName) == nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCustomInterstitialAdProvider, requestInterstitialAdWithProperties:isPreload - Could not create partner's class object for %@, failing over", self.adClassName]];
        NSError* error = [NSError errorWithDomain:NSStringFromClass([self class]) code:100 userInfo:@{NSLocalizedDescriptionKey : [NSString stringWithFormat:@"Could not link against %@, failing over.", self.adClassName]}];
        
        @try {
            [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }

        return;
    }
    
    self.properties = properties;
    self.isPreload = preload;
    
    // check for VC
    self.vcData = [ASVirtualCurrency getVirtualCurrencyData:properties[ASInterstitialParameter_HTMLHeaders]];
    
    if ([initializedPartners containsObject:self.adClassName]) {
        [self loadAd];
    } else {
        [self initializeAndLoadAd];
    }
}

- (void)initializeAndLoadAd {
    @try {
        [self initializePartnerAd:self.properties];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    self.adInitializationDate = [[NSDate date] dateByAddingTimeInterval:self.timeout];
    self.adInitializationTimer = [NSTimer scheduledTimerWithTimeInterval:self.pollingInterval target:self selector:@selector(adInitializationTimerCallback:) userInfo:nil repeats:YES];
}

- (void)adInitializationTimerCallback:(NSTimer *)timer {
    NSDate *now = [NSDate date];
    BOOL hasAdTimedOut = [now compare:self.adInitializationDate] == NSOrderedDescending;
    
    @try {
        if ([self hasPartnerAdInitialized]) {
            [self cancelAdInitializationTimer];
            [initializedPartners addObject:self.adClassName];
            [self loadAd];
        } else if ([self hasPartnerAdFailedToLoad]) {
            [self cancelAdInitializationTimer];
            [self adFailed:[NSString stringWithFormat:@"Failed to load %@ ad", self.adClassName]];
        } else if (hasAdTimedOut) {
            [self cancelAdInitializationTimer];
            [self adFailed:[NSString stringWithFormat:@"Partner's ad timed out after %f seconds", self.timeout]];
            [self terminatePartnerAd];
        } else {
            return;
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adFailed:(NSString *)msg {
    NSError *error = [NSError errorWithDomain:NSStringFromClass([self class]) code:100 userInfo:@{NSLocalizedDescriptionKey:msg}];
    @try {
        [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)loadAd {
    @try {
        [self loadPartnerAd:self.properties];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
    self.adLoadingDate = [[NSDate date] dateByAddingTimeInterval:self.timeout];
    self.adLoadingTimer = [NSTimer scheduledTimerWithTimeInterval:self.pollingInterval target:self selector:@selector(adLoadingTimerCallback:) userInfo:nil repeats:YES];
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    @try {
        [self showPartnerAd:rootViewController];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adLoadingTimerCallback:(NSTimer *)timer {
    NSDate *now = [NSDate date];
    BOOL hasAdTimedOut = [now compare:self.adLoadingDate] == NSOrderedDescending;

    @try {
        if ([self hasPartnerAdLoaded]) {
            [self asPartnerInterstitialAdDidLoad:nil];
        } else if ([self hasPartnerAdFailedToLoad]) {
            [self cancelAdLoadingTimer];
            [self adFailed:[NSString stringWithFormat:@"Failed to load %@ ad", self.adClassName]];
        } else if (hasAdTimedOut) {
            [self cancelAdLoadingTimer];
            [self adFailed:[NSString stringWithFormat:@"Partner's ad timed out after %f seconds", self.timeout]];
            [self terminatePartnerAd];
        } else {
            return;
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)cancelAdInitializationTimer {
    [self.adInitializationTimer invalidate];
    self.adInitializationTimer = nil;
}

- (void)cancelAdLoadingTimer {
    [self.adLoadingTimer invalidate];
    self.adLoadingTimer = nil;
}

- (NSString *)valueInProperty:(NSDictionary *)properties forKey:(NSString *)key {
    NSDictionary *parameters = properties[ASInterstitialParameter_Parameters];
    return parameters[key];
}

- (void)asPartnerInterstitialAdDidLoad:(id)ad {
    @try {
        [self cancelAdLoadingTimer];
        
        // Fire virtual currency loaded event, if necessary
        if (self.vcData != nil) {
            [self.delegate interstitialAdProvider:self didVirtualCurrencyLoad:self.vcData];
        }
        
        if(self.isPreload) {
            [self.delegate interstitialAdProvider:self didPreloadAd:ad];
        } else {
            [self.delegate interstitialAdProvider:self didLoadAd:ad];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}


- (void)asPartnerInterstitialAdWillDisappear {
    @try {
        [self.delegate interstitialAdProviderWillDisappear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)asPartnerInterstitialAdDidDisappear {
    @try {
        [self.delegate interstitialAdProviderDidDisappear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)asPartnerInterstitialAdWillAppear {
    @try {
        [self.delegate interstitialAdProviderWillAppear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)asPartnerInterstitialAdDidAppear {
    @try {
        [self.delegate interstitialAdProviderDidAppear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)asPartnerInterstitialAdWasTouched {
    @try {
        [self.delegate interstitialAdProviderAdWasTouched:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)asPartnerInterstitialVideoCompleted {
    @try {
        [self.delegate interstitialAdProviderDidAdComplete:self];
        
        // Fire virtual currency events, if necessary
        NSDictionary *virtualCurrency = [ASVirtualCurrency getVirtualCurrencyData:self.properties[ASInterstitialParameter_HTMLHeaders]];
        if (virtualCurrency) {
            [self.delegate interstitialAdProvider:self didVirtualCurrencyReward:virtualCurrency];
            [ASVirtualCurrency vcServerCallbackWithVCData:virtualCurrency];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}


- (void)initializePartnerAd:(NSDictionary *)properties { }

- (BOOL)hasPartnerAdInitialized {
    return NO;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties { }

- (BOOL)hasPartnerAdLoaded {
    return NO;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return NO;
}

- (void)showPartnerAd:(UIViewController *)rootViewController { }

- (void)terminatePartnerAd { }

@end

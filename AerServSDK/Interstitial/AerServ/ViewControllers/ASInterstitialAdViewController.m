//
//  ASInterstitialAdViewController.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/25/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdViewController.h"

@interface ASInterstitialAdViewController ()

@property (nonatomic, copy) NSDictionary* properties;
@property (nonatomic, copy) NSDictionary* parameters;
@property (nonatomic, strong) UITapGestureRecognizer *playPauseTapRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *menuTapRecognizer;

@end

@implementation ASInterstitialAdViewController

- (instancetype)init {
    NSAssert(0, @"This is not a valid initializer");
    
    return nil;
}

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASInterstitialAdViewController, dealloc"];
    self.delegate = nil;
    self.properties = nil;
    self.parameters = nil;
    
    // remove gesture recognizers
    [self.view removeGestureRecognizer:self.playPauseTapRecognizer];
    self.playPauseTapRecognizer.allowedPressTypes = nil;
    self.playPauseTapRecognizer = nil;
    
    [self.view removeGestureRecognizer:self.menuTapRecognizer];
    self.menuTapRecognizer.allowedPressTypes = nil;
    self.menuTapRecognizer = nil;

}

- (instancetype)initWithDelegate:(id<ASInterstitialAdViewControllerDelegate>) delegate {
    if (self = [super init]) {
        self.delegate = delegate;
        self.view.backgroundColor = [UIColor blackColor];
        
        self.playPauseTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPlayPauseTap:)];
        self.playPauseTapRecognizer.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause], [NSNumber numberWithInteger:UIPressTypeSelect]];
        [self.view addGestureRecognizer:self.playPauseTapRecognizer];
        
        self.menuTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMenuTap:)];
        self.menuTapRecognizer.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypeMenu]];
        [self.view addGestureRecognizer:self.menuTapRecognizer];
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    @try {
        [self.delegate interstitialAdViewControllerViewWillAppear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
        
    @try {
        [self.delegate interstitialAdViewControllerViewDidAppear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    @try {
        [self.delegate interstitialAdViewControllerViewWillDisappear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    @try {
        [self.delegate interstitialAdViewControllerViewDidDisappear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

#pragma mark - Gesture Recognizer Methods

- (void)onCloseAd {
    [ASSDKLogger logStatement:@"ASInterstitialAdViewController, onCloseAd"];
    @try {
        if(self.presentingViewController != nil)
            [self dismissViewControllerAnimated:NO completion:nil];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onPlayPauseTap:(UITapGestureRecognizer *)gesture {
    [ASSDKLogger logStatement:@"ASInterstitialAdViewController, onPlayPauseTap:"];
    @try {
        
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onMenuTap:(UITapGestureRecognizer *)gesture {
    [ASSDKLogger logStatement:@"ASInterstitialAdViewController, onMenuTap:"];
}

#pragma mark - Generic interstitail ad view methods

- (void)startAd { }

- (void)cancel {
    [ASSDKLogger logStatement:@"ASInterstitialAdViewController, cancel"];
    @try {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
}

@end

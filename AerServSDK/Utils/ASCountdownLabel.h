//
//  ASCountdownLabel.h
//  AerServSDK
//
//  Created by Vasyl Savka on 11/3/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kCountdownLblW 60.0f
#define kCountdownLblH 25.0f

@interface ASCountdownLabel : UILabel

- (instancetype)init;

- (void)changePosToX:(CGFloat)xPos andY:(CGFloat)yPos;

@end

//
//  ASFacebookInterstitialAdProvider.h
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ASCustomInterstitialAdProvider.h"

@interface ASFacebookInterstitialAdProvider : ASCustomInterstitialAdProvider

@end

//
//  ASAppLovinDelegateProxy.h
//  AerServSDK
//
//  Created by Albert Zhu on 11/25/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALSdk.h"
#import "ALAdRewardDelegate.h"
#import "ASAppLovinInterstitialAdProvider.h"

@interface ASAppLovinDelegateProxy : NSObject <ALAdLoadDelegate, ALAdDisplayDelegate,
                                                ALAdVideoPlaybackDelegate, ALAdRewardDelegate>

@property (nonatomic, strong) ASAppLovinInterstitialAdProvider *provider;

+ (ASAppLovinDelegateProxy *)sharedProxy;
+ (void)addAppLovinProvider:(ASAppLovinInterstitialAdProvider *)provider;
+ (void)removeAppLovinProvider;

@end

//
//  ASUnityInstance.h
//  AerServSDK
//
//  Created by Albert Zhu on 7/25/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#ifndef kAerServ_UnityInstance_h
#define kAerServ_UnityInstance_h

#import <Foundation/Foundation.h>
#import <UnityAds.h>
#import "ASGlobalProviderSetup.h"

#define kUnityConfigGameIdKey @"unityGameId"

#define kUnityAdsClassStr @"UnityAds"

#define kUnityGameIdKey @"UnityGameId"
#define kUnityZoneIdKey @"UnityZoneId"
#define kUnityTestModeKey @"UnityTestMode"

#define kUnityDebugMode 0

@class ASUnityInterstitialAdProvider;

@interface ASUnityInstance : NSObject

@property (nonatomic, strong) Class unityAdsClass;
@property (nonatomic, assign) ASProviderSetupState state;

+ (ASUnityInstance *)instance;
+ (void)startWithUnityGameID:(NSString *)unityGameID;
+ (void)unitySetupWithConfig:(NSDictionary *)adapterConfig;

+ (void)addUnityProvider:(ASUnityInterstitialAdProvider *)provider forPlacementId:(NSString *)placementId;
+ (ASUnityInterstitialAdProvider *)getUnityProviderForPlacementId:(NSString *)placementId;
+ (void)removeProviderForPlacementId:(NSString *)placementId;
+ (NSArray *)getProviderPlacementList;

+ (void)addToReadyListForPlacmentId:(NSString *)placementId;
+ (BOOL)checkForReadyWithPlacementId:(NSString *)placementId;
+ (void)removeFromReadyListForPlacementId:(NSString *)placementId;
+ (void)removeAllFromReadyList;

@end

#endif

//
//  ASAdMob.h
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/23/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ASInterstitialAdProvider.h"

@interface ASAdMobInterstitialAdProvider : ASInterstitialAdProvider

@end

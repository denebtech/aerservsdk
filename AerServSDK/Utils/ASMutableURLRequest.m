//
//  ASMutableURLRequest.m
//  AerServSDK
//
//  Created by Gauthier on 8/1/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASMutableURLRequest.h"
#import "ASAnalyticsConfigChecker.h"
#import "ASStaticLoggerFields.h"

@implementation ASMutableURLRequest

+ (void)createUAString {    
    NSString *webViewAgent = @"AppleTV";
    
    // TODO: find a way to access the framework and pull VERSION from in there
    [ASStaticLoggerFields setSDKVersion:kAerServSDKVersion];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMutableURLRequest, createUAString - sdkv: %@", [ASStaticLoggerFields fields].sdkv]];
    [ASStaticLoggerFields setUserAgent:[NSString stringWithFormat:@"%@ %@/%@", webViewAgent, kAerServSDKName, kAerServSDKVersion]];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMutableURLRequest, createUAString - uaStr: %@", [ASStaticLoggerFields fields].ua]];
    
    // run analytics check
    [ASAnalyticsConfigChecker checkAnalyticsConfigWithPlacements:nil orSiteId:nil];
}

+ (id)requestWithURL:(NSURL *)theURL {
    ASMutableURLRequest *request = [super requestWithURL:theURL];
    
    if(![ASStaticLoggerFields fields].ua) {
        [ASMutableURLRequest createUAString];
    }
    
    [request addValue:[ASStaticLoggerFields fields].ua forHTTPHeaderField:kHTTPHeaderUserAgentFieldKey];
    
    return request;
}

@end

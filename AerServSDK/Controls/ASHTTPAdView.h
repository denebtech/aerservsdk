//
//  ASWebView.h
//  AerServSDK
//
//  Created by Scott A Andrew on 4/14/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASWebView.h"

#define kInitialLoadingDelayInMS 250
#define kBitMapCheckDelayInMS 250
#define kBitMapCheckMaxAttempts 4

@protocol ASHTTPAdViewDelegate;

@interface ASHTTPAdView : ASWebView

// use this for ad notificatons.. Do NOT SET THE DELEGATE WE NEED IT.
// IF YOU DO YOU NEED TO CALL BACK SAFELY.
@property (nonatomic, weak) id<ASHTTPAdViewDelegate> adDelegate;

//- (void)rotateToOrientation:(UIInterfaceOrientation)orientation;
- (void)forceRedraw;
- (void)scaleAd:(float)scale;
- (void)resetCache;

@end


@protocol ASHTTPAdViewDelegate <NSObject>

@optional

- (void)HTTPAdViewFinsihedLoading:(ASHTTPAdView*)adView;
- (void)HTTPAdViewFailedLoading:(ASHTTPAdView *)adView withError:(NSError*)error;

// return YES to continue loading in the current view. NO to stop.
- (BOOL)HTTPAdViewWasClicked:(ASHTTPAdView*)adView request:(NSURLRequest*)request;

- (void)AVPlayerCreated:(AVPlayer *)avPlayer;

- (void)HTTPAdViewDidFireBPF:(ASHTTPAdView *)adView;

@end

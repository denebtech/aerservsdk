//
//  ASViewController.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/24/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialViewController.h"
#import "ASInterstitialAdManager.h"
#import "ASInterstitialAdViewController.h"
#import "ASCloseButton.h"
//#import "ASAlertManager.h"
#import "AVPlayer+StateChange.h"
#import "ASLocationUtils.h"
#import "ASTransactionInfo.h"

const NSTimeInterval kASDefaultInterstitialAdTimeout = 15.0f;

@interface ASInterstitialViewController () <ASInterstitialAdManagerDelegate>

@property (nonatomic, weak) id<ASInterstitialViewControllerDelegate> delegate;
@property (nonatomic, copy) NSString *placementID;
@property (nonatomic, assign) BOOL isReady;

@property (nonatomic, strong) ASInterstitialAdManager *adManager;
@property (nonatomic, strong) NSTimer *timeoutTimer;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, weak) AVPlayer *avPlayer;

@end



@implementation ASInterstitialViewController

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASInterstitialViewController, dealloc"];
    _placementID = nil;
    _keyWords = nil;
    _pubKeys = nil;
    _userId = nil;
    _adManager = nil;
    _delegate = nil;
    [_timeoutTimer invalidate];
    _timeoutTimer = nil;
    _avPlayer = nil;
}

- (instancetype)initWithPlacmentID:(NSString*)placementID withDelegate:(id<ASInterstitialViewControllerDelegate>)delegate {
    if (self = [super init]) {
        _delegate = delegate;
        _placementID = placementID;
        _env = kASEnvProduction;
        _timeoutInterval = kASDefaultInterstitialAdTimeout;
        _locationServicesEnabled = NO;
        _platform = kASPlatformCS;
    }
    
    return self;
}

- (instancetype)viewControllerForPlacementID:(NSString*)placementID withDelegate:(id<ASInterstitialViewControllerDelegate>)delegate {
    return [[ASInterstitialViewController alloc] initWithPlacmentID:placementID withDelegate:delegate];
}

+ (instancetype)viewControllerForPlacementID:(NSString*)placementID withDelegate:(id<ASInterstitialViewControllerDelegate>)delegate {
    return [[ASInterstitialViewController alloc] initWithPlacmentID:placementID withDelegate:delegate];
}

#pragma mark - public methods

- (void)loadAd {
    @try {
        [ASSDKLogger startLogWithPLC:self.placementID andKeywords:self.keyWords andPubKeys:self.pubKeys];
        [ASSDKLogger logStatement:@"ASInterstitialViewController, loadAd"];

        [self killTimeoutTimer];
        self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:self.timeoutInterval target:self selector:@selector(onTimeoutTimer) userInfo:nil repeats:NO];
        
        self.isLoading = YES;
        
        // start ad response logger
        [ASAdResponseLogger startAdResponseLogWithPLC:self.placementID];
        
        // create our ad fetcher, unless I've preloaded and there already is one (in which case the preloading flag should be false, because now it is time to show
        // the ad...i.e. when preload is true, I always need a new adManager to fetch a new ad to preload...)
        if(self.adManager == nil || self.isPreload) {
            self.adManager = [ASInterstitialAdManager new];
        }
        self.adManager.timeoutInt = self.timeoutInterval;
        self.adManager.outlineAd = self.showOutline;
        self.adManager.isPreload = self.isPreload;
        
        // check flag to see if the publisher has enabled location services or not
        [ASLocationUtils getInstance].publisherRequestsLocationServicesEnabled = self.locationServicesEnabled;
        
        // we are the delegate.
        self.adManager.delegate = self;
        
        // go get our ad.
        [self.adManager fetchAdForPlacementID:self.placementID
                                        inEnv:self.env
                                usingkeyWords:self.keyWords
                                   andPubKeys:self.pubKeys
                                   withUserID:self.userId
                                   onPlatform:self.platform
         ];
        
        // unset preload after first 'fetch'
        if(self.isPreload) {
            self.isPreload = NO;
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)showFromViewController:(UIViewController*)parentViewController {
    [ASSDKLogger logStatement:@"ASInterstitialViewController, showFromViewController:"];
    @try {
        if(!self.isReady) {
            NSLog(@"AerServSDK -- Ad is not ready to be shown.");
            return;
        }
        
        if(parentViewController != nil) {
            [ASSDKLogger logStatement:@"ASInterstitialViewController, showAd - isReady & parentVC is defined, so show"];
            [self.adManager presentInterstitialFromViewController:parentViewController];
        } else {
            NSLog(@"AerServSDK -- Parent View Controller is not defined.");
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)pause{
    [self.avPlayer changeStateToPause];
}

- (void)play{
    [self.avPlayer changeStateToPlay];
}

- (void)showLogs:(BOOL)yesOrNo {
    [ASSDKLogger showLogs:yesOrNo];
}

#pragma mark - Timeout Methods

- (void)killTimeoutTimer {
    if (self.timeoutTimer != nil) {
        [self.timeoutTimer invalidate];
        self.timeoutTimer = nil;
    }
}

- (void)onTimeoutTimer {
    [ASSDKLogger logStatement:@"ASInterstitialViewController, onTimeoutTimer"];
    @try {
        NSError *error = [NSError errorWithDomain:@"Loading Timeout"
                                             code:100
                                         userInfo:@{NSLocalizedDescriptionKey : [NSString stringWithFormat:@"The interstitial ad took longer than %.2f to load", self.timeoutInterval ]}];
        [self interstitialAdManager:self.adManager didFailToFetchAdwithError:error];
        [self.adManager reportErrorForProvider];
        [self.adManager cancel];
        self.adManager = nil;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}


- (void)onCloseAd {
    // dismiss us when the close button is pressed.
    
    [ASSDKLogger logStatement:@"ASInterstitialViewController, onCloseAd - Dismissing interstitial ad."];
    @try {
        [self killTimeoutTimer];
        
        // if someone got presented on top of us then dismiss it.. Then us.
        if (self.presentedViewController != nil) {
            
            // dismis the controller being presetned without animation then dimsiss us.
            __weak ASInterstitialViewController* interstitialViewController = self;
            [self.presentedViewController dismissViewControllerAnimated:NO completion: ^{
                [interstitialViewController dismissViewControllerAnimated:YES  completion:nil];
            }];
        }
        else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - View Controller Default Methods

- (void)viewWillDisappear:(BOOL)animated {
    @try {
        [super viewWillDisappear:animated];
        // tell our delegate that we are disappearing if it wants to know.
        if ([self.delegate respondsToSelector:@selector(interstitialViewControllerWillDisappear:)])
            [self.delegate interstitialViewControllerWillDisappear:self];
        [self killTimeoutTimer];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    @try {
        [super viewDidDisappear:animated];
        if ([self.delegate respondsToSelector:@selector(interstitialViewControllerDidDisappear:)])
            [self.delegate interstitialViewControllerDidDisappear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - Interstitial Ad Manager Delegates

- (void)interstitialAdManager:(ASInterstitialAdManager*)manager didFailToFetchAdwithError:(NSError*)error {
    if (!self.isLoading) {
        return;
    }
    self.isLoading = NO;
    
    // if there was an error make sure to report back to our delegate.
    if([self.delegate respondsToSelector:@selector(interstitialViewControllerAdFailedToLoad:withError:)]) {
        [self.delegate interstitialViewControllerAdFailedToLoad:self withError:error];
    }
    [self killTimeoutTimer];
}

- (void)interstitialAdManagerCreatedAdController:(ASInterstitialAdManager *)manager {
    if (self.isLoading) {
        self.isLoading = NO;
        
        self.isReady = YES;
        [self killTimeoutTimer];

        // tell our delegate we were successful. It should trigger them to display us.
        if ([self.delegate respondsToSelector:@selector(interstitialViewControllerAdLoadedSuccessfully:)])
            [self.delegate interstitialViewControllerAdLoadedSuccessfully:self];
    }
}

- (void)interstitialAdManagerDidPreload:(ASInterstitialAdManager *)manager {
    if (self.isLoading) {
        [ASSDKLogger logStatement:@"ASInterstitialViewController | interstitialAdManagerDidPreload"];
        self.isLoading = NO;
        
        self.isReady = YES;
        [self killTimeoutTimer];
        
        if ([self.delegate respondsToSelector:@selector(interstitialViewControllerDidPreloadAd:)]) {
            [self.delegate interstitialViewControllerDidPreloadAd:self];
        }
    }
}

- (void)interstitialAdManagerAdWillAppear:(ASInterstitialAdManager*)manager {
    
    if ([self.delegate respondsToSelector:@selector(interstitialViewControllerWillAppear:)])
        [self.delegate interstitialViewControllerWillAppear:self];
}

- (void)interstitialAdManagerAdDidAppear:(ASInterstitialAdManager*)manager {
    if ([self.delegate respondsToSelector:@selector(interstitialViewControllerDidAppear:)])
        [self.delegate interstitialViewControllerDidAppear:self];
}

- (void)interstitialAdManagerAdCompleted:(ASInterstitialAdManager*)manager {
    if ([self.delegate respondsToSelector:@selector(interstitialViewControllerAdDidComplete:)])
        [self.delegate interstitialViewControllerAdDidComplete:self];
}

- (void)interstitialAdManagerAdDidDisappear:(ASInterstitialAdManager*)manager {
    if ([self.delegate respondsToSelector:@selector(interstitialViewControllerDidDisappear:)])
        [self.delegate interstitialViewControllerDidDisappear:self];
}

- (void)interstitialAdManagerAdWillDisappear:(ASInterstitialAdManager*)manager {
    if ([self.delegate respondsToSelector:@selector(interstitialViewControllerWillDisappear:)])
        [self.delegate interstitialViewControllerWillDisappear:self];
}

- (void)interstitialAdManagerAdDidExpire:(ASInterstitialAdManager*)manager {
//    if ([self.delegate respondsToSelector:@selector(interstitialViewControllerAdExpired:)])
//        [self.delegate interstitialViewControllerAdExpired:self];
}

- (void)inerstititalAdManagerAdWasTouched:(ASInterstitialAdManager *)manager {
    if ([self.delegate respondsToSelector:@selector(interstitialViewControllerAdWasTouched:)]) {
        [self.delegate interstitialViewControllerAdWasTouched:self];
    }
}

- (void)didVirtualCurrencyLoad:(NSDictionary *)vcData {
    if ([self.delegate respondsToSelector:@selector(interstitialViewControllerDidVirtualCurrencyLoad:vcData:)]) {
        [self.delegate interstitialViewControllerDidVirtualCurrencyLoad:self vcData:vcData];
    }
}

- (void)didVirtualCurrencyReward:(NSDictionary *)vcData {
    if ([self.delegate respondsToSelector:@selector(interstitialViewControllerDidVirtualCurrencyReward:vcData:)]) {
        [self.delegate interstitialViewControllerDidVirtualCurrencyReward:self vcData:vcData];
    }
}

- (void)AVPlayerCreated:(AVPlayer *)avPlayer{
    self.avPlayer = avPlayer;
}

- (void)interstitialAdManager:(ASInterstitialAdManager *)manager didFireAdvertiserEventWithMessage:(NSString *)msg {
    if([self.delegate respondsToSelector:@selector(interstitialViewController:didFireAdvertiserEventWithMessage:)]) {
        [self.delegate interstitialViewController:self didFireAdvertiserEventWithMessage:msg];
    }
}

- (void)interstitialAdManager:(ASInterstitialAdManager *)manager didFindAdTransactionInfo:(ASTransactionInfo *)transactionInfo {
    if([self.delegate respondsToSelector:@selector(interstitialViewController:didShowAdWithTransactionInfo:)]) {
        [self.delegate interstitialViewController:self didShowAdWithTransactionInfo:[transactionInfo asTransactionInfoObj]];
    }
}

@end

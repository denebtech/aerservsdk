//
//  ASXMLQueryEngine.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/26/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const ASXMLNameKey;
extern NSString* const ASXMLValueKey;
extern NSString* const ASXMLChildrenKey;
extern NSString* const ASXMLAttributesKey;

@interface ASXMLQueryEngine : NSObject

- (instancetype)initWithXMLData:(NSData *)data;
- (NSArray *)queryWithXPath:(NSString *)xPathQuery;

@end

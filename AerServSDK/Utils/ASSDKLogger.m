//
//  ASSDKLogger.m
//  AerServSDK
//
//  Created by Vasyl Savka on 7/15/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASSDKLogger.h"
#import "ASCommManager.h"

@interface ASSDKLogger()

@property (nonatomic, assign) BOOL shouldClear;
@property (nonatomic, assign) BOOL shouldShowLogs;

@end

@implementation ASSDKLogger

+ (ASSDKLogger *)logInstance {
    static ASSDKLogger *logInstance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        logInstance = [ASSDKLogger new];
        
        logInstance.dnt = -1;
        logInstance.logQ = dispatch_queue_create("com.aerserv.ios-sdk.logQueue", DISPATCH_QUEUE_SERIAL);
        logInstance.logs = [NSMutableArray array];
        logInstance.cb = @"cache-buster";
        logInstance.shouldClear = NO;
        logInstance.shouldShowLogs = NO;
    });
    
    return logInstance;
}

+ (void)startLogWithPLC:(NSString *)plcStr andKeywords:(NSArray *)keywordsArray andPubKeys:(NSDictionary *)pubKeys{
//    if([self logInstance].shouldClear) {
//        __weak NSMutableArray *tempArr = [self logInstance].logs;
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            [tempArr removeAllObjects];
//        });
//        [self logInstance].logs = [NSMutableArray array];
//    }
//        
//    
//    static dispatch_once_t onceToken;
//    __weak ASSDKLogger *logger = [ASSDKLogger logInstance];
//    dispatch_once(&onceToken, ^{
//        logger.shouldClear = YES;
//    });
    
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASSDKLogger, startLogWithPLC:andKeywords:andPubKeys: - plc: %@", plcStr]];
    [self logInstance].plc = plcStr;
    
    NSString *keywordsStr = @"";
    for(NSString *keyword in keywordsArray) {
        keywordsStr = [keywordsStr stringByAppendingString:[NSString stringWithFormat:@"%@,", keyword]];
    }
    keywordsStr = (keywordsStr.length > 1) ? [keywordsStr substringToIndex:(keywordsStr.length - 1)] : @"";
    [self logInstance].keywords = keywordsStr;
    
    NSString *pubKeysStr = @"";
    for(NSString *key in pubKeys.allKeys){
        pubKeysStr = [pubKeysStr stringByAppendingString:[NSString stringWithFormat:@"%@=%@,", key, [pubKeys valueForKey:key]]];
    }
    pubKeysStr = (pubKeysStr.length > 1) ? [pubKeysStr substringToIndex:(pubKeysStr.length - 1)] : @"";
    [self logInstance].pubKeys = pubKeysStr;
}

+ (void)showLogs:(BOOL)yesOrNo {
    [self logInstance].shouldShowLogs = yesOrNo;
}

+ (void)logStatement:(NSString *)logStr {
    @try {
        __weak ASSDKLogger *logger = [self logInstance];
        if(logger != nil) {
            dispatch_async(logger.logQ, ^{
                if(logger.logs != nil) {
                    while(logger.logs.count >= kASSDKLoggerQueueLimit) {
                        if(logger.logs.count > 0 && logger.logs[0] != nil)
                            [logger.logs removeObjectAtIndex:0];
                    }
                    [logger.logs addObject:logStr];
                    if(logger.shouldShowLogs) {
                        NSLog(@"AerServSDK -- %@", logStr);
                    }
                }
            });
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}


+ (void)onException:(NSException *)ex {
    NSDictionary *jsonObj = nil;
    @try {
        jsonObj = @{ kASSDKLoggerOSKey : [ASStaticLoggerFields fields].os,
                     kASSDKLoggerOSVKey : [ASStaticLoggerFields fields].osv,
                     kASSDKLoggerADIDKey : [ASStaticLoggerFields fields].adid,
                     kASSDKLoggerOIDKey : [ASStaticLoggerFields fields].oid,
                     kASSDKLoggerBundleIDKey : [ASStaticLoggerFields fields].bundleid,
                     kASSDKLoggerPLCKey : ([self logInstance].plc) ? [self logInstance].plc : @"N/A",
                     kASSDKLoggerKeywordsKey : ([self logInstance].keywords) ? [self logInstance].keywords : @"N/A",
                     kASSDKLoggerPubKeysKey : ([self logInstance].pubKeys) ? [self logInstance].pubKeys : @"N/A",
                     kASSDKLoggerDNTKey : [NSString stringWithFormat:@"%ld", (long)[self logInstance].dnt],
                     kASSDKLoggerCarrierKey : ([self logInstance].carrier) ? [self logInstance].carrier : @"N/A",
                     kASSDKLoggerNetworkKey : ([self logInstance].network) ? [self logInstance].network : @"N/A",
                     kASSDKLoggerMCCKey : ([self logInstance].mcc) ? [self logInstance].mcc : @"N/A",
                     kASSDKLoggerLatKey : ([self logInstance].coordLat) ? [self logInstance].coordLat : @"N/A",
                     kASSDKLoggerLongKey : ([self logInstance].coordLong) ? [self logInstance].coordLong : @"N/A",
                     kASSDKLoggerSDKVKey : ([ASStaticLoggerFields fields].sdkv) ? [ASStaticLoggerFields fields].sdkv : @"N/A",
                     kASSDKLoggerUAKey : ([ASStaticLoggerFields fields].ua) ? [ASStaticLoggerFields fields].ua : @"N/A",
                     kASSDKLoggerLogsKey : [self logInstance].logs,
                     kASSDKLoggerExKey : @{ kASSDKLoggerExNameKey : ex.name,
                                         kASSDKLoggerExReasonKey : ex.reason,
                                         kASSDKLoggerExTraceKey : ex.callStackSymbols
                                         }
                     };
    }
    @catch (NSException *exception) {
        [[ASSDKLogger logInstance].logs arrayByAddingObjectsFromArray:@[ ex.description, [NSString stringWithFormat:@"Caught exception inside ASSDKLogger.on: %@", exception.description] ]];
        jsonObj = @{ kASSDKLoggerLogsKey : [ASSDKLogger logInstance].logs };
    }
    
    NSError *jsonErr = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:0 error:&jsonErr];
    if(!jsonErr) {
        [ASCommManager sendPostRequestToEndPoint:kASDebugPOSTURL
                                    withJSONData:jsonData
                                         forTime:kASLoggerConnTimeout
                          endWithCompletionBlock:^(NSURLResponse *resp, NSData *data, NSError *err) {
                              if(err != nil) {
                                  [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASSDKLogger, onException: - POST TO DEBUG ERROR: %@", err.localizedDescription]];
                              } else {
                                  [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASSDKLogger, onException: - POST SENT TO DEBUG, jsonObj: %@", jsonObj]];
                              }
                          }
        ];
    } else {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASSDKLogger, onException - POST NOT SENT, JSONSERIALIZATION ERROR: %@", jsonErr]];
    }
    [[self logInstance].logs removeAllObjects];
}

@end

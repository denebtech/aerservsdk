//
//  ASVungleInterstitialAdProvider.m
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/28/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVungleInterstitialAdProvider.h"
#import "ASVungleInstance.h"
#import "ASVirtualCurrency.h"

#import <VungleSDK.h>

@interface ASVungleInterstitialAdProvider()<VungleSDKDelegate>

@property (nonatomic, strong) NSTimer* adCheckTimer;
@property (nonatomic, assign) BOOL isPreload;
@property (nonatomic, strong) NSDictionary *properties;

@end

@implementation ASVungleInterstitialAdProvider

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASVungleInterstitialAdProvider, dealloc"];
    [self cleanupAd];
}

- (void)cleanupTimer {
    if (self.adCheckTimer != nil) {
        [self.adCheckTimer invalidate];
        self.adCheckTimer = nil;
    }
}

- (void)cleanupAd {
    [self cleanupTimer];
    
    [[[ASVungleInstance instance].vungleClass sharedSDK] setDelegate:nil];
}

- (void)onAdCheckTimer:(NSTimer *)timer {
    if ([[[ASVungleInstance instance].vungleClass sharedSDK] isAdPlayable]) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVungleInterstitialAdProvider, onAdCheckTimer - Cached ad availabe with preload: %d", self.isPreload]];
        [self cleanupTimer];
		
		if(self.isPreload) {
            [self.delegate interstitialAdProvider:self didPreloadAd:nil];
        } else {
            [self.delegate interstitialAdProvider:self didLoadAd:self];
        }
    }
}


- (void)requestInterstitialAdWithProperties:(NSDictionary *)info isPreload:(BOOL)preload {
    self.properties = info;
    self.isPreload = preload;
    
    if(![ASVungleInstance instance].vungleClass || [ASVungleInstance instance].state == kASProviderSetupNone) {
        [ASVungleInstance instance].vungleClass = NSClassFromString(@"VungleSDK");
        
        if( [ASVungleInstance instance].vungleClass == nil || kIS_iOS_6) {
            [ASSDKLogger logStatement:@"ASVungleInterstitialAdProvider, requestInterstitialAdWithProperties:isPreload: - Could not create Vungle class object, failing over"];
            
            NSError* error = [NSError errorWithDomain:NSStringFromClass([ASVungleInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Could not link against VungleSDK.  Failing over"}];
            
            [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
            [ASVungleInstance instance].vungleClass = nil;
            
            return;
        }
        
        // checking for virtual currency data
        if(info[ASInterstitialParameter_HTMLHeaders] != nil) {
            NSDictionary *vcData = [ASVirtualCurrency getVirtualCurrencyData:info[ASInterstitialParameter_HTMLHeaders]];
            if(vcData != nil)
                [self.delegate interstitialAdProvider:self didVirtualCurrencyLoad:vcData];
        }
        
        // starting Vungle from received
        NSDictionary *parameters = info[ASInterstitialParameter_Parameters];
        [[[ASVungleInstance instance].vungleClass sharedSDK] startWithAppId:parameters[@"appId"]];
    }
    [[[ASVungleInstance instance].vungleClass sharedSDK] setDelegate:self];
    
    // there is no callback when the add is actually loaded. So we are going to poll every 1/2 second.
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVungleInterstitialAdProvider, requestInterstitialAdWithProperties:isPreload: - Schedule timer to poll for cached ad every %0.1f", kVungleInterstitialAdCachePollTime]];
    self.adCheckTimer = [NSTimer scheduledTimerWithTimeInterval:kVungleInterstitialAdCachePollTime
                                                         target:self
                                                       selector:@selector(onAdCheckTimer:)
                                                       userInfo:nil repeats:YES];
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASVungleInterstitialAdProvider, cancel"];
    [self cleanupAd];
}


- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    id sdk = [[ASVungleInstance instance].vungleClass sharedSDK];
    
    // kill our timer just in case someone was impatient.
    [self cleanupTimer];
    
    if ([sdk isAdPlayable]) {
        [ASSDKLogger logStatement:@"ASVungleInterstitialAdProvider, showInterstitialFromRootViewController: - Cached Ad available so present and play ad"];
        NSError *err = nil;
        [sdk playAd:rootViewController withOptions:nil error:&err];
        if(err != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVungleInterstitialAdProvider, showInterstitialFromRootViewController: - ERROR while playing ad, err: %@", err.localizedDescription]];
        }
    }
    else {
        [ASSDKLogger logStatement:@"ASVungleInterstitialAdProvider, showInterstitialFromRootViewController: - No cached Ad available, faile over"];
        NSError *error = [NSError errorWithDomain:NSStringFromClass([ASVungleInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Ad is not available."}];
        
        [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
    }
}

#pragma mark - VungleSDKDelegate Methods

- (void)vungleSDKwillShowAd {
    [ASSDKLogger logStatement:@"ASVungleInterstitialAdProvider, vungleSDKwillShowAd"];
    [self.delegate interstitialAdProviderWillAppear:self];
    [self.delegate interstitialAdProviderDidAppear:self];
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
}

- (void)vungleSDKwillCloseAdWithViewInfo:(NSDictionary*)viewInfo willPresentProductSheet:(BOOL)willPresentProductSheet {
    if (!willPresentProductSheet) {
        [ASSDKLogger logStatement:@"ASVungleInterstitialAdProvider, vungleSDKwillCloseAdWithViewInfo:willPresentProductSheet: - Will not present product sheet, so clean up ad"];
        
        if (viewInfo[@"completedView"]) {
            [self.delegate interstitialAdProviderDidAdComplete:self];
            
            // Fire virtual currency events, if necessary
            NSDictionary *virtualCurrency = [ASVirtualCurrency getVirtualCurrencyData:self.properties[ASInterstitialParameter_HTMLHeaders]];
            if (virtualCurrency) {
                [self.delegate interstitialAdProvider:self didVirtualCurrencyReward:virtualCurrency];
                [ASVirtualCurrency vcServerCallbackWithVCData:virtualCurrency];
            }
        }
        
        [self.delegate interstitialAdProviderWillDisappear:self];
        [self.delegate interstitialAdProviderDidDisappear:self];
        
        [self cleanupAd];
    } else {
        [ASSDKLogger logStatement:@"ASVungleInterstitialAdProvider, vungleSDKwillCloseAdWithViewInfo:willPresentProductSheet: - Product shee with be presented"];
    }
}

- (void)vungleSDKwillCloseProductSheet:(id)productSheet {
    [ASSDKLogger logStatement:@"ASVungleInterstitialAdProvider, vungleSDKwillCloseProductSheet:"];
    // Fire virtual currency events, if necessary
    NSDictionary *virtualCurrency = [ASVirtualCurrency getVirtualCurrencyData:self.properties[ASInterstitialParameter_HTMLHeaders]];
    if (virtualCurrency) {
        [self.delegate interstitialAdProvider:self didVirtualCurrencyReward:virtualCurrency];
        [ASVirtualCurrency vcServerCallbackWithVCData:virtualCurrency];
    }
    [self.delegate interstitialAdProviderWillDisappear:self];
    [self.delegate interstitialAdProviderDidDisappear:self];
    [self cleanupAd];
}

- (void)vungleSDKAdPlayableChanged:(BOOL)isAdPlayable {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVUngleInterstitialAdProvider, vungleSDKAdPlayableChanged: - isAdPlayable: %d", isAdPlayable]];
    if(isAdPlayable) {
        [self onAdCheckTimer:nil];
    }
}

@end

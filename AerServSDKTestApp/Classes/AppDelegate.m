
//  AppDelegate.m
//  AerServSDKTestApp
//
//  Created by Arash Sharif on 11/5/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <AerServSDK/AerServSDK.h>
#import "ASSDKLogger.h"
#import "ASCacheUtil.h"

#import <Tapjoy/Tapjoy.h>

@interface AppDelegate ()

@property (nonatomic, strong) ASCacheUtil *cache;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"--- Main Thread: %d", [[NSThread currentThread] isMainThread]]];
    
    #if kLogRedirects
    // set shared cached
    _cache = [ASCacheUtil new];
    [NSURLCache setSharedURLCache:_cache];
    #endif

    
    // init crashlytics
    [Fabric with:@[[Crashlytics class]]];
    
    // initialize aerservsdk for siteid
//    [AerServSDK initializeWithAppID:@"100761"]; //@"104486" //@"102430" @//"1000277"
//    [AerServSDK initializeWithPlacments:@[@"1010889", @"1010888"]];
    
    // tapjoy setup    
    [Tapjoy setDebugEnabled:YES];
    [Tapjoy connect:@"lMRtVHXzS5qooacI6fqIVgEBvfsvzD4oWMWXLzNmh5TuAdCMyKngJk3vHF9L"];

    
    if(kIS_iOS_6) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    _cache = nil;
}

@end

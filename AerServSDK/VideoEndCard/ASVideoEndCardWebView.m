//
//  ASVideoEndCardWebView.m
//  AerServSDK
//
//  Created by Albert Zhu on 4/29/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASVideoEndCardWebView.h"
#import "ASVideoEndCardAd.h"

#import "ASCommManager.h"

@interface ASVideoEndCardWebView () <UIWebViewDelegate>

@property (nonatomic, strong) ASVideoEndCardAd *vecAd;
@property (nonatomic, assign) BOOL didLoadOnce;

@end

@implementation ASVideoEndCardWebView

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASVideoEndCardWebView, dealloc"];
    _vecWebViewDeleagte = nil;
    _vecAd = nil;
}

- (instancetype)initWithVideoEndCardAd:(ASVideoEndCardAd *)vecAd andWebViewDelegate:(id<ASVideoEndCardWebViewDelegate>)vecWebViewDelegate {
    [ASSDKLogger logStatement:@"ASVideoEndCardWebView, initWithVideoEndCardAd:andWebViewDelegate:"];
    CGFloat lsWidth = MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    CGFloat lsHeight = MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    CGRect viewFrame = CGRectMake(0.0f, 0.0f, lsWidth, lsHeight);
    
    if(self = [super initWithFrame:viewFrame]) {
        _vecAd = vecAd;
        _vecWebViewDeleagte = vecWebViewDelegate;
        
        self.delegate = self;
    }
    
    return self;
}

+ (instancetype)webViewForVideoEndCardAd:(ASVideoEndCardAd *)vecAd andWebViewDelegate:(id<ASVideoEndCardWebViewDelegate>)vecWebViewDelegate {
    [ASSDKLogger logStatement:@"ASVideoEndCardWebView, webViewForVideoEndCardAd:andWebViewDelegate:"];
    return [[ASVideoEndCardWebView alloc] initWithVideoEndCardAd:vecAd andWebViewDelegate:vecWebViewDelegate];
}

#pragma mark - Public Methods

- (void)loadEndCard {
    [ASSDKLogger logStatement:@"ASVideoEndCardWebView, loadEndCard"];
    [self loadData:[self.vecAd getEndCardHTMLData] MIMEType:kVideoEndCardMIMEType textEncodingName:kVideoEndCardEncodingName baseURL:[NSURL URLWithString:kVideoEndCardBaseURL]];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    [ASSDKLogger logStatement:@"ASVideoEndCardWebView, webView:shouldStartLoadWithRequest:navigationType:"];
    if(self.didLoadOnce && [request.URL.absoluteString isEqualToString:self.vecAd.redirectUrl.absoluteString]) {
        [self.vecWebViewDeleagte videoEndCardWebViewWasClicked:self];
    }
    
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [ASSDKLogger logStatement:@"ASVideoEndCardWebView, webView:didFailLoadWithError:"];
    if(!self.didLoadOnce)
        [self.vecWebViewDeleagte videoEndCardWebView:self didFailWithError:error];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [ASSDKLogger logStatement:@"ASVideoEndCardWebView, webViewDidFinishLoad:"];
    
    if(!self.didLoadOnce) {
        self.didLoadOnce = YES;
        [self.vecWebViewDeleagte videoEndCadWebViewDidFinishLoad:self];
    }
}

@end

//
//  ASVASTBannerAdProvider.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/22/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVASTBannerAdProvider.h"
#import "ASVASTAd.h"
#import "ASVASTVideoPlayer.h"
#import "ASVideoView.h"
#import "ASVASTCreative.h"
#import "ASVASTVideoPlayerView.h"
#import "ASVASTParser.h"
#import "ASVASTAd.h"
#import "ASVASTContainer.h"
#import "ASCommManager.h"

#import "NSURL+ASUtils.h"

@interface ASVASTBannerAdProvider()<ASVASTAdDelegate, ASVASTVideoPlayerDelegate>

@property (nonatomic, copy) NSDictionary *properties;
@property (nonatomic, strong) ASVASTVideoPlayer *videoPlayer;
@property (nonatomic, assign) BOOL didFirstVideoPlayed;
@property (nonatomic, strong) ASVASTAd *ad;

@end



@implementation ASVASTBannerAdProvider

@synthesize closeOffset;

// called from ASBannerAdProvider within try/catch
- (void)requestAdWithSize:(CGSize)size withProperties:(NSDictionary *)properties isPreload:(BOOL)preload {
    CGRect frame = CGRectZero;
    
    frame.size = size;
    
    self.properties = properties;
    self.isPreload = preload;
    self.closeOffset = [self.delegate closeOffset];
    
    // if we already have data.. Then we can go ahead and have it processed..
    if (properties[ASBannerParameter_HTMLData] != nil && properties[ASBannerParameter_HTMLHeaders]) {
        if (![self processData:properties[ASBannerParameter_HTMLData] withResponseHeaders:properties[ASBannerParameter_HTMLHeaders]]){
            [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, requestAdWithSize:withProperties:isPreload: - Failed to process VAST banner ad data"];
        };
    }
    else if (properties[ASBannerParameter_Parameters] != nil) {
        NSString* urlString = properties[ASBannerParameter_Parameters][ASBannerParameter_Parameter_URL];

        NSURL* url = [NSURL URLWithAddress:urlString];

        if (url == nil) {
            NSError* error = [NSError errorWithDomain:NSStringFromClass([ASVASTBannerAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Unable to create URL to retrieve ad from."}];
            
            [self.delegate bannerProvider:self didFailToLoadAdWithError:error];
            
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTBannerAdProvider, requestAdWithSize:withProperties:isPreload: - Failed to create URL %@", url]];
        }
        else {
            [self fetchAdFromURL:url];
        }

    }
}

// called from ASBannerAdProvider within try/catch
- (void)startPreloadedBannerAd {
    if(self.isPreload && [self shouldShowAd]) {
        [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, startPreloadedBannerAd"];
        [self.delegate bannerProvider:self didFinishLoadingPreloadAd:self.videoPlayer.view];
        [self.videoPlayer startAd];
    }
}

// called from requestAdWithSize:withProperties:isPreload:
- (void)fetchAdFromURL:(NSURL*)url {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTBannerAdProvider, fetchAdFromURL - url: %@", url]];
    
    __weak ASVASTBannerAdProvider* adProvider = self;
    [ASCommManager sendGetRequestToEndPoint:url forTime:self.timeInt endWithCompletionBlock:^(NSURLResponse *resp, NSData *data, NSError *err) {
        @try {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)resp;
            if (err != nil || [httpResponse statusCode] != 200) {
                [adProvider.delegate bannerProvider:adProvider didFailToLoadAdWithError:err];
            }
            
            NSDictionary *headers = [httpResponse allHeaderFields];
            [adProvider.delegate bannerProvider:adProvider willUpdateForFailoverWithResponseHeaders:headers];
            [adProvider processData:data withResponseHeaders:headers];
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];

}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, cancel - stop ad"];
    [super cancel];
    [self.videoPlayer stopAd];
}

// called from requestAdWithSize:withProperties:isPreload: and fetchAdFromURL:
- (BOOL)processData:(NSData *)data withResponseHeaders:(NSDictionary *)headers {
    [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, processData:withResponseHeaders"];
    
    ASVASTContainer *container = [ASVASTParser findAdWithXMLData:data withDelegate:self asPreload:self.isPreload];
    
    if (container.vastAd != nil) {
        self.ad = container.vastAd;
        if (self.ad.isWrapper) {
            [self.ad resolveWrappedAd];
        }
    }
    
    return (self.ad != nil && !container.didFailoverAlready);
}

#pragma mark - ASVASTAdDelegate methods

- (void)vastAdDidLoad:(ASVASTAd *)ad {
    if (ad != nil) {
        __weak ASVASTBannerAdProvider* provider = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                provider.videoPlayer = [[ASVASTVideoPlayer alloc] init];
                provider.videoPlayer.delegate = provider;
                [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, vastAdDidLoad - VAST banner video player created"];
                
                [provider.videoPlayer playVASTAd:ad];
            }
            @catch (NSException *exception) {
                [provider.delegate bannerProvider:provider didFailToLoadAdWithError:[NSError errorWithDomain:NSStringFromClass([ASVASTBannerAdProvider class])
                                                                                                        code:100
                                                                                                    userInfo:@{NSLocalizedDescriptionKey : @"Banner VAST did not load correctly"}]];
                [ASSDKLogger onException:exception];
            }
        });
    }
}

- (void)vastAdDidFailToLoad:(ASVASTAd *)ad withError:(NSError *)error andVastErrorCode:(ASVASTErrorCode)errCode {[ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTBannerAdProvider, vastAdDidFailToLoad:withError:andVastErrorCode: - error: %@ & VASTErrorCode: %ld", error.localizedDescription, (long)errCode]];
    
    if(errCode != kVASTErrorCodeNoERROR) {
        [ASCommManager sendAsynchronousRequestsForEvent:[self.ad.errorEvent replaceErrorMacroWithCode:errCode]];
        [self.delegate bannerProvider:self didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Banner Provider Fires Error Event from VAST Ad with errCode: %ld", (long)errCode]];
    }
    
    [self.delegate bannerProvider:self didFailToLoadAdWithError:nil];
}

- (void)vastAd:(ASVASTAd *)vastAd didFireAdvertiserEventWithMessage:(NSString *)msg {
    
}

#pragma mark - ASVASTVideoPlayerDelegate methods

- (BOOL)shouldShowAd {
    return YES;
}

- (void)VASTVideoPlayerVideoIsReady:(ASVASTVideoPlayer *)videoPlayer {
    [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, VASTVideoPlayerVideoIsReady"];
    CGRect frame = CGRectZero;
    frame.size = [videoPlayer contentSize];
//    CGFloat minDimension = MIN(frame.size.width, frame.size.height);
//    frame.size = CGSizeMake(minDimension, minDimension);
    
    self.videoPlayer.view.frame = frame;
    if(!self.isPreload) {
        // display the ad
        if (!self.didFirstVideoPlayed) {
            [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, VASTVideoPlayerVideoIsReady - !isPreload && !didFirstVideoPlay, load ad"];
            [self.delegate bannerProvider:self didLoadAd:self.videoPlayer.view];
            self.didFirstVideoPlayed = YES;
        } else {
            [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, VASTVideoPlayerVideoIsReady - !isPreload && first video played already, do nothing"];
        }
        
        // start the ad
        [self.videoPlayer startAd];
    } else {
        // notify of preload ready
        [self.delegate bannerProvider:self didPreloadAd:self.videoPlayer.view];
    }
}

- (void)VASTVideoPlayerFailedToLoad:(ASVASTVideoPlayer *)videoPlayer withErrorCode:(ASVASTErrorCode)errCode {
    [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, VASTVideoPlayerFailedToLoad - VAST banner video failed to load"];
    
    if(errCode != kVASTErrorCodeNoERROR) {
        [ASCommManager sendAsynchronousRequestsForEvent:[self.ad.errorEvent replaceErrorMacroWithCode:errCode]];
        [self.delegate bannerProvider:self didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Banner Provider Fires Error Event from VAST Video Player with errCode: %ld", (long)errCode]];
    }
    
    [self.videoPlayer stopAd];
    if(self.videoPlayer.ad.seqAd != nil) {
        [self.delegate bannerProvider:self willShowSkip:NO];
        [self.videoPlayer playVASTAd:self.videoPlayer.ad.seqAd];
    } else if(self.delegate != nil) {
        [self.delegate bannerProvider:self
             didFailToLoadAdWithError:[NSError errorWithDomain:NSStringFromClass([ASVASTBannerAdProvider class])
                                                          code:100
                                                      userInfo:@{NSLocalizedDescriptionKey : @"Unable to load ad for playback"}]];
    }
}

- (void)VASTVideoPlayerAdCompleted:(ASVASTVideoPlayer *)videoPlayer {
    [self.delegate bannerProviderDidShowAdCompletely:self];
}

- (void)VASTVideoPlayer:(ASVASTVideoPlayer *)player wasClicked:(NSURL* )URL {
    [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, VASTVideoPlayer:wasClicked: - VAST banner video was clicked"];
    [self.delegate bannerProviderAdWasClicked:self];
    [[UIApplication sharedApplication] openURL:URL];
}

- (void)VASTVideoPlayerWillRewardVC:(ASVASTVideoPlayer *)player {
    [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, VASTVideoPlayeWillRewardVC:"];
}

- (UIView *)VASTVideoPlayerContainingView:(ASVASTVideoPlayer *)videoPlayer {
    return (UIView *)[self.delegate bannerProviderWillGetAdView:self];
}

- (void)AVPlayerCreated:(AVPlayer *)avPlayer {
    [self.delegate AVPlayerCreated:avPlayer];
}

- (void)VASTVideoPlayer:(ASVASTVideoPlayer *)videoPlayer willShowSkip:(BOOL)show {
    [self.delegate bannerProvider:self willShowSkip:show];
}

- (void)VASTVideoPlayerSizeChanged:(ASVASTVideoPlayer *)player {
    [ASSDKLogger logStatement:@"ASVASTBannerAdProvider, VASTVideoPlayerSizeChange:"];
    [self.delegate bannerProviderAdChangedSize:self];
}

- (void)VASTVideoPlayer:(ASVASTVideoPlayer *)videoPlayer didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate bannerProvider:self didFireAdvertiserEventWithMessage:msg];
}

@end

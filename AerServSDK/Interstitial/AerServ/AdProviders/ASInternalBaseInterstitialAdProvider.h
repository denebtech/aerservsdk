//
//  ASInternalBaseInterstitialAdProvider.h
//  AerServSDK
//
//  Created by Vasyl Savka on 4/9/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdProvider.h"
#import "ASInterstitialAdViewController.h"

@interface ASInternalBaseInterstitialAdProvider : ASInterstitialAdProvider <ASInterstitialAdViewControllerDelegate>

@property (nonatomic, strong) ASInterstitialAdViewController *viewControllerToPresent;
@property (readonly, nonatomic, strong) NSDictionary *properties;
@property (readonly, nonatomic, assign) BOOL isPreload;

@end

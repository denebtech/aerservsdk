//
//  ASVungleInterstitialAdProvider.h
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/28/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdProvider.h"
#import <Foundation/Foundation.h>

#define kVungleInterstitialAdCachePollTime 0.5f

@interface ASVungleInterstitialAdProvider : ASInterstitialAdProvider

@end

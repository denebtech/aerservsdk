//
//  ASAdMobTestDevices.h
//  AerServSDK
//
//  Created by Albert Zhu on 7/20/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#ifndef AerServSDK_ASAdMobTestDevices_h
#define AerServSDK_ASAdMobTestDevices_h

#define kASAdMobTestDevices @[ @"Simulator", @"615bf0b678aad37cb174f8b5a9bd0b6f", @"271a6019953dc23cf132799ccf2f4b43", @"f549328c171f243deb42a8d560e39f72", @"9adba534f084342ae959bbefa58341f6", @"75ac95817e55bcbda5944319a0ddcc2b", @"ce27b1b3325f04462f60f8cc40e658bf" ]

#endif

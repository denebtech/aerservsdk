//
//  ASYahooBannerAdProvider.m
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASYahooBannerAdProvider.h"
#import "Flurry.h"
#import "FlurryAdBanner.h"
#import "FlurryAdBannerDelegate.h"

@interface ASYahooBannerAdProvider() <FlurryAdBannerDelegate>

@property (nonatomic, strong) id flurryAdBanner;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;

@end


@implementation ASYahooBannerAdProvider

- (id)init {
    self.automaticallyRefreshAds = YES;
    return [super initWithAdClassName:@"Flurry" timeout:kCustomBannerAdTimeout];
}

- (void)initializePartnerAd:(NSDictionary *)properties {
    NSString *apiKey = [self valueInProperty:properties forKey:@"YahooApiKey"];
    Class flurryClass = NSClassFromString(@"Flurry");
    [flurryClass startSession:apiKey];
}

- (BOOL)hasPartnerAdInitialized {
    return YES;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    self.adLoaded = NO;
    self.adFailed = NO;
    
    NSString *adSpace = [self valueInProperty:properties forKey:@"YahooAdSpace"];
    Class flurryAdBannerClass = NSClassFromString(@"FlurryAdBanner");
    self.flurryAdBanner = [[flurryAdBannerClass alloc] initWithSpace:adSpace];
    [self.flurryAdBanner setAdDelegate:self];
    [self.flurryAdBanner fetchAdForFrame:[self partnerAdView].frame];
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd {
    [self.flurryAdBanner displayAdInView:[self partnerAdView] viewControllerForPresentation:[self asBannerViewController]];
}

- (void)startPreloadedBannerAd {
    [self.delegate bannerProvider:self didFinishLoadingPreloadAd:nil];
    [self showPartnerAd];
}

- (void)cancel {
    @try {
        [super cancel];
        [self.flurryAdBanner setAdDelegate:nil];
        self.flurryAdBanner = nil;
    } @catch (NSException *exception) {
        [ASSDKLogger logStatement:@"ASYahooBannerAdProvider, cancel -- EXCEPTION"];
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - FlurryAdBannerDelegate

- (void)adBanner:(FlurryAdBanner *)bannerAd adError:(FlurryAdError)adError errorDescription:(NSError *)errorDescription {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASYahooBannerAdProvider, adError: %@", errorDescription]];
    [self killRefreshTimer];
    self.adFailed = YES;
}

- (void)adBannerDidDismissFullscreen:(FlurryAdBanner *)bannerAd {
    [ASSDKLogger logStatement:@"ASYahooBannerAdProvider, adBannerDidDismissFullscreen"];
}

- (void)adBannerDidFetchAd:(FlurryAdBanner *)bannerAd {
    [ASSDKLogger logStatement:@"ASYahooBannerAdProvider, adBannerDidFetchAd"];
    self.adLoaded = YES;
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
    [self asPartnerBannerAdDidLoad:nil];
}

- (void)adBannerDidReceiveClick:(FlurryAdBanner *)bannerAd {
    [ASSDKLogger logStatement:@"ASYahooBannerAdProvider, adBannerDidReceiveClick"];
    [self asPartnerBannerAdWasClicked];
}

- (void)adBannerDidRender:(FlurryAdBanner *)bannerAd {
    [ASSDKLogger logStatement:@"ASYahooBannerAdProvider, adBannerDidRender"];
    [self scheduleRefresh];
}

- (void)adBannerVideoDidFinish:(FlurryAdBanner *)bannerAd {
    [ASSDKLogger logStatement:@"ASYahooBannerAdProvider, adBannerVideoDidFinish"];
}

- (void)adBannerWillDismissFullscreen:(FlurryAdBanner *)bannerAd {
    [ASSDKLogger logStatement:@"ASYahooBannerAdProvider, adBannerWillDismissFullscreen"];
    [self asPartnerBannerWillEndAction];
}

- (void)adBannerWillLeaveApplication:(FlurryAdBanner *)bannerAd {
    [ASSDKLogger logStatement:@"ASYahooBannerAdProvider, adBannerWillLeaveApplication"];
    [self asPartnerBannerWillLeaveApplication];
}

- (void)adBannerWillPresentFullscreen:(FlurryAdBanner *)bannerAd {
    [ASSDKLogger logStatement:@"ASYahooBannerAdProvider, adBannerWillPresentFullscreen"];
    [self asPartnerBannerWillBeginAction];
}

@end

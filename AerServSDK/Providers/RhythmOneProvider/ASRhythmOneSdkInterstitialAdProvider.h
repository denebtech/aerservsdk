//
//  ASRhythmOneInterstitialProvider.h
//  AerServSDK
//
//  Created by Albert Zhu on 7/1/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <AerServSDK/AerServSDK.h>

#define kRhythmOneAdClassStr @"RhythmOneAd"
#define kRhythmOneAdInterstitialClassStr @"RhythmOneAdInterstitial"

#define kRhythmOneAppIdKey @"RhythmOneAppId"
#define kRhythmOnePlacementKey @"RhythmOnePlacement"
#define kRhythmOneMediaTypeKey @"RhythmOneMediaType"
#define kRhythmOneFullscreenKey @"RhythmOneFullscreen"
#define kRhythmOneSkipButtonVisibleKey @"RhythmOneSkipButtonVisible"

#define kRhythmOneAdParamsAppIdKey @"appId"
#define kRhythmOneAdParamsPlacementKey @"placement"
#define kRhythmOneAdParamsMediaTypeKey @"mediaType"
#define kRhythmOneAdParamsFullscreenKey @"fullscreen"
#define kRhythmOneAdParamsSkipButtonVisibleKey @"skipButtonVisible"
#define kRhythmOneAdParamsLatKey @"lat"
#define kRhythmOneAdParamsLonKey @"lon"

#define kRhythmOneErrorEventErrorCodeKey @"errorCode"

extern NSString *const RhythmEventAdLoaded;
extern NSString *const RhythmEventAdError;
extern NSString *const RhythmEventAdImpression;
extern NSString *const RhythmEventAdClickThru;
extern NSString *const RhythmEventAdSkipped;
extern NSString *const RhythmEventAdVideoComplete;
extern NSString *const RhythmEventAdUserClose;
extern NSString *const RhythmEventAdClosed;

@interface ASRhythmOneSdkInterstitialAdProvider : ASCustomInterstitialAdProvider

@end

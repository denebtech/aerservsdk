//
//  NSString+ASUtils.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/25/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "NSString+ASUtils.h"

@implementation NSString (ASUtils)

- (BOOL)checkForEmpty {
    return [self isEqual:kEmptyStr];
}

- (id)JSONObjectValue {
    NSData* data = [self dataUsingEncoding:NSUTF8StringEncoding];
    
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
}

- (NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
	return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                               (CFStringRef)self,
                                                               NULL,
                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                               CFStringConvertNSStringEncodingToEncoding(encoding)));
}

- (NSTimeInterval)timeIntervalValue {
    NSArray* timeComponens = [self componentsSeparatedByString:@":"];
    
    if (timeComponens == 0)
        return 0;
    
    // lets get our mulitplier.. 60 to some power depending on the .. remember 60^0 = 1..
    NSInteger multiplier = pow(60,([timeComponens count] - 1));
    NSTimeInterval seconds = 0;
    
    for (NSString* componentString in timeComponens) {
        NSInteger componentValue = [componentString integerValue];
        
        seconds += componentValue * multiplier;
        
        multiplier /= 60;
    }
    
    return seconds;
}

- (NSString *)removeCharWhitespace {
    NSString *newStr = self;
    newStr = [newStr stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    newStr = [newStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return newStr;
}

- (NSString *)urlencode {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[self UTF8String];
    int sourceLen = (int)strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

- (BOOL)containsStr:(NSString *)inStr {
    NSRange range = [self rangeOfString:inStr];
    return range.location != NSNotFound;
}

@end

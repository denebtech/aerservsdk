//
//  ASAppLovinInstance.h
//  AerServSDK
//
//  Created by Albert Zhu on 11/25/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASGlobalProviderSetup.h"

#define kALSdkClassStr @"ALSdk"
#define kALSdkSettingsClassStr @"ALSdkSettings"
#define kALAdClassStr @"ALAd"
#define kALInterstitialAdClassStr @"ALInterstitialAd"
#define kALIncentivizedInterstitialAdClassStr @"ALIncentivizedInterstitialAd"

#define kASALVerboseLoggingKey @"AppLovinVerboseLogging"
#define kASALPreloadSizesKey @"AppLovinPreloadSizes"
#define kASALPreloadTypesKey @"AppLovinPreloadTypes"
#define kASALSdkKey @"AppLovinSdkKey"

#define kAppLovinSDKKey @"key"
#define kAppLovinVerboseLoggingKey @"verboseLogging"
#define kAppLovinPreloadSizesKey @"preloadSizes"
#define kALDefaultPreloadSizesStr @"BANNER,INTER"
#define kAppLovinPreloadTypesKey @"preloadTypes"
#define kALDefaultPreloadTypesStr @"REGULAR,VIDEOA"

@interface ASAppLovinInstance : NSObject

@property (nonatomic, strong) id alSdk;
@property (nonatomic, strong) id alPreInitInterstitialAd;
@property (nonatomic, assign) ASProviderSetupState state;

+ (ASAppLovinInstance *)instance;
+ (void)appLovinSetupWithConfig:(NSDictionary *)adapterConifg;

@end

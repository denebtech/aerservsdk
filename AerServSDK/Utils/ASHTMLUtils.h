//
//  ASHTMLUtils.h
//  AerServSDK
//
//  Created by Albert Zhu on 3/25/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kEventsAerServDomain @"events.aerserv"
#define kBPFEventStr @"ev=22"

@interface ASHTMLUtils : NSObject

+ (NSString *)htmlHeadStrInWebView:(UIWebView *)webview;
+ (NSString *)htmlBodyStrInWebView:(UIWebView *)webview;

+ (NSString *)findBPFFromHTMLData:(NSData *)htmlData;
+ (BOOL)doesContainBPFInWebView:(UIWebView *)webView;

@end

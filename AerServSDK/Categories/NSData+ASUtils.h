//
//  NSData+ASUtils.h
//  AerServSDK
//
//  Created by Vasyl Savka on 2/23/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (ASUtils)

- (NSString *)stringEncoded;

@end

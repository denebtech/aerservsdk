//
//  ASCloseButton.h
//  AerServSDK
//
//  Created by Vasyl Savka on 4/1/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASCloseButton : UIButton

@end

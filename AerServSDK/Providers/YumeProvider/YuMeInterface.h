//
//  YuMeInterface.h
//  YuMeiOSSampleApp
//
//  Created by Bharath Ramakrishnan on 4/5/13.
//  Copyright (c) 2014 YuMe Advertising Pvt. Ltd.,. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YuMeViewController.h"
#import "YuMeSDKInterface.h"
#import "YuMeTypes.h"

@interface YuMeInterface : NSObject<YuMeAppDelegate> {
    
    /* YuMe SDK Instance */
    YuMeSDKInterface *pYuMeSDK;
}

/* YuMeViewController Instance */
@property (nonatomic, assign) YuMeViewController *viewController;

/* Internal functions that interfaces with YuMe SDK APIs */

- (BOOL) yumeInit:(NSString *)configId adParams:(YuMeAdParams *)pAdParams;

- (BOOL) yumeDeInit;

- (BOOL) yumeInitAd;

- (BOOL) yumeShowAd:(UIView *)pAdView viewController:(UIViewController *)pAdViewController;

- (BOOL) yumeIsAdAvailable;

- (NSString *) yumeGetVersion;

- (YuMeSDKInterface *) getYuMeSDKHandle;

@end //@interface YuMeInterface

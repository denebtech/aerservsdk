//
//  ASCategoryHelper.m
//  AerServSDK
//
//  Created by Vasyl Savka on 6/8/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASCategoryHelper.h"

@implementation ASCategoryHelper

+ (ASCategoryHelper *)instance {
    static ASCategoryHelper *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ASCategoryHelper alloc] init];
    });
    
    return instance;
}

@end

//
//  ASUnityInstance.m
//  AerServSDK
//
//  Created by Albert Zhu on 7/25/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASUnityInstance.h"
#import "ASUnityDelegateProxy.h"
#import "ASUnityInterstitialAdProvider.h"

@interface ASUnityInstance()

@property (nonatomic, copy) NSString *gameID;

@property (nonatomic, strong) NSMutableDictionary *unityProviders;
@property (nonatomic, strong) NSMutableArray *placementsReady;

@end



@implementation ASUnityInstance

- (void)dealloc {
    [self cleanUp];
}

+ (ASUnityInstance *)instance {
    static ASUnityInstance *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [ASUnityInstance new];
        instance.unityAdsClass = NSClassFromString(kUnityAdsClassStr);
        instance.gameID = @"";
        instance.state = kASProviderSetupNone;
    });
    
    return instance;
}

- (void)cleanUp {
    [ASSDKLogger logStatement:@"ASUnityInstance, cleanUp"];
    //    [self.unityAdsClass setDelegate:nil];
    self.gameID = nil;
    
    [self.unityProviders removeAllObjects];
    self.unityProviders = nil;
    [self.placementsReady removeAllObjects];
    self.placementsReady = nil;
}

+ (void)startWithUnityGameID:(NSString *)unityGameID {
    if(![self instance].unityAdsClass || ![[self instance].gameID isEqualToString:unityGameID]) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInstance, startWithUnityGameID - unityGameID: %@", unityGameID]];
        [ASUnityInstance instance].state = kASProviderSetupStarted;
        [[self instance].unityAdsClass setDebugMode:(kUnityDebugMode ? YES : NO)];
        [[self instance].unityAdsClass initialize:unityGameID delegate:[ASUnityDelegateProxy sharedDelegate]];
        [self instance].gameID = unityGameID;
        [ASUnityInstance instance].state = kASProviderSetupReady;
    }
}

+ (void)unitySetupWithConfig:(NSDictionary *)adapterConfig {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInstance, unitySetupWithConfig: - adapterConfig: %@", adapterConfig]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            [ASUnityInstance startWithUnityGameID:adapterConfig[kUnityConfigGameIdKey]];
        } @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
}

#pragma mark - Unity provider functionality

+ (void)addUnityProvider:(ASUnityInterstitialAdProvider *)provider forPlacementId:(NSString *)placementId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInstance, addUnityProvider:forPlacementId: - placementId: %@", placementId]];
    if(![ASUnityInstance instance].unityProviders) {
        [ASUnityInstance instance].unityProviders = [NSMutableDictionary dictionary];
    }
    
    [[ASUnityInstance instance].unityProviders setObject:provider forKey:placementId];
}

+ (ASUnityInterstitialAdProvider *)getUnityProviderForPlacementId:(NSString *)placementId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInstance, getUnityProviderForPlacementId: - placementId: %@", placementId]];
    return [[ASUnityInstance instance].unityProviders objectForKey:placementId];
}

+ (void)removeProviderForPlacementId:(NSString *)placementId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInstance, removeProviderForPlacementId: - placementId: %@", placementId]];
    [[ASUnityInstance instance].unityProviders removeObjectForKey:placementId];
}

+ (NSArray *)getProviderPlacementList {
    [ASSDKLogger logStatement:@"ASUnityInstance, getProviderPlacementList"];
    return [[ASUnityInstance instance].unityProviders allKeys];
}

#pragma mark - Unity placements functionality

+ (void)addToReadyListForPlacmentId:(NSString *)placementId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInstance, addToReadyListForPlacmentId: - placementId: %@", placementId]];
    if(![ASUnityInstance instance].placementsReady)
        [ASUnityInstance instance].placementsReady = [NSMutableArray array];
    if(![ASUnityInstance checkForReadyWithPlacementId:placementId])
        [[ASUnityInstance instance].placementsReady addObject:placementId];
}

+ (BOOL)checkForReadyWithPlacementId:(NSString *)placementId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInstance, checkForReadyWithPlacementId: - placementId: %@", placementId]];
    return [[ASUnityInstance instance].placementsReady containsObject:placementId];
}

+ (void)removeFromReadyListForPlacementId:(NSString *)placementId {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASUnityInstance, removeFromReadyListForPlacementId: - placementId: %@", placementId]];
    [[ASUnityInstance instance].placementsReady removeObject:placementId];
}

+ (void)removeAllFromReadyList {
    [[ASUnityInstance instance].placementsReady removeAllObjects];
}

@end

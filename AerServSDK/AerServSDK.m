//
//  AerServSDK.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/23/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "AerServSDK.h"
#import "ASAnalyticsConfigChecker.h"

#import "ASPubSettingUtils.h"

@implementation AerServSDK

+ (void)initializeWithPlacments:(NSArray *)plcArr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"AerServSDK, initializeWithPlacements: %@", plcArr]];
    @try {
        [ASAnalyticsConfigChecker checkAnalyticsConfigWithPlacements:plcArr orSiteId:nil];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

+ (void)initializeWithAppID:(NSString *)appIdStr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"AerServSDK, initializeWithAppID: %@", appIdStr]];
    @try {
        [ASAnalyticsConfigChecker checkAnalyticsConfigWithPlacements:nil orSiteId:appIdStr];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

//
//  ASGlobalProviderSetup.h
//  AerServSDK
//
//  Created by Albert Zhu on 11/23/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ASProviderSetupState) {
    kASProviderSetupNone,
    kASProviderSetupStarted,
    kASProviderSetupReady
};

#define kAdapterNameKey @"adapterName"
#define kAdapterNameAdColonyKey @"AdColony"
#define kAdapterNameVungleKey @"Vungle"
#define kAdapterNameChartboostKey @"Chartboost"
#define kAdapterNameAppLovinKey @"AppLovin"

@interface ASGlobalProviderSetup : NSObject

@property (nonatomic, strong) NSDictionary *adapterInitState;
@property (nonatomic, strong) NSDictionary *adapterConfig;

+ (void)initWithAdapterConfig:(NSArray *)adapterConfigArr;

@end

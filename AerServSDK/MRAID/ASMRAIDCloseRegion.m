//
//  ASMRAIDCloseRegion.m
//  AerServSDK
//
//  Created by Albert Zhu on 10/6/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASMRAIDCloseRegion.h"
#import "ASMRAIDCloseButton.h"

@interface ASMRAIDCloseRegion ()

@property (nonatomic, assign) id target;
@property (nonatomic, assign) SEL action;
@property (nonatomic, strong) UITapGestureRecognizer *closeRegionTapGestRecog;
@property (nonatomic, strong) ASMRAIDCloseButton *closeBtn;

@end

@implementation ASMRAIDCloseRegion

- (void)dealloc {
    
    [_closeBtn removeFromSuperview];
    _closeBtn = nil;
    
    [self removeGestureRecognizer:_closeRegionTapGestRecog];
    _closeRegionTapGestRecog = nil;
    
}

- (instancetype)initWithTarget:(id)target andSelector:(SEL)action inView:(UIView *)view {
    
    if(self = [super initWithFrame:CGRectMake(view.frame.size.width-kASMRAIDCloseRegionWH, 0.0f,
                                              kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH)]) {
        self.backgroundColor = [UIColor clearColor];
        self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin;
        
        [view addSubview:self];
        [view bringSubviewToFront:self];
        
        _target = target;
        _action = action;
        
        _closeRegionTapGestRecog = [[UITapGestureRecognizer alloc] initWithTarget:_target action:_action];
        _closeRegionTapGestRecog.numberOfTapsRequired = 1;
        [self addGestureRecognizer:_closeRegionTapGestRecog];
        
        _closeBtn = [ASMRAIDCloseButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn addTarget:_target action:_action forControlEvents:UIControlEventTouchUpInside];
        _closeBtn.frame = CGRectMake((kASMRAIDCloseRegionWH - kASMRAIDCloseRegionWH)/2, (kASMRAIDCloseRegionWH - kASMRAIDCloseRegionWH)/2, kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH);
        _closeBtn.autoresizingMask = UIViewAutoresizingNone;
        _closeBtn.hidden = YES;
        [self addSubview:_closeBtn];
    }
    
    return self;
    
}

- (void)hideCloseBtn:(BOOL)yesOrNo {
    self.closeBtn.hidden = yesOrNo;
}

- (BOOL)isCloseBtnHidden {
    return self.closeBtn.hidden;
}

@end

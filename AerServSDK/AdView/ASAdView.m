//
//  ASAdView.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/9/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASAdView.h"
#import "ASBannerAdManager.h"
#import "AVPlayer+StateChange.h"
#import "ASLocationUtils.h"
//#import "ASMRAIDView.h"
#import "ASCloseButton.h"
#import "ASTransactionInfo.h"
#import "ASVASTVideoPlayerView.h"

#define kASAdViewShowLayoutDebug 0

const CGSize ASBannerSize = {320, 50};
const CGSize ASLargeBannerSize = {320, 100};
const CGSize ASMediumRectSize = {300, 250};
const CGSize ASLeaderBoardSize = {728, 90};
const CGSize ASWideSkypscraperSize = {160, 600};

const NSTimeInterval kASDefaultAdViewTimeout = 15.0f;

@interface ASAdView() <ASBannerAdManagerDelegate>

@property (nonatomic, copy) NSString *placementID;

@property (nonatomic, assign) CGSize originalSize;
@property (nonatomic, assign) CGSize adSize;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) ASBannerAdManager *adManager;
@property (nonatomic, assign) BOOL bannerRefreshActive;
@property (nonatomic, assign) BOOL bannerRefreshActiveBeforeHidden;
@property (nonatomic, strong) NSTimer *timeoutTimer;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, weak) AVPlayer *avPlayer;
@property (nonatomic, strong) ASCloseButton *closeBtn;

@end



@implementation ASAdView

- (void)dealloc {
    // make sure we cancel what ever we are doing in the ad..
    [ASSDKLogger logStatement:@"ASAdView, dealloc"];
    [_closeBtn removeFromSuperview];
    _closeBtn = nil;
    _adManager = nil;
    _avPlayer = nil;
    _delegate = nil;
    _placementID = nil;
    _keyWords = nil;
    _pubKeys = nil;
    _location = nil;
    _userId = nil;
}

- (instancetype)initWithPlacementID:(NSString *)placementID size:(CGSize)size {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, initWithPlacementID:size: - plc: %@, size: %@", placementID, NSStringFromCGSize(size)]];
    return [self initWithPlacementID:placementID andAdSize:size];
}

- (instancetype)initWithPlacementID:(NSString *)placementID andAdSize:(CGSize)adSize {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, initWithPlacementID:andAdSize: - plc: %@, adSize: %@", placementID, NSStringFromCGSize(adSize)]];
    if(self = [super initWithFrame:CGRectMake(0, 0, adSize.width, adSize.height)]) {
        _placementID = placementID;
        _originalSize = adSize;
        _isPreload = NO;
        _env = kASEnvProduction;
        _adManager = [[ASBannerAdManager alloc] init];
        _adManager.delegate = self;
        
        _timeoutInterval = kASDefaultAdViewTimeout;
        _bannerRefreshTimeInterval = -1;
        _locationServicesEnabled = NO;
        _bannerRefreshActive = YES;
        _platform = kASPlatformCS;
        
        self.clipsToBounds = YES;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    }
    
    return self;
}


+ (instancetype)viewWithPlacementID:(NSString *)placementID andAdSize:(CGSize)adSize; {
    return [[ASAdView alloc] initWithPlacementID:placementID andAdSize:adSize];
}

- (void)layoutSubviews {
    // if we are to automatically size the ad to fit.. Do some work.
    if (self.sizeAdToFit && self.contentView != nil) {
        // web views are scaled a bit differently. we will scale them using their
        // transform. This seems to supply a very universal scale. It also means
        // they can't scale them selves.
        [ASSDKLogger logStatement:@"ASAdView, layoutSubviews - Size ad to fit"];
        #if kASAdViewShowLayoutDebug
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, layoutSubviews - ENTER: self.adSize: %@, self.frame: %@, self.contentView.frame: %@", NSStringFromCGSize(self.adSize), NSStringFromCGRect(self.frame), NSStringFromCGRect(self.contentView.frame)]];
        #endif
        @try {
//            if ([self.contentView isKindOfClass:[UIWebView class]]) {
//                [ASSDKLogger logStatement:@"ASAdView, layoutSubviews - UIWebView as content"];
//                float heightScale = CGRectGetHeight(self.frame)/self.adSize.height;
//                float widthScale = CGRectGetWidth(self.frame)/self.adSize.width;
//                
//                float scale = MIN(heightScale, widthScale);
//                
//                // we don't scale the ad bigger.
//                if (scale > 1.0)
//                    scale = 1.0;
//              
//                #if kASAdViewShowLayoutDebug
//                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, layoutSubviews - before transform, heightScale: %ld | widthScale: %ld", (long)heightScale, (long)widthScale]];
//                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, layoutSubviews - before transform, contentView.frame: %@", NSStringFromCGRect(self.contentView.frame)]];
//                #endif
//                self.contentView.transform = CGAffineTransformMakeScale(scale, scale);
//                #if kASAdViewShowLayoutDebug
//                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, layoutSubviews - after transform, contentView.frame: %@", NSStringFromCGRect(self.contentView.frame)]];
//                #endif
//                
//                
//                // reposition to center of asadview frame
//                CGFloat xPos = (self.frame.size.width - self.contentView.frame.size.width)/2;
//                CGFloat yPos = (self.frame.size.height - self.contentView.frame.size.height)/2;
//                self.contentView.frame = CGRectMake(xPos, yPos, self.contentView.frame.size.width, self.contentView.frame.size.height);
//            } else if([self.contentView isKindOfClass:[ASMRAIDView class]]) {
//                #if kASAdViewShowLayoutDebug
//                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, layoutSubviews - before orientation change, contentView.frame: %@", NSStringFromCGRect(self.contentView.frame)]];
//                #endif
//                ASMRAIDView *tempMraid = (ASMRAIDView *)self.contentView;
//                [tempMraid mraidViewWillAttemptOrientationChange];
//                #if kASAdViewShowLayoutDebug
//                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, layoutSubviews - after orientation change, contentView.frame: %@", NSStringFromCGRect(self.contentView.frame)]];
//                #endif
//            } else {
                CGFloat xPos = 0.0f, yPos = 0.0f, width = 0.0f, height = 0.0f;
                
                // if the ad is bigger than the view then resize the ad to fit. Otherwise resize content to fit the ad.
                if (self.adSize.height > self.frame.size.height ||
                    self.adSize.width > self.frame.size.width ||
                    self.adSize.height > self.contentView.frame.size.height ||
                    self.adSize.width > self.contentView.frame.size.width) {
                    #if kASAdViewShowLayoutDebug
                    [ASSDKLogger logStatement:@"ASAdView, layoutSubviews - If the ad size height or width are greater than the view's frame, make the view's frame the ad size"];
                    #endif
                    width = self.frame.size.width;
                    height = self.frame.size.height;
                } else {
                    // when content view is greater in size then the actual banner, resize the content view to match the banner
                    if(self.contentView.frame.size.width > self.frame.size.width)
                        self.contentView.frame = CGRectMake(0.0f, self.contentView.frame.origin.y, self.frame.size.width, self.contentView.frame.size.height);
                    
                    if(self.contentView.frame.size.height > self.frame.size.height)
                        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, 0.0f, self.contentView.frame.size.width, self.frame.size.height);
                    
                    // center the ad in the content view space
                    #if kASAdViewShowLayoutDebug
                    [ASSDKLogger logStatement:@"ASAdView, layoutSubviews - Ad size height or width are within the view's bounds, change the content view's size to the ad size"];
                    #endif
                    xPos = (self.contentView.frame.size.width - self.adSize.width)/2;
                    yPos = (self.contentView.frame.size.height - self.adSize.height)/2;
                    width = self.adSize.width;
                    height = self.adSize.height;
                }
                
                self.contentView.frame = CGRectMake(xPos, yPos, width, height);
                
                #if kASAdViewShowLayoutDebug
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, layoutSubviews - adSize: %@, contentView.frame: %@", NSStringFromCGSize(self.adSize), NSStringFromCGRect(self.contentView.frame)]];
                #endif
//            }
            
            #if kASAdViewShowLayoutDebug
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, layoutSubviews - self.adSize: %@, self.frame: %@, self.contentView.frame: %@", NSStringFromCGSize(self.adSize), NSStringFromCGRect(self.frame), NSStringFromCGRect(self.contentView.frame)]];
            #endif
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }
}

#pragma mark - Timer Methods

- (void)killTimeoutTimer {
    if (self.timeoutTimer != nil) {
        [ASSDKLogger logStatement:@"ASAdView, killTimeoutTimer"];
        [self.timeoutTimer invalidate];
        self.timeoutTimer = nil;
    }
}

- (void)onTimeoutTimer {
    [ASSDKLogger logStatement:@"ASAdView, onTimeoutTimer"];
    @try {
        NSError *error = [NSError errorWithDomain:@"Loading Timeout"
                                             code:100
                                         userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"The banner ad took longer than %.2f to load", self.timeoutInterval]}];
        [self.adManager reportErrorForProvider];
        [self.adManager.delegate bannerAdManager:self.adManager didFailToFetchAdwithError:error];
        [self cancel];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - ASAdView Public Methods

- (void)loadAd {
    @try {
        [ASSDKLogger startLogWithPLC:self.placementID andKeywords:self.keyWords andPubKeys:self.pubKeys];
        [ASSDKLogger logStatement:@"ASAdview, loadAd"];
        
        // cancel/refresh ad manager
        [self.adManager cancel];
        
        // reset the timeout timer
        [self killTimeoutTimer];
        self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:self.timeoutInterval target:self selector:@selector(onTimeoutTimer) userInfo:nil repeats:NO];
        
        // check banner refresh interval for never case
        if(self.bannerRefreshTimeInterval == kBannerRefreshNeverValue) {
            self.bannerRefreshTimeInterval = kBannerRefreshNeverTokenValue;
        }
        
        // start ad response logger
        [ASAdResponseLogger startAdResponseLogWithPLC:self.placementID];
        
        // I need to fetch a NEW ad when preloading
        if (!self.adManager || self.isPreload) {
            self.adManager = [[ASBannerAdManager alloc] init];
        }
        self.adManager.delegate = self;
        self.adManager.isPreload = self.isPreload;
        self.adManager.timeoutInt = self.timeoutInterval;
        self.adManager.bannerRefershTimeInterval = self.bannerRefreshTimeInterval;
        
        // check flag to see if the publisher has enabled location services or not
        [ASLocationUtils getInstance].publisherRequestsLocationServicesEnabled = self.locationServicesEnabled;
        
        // fetch ad
        [self.adManager fetchBannerAdForPlacementID:self.placementID
                                              inEnv:self.env
                                      usingKeyWords:self.keyWords
                                         andPubKeys:self.pubKeys
                                         withUserID:self.userId
                                         onPlatform:self.platform
         ];
        
        self.isLoading = YES;
        if(self.isPreload) {
            self.isPreload = NO;
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)forceRefreshAd {
    [ASSDKLogger logStatement:@"ASAdView, forceRefreshAd"];
    @try {
        [self.adManager forceRefresh];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (CGSize)adContentSize {
    return self.contentView ? self.adSize : self.originalSize;
}

- (void)stopAutomaticallyRefreshingContents {
    [ASSDKLogger logStatement:@"ASAdView, stopAutomaticallyRefreshingAds"];
    @try {
        [self.adManager stopAutomaticallyRefreshingAds];
        self.bannerRefreshActive = NO;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)startAutomaticallyRefreshingContents {
    [ASSDKLogger logStatement:@"ASAdView, startAutomaticallyRefreshingAds"];
    @try {
        [self.adManager startAutomaticallyRefreshingAds];
        self.bannerRefreshActive = YES;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

//- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation {
//    [ASSDKLogger logStatement:@"ASAdView, rotateToOrientation:"];
//    @try {
//        [self.adManager rotateToOrientation:newOrientation];
//        [self.adManager rotateToOrientation:newOrientation withSize:self.frame.size];
//    }
//    @catch (NSException *exception) {
//        [ASSDKLogger onException:exception];
//    }
//}

- (void)play {
    [self.avPlayer changeStateToPlay];
}

- (void)pause {
    [self.avPlayer changeStateToPause];
}

- (void)showPreloadedBanner {
    [ASSDKLogger logStatement:@"ASAdView, showPreloadedBanner"];
    @try {
        [self bringSubviewToFront:self.contentView];
        [self.adManager startPreloadedBannerAd]; // need to start the video -- AZ
        [self layoutSubviews];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)cancel {  // do we want to add a cancel feature to ASAdView -- AZ
    [ASSDKLogger logStatement:@"ASAdView, cancel"];
    @try {
        self.isLoading = NO;
        [self killTimeoutTimer];
        if(self.closeBtn != nil) {
            [self.closeBtn removeFromSuperview];
            self.closeBtn = nil;
        }
        [self.adManager cancel]; // providerBeingAttempted is canceled inside ASBannerAdManager
        if(self.contentView != nil) {
            [self.contentView removeFromSuperview];
            self.contentView = nil;
            #if kASAdViewShowLayoutDebug
            NSLog(@"ASAdView, cancel -- dereference contentView HERE");
            #endif
        }
        [self removeFromSuperview];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - Overload UIView Method

- (void)setHidden:(BOOL)hidden {
    [super setHidden:hidden];
    
    if(hidden) {
        self.bannerRefreshActiveBeforeHidden = self.bannerRefreshActive;
        [self stopAutomaticallyRefreshingContents];
    } else {
        if(self.bannerRefreshActiveBeforeHidden) {
            [self startAutomaticallyRefreshingContents];
        }
    }
}

#pragma mark - Close Button Methods

- (void)createCloseButton {
    [ASSDKLogger logStatement:@"ASAdView, createCloseButton"];
    
    // create our close button.
    ASCloseButton* closeButton = [ASCloseButton buttonWithType:UIButtonTypeCustom];
    closeButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    // hook up hte close handler.
    [closeButton addTarget:self action:@selector(onCloseAd) forControlEvents:UIControlEventTouchUpInside];
    
    // add to the view.
    [self.contentView addSubview:closeButton];
    
    // glue the close button to the upper right.
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:closeButton
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1.0f
                                                                  constant:-5]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:closeButton
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeTop
                                                                multiplier:1.0f
                                                                  constant:5]];
    
    self.closeBtn = closeButton;
}

- (void)onCloseAd {
    [ASSDKLogger logStatement:@"ASAdView, onCloseAd"];
    @try {
        [self cancel];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - ASBannerAdManagerDelegate Protocol Methods

- (UIViewController *)viewControllerForPresentingModalView {
    return [self.delegate viewControllerForPresentingModalView];
}

- (NSString *)placementID {
    return _placementID;
}

- (ASEnvironmentType)env {
    return _env;
}

- (CGSize)requestedSize {
    return self.originalSize;
}

- (void)bannerAdManager:(ASBannerAdManager *)adManager didFailToFetchAdwithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, bannerAdManager:didFailToFetchAdWithError: - error: %@", error.description]];
    if (!self.isLoading) {
        return;
    }
    self.isLoading = NO;
    
    if ([self.delegate respondsToSelector:@selector(adViewDidFailToLoadAd:withError:)])
        [self.delegate adViewDidFailToLoadAd:self withError:error];
    [self killTimeoutTimer];
}

- (void)bannerAdManager:(ASBannerAdManager *)adManager didPreloadAd:(UIView *)ad {
    if(self.adManager != nil) {
        if(self.adManager.isPreload) {
            [ASSDKLogger logStatement:@"ASAdView, bannerAdManager:didPreloadAd:"];
            if(!self.isLoading) return;
            self.isLoading = NO;
            [self killTimeoutTimer];
            [self.contentView removeFromSuperview];            
            self.contentView.hidden = YES;
        } else {
            [self bannerAdManager:self.adManager didLoadAd:ad];
            [self.adManager startPreloadedBannerAd];
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(adViewDidPreloadAd:)]) {
        [self.delegate adViewDidPreloadAd:self];
    }
}
- (void)bannerAdManager:(ASBannerAdManager *)adManager didFinishLoadingPreloadAd:(UIView *)ad {
    
    // Some Web views (vdopia) come with a 1x1 size and get reset via JS.
    // For these use our container's size instead of the document's size.
    if (ad.frame.size.width<=1 && ad.frame.size.height<=1) {
        ad.frame = self.frame;
    }
    self.contentView = ad;
    ad.layer.borderWidth = self.outlineAd ? 2 : 0;
    ad.layer.borderColor = [UIColor redColor].CGColor;
    self.adSize = self.contentView.frame.size;
    [self addSubview:self.contentView];
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, bannerAdManager:didFinishLoadingPreloadAd: - adSize: %@", NSStringFromCGSize(self.adSize)]];
}

- (void)bannerAdManager:(ASBannerAdManager *)adManager didLoadAd:(UIView*)ad {
    if (!self.isLoading) return;
    self.isLoading = NO;
    [self killTimeoutTimer];
    
    [self.contentView removeFromSuperview];
    self.contentView = nil;
    
    // Some Web views (vdopia) come with a 1x1 size and get reset via JS.
    // For these use our container's size instead of the document's size.
    if (ad.frame.size.width<=1 && ad.frame.size.height<=1) {
        ad.frame = self.frame;
    }
    self.contentView = ad;
    ad.layer.borderWidth = self.outlineAd ? 2 : 0;
    ad.layer.borderColor = [UIColor redColor].CGColor;
    self.adSize = self.contentView.frame.size;
    [self addSubview:self.contentView];
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, bannerAdManager:didLoadAd: - adSize: %@", NSStringFromCGSize(self.adSize)]];
    if ([self.delegate respondsToSelector:@selector(adViewDidLoadAd:)])
        [self.delegate adViewDidLoadAd:self];
}

- (void)bannerAdManagerWillBeginAction:(ASBannerAdManager*)adManager {
    if(self.avPlayer.rate != 0) { // pause player when action begins
        [self.avPlayer changeStateToPause];
    }
    if ([self.delegate respondsToSelector:@selector(willPresentModalViewForAd:)])
        [self.delegate willPresentModalViewForAd:self];
}

- (void)bannerAdManagerWillEndAction:(ASBannerAdManager*)provider {
    if(self.avPlayer.rate == 0) { // resume player when action ends
        [self.avPlayer changeStateToPlay];
    }
    if ([self.delegate respondsToSelector:@selector(didDismissModalViewForAd:)])
        [self.delegate didDismissModalViewForAd:self];
}

- (void)bannerAdManagerWillLeaveApplication:(ASBannerAdManager*)provider {
    if ([self.delegate respondsToSelector:@selector(willLeaveApplicatonFromAd:)])
        [self.delegate willLeaveApplicatonFromAd:self];
}

- (void)bannerAdManagerAdChangedSize:(ASBannerAdManager *)adManager {
    self.adSize = self.contentView.frame.size;
    [self setNeedsLayout];
    
    if ([self.delegate respondsToSelector:@selector(adSizedChanged:)])
        [self.delegate adSizedChanged:self];
}

- (void)bannerAdManagerAdWasClicked:(ASBannerAdManager *)adManager {
    if ([self.delegate respondsToSelector:@selector(adWasClicked:)]) {
        [self.delegate adWasClicked:self];
    }
}

- (void)bannerAdManagerDidShowAdCompletely:(ASBannerAdManager *)adManager {
    [self cancel];
    
    if([self.delegate respondsToSelector:@selector(adViewDidCompletePlayingWithVastAd:)]) {
        [self.delegate adViewDidCompletePlayingWithVastAd:self];
    }
}

- (void)AVPlayerCreated:(AVPlayer *)avPlayer {
    self.avPlayer = avPlayer;
}

- (void)bannerAdManager:(ASBannerAdManager *)adManager willShowSkip:(BOOL)show {
    // check if there's a content view to create the close button in first
    if(self.contentView != nil && !self.closeBtn) {
        [self createCloseButton];
    }
    
    [self.closeBtn setHidden:!show];
}

- (void)bannerAdManager:(ASBannerAdManager *)adManager willChangeFrameTo:(CGRect)newFrame {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, bannerAdManager:willChangeFrameTo: - frame: %@", NSStringFromCGRect(newFrame)]];
    
    self.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f];
    self.frame = newFrame;
}

- (ASAdView *)bannerAdManagerWillGetAdView:(ASBannerAdManager *)adManager {
    return self;
}

- (UIView *)bannerAdManagerWillGetContentView:(ASBannerAdManager *)adManager {
    return self.contentView;
}

//- (void)bannerAdManager:(ASBannerAdManager *)adManager willSetContentViewToMRAIDView:(ASMRAIDView *)mraidView {
//    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdView, bannerAdManager:willSetContentViewToMRAIDView: - mraidView.frame: %@", NSStringFromCGRect(mraidView.frame)]];
//    self.contentView = mraidView;
//}

- (void)bannerAdManager:(ASBannerAdManager *)adManager didLoadPlayerLayer:(AVPlayerLayer *)pl {
    CALayer *superLayer = [self superview].layer;
    [superLayer insertSublayer:pl atIndex:(unsigned int)[superLayer.sublayers count]];
    
}

- (void)bannerAdManagerWillAttemptRefresh:(ASBannerAdManager *)adManager {
    [self loadAd];
}

- (void)bannerAdManager:(ASBannerAdManager *)adManager didFireAdvertiserEventWithMessage:(NSString *)msg {
    if([self.delegate respondsToSelector:@selector(adView:didFireAdvertiserEventWithMessage:)]) {
        [self.delegate adView:self didFireAdvertiserEventWithMessage:msg];
    }
}

- (void)bannerAdManager:(ASBannerAdManager *)manager didFindAdTransactionInfo:(ASTransactionInfo *)transactionInfo {
    if([self.delegate respondsToSelector:@selector(adView:didShowAdWithTransactionInfo:)]) {
        [self.delegate adView:self didShowAdWithTransactionInfo:[transactionInfo asTransactionInfoObj]];
    }
}

@end

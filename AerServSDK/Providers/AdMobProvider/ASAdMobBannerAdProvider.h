//
//  ASAdMobBannerAdProvider.h
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/26/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ASBannerAdProvider.h"


@interface ASAdMobBannerAdProvider : ASBannerAdProvider

@end

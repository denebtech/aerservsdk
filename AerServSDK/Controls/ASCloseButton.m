//
//  ASCloseButton.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/1/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASCloseButton.h"

@implementation ASCloseButton

- (instancetype)init {
    self = [super init];
    
    self.contentMode = UIViewContentModeRedraw;
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    // draw shaded background
    UIBezierPath *ovalPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    UIColor *bgColor = self.isHighlighted ? [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.438f] : [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.438f];
    [bgColor setFill];
    [ovalPath fill];
    
    // create X
    NSString *closeX = @"X";
    UIFont *closeXFont = [UIFont fontWithName:@"AvenirNext-Bold" size:16];
    UIColor *fontColor = self.isHighlighted ? [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f] : [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    [fontColor setFill];
    NSMutableParagraphStyle* paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByClipping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSDictionary *closeXAttr = @{NSFontAttributeName : closeXFont,
                                 NSParagraphStyleAttributeName : paragraphStyle,
                                 NSForegroundColorAttributeName : fontColor};
    [closeX drawAtPoint:CGPointMake(10.0f, 6.0f) withAttributes:closeXAttr];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    [self setNeedsDisplay];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    [self setNeedsDisplay];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    
    [self setNeedsDisplay];
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(32.0f, 32.0f);
}

@end

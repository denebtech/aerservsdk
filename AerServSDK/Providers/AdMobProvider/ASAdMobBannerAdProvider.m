//
//  ASAdMobBannerAdProvider.m
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/26/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "GoogleMobileAds.h"

#import "ASAdMobBannerAdProvider.h"
#import "ASAdMobTestDevices.h"
#import "NSString+ASUtils.h"

@interface ASAdMobBannerAdProvider()<GADBannerViewDelegate>

@property (nonatomic, strong) id gadBannerView;

@end

@implementation ASAdMobBannerAdProvider

- (void)dealloc {
    self.gadBannerView = nil;
}

- (instancetype)init {
    if (self = [super init]) {
        self.gadBannerView = [[NSClassFromString(@"GADBannerView") alloc] initWithFrame:CGRectZero];
        self.automaticallyRefreshAds = YES;

        if( self.gadBannerView != nil && [self.gadBannerView respondsToSelector:@selector(setDelegate:)]) {
            [self.gadBannerView setDelegate:(id)self];
        }
    }

    return self;
}

- (void)requestAdWithSize:(CGSize)size withProperties:(NSDictionary *)properties isPreload:(BOOL)preload {
    if(self.gadBannerView == nil && ![self.gadBannerView respondsToSelector:@selector(setDelegate:)]) {
        [ASSDKLogger logStatement:@"ASAdMobBannerAdProvider, requestAdWithSize:withProperties:isPreload: - Could not create AdMob Banner class object, failing over"];
        NSError* error = [NSError errorWithDomain:NSStringFromClass([ASAdMobBannerAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Could not link against AdMob.  Failing over"}];
        
        [self.delegate bannerProvider:self didFailToLoadAdWithError:error];
        return;
        
    }
    
    self.parameters = properties[ASBannerParameter_Parameters];
    self.isPreload = preload;

    CGSize adUnitsize = [self sizeFromHeaderString:self.parameters[@"AdMobAdUnitSize"]];
    if (adUnitsize.width < 325 || adUnitsize.height < 50) {
        adUnitsize = [self sizeFromHeaderString:@"325x50"];
    }
    
    CGRect adFrame = {{0,0}, adUnitsize};
    [self.gadBannerView setFrame:adFrame];
    [self.gadBannerView setAdUnitID:self.parameters[@"AdMobAdUnitID"]];
    [self.gadBannerView setRootViewController: [self.delegate viewControllerForPresentingModalView]];
    
    CLLocation *location = properties[ASBannerParameter_Location];
    
    [ASSDKLogger logStatement:@"ASAdMobBannerAdProvider, requestAdWithSize:withProperties:isPreload: - Banner view load request"];
    [self.gadBannerView loadRequest:(NSURLRequest*)[self buildRequest:location]];
}

- (CGSize)sizeFromHeaderString:(NSString*)sizeString {
    // we expect <width>x<height> so let just seperate at the x.
    CGSize size;
    if ([sizeString containsStr:@"x"]) {
        NSArray *sizeComponents = [[sizeString lowercaseString] componentsSeparatedByString:@"x"];
        size = CGSizeMake([sizeComponents[0] integerValue], [sizeComponents[1] integerValue]);
    } else {
        size = CGSizeZero;
    }
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdMobBannerAdProvider, sizeFromHeaderString: - size: %@", NSStringFromCGSize(size)]];
    return size;
}

- (id)buildRequest:(CLLocation *)location {
    id request = [NSClassFromString(@"GADRequest") request];
    
    if (location != nil) {
        [ASSDKLogger logStatement:@"ASAdMobBannerAdProvider, buildRequest - request built with location"];
        [request setLocationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude accuracy:location.horizontalAccuracy];
    } else {
        [ASSDKLogger logStatement:@"ASAdMobBannerAdProvider, buildRequest - request without location"];
    }
    
    [request setTestDevices:kASAdMobTestDevices];
    
    return request;
}

- (void)startPreloadedBannerAd {
    [self.delegate bannerProvider:self didFinishLoadingPreloadAd:self.gadBannerView];
}

#pragma mark - GADBannerViewDelegate

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    [ASSDKLogger logStatement:@"ASAdMobBannerAdProvider, adViewDidReceiveAd: - Google AdMob Banner Ad did load"];
    @try {
        if(self.isPreload) {
            [self.delegate bannerProvider:self didPreloadAd:bannerView];
        } else {
            [self.delegate bannerProvider:self didLoadAd:bannerView];
        }
        [self scheduleRefresh];
        [ASAdResponseLogger endAdResponseAndSendEvent];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(id)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdMobBannerAdProvider, adView:didFailToReceiveAdWithError: - Google AdMob Banner failed to load with error: %@", [error localizedDescription]]];
    @try {
        [self.delegate bannerProvider:self didFailToLoadAdWithError:error];
        [self killRefreshTimer];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adViewWillPresentScreen:(GADBannerView *)bannerView {
    [ASSDKLogger logStatement:@"ASAdMobBannerAdProvider, adViewWillPresentScreen: - Google AdMob Banner will present modal VC"];
    @try {
        [self.delegate bannerProviderWillBeginAction:self];
        [self scheduleRefresh];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adViewDidDismissScreen:(GADBannerView *)bannerView {
    [ASSDKLogger logStatement:@"ASAdMobBannerAdProvider, adViewDidDismissScreen: - Google AdMob Banner did dismiss modal"];
    @try {
        [self.delegate bannerProviderWillEndAction:self];
        [self killRefreshTimer];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView {
    [ASSDKLogger logStatement:@"ASAdMobBannerAdProvider, adViewWillLeaveApplication: - Google AdMob Banner will leave the application"];
    @try {
        [self.delegate bannerProviderAdWasClicked:self];
        [self.delegate bannerProviderWillLeaveApplication:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

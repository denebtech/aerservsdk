//
//  ASStaticLoggerFields.m
//  AerServSDK
//
//  Created by Vasyl Savka on 10/30/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASStaticLoggerFields.h"
#import <AdSupport/AdSupport.h>

@interface ASStaticLoggerFields ()

@property (nonatomic, copy) NSString *os;
@property (nonatomic, copy) NSString *osv;
@property (nonatomic, copy) NSString *adid;
@property (nonatomic, copy) NSString *oid;
@property (nonatomic, copy) NSString *bundleid;
@property (nonatomic, copy) NSString *sdkv;
@property (nonatomic, copy) NSString *ua;

@end

@implementation ASStaticLoggerFields

+ (ASStaticLoggerFields *)fields {
    static ASStaticLoggerFields *fields;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        fields = [ASStaticLoggerFields new];
        fields.os = [UIDevice currentDevice].systemName;
        fields.osv = [UIDevice currentDevice].systemVersion;
        fields.adid = [[ASIdentifierManager sharedManager].advertisingIdentifier UUIDString];
        fields.oid = [[ASIdentifierManager sharedManager].advertisingIdentifier UUIDString];
        fields.bundleid = [NSBundle mainBundle].bundleIdentifier;
        
        fields.sdkv = nil;
        fields.ua = nil;
    });
    
    return fields;
}

+ (void)setSDKVersion:(NSString *)ver {
    [self fields].sdkv = ver;
}

+ (void)setUserAgent:(NSString *)agent {
    [self fields].ua = agent;
}

+ (BOOL)areStaticFieldsDefined {
    return ([ASStaticLoggerFields fields].sdkv != nil && [ASStaticLoggerFields fields].ua != nil);
}

@end

//
//  ASAdResponseLogger.m
//  AerServSDK
//
//  Created by Vasyl Savka on 10/28/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASAdResponseLogger.h"
#import "ASAnalyticsConfigChecker.h"

@interface ASAdResponseLogger()

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;

@end


@implementation ASAdResponseLogger

+ (ASAdResponseLogger *)logInstance {
    static ASAdResponseLogger *logInstance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        logInstance = [ASAdResponseLogger new];
    });
    
    return logInstance;
}

+ (void)resetFields {
    [super resetFields];
    [self logInstance].startTime = nil;
    [self logInstance].endTime = nil;
}

+ (void)startAdResponseLogWithPLC:(NSString *)plc {
    [ASSDKLogger logStatement:@"ASAdResponseLogger, startAdResponseLogWithPLC:"];
    
    [self resetFields];
    
    [self logInstance].plc = plc;
    [self logInstance].category = kCategoryAdRequestStr;
    [self logInstance].action = kActionRequestAndRenderStr;
    [self logInstance].startTime = [NSDate date];
}

+ (NSNumber *)durationInMillisecondsAsANumberObj {
    return [NSNumber numberWithDouble:([[self logInstance].endTime timeIntervalSinceDate:[self logInstance].startTime] * 1000)];
}

+ (NSData *)createJSONAdResponseEventData {
    
    if(![ASStaticLoggerFields areStaticFieldsDefined]) return nil;
    
    #if kAdResponseDebug
    NSLog(@"-- category: %@", [self logInstance].category);
    NSLog(@"-- action: %@", [self logInstance].action);
    NSLog(@"-- timestamp: %@", [self epochTimeAsNSNumberObj]);
    NSLog(@"-- plc: %@", [self logInstance].plc);
    NSLog(@"-- sdkv: %@", [ASStaticLoggerFields fields].sdkv);
    NSLog(@"-- osv: %@", [ASStaticLoggerFields fields].osv);
    NSLog(@"-- duration: %@", [self durationInMillisecondsAsANumberObj]);
    #endif
    
    NSDictionary *eventObj = @{
                               kCategoryKey : [self logInstance].category,
                               kActionKey : [self logInstance].action,
                               kTimestampKey : [self epochTimeAsNSNumberObj],
                               kPLCKey : [self logInstance].plc,
                               kSDKVersionKey : [ASStaticLoggerFields fields].sdkv,
                               kOSVersionKey : [ASStaticLoggerFields fields].osv,
                               kDurationMillisKey : [self durationInMillisecondsAsANumberObj]
                               };
    
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdResponseLogger, createJSONAdResponseEventData - eventObj: %@", eventObj]];
    
    return [self createSendDataWithEventObj:eventObj];
}

+ (void)endAdResponseAndSendEvent {
    [ASSDKLogger logStatement:@"ASAdResponseLogger, endAdResponseAndSendEvent"];
    
    if(![self logInstance].startTime || [ASAnalyticsConfigChecker checkDisblaedEventsforCategory:kCategoryAdRequestStr andAction:kActionRequestAndRenderStr]) {
        [ASSDKLogger logStatement:@"ASAdResponseLogger, endAdResponseAndSendEvent -- Either start time has been cleared or ad request/request and render has been disabled, nothing to report. RETURN"];
        return;
    }
    
    [self logInstance].endTime = [NSDate date];
    
    // create jsondata
    NSData *jsonEventData = [ASAdResponseLogger createJSONAdResponseEventData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{

        // attempt to send analytics
        [ASAdResponseLogger attemptToSendAnalyticsEventsWithData:jsonEventData];
        
        // clear fields so the ad response doens't get reported twice
        [ASAdResponseLogger resetFields];
    });
}

@end

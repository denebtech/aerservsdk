//
//  ASTremorInterstitialAdProvider.m
//  TremorProvider
//
//  Created by Gauthier on 7/11/14.
//  Copyright (c) 2014 AerServ. All rights reserved.
//

#import "TremorVideoAd.h"

#import "ASTremorInterstitialAdProvider.h"
#import "ASTremorViewController.h"

@interface ASTremorInterstitialAdProvider() <ASInterstitialAdViewControllerDelegate>

@property (nonatomic, strong) ASTremorViewController *viewController;
@property (nonatomic, assign) int attempts;
@end

@implementation ASTremorInterstitialAdProvider

- (void)requestInterstitialAdWithPropeties:(NSDictionary *)info {
    
    NSDictionary* params = info[ASInterstitialParameter_Parameters];
    NSError* error;
    
    if (params[@"appId"] == nil) {
        error = [NSError errorWithDomain:NSStringFromClass([self class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Invalid appId"}];
    }
    self.viewController = [[ASTremorViewController alloc] initWithDelegate:self withAppID:params[@"appId"]];
    [self.delegate interstitialAdProvider:self didLoadAd:nil];
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    if ([TremorVideoAd isAdReady]) {
        [rootViewController presentViewController:self.viewController animated:NO completion:nil];
    }
    else {
        if (self.attempts > 50) {
            NSError* error = [NSError errorWithDomain:NSStringFromClass([self class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Tremor SDK timed out"}];
            [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
        }
        else {
            [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(handleTimer:) userInfo:rootViewController repeats:NO];
            self.attempts++;
        }
    }
}

- (void)handleTimer:(NSTimer*)theTimer {
    [self showInterstitialFromRootViewController:[theTimer userInfo]];
}


-(void) interstitialAdViewControllerViewWillAppear:(ASInterstitialAdViewController*)viewController {
    [self.delegate interstitialAdProviderWillAppear:self];
}

-(void) interstitialAdViewControllerViewDidAppear:(ASInterstitialAdViewController*)viewController {
    [self.delegate interstitialAdProviderDidAppear:self];
}

-(void) interstitialAdViewControllerViewWillDisappear:(ASInterstitialAdViewController*)viewController {
    [self.delegate interstitialAdProviderWillDisappear:self];
}
-(void) interstitialAdViewControllerViewDidDisappear:(ASInterstitialAdViewController*)viewController {
    [self.delegate interstitialAdProviderWillDisappear:self];
}


-(void) interstitialAdViewControllerCreatedAdSuccessfully:(ASInterstitialAdViewController*)controller {
    [self.delegate interstitialAdProvider:self didLoadAd:self];
}

- (void) interstitialAdViewControllerFailed:(ASInterstitialAdViewController *)controller withError:(NSError *)error {
    [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
}

-(void)interstitialAdViewControllerAdLoadingWasCancelled:(ASInterstitialAdViewController*)controller {
    
}

-(void) interstitialAdViewControllerAdFinished:(ASInterstitialAdViewController*)controller {
    self.viewController = nil;
}

-(void) interstitialAdViewControllerAdWasTouched:(ASInterstitialAdViewController *)controller {
    
    [self.delegate interstitialAdProviderAdWasTouched:self];
}

@end

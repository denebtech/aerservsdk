//
//  ASMRAIDViewController.h
//  AerServSDK
//
//  Created by Albert Zhu on 10/6/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ASMRAIDView;

@interface ASMRAIDViewController : UIViewController

@property (nonatomic, strong) UIViewController *origRootVC;
@property (readonly, nonatomic, assign) BOOL didInvokeCloseFromVC;

- (instancetype)initWithMRAIDView:(ASMRAIDView *)mv;
- (void)dismissMRAIDInterVCWithBlock:(void (^)(void))completionBlock;
- (void)onClose;

@end

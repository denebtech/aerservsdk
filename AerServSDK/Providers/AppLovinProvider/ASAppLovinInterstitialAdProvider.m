//
//  ASAppLovinInterstitialAdProvider.m
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ALSdk.h"
#import "ALAd.h"
#import "ALInterstitialAd.h"
#import "ALIncentivizedInterstitialAd.h"
#import "ASAppLovinInterstitialAdProvider.h"
#import "ASAppLovinInstance.h"
#import "ASAppLovinDelegateProxy.h"
#import "ASCustomInterstitialAdProvider.h"
#import "ASVirtualCurrency.h"

@interface ASAppLovinInterstitialAdProvider()

@property (nonatomic, strong) id alInterstitialAd;
@property (nonatomic, strong) id alIncentivizedInterstitalAd;

@end



@implementation ASAppLovinInterstitialAdProvider

- (void)dealloc {
    [ASAppLovinInstance instance].alSdk = nil;
    [self cleanUpInterstitialAd];
    [self cleanUpIncentivizedInterstitialAd];
}

- (instancetype)init {
    if (self = [super initWithAdClassName:kALSdkClassStr timeout:kCustomInterstitialTimeout]) {
        // Add init code here
    }
    return self;
}

- (void)initializePartnerAd:(NSDictionary *)properties { }

- (BOOL)hasPartnerAdInitialized {
    return YES;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    [ASAppLovinDelegateProxy addAppLovinProvider:self];
    
    // if we have an ad ready, grab it
    if([ASAppLovinInstance instance].alPreInitInterstitialAd != nil) {
        self.alInterstitialAd = [ASAppLovinInstance instance].alPreInitInterstitialAd;
        [ASAppLovinInstance instance].alPreInitInterstitialAd = nil;
        
        [self.alInterstitialAd setAdLoadDelegate:[ASAppLovinDelegateProxy sharedProxy]];
        [self.alInterstitialAd setAdDisplayDelegate:[ASAppLovinDelegateProxy sharedProxy]];
        [self.alInterstitialAd setAdVideoPlaybackDelegate:[ASAppLovinDelegateProxy sharedProxy]];
    }

    // if there's VC, we don't want the regular interstitial ad anymore
    if(self.vcData != nil) {
        [self cleanUpInterstitialAd];
        [ASAppLovinInstance instance].state = kASProviderSetupNone;
    }
    
    // setup ad if need to
    if(![ASAppLovinInstance instance].alSdk || [ASAppLovinInstance instance].state == kASProviderSetupNone) {
        Class alSdkSettingsClass = NSClassFromString(kALSdkSettingsClassStr);
        id alSdkSettings = [alSdkSettingsClass alloc];
        
        // Set verbose logging flag
        BOOL isVerboseLogging = [@"true" isEqualToString:[self valueInProperty:properties forKey:kASALVerboseLoggingKey]];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"isVerboseLogging is %d", isVerboseLogging]];
        [alSdkSettings setIsVerboseLogging:isVerboseLogging];
        
        // Set auto preload sizes
        NSString *preloadSizes = [self valueInProperty:properties forKey:kASALPreloadSizesKey];
        if ([preloadSizes length] == 0) {
            preloadSizes = kALDefaultPreloadSizesStr;
        }
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"preloadSizes is %@", preloadSizes]];
        [alSdkSettings setAutoPreloadAdSizes:preloadSizes];
        
        // Set auto preload types
        NSString *preloadTypes = [self valueInProperty:properties forKey:kASALPreloadTypesKey];
        if ([preloadTypes length] == 0) {
            preloadTypes = kALDefaultPreloadTypesStr;
        }
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"preloadTypes is %@", preloadTypes]];
        [alSdkSettings setAutoPreloadAdTypes:preloadTypes];
        
        // Init ad
        Class alSdkClass = NSClassFromString(kALSdkClassStr);
        NSString *sdkKey = [self valueInProperty:properties forKey:kASALSdkKey];
        id alSdk = [alSdkClass sharedWithKey:sdkKey settings:alSdkSettings];
        [alSdk initializeSdk];
        [ASAppLovinInstance instance].alSdk = alSdk;
    }

    if(!self.vcData && self.alInterstitialAd != nil) {
        // init interstitial ad
        Class alInterstitialAdClass = NSClassFromString(kALInterstitialAdClassStr);
        self.alInterstitialAd = [[alInterstitialAdClass alloc] initWithSdk:[ASAppLovinInstance instance].alSdk];
        [self.alInterstitialAd setAdLoadDelegate:[ASAppLovinDelegateProxy sharedProxy]];
        [self.alInterstitialAd setAdDisplayDelegate:[ASAppLovinDelegateProxy sharedProxy]];
        [self.alInterstitialAd setAdVideoPlaybackDelegate:[ASAppLovinDelegateProxy sharedProxy]];
    } else if(self.vcData != nil) {
        // init incentivized interstitial ad
        Class alIncentivizedInterstitialAdClass = NSClassFromString(kALIncentivizedInterstitialAdClassStr);
        self.alIncentivizedInterstitalAd = [[alIncentivizedInterstitialAdClass alloc] initWithSdk:[ASAppLovinInstance instance].alSdk];
        [self.alIncentivizedInterstitalAd setAdDisplayDelegate:[ASAppLovinDelegateProxy sharedProxy]];
        [self.alIncentivizedInterstitalAd setAdVideoPlaybackDelegate:[ASAppLovinDelegateProxy sharedProxy]];
        [self.alIncentivizedInterstitalAd preloadAndNotify:[ASAppLovinDelegateProxy sharedProxy]];
    }
}

- (BOOL)hasPartnerAdLoaded {
    BOOL hasLoaded = NO;
    
    [ASAppLovinDelegateProxy addAppLovinProvider:self];
    if(self.alInterstitialAd != nil) {
        hasLoaded = [self.alInterstitialAd isReadyForDisplay];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinInterstitialAdProvider, hasPartnerAdLoaded - ALInterstitialAd.isReadyForDisplay: %d", hasLoaded]];
    } else if(self.alIncentivizedInterstitalAd != nil) {
        hasLoaded = [self.alIncentivizedInterstitalAd isReadyForDisplay];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinInterstitialAdProvider, hasPartnerAdLoaded - ALIncentvizedInterstitialAd.isReadyForDisplay: %d", hasLoaded]];
    }
    
    return hasLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.failedToReceiveAd;
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    [ASAppLovinDelegateProxy addAppLovinProvider:self];
    if(self.alInterstitialAd != nil) {
        if ([self.alInterstitialAd isReadyForDisplay]) {
            [self.alInterstitialAd show];
        }
    } else if(self.alIncentivizedInterstitalAd != nil) {
        if([self.alIncentivizedInterstitalAd isReadyForDisplay]) {
            [self.alIncentivizedInterstitalAd showAndNotify:[ASAppLovinDelegateProxy sharedProxy]];
        }
    }
}

- (void)terminatePartnerAd {
    [self cleanupAd];
}

- (void)cleanUpInterstitialAd {
    [self.alInterstitialAd setAdLoadDelegate:nil];
    [self.alInterstitialAd setAdDisplayDelegate:nil];
    [self.alInterstitialAd setAdVideoPlaybackDelegate:nil];
    [self.alInterstitialAd dismiss];
    self.alInterstitialAd = nil;
}

- (void)cleanUpIncentivizedInterstitialAd {
    [self.alIncentivizedInterstitalAd setAdDisplayDelegate:nil];
    [self.alIncentivizedInterstitalAd setAdVideoPlaybackDelegate:nil];
    [self.alIncentivizedInterstitalAd dismiss];
    self.alIncentivizedInterstitalAd = nil;
}

- (void)cleanupAd {
    if(self.alInterstitialAd != nil) {
        [self cleanUpInterstitialAd];
    } else if(self.alIncentivizedInterstitalAd != nil) {
        [self cleanUpIncentivizedInterstitialAd];
    }
    [ASAppLovinDelegateProxy removeAppLovinProvider];
}

- (void)cancel {
    [self cleanupAd];
}

#pragma mark - overloaded methods

- (void)asPartnerInterstitialAdDidLoad:(id)ad {
    @try {
        if([self.delegate interstitialAdProviderIsPreload:self]) {
            [self.delegate interstitialAdProvider:self didPreloadAd:ad];
        } else {
            [self.delegate interstitialAdProvider:self didLoadAd:ad];
        }
        
        Class alAdClass = NSClassFromString(kALAdClassStr);
        if([ad isKindOfClass:[alAdClass class]] &&
           [ad respondsToSelector:@selector(isVideoAd)] &&
           [ad isVideoAd]) {
            // Fire virtual currency events, if necessary
            NSDictionary *virtualCurrency = (!self.vcData) ? [ASVirtualCurrency getVirtualCurrencyData:self.properties[ASInterstitialParameter_HTMLHeaders]] : self.vcData;
            if (virtualCurrency) {
                [self.delegate interstitialAdProvider:self didVirtualCurrencyLoad:virtualCurrency];
            }
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

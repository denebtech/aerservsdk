//
//  ASSpotXInterstitialAdProvider.m
//  AerServSDK
//
//  Created by Vasyl Savka on 10/14/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASSpotXchangeVastInterstitialAdProvider.h"
#import "ASSpotXAd.h"

@interface ASSpotXchangeVastInterstitialAdProvider() <SpotXAdDelegate>

@property (nonatomic, strong) ASSpotXAd *ad;
@property (nonatomic, copy) NSString *channelId;

@property (nonatomic, assign) BOOL adInitialized;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;

@end



@implementation ASSpotXchangeVastInterstitialAdProvider

- (void)dealloc {
    _ad = nil;
    _channelId = nil;
}

- (instancetype)init {
    if(self = [super initWithAdClassName:NSStringFromClass([ASSpotXAd class]) timeout:kCustomInterstitialTimeout]) { }
    
    return self;
}

#pragma mark - Overloaded Custom Interstitial Ad Provider Methods

- (void)initializePartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASSpotXchangeVastInterstitialAdProvider, initializePartnerAd:"];
    self.adInitialized = YES;
}

- (BOOL)hasPartnerAdInitialized {
    return self.adInitialized;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    return !self.adInitialized;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    [ASSDKLogger logStatement:@"ASSpotXchangeVastInterstitialAdProvider, loadPartnerAd:"];
    self.adLoaded = NO;
    self.adFailed = NO;
    
    NSDictionary *mediationProperties = properties[ASInterstitialParameter_Parameters];
    self.channelId = mediationProperties[kSpotXchangeVastSdkChannelIdKey];
    self.ad = [ASSpotXAd adWithChannelId:self.channelId andDelegate:self];
    if(self.ad != nil) {
        self.adLoaded = YES;
    }
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    [ASSDKLogger logStatement:@"ASSpotXchangeVastInterstitialAdProvider, showPartnerAd:"];
    [self.ad showWith:rootViewController asAnimated:NO];
}

- (void)terminatePartnerAd { }

#pragma mark - SpotXAdDelegate Protocol Methods

- (void)spotXAdDidStart:(ASSpotXAd *)ad {
    [ASSDKLogger logStatement:@"ASSpotXchangeVastInterstitialAdProvider, spotXAdDidStart+:"];
    [self asPartnerInterstitialAdWillAppear];
    [self asPartnerInterstitialAdDidAppear];
}

- (void)spotXAdDidFinish:(ASSpotXAd *)ad {
    [ASSDKLogger logStatement:@"ASSpotXchangeVastInterstitialAdProvider, spotXAdDidFinish:"];
    [self asPartnerInterstitialVideoCompleted];
    [self asPartnerInterstitialAdWillDisappear];
    [self asPartnerInterstitialAdDidDisappear];
}

- (void)spotXAdDidFail:(ASSpotXAd *)ad withError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASSpotXchangeVastInterstitialAdProvider, spotXAdDidFail:withError: - error: %@", error.localizedDescription]];
    self.adFailed = YES;
}

@end

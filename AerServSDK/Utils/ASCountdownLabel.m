//
//  ASCountdownLabel.m
//  AerServSDK
//
//  Created by Vasyl Savka on 11/3/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASCountdownLabel.h"

@implementation ASCountdownLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init {
    if(self = [super initWithFrame:CGRectMake(0.0f, 0.0f, kCountdownLblW, kCountdownLblH)]) {
        self.textAlignment = NSTextAlignmentCenter;
        self.textColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor clearColor];
        self.shadowColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f];
        self.shadowOffset = CGSizeMake(1.0f, 1.0f);
        self.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin;
        self.text = @"";
        self.font = [UIFont fontWithName:@"Helvetica" size:24.0];
        self.hidden = YES;
    }
    
    return self;
}

- (void)changePosToX:(CGFloat)xPos andY:(CGFloat)yPos {
    self.frame = CGRectMake(xPos, yPos, kCountdownLblW, kCountdownLblH);
}

@end

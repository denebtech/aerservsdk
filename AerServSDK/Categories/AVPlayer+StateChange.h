//
//  AVPlayer+StateChange.h
//  AerServSDK
//
//  Created by Vasyl Savka on 9/18/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface AVPlayer (StateChange)

- (void)changeStateToPlay;
- (void)changeStateToPause;
- (void)playPause;

@end

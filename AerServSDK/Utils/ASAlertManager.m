//
//  ASAlertManager.m
//  AerServSDK
//
//  Created by Albert Zhu on 6/23/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASAlertManager.h"

#define k1Sec 1.0f

@interface ASAlertManager()

@property (nonatomic, strong) dispatch_semaphore_t alertSema;

@end

@implementation ASAlertManager

+ (ASAlertManager *)manager {
    static ASAlertManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
            manager = [ASAlertManager new];
            manager.alertSema = nil;
    });
    
    return manager;
}

+ (void)semaphoreCreateOrWait {
    if(![ASAlertManager manager].alertSema) {
        [ASAlertManager manager].alertSema = dispatch_semaphore_create(0);
    } else {
        dispatch_semaphore_wait([ASAlertManager manager].alertSema, DISPATCH_TIME_FOREVER);
    }
}

+ (void)semaphoreSignal {
    dispatch_semaphore_signal([ASAlertManager manager].alertSema);
}

//+ (void)showAlertViewWithMessage:(NSString *)msg forTimeInt:(NSTimeInterval)interval {
//    #if kShowAlert
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
//    [alert show];
//    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        @try {
//            [ASSDKLogger logStatement:@"ASAlertManager, showAlertViewWithMessage:forTimeInt: - Dispatch After Block, dismissing alert"];
//            [alert dismissWithClickedButtonIndex:0 animated:YES];
//            [ASAlertManager semaphoreSignal];
//        }
//        @catch (NSException *exception) {
//            [ASSDKLogger onException:exception];
//        }
//    });
//    #endif
//}

+ (void)showAlertControllerWithMessage:(NSString *)msg forTimeInt:(NSTimeInterval)interval {
    #if kShowAlert
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIViewController *rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;
    [rootVC presentViewController:alert animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        @try {
            [ASSDKLogger logStatement:@"ASAlertManager, showAlertControllerWithMessage:forTimeInt: - Dispatch After Block, dismissing alert"];
            if([rootVC.presentedViewController isEqual:alert]) {
                [rootVC dismissViewControllerAnimated:YES completion:^{
                    [ASAlertManager semaphoreSignal];
                }];
            } else {
                [ASAlertManager semaphoreSignal];
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
    #endif
}

//+ (void)showAlertWithMessage:(NSString *)msg forTimeInt:(NSTimeInterval)interval {
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        @try {
//            [ASAlertManager semaphoreCreateOrWait];
//            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAlertManager, showAlertWithMessage:forTimeInt: - msg: %@", msg]];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                @try {
//                    if(kIS_iOS_8_OR_LATER) {
//                        [ASAlertManager showAlertControllerWithMessage:msg forTimeInt:interval];
//                    } else {
//                        [ASAlertManager showAlertViewWithMessage:msg forTimeInt:interval];
//                    }
//                }
//                @catch (NSException *exception) {
//                    [ASSDKLogger onException:exception];
//                }
//            });
//        }
//        @catch (NSException *exception) {
//            [ASSDKLogger onException:exception];
//        }
//    });
//}

@end

//
//  ASVirtualCurrency.h
//  AerServSDK
//
//  Created by Eric Koyanagi on 3/20/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#ifndef AerServSDK_ASVirtualCurrency_h
#define AerServSDK_ASVirtualCurrency_h

#define kXAerServKey @"X-AerServ"
#define kVirtualCurrencyKey @"vc"
#define kServerCallbackEventURLKey @"servercallbackeventurl"
#define kServerCallbackEventTimeout 3.0f

/**
 * This is a util class to parse virtual currency headers
 *
 */
@interface ASVirtualCurrency : NSObject

+ (NSDictionary *)getVirtualCurrencyData:(NSDictionary *)headers;
+ (void)vcServerCallbackWithVCData:(NSDictionary *)vcData;

@end

#endif

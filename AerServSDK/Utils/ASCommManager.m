//
//  ASCommManager.m
//  AerServSDK
//
//  Created by Vasyl Savka on 1/29/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASCommManager.h"
#import "ASMutableURLRequest.h"
#import "ASVASTEvent.h"
#import "NSData+ASUtils.h"

@interface ASCommManager()

@property (atomic, strong) NSMutableDictionary *handlerForReq;
@property (atomic, strong) NSMutableDictionary *respForReq;
@property (atomic, strong) NSMutableDictionary *dataForReq;

@end

@implementation ASCommManager

#pragma mark - Singleton

+ (ASCommManager *)manager {
    static ASCommManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [ASCommManager new];
        manager.handlerForReq = [NSMutableDictionary dictionary];
        manager.respForReq = [NSMutableDictionary dictionary];
        manager.dataForReq = [NSMutableDictionary dictionary];
    });
    
    return manager;
}

#pragma mark - Helpers

+ (NSString *)urlFromReq:(NSURLRequest *)request {
    return [request URL].absoluteString;
}

+ (NSString *)origReqUrlFromConnection:(NSURLConnection *)conn {
    return [conn originalRequest].URL.absoluteString;
}

+ (NSString *)origReqUrlFromSessionTask:(NSURLSessionTask *)sessTask {
    return [sessTask originalRequest].URL.absoluteString;
}

+ (ASMutableURLRequest *)requestWithEndPoint:(id)epStrOrUrl withHttpMethod:(NSString *)httpMethod forTime:(NSTimeInterval)timeInt {
    ASMutableURLRequest *urlReq;
    
    @try {
        if([epStrOrUrl isKindOfClass:[NSString class]]) {
            NSString *endPointStr = (NSString *)epStrOrUrl;
            urlReq = [ASMutableURLRequest requestWithURL:[NSURL URLWithString:endPointStr]];
        } else if([epStrOrUrl isKindOfClass:[NSURL class]]) {
            NSURL *endPointUrl = (NSURL *)epStrOrUrl;
            urlReq = [ASMutableURLRequest requestWithURL:endPointUrl];
        }
        if(urlReq != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, requestWithEndPoint:withHttpMethod:forTime: - urlReq.URL: %@", urlReq.URL.absoluteString]];
            [urlReq setHTTPMethod:httpMethod];
            [urlReq setTimeoutInterval:timeInt];
            [urlReq setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
        }
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
    return urlReq;
}

+ (NSDictionary *)httpHeadersWithLength:(NSUInteger)len {
    return @{@"Accept" : @"application/json",
             @"Content-Type" : @"application/json",
             @"Content-Length" : [NSString stringWithFormat:@"%lu", (unsigned long)len]};
}

+ (void)finishCommAttemptWithReqKey:(NSString *)reqKey {
    @try {
        if(reqKey != nil) {
            [self manager].handlerForReq[reqKey] = nil;
            [self manager].respForReq[reqKey] = nil;
            [self manager].dataForReq[reqKey] = nil;
        }
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - GET Request

+ (void)sendGetRequestToEndPoint:(id)epStrOrUrl forTime:(NSTimeInterval)timeInt endWithCompletionBlock:(void(^)(NSURLResponse *resp, NSData *data, NSError *err))handler {
    [ASSDKLogger logStatement:@"ASCommManager, sendGetRequestToEndPoint:forTime:endWithCompletionBlock: - ENTER"];
    
    @try {
        // build url request object and hashkey
        ASMutableURLRequest *urlReq = [self requestWithEndPoint:epStrOrUrl withHttpMethod:kHTTPMethodGET forTime:timeInt];
        [urlReq setAllHTTPHeaderFields:[self httpHeadersWithLength:0]];
        NSString *reqKey = [self urlFromReq:urlReq];
        
        // check if we've seen the request before, if so return
        if(!reqKey || [self manager].handlerForReq[reqKey] != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, sendGetRequestToEndPoint:forTime:endWithCompletionBlock: - request key does not exist or request already in process, DROPPING THIS REQUEST"]];
            return;
        }
        
        // store a copy of the handler
        void (^handlerCopy)(NSURLResponse *resp, NSData *data, NSError *err) = [handler copy];
        if(reqKey != nil) {
            [self manager].handlerForReq[reqKey] = handlerCopy;
        
            // send get request
            if(kIS_iOS_7_OR_LATER) {
                [self sendSessionWithRequest:urlReq];
            } else {
                [self sendConnectionWithRequest:urlReq];
            }
        }
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - POST Request

+ (void)sendPostRequestToEndPoint:(id)epStrOrUrl withJSONData:(NSData *)jsonData forTime:(NSTimeInterval)timeInt endWithCompletionBlock:(void(^)(NSURLResponse *resp, NSData *data, NSError *err))handler {
    [ASSDKLogger logStatement:@"ASCommManager, sendPostRequestToEndPoint:withJSONData:forTime:endWithCompletionBlock: - ENTER"];
    
    @try {
        // build url request object and hashkey
        ASMutableURLRequest *urlReq = [self requestWithEndPoint:epStrOrUrl withHttpMethod:kHTTPMethodPOST forTime:timeInt];
        [urlReq setHTTPBody:jsonData];
        [urlReq setAllHTTPHeaderFields:[self httpHeadersWithLength:jsonData.length]];
        NSString *reqKey = [self urlFromReq:urlReq];
        
        // check if we've seen the request before, if so return
        if(!reqKey || [self manager].handlerForReq[reqKey] != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, sendGetRequestToEndPoint:forTime:endWithCompletionBlock: - request key does not exist or request already in process, DROPPING THIS REQUEST"]];
            return;
        }
        
        // store a copy of the handler
        void (^handlerCopy)(NSURLResponse *resp, NSData *data, NSError *err) = [handler copy];
        if(reqKey != nil) {
            [self manager].handlerForReq[reqKey] = handlerCopy;
        
            // send post request
            if(kIS_iOS_7_OR_LATER) {
                [self sendSessionWithRequest:urlReq];
            } else {
                [self sendConnectionWithRequest:urlReq];
            }
        }
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - Async Requests

+ (void)sendAsynchronousRequestsForEvent:(ASVASTEvent *)event {
    @try {
        if (event != nil && event.canBeSent) {
            for (NSURL *url in event.urls) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, sendAsynchronousRequestsForEvent - Event(%ld)\t-\turl: %@", (long)event.type, url]];
                [self sendAsynchronousRequestForURL:url];
            }
            
            if (event.oneTime)
                event.canBeSent = NO;
        }
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

+ (void)sendAsynchronousRequestForURL:(NSURL *)url {
    [ASCommManager sendGetRequestToEndPoint:url forTime:kASEventTimeout endWithCompletionBlock:^(NSURLResponse* resp, NSData* data, NSError* err) {
        @try {
            if(err != nil) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, sendAsynchronousRequestsForEvent - Could not hit %@\t-\terror: %@", url, err.localizedDescription]];
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
}

#pragma mark - Connection/Session Initializers

+ (void)sendConnectionWithRequest:(NSURLRequest *)urlReq {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, sendConnectionPostWithRequest: - urLReq: %@", [self urlFromReq:urlReq]]];
    
    // create and start connection object
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:urlReq delegate:[ASCommManager manager]];
            [connection start];
        } @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
}

+ (void)sendSessionWithRequest:(NSURLRequest *)urlReq {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, sendSessionPostWithRequest: - urLReq: %@", [self urlFromReq:urlReq]]];
    
    @try {
        // setup session configuration
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = urlReq.timeoutInterval;
        sessionConfig.timeoutIntervalForResource = urlReq.timeoutInterval;
        sessionConfig.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        sessionConfig.HTTPAdditionalHeaders = urlReq.allHTTPHeaderFields;
        
        // create data session and start task
        NSURLSession *dataSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:[self manager] delegateQueue:nil];
        NSURLSessionTask *task = [dataSession dataTaskWithRequest:urlReq];
        [task resume];
    } @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
    @try {
        if([challenge previousFailureCount] > 0) {
            [ASSDKLogger logStatement:@"ASCommManager, connection:willSendRequestForAuthenticationChallenege: - previousFailureCount > 0"];
            [[challenge sender] cancelAuthenticationChallenge:challenge];
        } else {
            [ASSDKLogger logStatement:@"ASCommManager, connection:willSendRequestForAuthenticationChallenege: - use credential"];
            [[challenge sender] useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
                   forAuthenticationChallenge:challenge];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
        
        // finish and clean up if exception is hit
        NSString *reqKey = [ASCommManager origReqUrlFromConnection:connection];
        [ASCommManager finishCommAttemptWithReqKey:reqKey];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, connection:didFailWithError: - ERROR: %@", error.localizedDescription]];
    
    NSString *reqKey = [ASCommManager origReqUrlFromConnection:connection];
    @try {
        // fire error if request key exists
        if(reqKey != nil) {
            void (^handler)(NSURLResponse *resp, NSData *data, NSError *err) = [ASCommManager manager].handlerForReq[reqKey];
            if(handler != nil) {
                handler(nil, nil, error);
            }
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    @finally {
        // finish and clean up
        [ASCommManager finishCommAttemptWithReqKey:reqKey];

    }
}

#pragma mark - NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, connection:didReceiveResponse: respone.expectedContentLength: %lld", response.expectedContentLength]];
    
    NSString *reqKey = [ASCommManager origReqUrlFromConnection:connection];
    @try {
        // save response and init data if request key exists
        if(reqKey != nil) {
            [ASCommManager manager].respForReq[reqKey] = response;
            [ASCommManager manager].dataForReq[reqKey] = [NSMutableData data];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
        
        // finish and clean up if exception is hit
        [ASCommManager finishCommAttemptWithReqKey:reqKey];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, connection:didReceiveData: data.length: %lu", (unsigned long)data.length]];
    
    NSString *reqKey = [ASCommManager origReqUrlFromConnection:connection];
    @try {
        // append data if request keys exists
        if(reqKey != nil) {
            NSMutableData *connData = [ASCommManager manager].dataForReq[reqKey];
            [connData appendData:data];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
        
        // finish and clean up if exception is hit
        [ASCommManager finishCommAttemptWithReqKey:reqKey];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [ASSDKLogger logStatement:@"ASCommManager, connectionDidFinishLoading"];

    NSString *reqKey = [ASCommManager origReqUrlFromConnection:connection];
    @try {
        // fire handler if request key exists
        if(reqKey != nil) {
            void (^handler)(NSURLResponse *resp, NSData *data, NSError *err) = [ASCommManager manager].handlerForReq[reqKey];
            if(handler != nil) {
                handler([ASCommManager manager].respForReq[reqKey], [ASCommManager manager].dataForReq[reqKey], nil);
            }
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    @finally {
        // finish and clean up
        [ASCommManager finishCommAttemptWithReqKey:reqKey];
    }
}

#pragma mark - NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    [ASSDKLogger logStatement:@"ASCommManager, URLSession:task:didCompleteWithError:"];
    
    NSString *reqKey = [ASCommManager origReqUrlFromSessionTask:task];
    void (^handler)(NSURLResponse *resp, NSData *data, NSError *err) = (reqKey != nil) ? [ASCommManager manager].handlerForReq[reqKey] : nil;
    @try {
        if(error != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, URLSession:task:didCompleteWithError: - error: %@", error.localizedDescription]];
            
            // fire error
            if(handler != nil) {
                handler(nil, nil, error);
            }
        } else {
            // fire handler
            if(handler != nil && reqKey != nil) {
                handler([ASCommManager manager].respForReq[reqKey], [ASCommManager manager].dataForReq[reqKey], nil);
            }
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    @finally {
        // finish and clean uo
        [ASCommManager finishCommAttemptWithReqKey:reqKey];
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
    
    @try {
        if([challenge previousFailureCount] > 0) {
            [ASSDKLogger logStatement:@"ASCommManager, URLSession:didReceiveChallenge:completionHandler: - previousFailureCount > 0"];
            [[challenge sender] cancelAuthenticationChallenge:challenge];
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
        } else {
            [ASSDKLogger logStatement:@"ASCommManager, URLSession:didReceiveChallenge:completionHandler: - use credential"];
            [[challenge sender] useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
                   forAuthenticationChallenge:challenge];
            completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
        
        // finish session
        [session finishTasksAndInvalidate];
    }
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session {
    [ASSDKLogger logStatement:@"ASCommManager, URLSesssionDidFinishEventsForBackgroundURLSession:"];
}

#pragma mark - NSURLSessionDataDelelgate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    [ASSDKLogger logStatement:@"ASCommManager, URLSession:dataTask:didReceiveResponse:completionHandler:"];
    
    NSString *reqKey = [ASCommManager origReqUrlFromSessionTask:dataTask];
    @try {
        // save response and init data
        if(reqKey != nil) {
            [ASCommManager manager].respForReq[reqKey] = response;
            [ASCommManager manager].dataForReq[reqKey] = [NSMutableData data];
            
            // allow response
            completionHandler(NSURLSessionResponseAllow);
        } else {
            // cancel response if reqKey does not exist
            completionHandler(NSURLSessionResponseCancel);
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
        
        // cancel session and clean up
        completionHandler(NSURLSessionResponseCancel);
        [ASCommManager finishCommAttemptWithReqKey:reqKey];
    }
}

- (void)URLSession:(NSURLSession *)session dataTask:(nonnull NSURLSessionDataTask *)dataTask didBecomeDownloadTask:(nonnull NSURLSessionDownloadTask *)downloadTask {
    [ASSDKLogger logStatement:@"ASCommManager, URLSession:dataTask:didBecomeDownloadTask:"];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, URLSession:dataTask:didReceiveData: - append data: %lu", (unsigned long)data.length]];
    
    NSString *reqKey = [ASCommManager origReqUrlFromSessionTask:dataTask];
    @try {
        // append data to data object associated with this session
        NSMutableData *sessData = (reqKey != nil) ? [ASCommManager manager].dataForReq[reqKey] : nil;
        [sessData appendData:data];
        
        #if kCommShowData
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASCommManager, URLSession:dataTask:didReceiveData: -- sessData: %@", [sessData stringEncoded]]];
        #endif
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
        
        // finish and clean up
        [ASCommManager finishCommAttemptWithReqKey:reqKey];
    }
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask willCacheResponse:(NSCachedURLResponse *)proposedResponse completionHandler:(void (^)(NSCachedURLResponse * _Nullable))completionHandler {
    [ASSDKLogger logStatement:@"ASCommManager, URLSession:dataTask:willCacheResponse:completionHandler"];
    
    completionHandler(proposedResponse);
}

@end


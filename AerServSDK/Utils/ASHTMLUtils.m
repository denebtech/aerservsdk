//
//  ASHTMLUtils.m
//  AerServSDK
//
//  Created by Albert Zhu on 3/25/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASHTMLUtils.h"

@implementation ASHTMLUtils

+ (NSString *)htmlHeadStrInWebView:(UIWebView *)webview {
    return [webview stringByEvaluatingJavaScriptFromString:@"document.head.innerHTML"];
}

+ (NSString *)htmlBodyStrInWebView:(UIWebView *)webview {
    return [webview stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
}

+ (NSString *)findBPFFromHTMLData:(NSData *)htmlData {
    NSString *htmlStr = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];

    NSString *urlStr = nil;
    while([htmlStr containsStr:kEventsAerServDomain] && ![urlStr containsStr:kBPFEventStr]) {
        NSRange range = [htmlStr rangeOfString:kEventsAerServDomain];
        htmlStr = [htmlStr substringFromIndex:range.location];
        NSRange doubleQuoteRange = [htmlStr rangeOfString:@"\""];
        NSRange singleQuoteRange = [htmlStr rangeOfString:@"'"];
        range = (doubleQuoteRange.location < singleQuoteRange.location) ? doubleQuoteRange : singleQuoteRange;
        urlStr = [htmlStr substringToIndex:range.location];
        htmlStr = [htmlStr substringFromIndex:range.location];
    }
    
    return [urlStr containsStr:kBPFEventStr] ? [urlStr lowercaseString] : nil;
}

+ (BOOL)doesContainBPFInWebView:(UIWebView *)webView {
    NSString *htmlBody = [self htmlBodyStrInWebView:webView];
    return ([htmlBody containsStr:kEventsAerServDomain] && [htmlBody containsStr:kBPFEventStr]);
}

@end

//
//  ASYahooInterstitialAdProvider.h
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASCustomInterstitialAdProvider.h"
#import <Foundation/Foundation.h>

@interface ASYahooInterstitialAdProvider : ASCustomInterstitialAdProvider

@end

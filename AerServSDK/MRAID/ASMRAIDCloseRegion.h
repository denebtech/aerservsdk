//
//  ASMRAIDCloseRegion.h
//  AerServSDK
//
//  Created by Albert Zhu on 10/6/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kASMRAIDCloseRegionWH 50.0f
#define kASMRAIDCloseButtonWH 32.0f

@class ASMRAIDCloseButton;

@interface ASMRAIDCloseRegion : UIView

- (instancetype)initWithTarget:(id)target andSelector:(SEL)action inView:(UIView *)view;
- (void)hideCloseBtn:(BOOL)yesOrNo;
- (BOOL)isCloseBtnHidden;

@end

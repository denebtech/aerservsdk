//
//  NSURL+ASUtils.h
//  AerServSDK
//
//  Created by Vasyl Savka on 4/8/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define kUsePredefinedLocationString 0
#define kPredefinedLocationLatString @"30.2873178"
#define kPredefinedLocationLongString @"-97.732836"

typedef NS_ENUM(NSUInteger, ASLoadType) {
    kASLoadTypeRegular = 0,
    kASLoadTypePreload = 1,
    kASLoadTypePrecache = 2
};

@interface NSURL (ASUtils)

+ (instancetype)URLForPlacementID:(NSString *)placementID inEnv:(ASEnvironmentType)env atlocation:(CLLocation *)location usingKeyWords:(NSArray *)keyWordsArr andPubKeys:(NSDictionary *)pubKeys withUserID:(NSString *)userId asPreload:(BOOL)preload onPlatform:(ASPlatformType)platform;
- (instancetype)initWithPlacementID:(NSString *)placementID inEnv:(ASEnvironmentType)env atlocation:(CLLocation *)location usingKeyWords:(NSArray *)keyWordsArr andPubKeys:(NSDictionary *)pubKeys withUserID:(NSString *)userId asPreload:(BOOL)preload onPlatform:(ASPlatformType)platform;

+ (instancetype)URLWithAddress:(NSString *)address;
- (instancetype)initWithAddress:(NSString *)address;

@end

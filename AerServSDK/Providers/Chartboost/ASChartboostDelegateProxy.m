//
//  ASChartboostDelegateProxy.m
//  AerServSDK
//
//  Created by Albert Zhu on 11/25/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASChartboostDelegateProxy.h"

@interface ASChartboostDelegateProxy()

@property (nonatomic, strong) ASChartboostInterstitialAdProvider *provider;

@end

@implementation ASChartboostDelegateProxy

+ (ASChartboostDelegateProxy *)sharedProxy {
    static ASChartboostDelegateProxy *sharedProxy = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedProxy = [ASChartboostDelegateProxy new];
    });
    
    return sharedProxy;
}

+ (void)addChartboostProvider:(ASChartboostInterstitialAdProvider *)cbInterProvider {
    [ASSDKLogger logStatement:@"ASChartboostDelegateProxy, addChartboostProvider:"];
    [ASChartboostDelegateProxy sharedProxy].provider = cbInterProvider;
}

+ (void)removeChartboostProvider {
    [ASSDKLogger logStatement:@"ASChartboostDelegateProxy, removeChartboostProvider:"];
    [ASChartboostDelegateProxy sharedProxy].provider = nil;
}

#pragma mark - Chartboost Delegate Helpers

- (NSString *)translateToMessageFromCBLoadError:(CBLoadError)cbLoadError {
    NSString *msg;
    switch (cbLoadError) {
        case CBLoadErrorInternetUnavailable:
            msg = @"Internet unavailable";
            break;
        case CBLoadErrorInternal:
            msg = @"Internal error";
            break;
        case CBLoadErrorNetworkFailure:
            msg = @"Network failure";
            break;
        case CBLoadErrorWrongOrientation:
            msg = @"Wrong orientation";
            break;
        case CBLoadErrorTooManyConnections:
            msg = @"Too many connections";
            break;
        case CBLoadErrorFirstSessionInterstitialsDisabled:
            msg = @"First session interstitial disabled";
            break;
        case CBLoadErrorNoAdFound:
            msg = @"No ad found";
            break;
        case CBLoadErrorSessionNotStarted:
            msg = @"Session not started";
            break;
        default:
            msg = @"Unknown";
            break;
    }
    return msg;
}

#pragma mark - ChartboostDelegate Methods

- (void)didCacheInterstitial:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didCacheInterstitial: - location: %@", location]];
}

- (void)didFailToLoadInterstitial:(CBLocation)location withError:(CBLoadError)cbLoadError {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didFailToLoadInterstitial: - location: %@, error: %@", location, [self translateToMessageFromCBLoadError:cbLoadError]]];
    self.provider.failedToReceiveAd = YES;
    self.provider.didPlayVideo = NO;
}

- (void)didFailToRecordClick:(CBLocation)location withError:(CBClickError)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didFailToRecordClick:withError: - location: %@", location]];
    [self.provider asPartnerInterstitialAdWasTouched];
}

- (void)didDismissInterstitial:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didDismissInterstitial: - location: %@", location]];
    
    if(self.provider.didPlayVideo) {
        [self.provider asPartnerInterstitialVideoCompleted];
        self.provider.didPlayVideo = NO;
    }
    
    [self.provider asPartnerInterstitialAdWillDisappear];
    [self.provider asPartnerInterstitialAdDidDisappear];
    [ASChartboostDelegateProxy removeChartboostProvider];
}

- (void)didCloseInterstitial:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didCloseInterstitial: - location: %@", location]];
}

- (void)didClickInterstitial:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didClickInterstitial: - location: %@", location]];
    [self.provider asPartnerInterstitialAdWasTouched];
}

- (void)didDisplayInterstitial:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didDisplayInterstitial: - location: %@", location]];
    [self.provider asPartnerInterstitialAdWillAppear];
    [self.provider asPartnerInterstitialAdDidAppear];
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
}

- (void)willDisplayVideo:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, willDisplayVideo:: - location: %@", location]];
    self.provider.didPlayVideo = YES;
}

#pragma mark - ChartboostDelegate - Rewarded Video

- (BOOL)shouldDisplayRewardedVideo:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, shouldDisplayRewardedVideo: - location: %@, display: %d", location, (self.provider.vcData != nil)]];
    return self.provider.vcData != nil;
}

- (void)didDisplayRewardedVideo:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didDisplayRewardedVideo: - location: %@", location]];
    [self.provider asPartnerInterstitialAdWillAppear];
    [self.provider asPartnerInterstitialAdDidAppear];
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
}

- (void)didCacheRewardedVideo:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didCacheRewardedVideo: - location: %@", location]];
}

- (void)didFailToLoadRewardedVideo:(CBLocation)location withError:(CBLoadError)error {
    NSString *msg = [self translateToMessageFromCBLoadError:error];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didFailToLoadRewardedVideo:withError: - location: %@, error: %@", location, msg]];
    self.provider.failedToReceiveAd = YES;
}

- (void)didDismissRewardedVideo:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didDismissRewardedVideo: - location: %@", location]];
    [self.provider asPartnerInterstitialAdWillDisappear];
    [self.provider asPartnerInterstitialAdDidDisappear];
}

- (void)didCloseRewardedVideo:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didCloseRewardedVideo: - location: %@", location]];
}

- (void)didClickRewardedVideo:(CBLocation)location {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didClickRewardedVideo: - location: %@", location]];
    [self.provider asPartnerInterstitialAdWasTouched];
}

- (void)didCompleteRewardedVideo:(CBLocation)location withReward:(int)reward {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASChartboostDelegateProxy, didCompleteRewardedVideo:withReward: - location: %@, reward: %d", location, reward]];
    [self.provider asPartnerInterstitialVideoCompleted];
}

@end

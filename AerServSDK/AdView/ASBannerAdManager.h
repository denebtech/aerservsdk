//
//  ASBannerAdManager.h
//  AerServSDK
//
//  Created by Vasyl Savka on 4/14/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASAdManager.h"

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class ASAdView;
@class ASMRAIDView;
@class ASTransactionInfo;

@protocol ASBannerAdManagerDelegate;

@interface ASBannerAdManager : ASAdManager

@property (nonatomic, weak) id<ASBannerAdManagerDelegate> delegate;
@property (nonatomic, assign) NSTimeInterval bannerRefershTimeInterval;


- (void)fetchBannerAdForPlacementID:(NSString *)plcId inEnv:(ASEnvironmentType)env usingKeyWords:(NSArray *)keyWords andPubKeys:(NSDictionary *)pubKeys withUserID:(NSString *)userId onPlatform:(ASPlatformType)platform;
- (void)forceRefresh;

- (void)stopAutomaticallyRefreshingAds;
- (void)startAutomaticallyRefreshingAds;

//- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation;
//- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation withSize:(CGSize)currSize;
- (void)cancel;

- (void)startPreloadedBannerAd;

- (void)reportErrorForProvider;

@end

@protocol ASBannerAdManagerDelegate <NSObject>

- (UIViewController *)viewControllerForPresentingModalView;
- (CGSize)requestedSize;

- (void)bannerAdManager:(ASBannerAdManager*)adManager didFailToFetchAdwithError:(NSError *)error;
- (void)bannerAdManager:(ASBannerAdManager *)adManager didPreloadAd:(UIView *)ad;
- (void)bannerAdManager:(ASBannerAdManager *)adManager didLoadAd:(UIView *)ad;
- (void)bannerAdManager:(ASBannerAdManager *)adManager didFinishLoadingPreloadAd:(UIView *)ad;

- (void)bannerAdManagerWillBeginAction:(ASBannerAdManager *)adManager;
- (void)bannerAdManagerWillEndAction:(ASBannerAdManager *)provider;

- (void)bannerAdManagerWillLeaveApplication:(ASBannerAdManager *)provider;
- (void)bannerAdManagerAdChangedSize:(ASBannerAdManager *)adManager;
- (void)bannerAdManagerAdWasClicked:(ASBannerAdManager *)adManager;
- (void)bannerAdManagerDidShowAdCompletely:(ASBannerAdManager *)adManager;

- (void)AVPlayerCreated:(AVPlayer *)avPlayer;

- (void)bannerAdManager:(ASBannerAdManager *)adManager willShowSkip:(BOOL)show;
- (void)bannerAdManager:(ASBannerAdManager *)adManager willChangeFrameTo:(CGRect)newFrame;
- (ASAdView *)bannerAdManagerWillGetAdView:(ASBannerAdManager *)adManager;
- (UIView *)bannerAdManagerWillGetContentView:(ASBannerAdManager *)adManager;
//- (void)bannerAdManager:(ASBannerAdManager *)adManager willSetContentViewToMRAIDView:(ASMRAIDView *)mraidView;

- (void)bannerAdManager:(ASBannerAdManager *)adManager didLoadPlayerLayer:(AVPlayerLayer *)pl;

- (void)bannerAdManagerWillAttemptRefresh:(ASBannerAdManager *)adManager;
- (void)bannerAdManager:(ASBannerAdManager *)adManager didFireAdvertiserEventWithMessage:(NSString *)msg;
- (void)bannerAdManager:(ASBannerAdManager *)manager didFindAdTransactionInfo:(ASTransactionInfo *)transactionInfo;

@end

//
//  ASVASTEvent.h
//  AerServSDK
//
//  Created by Vasyl Savka on 4/2/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kShowVASTEventDebug 0

// NOTE: these are only used internally by the SDK and do not need to be in sync
// with EventEnum.java on the server side. They are only to make reading the logs easier.
// The SDK does not calculate event URL and gets them from the server.
typedef NS_ENUM(NSInteger, ASVASTEventType) {
    ASVASTEventType_Unknown = 0,
    ASVASTEventType_AdImpression = 1,
    ASVASTEventType_VideoStart = 2,
    ASVASTEventType_VideoFirstQuartile = 3,
    ASVASTEventType_VideoMidpoint = 4,
    ASVASTEventType_VideoThirdQuartile = 5,
    ASVASTEventType_VideoComplete = 6,
    ASVASTEventType_VideoClickTracking = 7,
    ASVASTEventType_AdError = 8,
    ASVASTEventType_CreativeView = 9,
    ASVASTEventType_Skip = 10,
    ASVASTEventType_Progress = 11,
    ASVASTEventType_AcceptInvitation = 12,
    ASVASTEventType_Collapse = 13,
    ASVASTEventType_Close = 14,
    ASVASTEventType_Pause = 15,
    ASVASTEventType_Resume = 16,
    ASVASTEventType_Mute = 17,
    ASVASTEventType_Unmute = 18,
    ASVASTEventType_PreloadReady = 19,
    ASVASTEventType_ShowAttempt = 20
};

@interface ASVASTEvent : NSObject<NSCopying>

@property (nonatomic, assign) BOOL oneTime;
@property (nonatomic, assign) BOOL canBeSent;
@property (nonatomic, assign) ASVASTEventType type;
@property (nonatomic, strong) NSMutableArray *urls;

- (void)addURL:(NSURL*)url;
- (void)addURLs:(NSArray *)urls;

@end

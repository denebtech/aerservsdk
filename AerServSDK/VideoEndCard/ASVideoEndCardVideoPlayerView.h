//
//  ASVideoEndCardVideoPlayerView.h
//  AerServSDK
//
//  Created by Albert Zhu on 4/27/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ASVideoView;

@interface ASVideoEndCardVideoPlayerView : UIView

@property (nonatomic, strong) ASVideoView *videoView;

- (void)changeFrame:(CGRect)frame;

@end

//
//  ASXMLQueryEngine.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/26/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASXMLQueryEngine.h"
#import "ASXMLNode.h"

#import <libxml/tree.h>
#import <libxml/parser.h>
#import <libxml/HTMLparser.h>
#import <libxml/xpath.h>
#import <libxml/xpathInternals.h>

#import "NSData+ASUtils.h"

NSString* const ASXMLNameKey = @"ASXMLNameKey";
NSString* const ASXMLValueKey = @"ASXMLValueKey";
NSString* const ASXMLChildrenKey = @"ASXMLChildrenKey";
NSString* const ASXMLAttributesKey = @"ASXMLAttributesKey";

@interface ASXMLQueryEngine()

@property (nonatomic, unsafe_unretained) xmlDocPtr docPtr;

@end

@implementation ASXMLQueryEngine

- (instancetype)initWithXMLData:(NSData *)data {
    // we expect nice XML..
    if (self = [super init]) {
        _docPtr = xmlReadMemory([data bytes], (int)[data length], "", NULL, XML_PARSE_RECOVER);
        
        if (!_docPtr) {
            [ASSDKLogger logStatement:@"ASXMLQueryEngine, initWithXMLData: - Can't open XML document"];
            return nil;
        } else {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASXMLQueryEngine, initWithXMLData: - XML:\n%@", [data stringEncoded]]];
        }
    }

    return self;
}

- (void)dealloc {
    xmlFreeDoc(_docPtr);
    _docPtr = NULL;
}

- (NSDictionary *)createDictionaryFromNode:(xmlNodePtr)currentNode parentDictionary:(NSMutableDictionary *)parentDictionary {
    NSMutableDictionary*  resultingNodeAsDictionary = [NSMutableDictionary new];
    
    if (currentNode->name)
        resultingNodeAsDictionary[ASXMLNameKey] = [NSString stringWithCString:(const char*)currentNode->name encoding:NSUTF8StringEncoding];
    
    // if we have content and this is NOT a document node process the content.
    if (currentNode->content && currentNode->type != XML_DOCUMENT_TYPE_NODE) {
        NSString* currentNodeValue = [NSString stringWithCString:(const char*)currentNode->content encoding:NSUTF8StringEncoding];
        
        
        if (currentNodeValue == nil) {
            [ASSDKLogger logStatement:@"ASXMLQueryEngine, createDictionaryFromNode:parentDirectory: - Couldn't decode XML string as UTF8. Trying another encoding"];
            currentNodeValue = [NSString stringWithCString:(const char*)currentNode->content encoding:[NSString defaultCStringEncoding]];
        }
        
        if (currentNodeValue == nil) {
            [ASSDKLogger logStatement:@"ASXMLQueryEngine, createDictionaryFromNode:parentDirectory: - Failed to decode XML using default encoding. Skipping node."];
            return nil;
        }
        
        // if this is just text append it to the parents text if any existed. if no then set the parents value to this.
        if ([resultingNodeAsDictionary[ASXMLNameKey] isEqualToString:@"text"] && parentDictionary) {
            currentNodeValue = [currentNodeValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString* existingValue = parentDictionary[ASXMLValueKey];
            NSString* newValue = nil;
            
            if (existingValue != nil)
                newValue = [existingValue stringByAppendingString:currentNodeValue];
            else
                newValue = currentNodeValue;
        
            parentDictionary[ASXMLValueKey] = newValue;
            
            return nil;
        }
        
        resultingNodeAsDictionary[ASXMLValueKey] = currentNodeValue;
    }
    
    // deal with any attributes.
    xmlAttr* attribute = currentNode->properties;
    
    if (attribute != nil) {
        NSMutableDictionary* attributeDictionary = [NSMutableDictionary new];
        
        while (attribute) {
            //NSMutableArray* attributeArray = [NSMutableArray new];
            //NSMutableDictionary* foundAttribute = [NSMutableDictionary new];
            NSString* attributeName = [NSString stringWithCString:(const char *)attribute->name encoding:NSUTF8StringEncoding];
            NSMutableDictionary* foundObjects = [NSMutableDictionary new];
            
            if (attribute->children) {
                NSDictionary* foundValues = [self createDictionaryFromNode:attribute->children parentDictionary:foundObjects];
                
                attributeDictionary[attributeName] = foundObjects[ASXMLValueKey];
                
                [foundObjects addEntriesFromDictionary:foundValues];
            }
            
            
            
            attribute = attribute->next;
        }
        
        resultingNodeAsDictionary[ASXMLAttributesKey] = attributeDictionary;
    }
    xmlNodePtr childNode = currentNode->children;
    
    if (childNode != nil) {
        NSMutableArray* childrenNodes = [NSMutableArray new];
        
        while (childNode) {
            NSDictionary* childNodeAsDictionary = [self createDictionaryFromNode:childNode parentDictionary:resultingNodeAsDictionary];
            
            if (childNodeAsDictionary) {
                // if we only have one child coming back.. check to see if its the nodes value if so we will promote for ease of use.
                if ([[childNodeAsDictionary allKeys] count] == 1) {
                    
                    // if the child node has a value keep going..
                    if (childNodeAsDictionary[ASXMLValueKey] != nil) {
                        
                        // if we already have a value append this string to our value. (just incase we got a frament).
                        // other wise this is our value and we will promotethe object.
                        if (resultingNodeAsDictionary[ASXMLValueKey] != nil) {
                            resultingNodeAsDictionary[ASXMLValueKey] = [resultingNodeAsDictionary[ASXMLValueKey] stringByAppendingString:childNodeAsDictionary[ASXMLValueKey]];
                        }
                        else {
                            resultingNodeAsDictionary[ASXMLValueKey] = childNodeAsDictionary[ASXMLValueKey];
                        }
                        
                        // since we grabbed the value and moved it to us get the next child and go throuh our look.
                        childNode = childNode->next;
                        continue;
                    }
                }
                
                [childrenNodes addObject:childNodeAsDictionary];
            }
            
            childNode = childNode->next;
        }
    
        if ([childrenNodes count] > 0)
            resultingNodeAsDictionary[ASXMLChildrenKey] = childrenNodes;
    }
    
    return resultingNodeAsDictionary;
}


- (NSArray *)queryWithXPath:(NSString*)xPathQuery {
//    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASXMLQueryEngine, queryWithXPath: - xPathQuery: %@", xPathQuery]];
    
    // searches are from the top of the document. This becomes an issue we will fix that.. But for now...
    xmlXPathContextPtr xPathContext;
    xmlXPathObjectPtr xPathObject;
    
    NSMutableArray* foundNodes = nil;
    
    // create our xPath context from our document.
    xPathContext = xmlXPathNewContext(self.docPtr);
    
    if (xPathContext != nil) {
    
        // query and get the results.
        xPathObject = xmlXPathEvalExpression((xmlChar*)[xPathQuery cStringUsingEncoding:NSUTF8StringEncoding], xPathContext);
    
        if (xPathObject != nil) {
            // get the node set out.. We will build our dictionary of nodes and chilren out of this
            xmlNodeSetPtr nodes = xPathObject->nodesetval;
            
            if (nodes != nil) {
                foundNodes = [NSMutableArray new];
                
                // lets enumerate each node..
                for (NSInteger nodeIndex = 0; nodeIndex < nodes->nodeNr; nodeIndex++) {
                    NSDictionary* nodeDictionary = [self createDictionaryFromNode:nodes->nodeTab[nodeIndex] parentDictionary:nil];
                    
                    if (nodeDictionary != nil) {
                        ASXMLNode* node = [[ASXMLNode alloc] initWithDictionary:nodeDictionary];
                        
                        [foundNodes addObject:node];
                    }
                }
            }
            
            xmlXPathFreeObject(xPathObject);
        }
        
        xmlXPathFreeContext(xPathContext);
    }
    
    return foundNodes;
}

@end

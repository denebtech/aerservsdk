//
//  ASStaticLoggerFields.h
//  AerServSDK
//
//  Created by Vasyl Savka on 10/30/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASStaticLoggerFields : NSObject

// stored on singleton creation
@property (readonly, nonatomic, copy) NSString *os;
@property (readonly, nonatomic, copy) NSString *osv;
@property (readonly, nonatomic, copy) NSString *adid;
@property (readonly, nonatomic, copy) NSString *oid;
@property (readonly, nonatomic, copy) NSString *bundleid;

// from ASMutableURLRequest on first request creation
@property (readonly, nonatomic, copy) NSString *sdkv;
@property (readonly, nonatomic, copy) NSString *ua;

+ (ASStaticLoggerFields *)fields;
+ (void)setSDKVersion:(NSString *)ver;
+ (void)setUserAgent:(NSString *)agent;
+ (BOOL)areStaticFieldsDefined;

@end

//
//  ASMillennialBannerAdProvider.h
//  AerServSDK
//
//  Created by Albert Zhu on 5/13/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <AerServSDK/AerServSDK.h>

#define kMMSDKClassStr @"MMSDK"
#define kMMInlineAdClassStr @"MMInlineAd"

#define kMMPlacementIdKey @"placementId"
#define kMMAdSizeKey @"adSize"

@interface ASMillennialBannerAdProvider : ASCustomBannerAdProvider

//- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation withSize:(CGSize)currSize;

@end

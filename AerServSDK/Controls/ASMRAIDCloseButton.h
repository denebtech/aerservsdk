//
//  ASMRAIDCloseButton.h
//  AerServSDK
//
//  Created by Vasyl Savka on 9/9/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASMRAIDCloseButton : UIButton

@end

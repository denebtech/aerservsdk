//
//  ASAdProvider.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/9/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdProvider.h"

const NSString *ASInterstitialParameter_Parameters = @"ASInterstitialParameter_Parameters";
const NSString *ASInterstitialParameter_HTMLHeaders = @"ASInterstitialParameter_HTMLHeaders";
const NSString *ASInterstitialParameter_HTMLData = @"ASInterstitialParameter_HTMLData";
const NSString *ASInterstitialParameter_Location = @"ASInterstitialParameter_Location";
const NSString *ASInterstitialParameter_OutlineView = @"ASInterstitialParameter_OutlineView";
const NSString *ASInterstitialParameter_Parameter_URL = @"url";

@implementation ASInterstitialAdProvider

- (void)requestInterstitialAdWithProperties:(NSDictionary *)info isPreload:(BOOL)preload {
    // The default implementation of this method does nothing. Subclasses must override this method and implement code to load an interstitial here.
}

- (BOOL)enableAutomaticImpressionAndClickTracking {
    // Subclasses may override this method to return NO to perform impression and click tracking manually.
    return YES;
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    // The default implementation of this method does nothing. Subclasses must override this method and implement code to display an interstitial here.
}

- (void)cancel {
    // Subclasses may override this method to perform additional tasks during cancel.
}


@end

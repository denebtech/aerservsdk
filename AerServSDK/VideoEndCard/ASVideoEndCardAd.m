//
//  ASVideoEndCardAd.m
//  AerServSDK
//
//  Created by Albert Zhu on 4/26/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASVideoEndCardAd.h"

#import "ASVASTEvent.h"
//#import "ASMoatKitUtil.h"

#import "NSURL+ASUtils.h"

@interface ASVideoEndCardAd ()

@property (nonatomic, strong) NSDictionary *dataObj;

@property (nonatomic, strong) NSURL *mediaFileUrl;
@property (nonatomic, strong) NSURL *redirectUrl;
@property (nonatomic, strong) ASVASTEvent *videoStart;
@property (nonatomic, strong) ASVASTEvent *videoComplete;
@property (nonatomic, strong) ASVASTEvent *videoClick;
@property (nonatomic, copy) NSString *closeOffset;
@property (nonatomic, assign) BOOL isPreload;

@end

@implementation ASVideoEndCardAd

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASVideoEndCardAd, dealloc"];
    _mediaFileUrl = nil;
    _redirectUrl = nil;
    _impression = nil;
    _videoStart = nil;
    _videoComplete = nil;
    _videoClick = nil;
    _closeOffset = nil;
}

- (instancetype)initWithJSONData:(NSData *)jsonData withDelegate:(id<ASVideoEndCardAdDelegate>)delegate asPreload:(BOOL)preload {
    [ASSDKLogger logStatement:@"ASVideoEndCardAd, initWithJSONData:withDelegate:asPreload:"];
    if(self = [super init]) {
        NSError *err;
        _dataObj = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&err];
        if(err != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVideoEndCard, initWithJSONData - JSON SERIALIZATION ERROR: %@", err.localizedDescription]];
            return nil;
        } else {
            _isPreload = preload;
            _delegate = delegate;
            __weak ASVideoEndCardAd *vecAd = self;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [vecAd processData];
            });
        }
    }
    
    return self;
}

+ (instancetype)adFromJSONData:(NSData *)jsonData withDelegate:(id<ASVideoEndCardAdDelegate>)delegate asPreload:(BOOL)preload {
    [ASSDKLogger logStatement:@"ASVideoEndCardAd, adFromJSONData:withDelegate:asPreload:"];
    return [[ASVideoEndCardAd alloc] initWithJSONData:jsonData withDelegate:delegate asPreload:preload];
}

- (ASVASTEvent *)eventSetupWithStringURLs:(NSArray *)strUrls ofType:(ASVASTEventType)eventType onlyOnce:(BOOL)once {
    [ASSDKLogger logStatement:@"ASVideoEndCardAd, eventSetupWithStringURLs:ofType:"];
    ASVASTEvent *event = [ASVASTEvent new];
    event.type = eventType;
    event.canBeSent = YES;
    event.oneTime = once;
    
    NSMutableArray *urls = [NSMutableArray new];
    for(NSString *strUrl in strUrls)
        [urls addObject:[NSURL URLWithString:strUrl]];
    [event addURLs:urls];
    
    return event;
}

#pragma mark - Setup Helpers

- (void)processData {
    [ASSDKLogger logStatement:@"ASVideoEndCardAd, processData"];
    
    // create media file url
    if(self.dataObj[kASVideoEndCardMediaFileURLKey] != nil) {
        self.mediaFileUrl = [NSURL URLWithAddress:self.dataObj[kASVideoEndCardMediaFileURLKey]];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVideoEndCardAd, processData - mediaFileUrl: %@", self.mediaFileUrl.absoluteString]];
    }
    
    // create redirect url
    if(self.dataObj[kASVideoEndCardRedirectURLKey] != nil) {
        self.redirectUrl = [NSURL URLWithAddress:self.dataObj[kASVideoEndCardRedirectURLKey]];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVideoEndCardAd, processData - redirectUrl: %@", self.redirectUrl.absoluteString]];
    }
    
    // create tracking events
    if(self.dataObj[kASVideoEndCardTrackingEventsKey] != nil) {
        NSArray *trackingEventsArr = self.dataObj[kASVideoEndCardTrackingEventsKey];
        
        for(NSDictionary *trackingEvent in trackingEventsArr) {
            NSString *eventType = trackingEvent[kASVideoEndCardTrackingTypeKey];
            NSArray *eventUrls = trackingEvent[kASVideoEndCardTrackingURLsKey];
            
            if([eventType isEqualToString:kASVideoEndCardTrackingVideoStartKey]) {
                self.videoStart = [self eventSetupWithStringURLs:eventUrls ofType:ASVASTEventType_VideoStart onlyOnce:YES];
                
//                // setup moat tracker with aerserv video start url
//                for(NSURL *url in self.videoStart.urls) {
//                    NSString *urlStr = url.absoluteString;
//                    if([urlStr containsStr:kEventsAerServDomain] && [urlStr containsStr:kVideoStartEventStr])
//                        [ASMoatKitUtil initMoatVideoTrackerWithTrackingEventURL:urlStr];
//                }
            } else if([eventType isEqualToString:kASVideoEndCardTrackingVideoCompleteKey]) {
                self.videoComplete = [self eventSetupWithStringURLs:eventUrls ofType:ASVASTEventType_VideoComplete onlyOnce:YES];
            } else if([eventType isEqualToString:kASVideoEndCardTrackingVideoClickKey]) {
                self.videoClick = [self eventSetupWithStringURLs:eventUrls ofType:ASVASTEventType_VideoClickTracking onlyOnce:NO];
            }
        }
    }
    
    // signal completion of video end card processing
    [self.delegate videoEndCardDidLoad];
}

- (void)processAdditionalParameters:(NSDictionary *)parameters {
    [ASSDKLogger logStatement:@"ASVideoEndCardAd, processAdditionalParameters:"];
    if(parameters[kCloseOffsetKey] != nil) {
        self.closeOffset = parameters[kCloseOffsetKey];
    }
}

#pragma mark - Getters

- (NSData *)getEndCardHTMLData {
    [ASSDKLogger logStatement:@"ASVideoEndCardAd, getEndCardHTMLData"];
    NSData *htmlData = nil;

    if(self.dataObj[kASVideoEndCardHTMLKey]) {
        NSString *htmlStr = self.dataObj[kASVideoEndCardHTMLKey];
        htmlData = [htmlStr dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    return htmlData;
}

@end

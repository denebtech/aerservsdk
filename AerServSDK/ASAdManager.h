//
//  ASAdManager.h
//  AerServSDK
//
//  Created by Vasyl Savka on 6/20/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kXAerServHandlersHeaderKey @"X-AerServ-Handlers"
#define kXAerServHeaderKey @"X-AerServ"
#define kCloseOffsetKey @"closeOffset"

@interface ASAdManager : NSObject

@property (nonatomic, assign) BOOL isPreload;
@property (nonatomic, assign) NSTimeInterval timeoutInt;

@end

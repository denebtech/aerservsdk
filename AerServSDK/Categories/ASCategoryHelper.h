//
//  ASCategoryHelper.h
//  AerServSDK
//
//  Created by Vasyl Savka on 6/8/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASCategoryHelper : NSObject

@property (nonatomic, copy) NSString *carrierName;
@property (nonatomic, assign) BOOL videoPauseState;

+ (ASCategoryHelper *)instance;

@end

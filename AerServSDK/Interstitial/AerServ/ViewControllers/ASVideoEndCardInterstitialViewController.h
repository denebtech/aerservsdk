//
//  ASVideoEndCardInterstitialViewController.h
//  AerServSDK
//
//  Created by Albert Zhu on 4/28/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdViewController.h"

#define kShowUIFrames 0

typedef NS_ENUM(NSInteger, ASVideoEndCardPhase) {
    kVideoEndCardInitialPhase = 0,
    kVideoEndCardVideoAdPhase,
    kVideoEndCardFullscreenEndCardPhase,
    kVideoEndCardCompletedPhase
};

@class ASVideoEndCardAd;

@interface ASVideoEndCardInterstitialViewController : ASInterstitialAdViewController

+ (instancetype)viewControllerWithVideoEndCardAd:(ASVideoEndCardAd *)vecAd andDelegate:(id<ASInterstitialAdViewControllerDelegate>)delegate;

@end

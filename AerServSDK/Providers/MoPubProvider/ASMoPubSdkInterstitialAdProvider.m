//
//  ASMoPubSdkInterstitialAdProvider.m
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASMoPubSdkInterstitialAdProvider.h"
#import "MPInterstitialAdController.h"
#import "ASCustomInterstitialAdProvider.h"

@interface ASMoPubSdkInterstitialAdProvider()<MPInterstitialAdControllerDelegate>

@property (nonatomic, strong) id mpInterstitialAdController;
@property (nonatomic, assign) BOOL failedToReceiveAd;

@end


@implementation ASMoPubSdkInterstitialAdProvider

- (instancetype)init {
    if (self = [super initWithAdClassName:@"MPInterstitialAdController" timeout:kCustomInterstitialTimeout]) {
        // Add init code here
    }
    return self;
}

- (void)initializePartnerAd:(NSDictionary *)properties {
    // Do nothing.  Nothing to be initialized.
}

- (BOOL)hasPartnerAdInitialized {
    // There is nothing to be initialized, so always return YES
    return YES;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    // There is nothing to be initialized, so always return NO
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    NSString *adUnitId = [self valueInProperty:properties forKey:@"MoPubAdUnit"];
    Class mpInterstitialAdControllerClass = NSClassFromString(@"MPInterstitialAdController");
    self.mpInterstitialAdController = [mpInterstitialAdControllerClass interstitialAdControllerForAdUnitId:adUnitId];
    [self.mpInterstitialAdController loadAd];
}

- (BOOL)hasPartnerAdLoaded {
    // Use the "ready" method in MPInterstitialAdController to determine if ad is loaded successfully
    BOOL isReady = NO;
    if([self.mpInterstitialAdController ready]) {
        isReady = YES;
        [ASAdResponseLogger endAdResponseAndSendEvent];
    }
    return isReady;
}

- (BOOL)hasPartnerAdFailedToLoad {
    // Use self.failedToReceiveAd flag to determine if ad has failed to load.  This flag is set by "ad failed" event from MoPub.
    return self.failedToReceiveAd;
}

- (void)cancel {
}

- (void)showPartnerAd:(UIViewController *)rootViewController {
    // Perform one last check, and show the ad
    if ([self.mpInterstitialAdController ready]) {
        [self.mpInterstitialAdController showFromViewController:rootViewController];
    }
}

#pragma mark - MPInterstitialAdControllerDelegate Methods

- (void)interstitialDidAppear:(MPInterstitialAdController *)interstitial {
    [ASSDKLogger logStatement:@"ASMoPubSdkInterstitialAdProvider, interstitialDidAppear"];
    [self asPartnerInterstitialAdDidAppear];
}

- (void)interstitialDidDisappear:(MPInterstitialAdController *)interstitialDidDisappear {
    [ASSDKLogger logStatement:@"ASMoPubSdkInterstitialAdProvider, interstitialDidAppear"];
    [self asPartnerInterstitialAdDidDisappear];
}

- (void)interstitialDidFailToLoadAd:(MPInterstitialAdController *)interstitial {
    // No need to let AerServ SDK know that ad has failed to load.  This is communicated in hasPartnerAdFailedToLoad.
    [ASSDKLogger logStatement:@"ASMoPubSdkInterstitialAdProvider, interstitialDidFailToLoadAd"];
    self.failedToReceiveAd = YES;
}

- (void)interstitialDidLoadAd:(MPInterstitialAdController *)interstitial {
    // No need to let AerServ SDK know that ad has loaded.  This is communicated in hasPartnerAdLoaded.
    [ASSDKLogger logStatement:@"ASMoPubSdkInterstitialAdProvider, interstitialDidLoadAd"];
}

- (void)interstitialDidReceiveTapEvent:(MPInterstitialAdController *)interstitial {
    [ASSDKLogger logStatement:@"ASMoPubSdkInterstitialAdProvider, interstitialDidReceiveTapEvent"];
    [self asPartnerInterstitialAdWasTouched];
}

- (void)interstitialWillAppear:(MPInterstitialAdController *)interstitial {
    [ASSDKLogger logStatement:@"ASMoPubSdkInterstitialAdProvider, interstitialWillAppear"];
    [self asPartnerInterstitialAdWillAppear];
    
}

- (void)interstitialWillDisappear:(MPInterstitialAdController *)interstitial {
    [ASSDKLogger logStatement:@"ASMoPubSdkInterstitialAdProvider, interstitialWillDisappear"];
    [self asPartnerInterstitialAdWillDisappear];
}

@end

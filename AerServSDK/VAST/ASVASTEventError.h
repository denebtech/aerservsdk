//
//  ASVASTEventError.h
//  AerServSDK
//
//  Created by Vasyl Savka on 12/17/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASVASTEvent.h"

#define kVASTEventShowDebug 0
#define KVASTEventErrorCodeMacro @"[ERRORCODE]"

typedef NS_ENUM(NSInteger, ASVASTErrorCode) {
    kVASTErrorCodeNoERROR = 00,
    kVASTErrorCodeXMLParsingError = 100,
    kVASTErrorCodeSchemaValidationError = 101,
    kVASTErrorCodeVersionNotSupported = 102, // MOT USED YET
    kVASTErrorCodeTraffickingError = 200, // NOT USED YET
    kVASTErrorCodeUnexpectedLinearity = 201, // NOT USED YET
    kVASTErrorCodeUnexpectedDuration = 202, // NOT USED YET
    kVASTErrorCodeUnexpectedSize = 203, // NOT USED YET
    kVASTErrorCodeGeneralWrapperError = 300,
    kVASTErrorCodeWrapperURITimeoutError = 301, // NOT USED YET
    kVASTErrorCodeWrapperLimitReached = 302, // NO WRAPPER LIMIT
    kVASTErrorCodeNoInlineVASTAfterWrappers = 303,
    kVASTErrorCodeGeneralLinearError = 400,
    kVASTErrorCodeFileNotFoundError = 401,
    kVASTErrorCodeMediaFileTimeout = 402, // NOT USED YET
    kVASTErrorCodeNoSupportedMediaFile = 403,
    kVASTErrorCodeProblemDisplayingMediaFile = 405,
//    kVASTErrorCodeGeneralNonLinearAdsError = 500,
//    kVASTErrorCodeNonLinearAdIncorrectDimensions = 501,
//    kVASTErrorCodeNonLinearAdNotFetchable = 502,
//    kVASTErrorCodeNonLinearAdTypeNotSupported = 503,
//    kVASTErrorCodeCompanionAdGeneralError = 600,
//    kVASTErrorCodeCompanionAdIncorrectDimensions = 601,
//    kVASTErrorCodeUnableToDisplayRequiredCompanionAd = 602,
//    kVASTErrorCodeCompanionAdNotFetchable = 603,
//    kVASTErrorCodeSupportedCompanionAdTypeNotFound = 604,
    kVASTErrorCodeUndefinedError = 900,
    kVASTErrorCodeGeneralVPAIDError = 901
};

@interface ASVASTEventError : ASVASTEvent

- (ASVASTEventError *)replaceErrorMacroWithCode:(ASVASTErrorCode)errCode;

@end

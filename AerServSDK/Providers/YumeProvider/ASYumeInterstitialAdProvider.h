//
//  YumeProvider.h
//  YumeProvider
//
//  Created by Gauthier on 7/28/14.
//  Copyright (c) 2014 AerServ. All rights reserved.
//

#import "ASInterstitialAdProvider.h"
#import <Foundation/Foundation.h>
@interface ASYumeInterstitialAdProvider : ASInterstitialAdProvider

-(void) interstitialAdViewControllerViewReallyDidDisappear;

@end

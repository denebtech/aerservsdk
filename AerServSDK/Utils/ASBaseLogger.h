//
//  ASBaseLogger.h
//  AerServSDK
//
//  Created by Vasyl Savka on 10/21/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASStaticLoggerFields.h"
#import "NSString+ASUtils.h"

#define kASLoggerConnTimeout 5.0f

#define kAnalyticsEventLoggingEndPoint @"https://debug.aerserv.com/sybok/analytics"

#define kBaseLoggerDebug 0
#define kBaseLoggerShowPOSTData 0

#define kCategoryKey @"category"
#define kActionKey @"action"
#define kTimestampKey @"timestamp"
#define kPLCKey @"plc"
#define kSDKVersionKey @"sdkVersion"
#define kOSVersionKey @"os"
#define kUserAgentKey @"userAgent"

#define kEventsKey @"events"

@interface ASBaseLogger : NSObject

@property (nonatomic, strong) dispatch_queue_t analyticsLogQ;

@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *action;
@property (nonatomic, copy) NSString *plc;

+ (ASBaseLogger *)logInstance;

+ (long long)epochTimeInMS;
+ (NSNumber *)epochTimeAsNSNumberObj;

+ (void)resetFields;
+ (void)setPlacement:(NSString *)plc;
+ (void)setCategory:(NSString *)cat;
+ (void)setAction:(NSString *)action;
//+ (void)attemptToDefineStaticFields;

+ (NSData *)createJSONData;
+ (NSData *)createSendDataWithEventObj:(NSDictionary *)eventObj;
+ (void)attemptToSendAnalyticsEventsWithData:(NSData *)jsonEventData;

@end

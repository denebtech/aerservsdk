//
//  ASMRAIDView.m
//  AerServSDK
//
//  Created by Albert Zhu on 8/26/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASMRAIDView.h"
#import "ASMRAIDParser.h"
#import "ASMRAIDJS.h"
#import "ASMRAIDCloseRegion.h"
#import "ASMRAIDSupportsUtil.h"
#import "ASMRAIDViewController.h"
#import "ASVASTEvent.h"
#import "ASAdView.h"
#import "ASWebView.h"

#import "ASConstants.h"
#import "ASHTMLUtils.h"
#import "ASCommManager.h"

#import "NSString+ASUtils.h"
#import "NSData+ASUtils.h"

@interface ASMRAIDView() <UIWebViewDelegate>

@property (nonatomic, assign) ASMRAIDState mState;
@property (nonatomic, assign) ASMRAIDPlacementType mPlcType;

@property (nonatomic, strong) ASMRAIDCloseRegion *closeRegion;

@property (nonatomic, strong) ASWebView *webView;

@property (nonatomic, copy) NSString *mimeTypeStr;
@property (nonatomic, copy) NSString *encodingStr;
@property (nonatomic, strong) NSData *htmlData;

@property (nonatomic, assign) BOOL didLeaveAppCauseOfMRAIDBridge;

@property (nonatomic, assign) BOOL allowOrienationChange;
@property (nonatomic, assign) CGRect currPos;
@property (nonatomic, assign) CGSize screenSize;
@property (nonatomic, assign) CGRect defaultBannerPos;
@property (nonatomic, assign) BOOL useCustomClose;

@property (nonatomic, copy) NSString *origDefaultOrientationStr;
@property (nonatomic, assign) BOOL isCloseRegionHiddenBeforeVideoPlay;

@property (nonatomic, assign) BOOL mraidFoundFromURL;
@property (nonatomic, assign) BOOL initialFromURLLoad;

@property (nonatomic, assign) BOOL didForceOrientaiton;
@property (nonatomic, assign) UIInterfaceOrientationMask preferredOrientation;

@end

@implementation ASMRAIDView

- (void)dealloc {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, dealloc - id: %@", self.restorationIdentifier]];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    
    self.rootVC = nil;
    self.supportUtil = nil;
    
    [self.webView removeFromSuperview];
    self.webView = nil;
    
    self.mimeTypeStr = nil;
    self.encodingStr = nil;
    self.htmlData = nil;
    
    [self.closeRegion removeFromSuperview];
    self.closeRegion = nil;
    
    self.origDefaultOrientationStr = nil;
    
    self.parentOfExpandedMRAIDView = nil;
    self.mraidVC = nil;
}

- (instancetype)initWithFrame:(CGRect)frame withData:(NSData *)data headers:(NSDictionary *)headers forRootVC:(UIViewController *)rootVC asPlcType:(ASMRAIDPlacementType)plcType delegate:(id<ASMRAIDDelegate>)delegate {
    if(self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height - (kIS_iOS_6 ? 2*kStatusBarHeight : 0.0f))]) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, initWithFrame:withData:headers:forRootVC:asPlcType:delegate: - frame: %@, data:\n\n%@\n\n", NSStringFromCGRect(frame), [data stringEncoded]]];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, initWithFrame:withData:headers:forRootVC:asPlcType:delegate: - headers:\n\n%@\n\n", headers]];

        // set state to loading for setup phase, additionally hide mraid view until ready to be shown
        _mState = kASMRAIDStateLoading;
        _delegate = delegate;
        _rootVC = rootVC;
        _preferredOrientation = kIS_IPHONE ? UIInterfaceOrientationMaskAllButUpsideDown : UIInterfaceOrientationMaskAll;
        self.backgroundColor = [UIColor blackColor];
        self.hidden = YES;
        self.restorationIdentifier = kASMRAIDPrimary;
        
        // initilize webview and add it to the current view
        _webView = [[ASWebView alloc] initWithFrame:self.frame];
        _initialFromURLLoad = NO;
        [self addSubview:_webView];
        [self initWebView];
        
        // setup close region w/ tap gesture recognizer and close button
        [self addCloseButton];
        
        // translate the html data into a string that's parsable, parese it to setup for mraid load, and then change html string back to html data
        NSDictionary *mraidInfo = [ASMRAIDParser stringForProcessedRawHTMLData:data];
        NSString *htmlStr = mraidInfo[kMRAIDHTMLKey];
        _vastImpressionURL = mraidInfo[kMRAIDVastImpressionURLKey];
        _htmlData = [htmlStr dataUsingEncoding:NSUTF8StringEncoding];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, initWithFrame:withData:headers:forRootVC:asPlcType:delegate: - data.length: %lu", (unsigned long)data.length]];
        
        // fire bpf advertiser event if a vast impression url could be made
        if(_vastImpressionURL != nil) {
            [_delegate mraidView:self didFireAdvertiserEventWithMessage:@"MRAID View Fires Banner Pixel Event"];
        }
        
        __weak ASMRAIDView *mraidView = self;
        // serach for forceOrietation
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            @try {
                NSString *foundOrientaiton = [ASMRAIDParser findForceOrientationString:htmlStr];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [mraidView forceOrientationWith:foundOrientaiton];
                });
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        });
        
        // get the type and encoding stripping away any leading/trailing spaces.
        NSString *contentType = headers[@"Content-Type"];
        NSArray *contentTypes = [contentType componentsSeparatedByString:@";"];
        _mimeTypeStr = (contentTypes[0] != nil) ? [contentTypes[0] stringByReplacingOccurrencesOfString:@" " withString:@""] : nil;
        _encodingStr = (contentTypes[1] != nil) ? [contentTypes[1] stringByReplacingOccurrencesOfString:@" " withString:@""] : nil;
        
        // load mraid ad with processed html, empty base url since we're loading from parsed static html
        [_webView loadData:_htmlData MIMEType:_mimeTypeStr textEncodingName:_encodingStr baseURL:[NSURL URLWithString:@""]];
        
        // set default placement type
        [self setPlacementType:plcType];
        
        // register notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidGotoBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
        
        // init the support feature class
        _supportUtil = [[ASMRAIDSupportsUtil alloc] initWithMRAID:self];
        
        // initially allow orientation change and not using custom close
        _allowOrienationChange = YES;
        _useCustomClose = NO;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame withURL:(NSURL *)url forRootVC:(UIViewController *)rootVC asPlcType:(ASMRAIDPlacementType)plcType andDelegate:(id<ASMRAIDDelegate>)delegate {
    
    if(self = [super initWithFrame:frame]) {
        // set state to loading for setup phase, additionally hide mraid view until ready to be shown
        _mState = kASMRAIDStateLoading;
        _delegate = delegate;
        _rootVC = rootVC;
        self.backgroundColor = [UIColor blackColor];
        self.hidden = YES;
        self.restorationIdentifier = kASMRAIDFromURL;
        
        // initilize webview and add it to the current view
        _webView = [[ASWebView alloc] initWithFrame:self.frame];
        _initialFromURLLoad = YES;
        [self addSubview:_webView];
        [self initWebView];
        
        // setup close region w/ tap gesture recognizer and close button
        [self addCloseButton];
        
        // store the placement type
        _mPlcType = plcType;
        
        // start webview load
        [_webView loadRequest:[NSURLRequest requestWithURL:url]];

        // register notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidGotoBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
    
    return self;
}

#pragma mark - Additional Loading Methods

- (void)initWebView {
    // generic setup for input webview
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.webView.delegate = self;
    self.webView.allowsInlineMediaPlayback = YES;
    self.webView.mediaPlaybackAllowsAirPlay = NO;
    self.webView.scrollView.bounces = NO;
    self.webView.scrollView.scrollEnabled = NO;
    self.webView.backgroundColor = [UIColor blackColor];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, initWebView: %@", NSStringFromCGRect(self.webView.frame)]];
    
    // load mraid.js
    [self loadMRAIDjsIntoWebView];
}

- (void)addCloseButton {
    [ASSDKLogger logStatement:@"ASMRAIDView, addCloseButton"];
    
    if(!self.closeRegion) {
        
        // checking whether the parent view is a banner or an interstitial ad container
        UIView *parView = [self.delegate respondsToSelector:@selector(adViewForMRAIDView:)] ? [self.delegate adViewForMRAIDView:self] : self.rootVC.view;
        
        // initializing the close region class object
        self.closeRegion = [[ASMRAIDCloseRegion alloc] initWithTarget:self andSelector:@selector(onClose) inView:parView];
    }
}

- (void)loadMRAIDjsIntoWebView {
    [ASSDKLogger logStatement:@"ASMRAIDView, loadMRAIDjsIntoWebView:"];
    
    // translating the byte array stored in ASMRAIDJS.h into a js string
    NSData *mraidjsData = [NSData dataWithBytesNoCopy:mraidJS length:mraidJSLen freeWhenDone:NO];
    NSString *mraidjsStr = [mraidjsData stringEncoded];
    mraidjsData = nil;
    
    #if kShowMRAIDJS
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, loadMRAIDjsIntoWebView: - id: %@, mraidjsStr:\n%@", self.restorationIdentifier, mraidjsStr]];
    #endif
    
    // injecting mraid javascript into the input webview
    [self.webView stringByEvaluatingJavaScriptFromString:mraidjsStr];
    mraidjsStr = nil;
}

#pragma mark - View Rotation Methods

- (void)mraidViewWillAttemptOrientationChange {
    @try {
        if((self.mState == kASMRAIDStateLoading && ![self.restorationIdentifier isEqualToString:kASMRAIDFromURL]) || self.mState == kASMRAIDStateHidden) {
            [ASSDKLogger logStatement:@"ASMRAID, mraidViewWillAttemptOrientationChange - MRAID is still loading or hidden, return"];
            return;
        }
        
        // check if orienation change is allowed or not
        NSDictionary *orientationProps = [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesOrientation];
        self.allowOrienationChange = !orientationProps || (self.mPlcType != kASMRAIDPlacementInterstitial && self.mState != kASMRAIDStateExpanded) ? YES : [orientationProps[kAllowOrientationChangeKey] boolValue];
        
        // grab the resize properties
        NSDictionary *resizeProps = [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesResize];
        BOOL allowOffscreen = [resizeProps[kAllowOffscreenKey] boolValue];
        
        // reset screen and max sizes according to the new orientation change
        [self setScreenAndMaxSize];
        
        if(self.allowOrienationChange || self.didForceOrientaiton) {
            // get the parent view's frame
            CGRect parRect = CGRectMake(0.0f, 0.0f, self.screenSize.width, self.screenSize.height);
            
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, mraidViewWillAttemptOrientationChange - self.frame: %@ - parRect: %@", NSStringFromCGRect(self.frame), NSStringFromCGRect(parRect)]];
            
            CGFloat xPos, yPos, width, height;
            if(self.mPlcType == kASMRAIDPlacementInterstitial) {
                
                // reset the default position to the parent view's frame
                [self setDefaultPositionWithX:parRect.origin.x Y:parRect.origin.y W:parRect.size.width H:parRect.size.height];
                
                if(self.mState == kASMRAIDStateDefault || self.mState == kASMRAIDStateExpanded) {
                    
                    // get the current position of the parent view
                    xPos = parRect.origin.x;
                    yPos = parRect.origin.y;
                    width = parRect.size.width;
                    height = parRect.size.height;
                    height -= (kIS_iOS_6 ? 2*kStatusBarHeight : 0.0f);
                    
                } else if(self.mState == kASMRAIDStateResized) {
                    
                    // get the resize properties
                    xPos = [resizeProps[kOffsetXKey] floatValue];
                    yPos = [resizeProps[kOffsetYKey] floatValue];
                    width = [resizeProps[kWidthKey] floatValue];
                    height = [resizeProps[kHeightKey] floatValue];
                    
                    // if the ad is not allowed offscreen then adjust accordingly
                    if(!allowOffscreen) {
                        
                        // adjust width & height to be limited
                        if(width > parRect.size.width) width = parRect.size.width;
                        else if(width < kASMRAIDCloseRegionWH) width = kASMRAIDCloseRegionWH;
                        if(height > parRect.size.height) height = parRect.size.height;
                        else if(height < kASMRAIDCloseRegionWH) height = kASMRAIDCloseRegionWH;
                        
                        // center ad
                        /*if(xPos < 0 || xPos + width > parRect.size.width)*/ xPos = (parRect.size.width - width)/2;
                        if(yPos < 0 || yPos + height > parRect.size.height) yPos = (parRect.size.height - height)/2;
                    }
                    
                    // resized mraid ads will always use custom close
                    [self setCustomCloseWithBOOL:YES];
                    
                }
                
                // reposition the mraid view
                [self.delegate repositionMRAIDView:self toNewFrame:CGRectMake(xPos, yPos, width, height)];
                
            } else if(self.mPlcType == kASMRAIDPlacementInline) {
                
                // grab the frame of the parent banner view
                CGRect adViewRect = [self.delegate adViewForMRAIDView:self].frame;
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, mraidWillAttemptOrientationChange -  ENTER INLINE, parRect: %@, adViewRect: %@, self.frame: %@", NSStringFromCGRect(parRect), NSStringFromCGRect(adViewRect), NSStringFromCGRect(self.frame)]];
                
                if(self.mState == kASMRAIDStateDefault) {
                    
                    // fit mraid banner onto screen according to the parent banner size
                    width = (adViewRect.size.width > self.frame.size.width) ? self.frame.size.width : adViewRect.size.width;
                    if(kIS_iOS_6_OR_LATER && !kIS_iOS_9_OR_LATER) width = parRect.size.width; // hack to fix iOS8 issue
                    height = (adViewRect.size.height > self.frame.size.height) ? self.frame.size.height : adViewRect.size.height;
                    
                    // center the mraid banner
                    xPos = (adViewRect.size.width - width)/2;
                    yPos = (adViewRect.size.height - height)/2;
                    
                    // reposition parent and mraid view
                    if([self.delegate respondsToSelector:@selector(repositionParentViewOfMRAID:toNewFrame:)]) {
                        [self.delegate repositionParentViewOfMRAID:self toNewFrame:adViewRect];
                    }
                    [self.delegate repositionMRAIDView:self toNewFrame:CGRectMake(xPos, yPos, width, height)];
                    
                    // reset default position
                    [self setDefaultPositionWithX:xPos Y:yPos W:width H:height];
                    
                } else if(self.mState == kASMRAIDStateExpanded) {
                    
                    // pull expanded size
                    NSDictionary *expandProp = [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesExpand];
                    CGFloat expandW = [expandProp[kWidthKey] floatValue];
                    CGFloat expandH = [expandProp[kHeightKey] floatValue];
                    
                    // adjust width & height according to current position and screen size
                    width = (expandW <= 0 || self.currPos.size.width > self.screenSize.width) ? self.screenSize.width : self.currPos.size.width;
                    if(expandW < self.screenSize.width && expandW > self.currPos.size.width) width = expandW;
                    height = (expandH <= 0 || self.currPos.size.height > self.screenSize.height) ? self.screenSize.height : self.currPos.size.height;
                    if(expandH < self.screenSize.height && expandH > self.currPos.size.height)  height = expandH;
                    
                    // if the width and height are still 0's, expand to fullscreen
                    if(expandW <= 0.0f) expandW = self.screenSize.width;
                    if(expandH <= 0.0f) expandH = self.screenSize.height;
                    
                    // center ad
                    xPos = (self.screenSize.width - width)/2;
                    yPos = (self.screenSize.height - height)/2;
                    
                    // reposition parent and mraid views
                    if([self.delegate respondsToSelector:@selector(repositionParentViewOfMRAID:toNewFrame:)]) {
                        [self.delegate repositionParentViewOfMRAID:self toNewFrame:CGRectMake(0.0f, 0.0f, self.screenSize.width, self.screenSize.height)];
                    }
                    [self.delegate repositionMRAIDView:self toNewFrame:CGRectMake(xPos, yPos, width, height)];
                    
                    // check if there's an expand from url, if so adjust the view accordingly
                    if(self.parentOfExpandedMRAIDView != nil) {
                        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, mraidViewWillAttemptOrientationChange, parentOfExpandedMRAIDView != nil - self.frame: %@, self.webView.frame: %@", NSStringFromCGRect(self.frame), NSStringFromCGRect(self.webView.frame)]];
                        width = (self.frame.size.width > self.screenSize.width || self.frame.size.width <= 0.0f) ? self.screenSize.width : self.frame.size.width;
                        height = (self.frame.size.height > self.screenSize.height || self.frame.size.height <= 0.0f) ? self.screenSize.height : self.frame.size.height;
                        xPos = (self.screenSize.width - self.frame.size.width)/2;
                        yPos = (self.screenSize.height - self.frame.size.height)/2;
                        
                        __weak ASMRAIDView *mraidView = self;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            @try {
                                [UIView animateWithDuration:kMRAIDAnimationDuration animations:^{
                                    mraidView.frame = CGRectMake(xPos, yPos, width, height);
                                    mraidView.webView.frame = CGRectMake(0.0f, 0.0f, width, height);
                                }];
                            }
                            @catch (NSException *exception) {
                                [ASSDKLogger onException:exception];
                            }
                        });
                    }
                    
                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, mraidWillAttemptOrientationChange - AFTER INLINE-EXPAND, parRect: %@, adViewRect: %@, self.frame: %@", NSStringFromCGRect(parRect), NSStringFromCGRect(adViewRect), NSStringFromCGRect(self.frame)]];
                    
                } else if(self.mState == kASMRAIDStateResized) {
                    
                    // pull resize properties
                    xPos = [resizeProps[kOffsetXKey] floatValue];
                    yPos = [resizeProps[kOffsetYKey] floatValue];
                    width = [resizeProps[kWidthKey] floatValue];
                    height = [resizeProps[kHeightKey] floatValue];
                    
                    // adjust width & height according to current position and screen size
                    if(width > parRect.size.width) width = parRect.size.width;
                    else if(width < kASMRAIDCloseRegionWH) width = kASMRAIDCloseRegionWH;
                    if(height > parRect.size.height) height = parRect.size.height;
                    else if(height < kASMRAIDCloseRegionWH) height = kASMRAIDCloseRegionWH;
                    
                    // center ad
                    /*if(xPos + width > parRect.size.width)*/ xPos = (parRect.size.width - width)/2;
                    if(yPos + height > parRect.size.height) yPos = (parRect.size.height - height)/2;
                    
                    // reposition parent and mraid views
                    if([self.delegate respondsToSelector:@selector(repositionParentViewOfMRAID:toNewFrame:)]) {
                        [self.delegate repositionParentViewOfMRAID:self toNewFrame:CGRectMake(xPos, yPos, width, height)];
                    }
                    [self.delegate repositionMRAIDView:self toNewFrame:CGRectMake(0.0f, 0.0f, width, height)];
                    
                    // resized mraid ads will always use custom close
                    [self setCustomCloseWithBOOL:YES];
                }
            }
            
            // reposition close button
            [self moveCloseRegionTo:resizeProps[kCustomClosePositionKey]];
            
            // update current position
            [self setCurrentPositionWithX:xPos Y:yPos W:width H:height];
            
            // store pre expand orientation
            if(!self.mraidVC) {
                if(kIS_PORTRAIT) {
                    self.origDefaultOrientationStr = kOrientationPortrait;
                } else if(kIS_LANDSCAPE) {
                    self.origDefaultOrientationStr = kOrientationLandscape;
                }
            }
            
            // change the avplayer layer frame
            if(self.supportUtil.mraidVideoPlayerLayer != nil) {
                self.supportUtil.mraidVideoPlayerLayer.frame = CGRectMake(0.0f, 0.0f, self.screenSize.width, self.screenSize.height);
            }
            
            // fire size change event
            [self handleEventFor:kASMRAIDEventSizeChange
                    withArgArray:@[[self stringForFloat:width], [self stringForFloat:height]]];
            
            #if kShowNewPos
            // checking current position
            NSLog(@"----- currPos: %@", [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesCurrentPos]);
            NSLog(@"----- defaultPos: %@", [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesDefaultPos]);
            NSLog(@"----- screenSize: %@", [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesScreenSize]);
            NSLog(@"----- maxSize: %@", [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesMaxSize]);
            #endif            
        } else {
            [ASSDKLogger logStatement:@"ASMRAIDView, mraidViewWillAttemptOrientationChange - either orientation change not allowed or parent's frame and mraid frame are equal sizes"];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - Notification Selectors

- (void)appDidGotoBackground {
    [ASSDKLogger logStatement:@"ASMRAIDView, appDidGotoBackground"];
    
    // change viewability to hidden
    [self setViewable:NO];
}

- (void)appWillEnterForeground {
    [ASSDKLogger logStatement:@"ASMRAIDView, appWillEnterForeground"];
    
    @try {
        
        // finish processing bridge command if the app went to background via a bridge task initialized from javascript
        if(self.didLeaveAppCauseOfMRAIDBridge) {
            [self bridgeCommandProcessed];
            self.didLeaveAppCauseOfMRAIDBridge = NO;
        }
        
        // change viewability back to seen
        [self setViewable:YES];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - Helpers

- (NSDictionary *)getPropertiesObjectFromJavascriptQuery:(NSString *)jsCmd {
    
    // process javascript query in webview expecting JSON string returned
    NSError *err = nil;
    NSString *propertiesStr = [self.webView stringByEvaluatingJavaScriptFromString:jsCmd];
    
    // deserialize the JSON string into a NSDictionary object
    NSDictionary *propertiesObj = [NSJSONSerialization JSONObjectWithData:[propertiesStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
    if(err != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, getPropertiesObjectFromJavascriptQuery: %@ - ERROR FROM JSONSERIALIZATION: %@", jsCmd, err.localizedDescription]];
        return nil;
    }
    
    return propertiesObj;
    
}

- (void)moveCloseRegionTo:(NSString *)closePos {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, moveCloseRegionTo: - closePos: %@", closePos]];
    
    // retrieving the frame of the banner or interstitial ad container
    CGRect parRect = ([self.delegate respondsToSelector:@selector(adViewForMRAIDView:)]) ? [self.delegate adViewForMRAIDView:self].frame : CGRectMake(0.0f, 0.0f, self.screenSize.width, self.screenSize.height);
    
    // resized mraid ad's can specify for a custom close position, the below if-else chain will position the close region
    // accoring to the input closePos take from the resized object. will default to top-right if closePos is not specified
    if(!closePos || [closePos isEqualToString:kCustomClosePosTopRight]) {
        
        self.closeRegion.frame = CGRectMake(parRect.size.width-kASMRAIDCloseRegionWH, 0.0f,
                                                 kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH);
        self.closeRegion.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin;
        
    } else if([closePos isEqualToString:kCustomClosePosTopLeft]) {
        
        self.closeRegion.frame = CGRectMake(0.0f, 0.0f, kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH);
        self.closeRegion.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        
    } else if([closePos isEqualToString:kCustomClosePosCenter]) {
        
        self.closeRegion.frame = CGRectMake((parRect.size.width - kASMRAIDCloseRegionWH)/2, (parRect.size.height - kASMRAIDCloseRegionWH)/2,
                                                 kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH);
        self.closeRegion.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin;
        
    } else if([closePos isEqualToString:kCustomClosePosBottomLeft]) {
        
        self.closeRegion.frame = CGRectMake(0.0f, parRect.size.height - kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH);
        self.closeRegion.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
        
    } else if([closePos isEqualToString:kCustomClosePosBottomRight]) {
        
        self.closeRegion.frame = CGRectMake(parRect.size.width - kASMRAIDCloseRegionWH, parRect.size.height - kASMRAIDCloseRegionWH,
                                                 kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH);
        self.closeRegion.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin;
        
    } else if([closePos isEqualToString:kCustomClosePosTopCenter]) {
        
        self.closeRegion.frame = CGRectMake((parRect.size.width - kASMRAIDCloseRegionWH)/2, 0.0f, kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH);
        self.closeRegion.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        
    } else if([closePos isEqualToString:kCustomClosePosBottomCenter]) {
        
        self.closeRegion.frame = CGRectMake((parRect.size.width - kASMRAIDCloseRegionWH)/2, parRect.size.height - kASMRAIDCloseRegionWH,
                                                 kASMRAIDCloseRegionWH, kASMRAIDCloseRegionWH);
        self.closeRegion.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    }
}

- (NSString *)stringForFloat:(CGFloat)inNum {
    return [NSString stringWithFormat:@"%f", inNum];
}

- (void)stopLoad {
    [self.webView stopLoading];
}

- (void)cleanup {
    self.supportUtil = nil;
    [self removeFromSuperview];
}

#pragma mark - Positioning and Sizes

- (void)setCurrentPositionWithX:(CGFloat)xPos Y:(CGFloat)yPos W:(CGFloat)width H:(CGFloat)height {
    
    // store current position in a local CGRect to be used during rotation
    self.currPos = CGRectMake(xPos, yPos, width, height);
    
    // define current position dictionary object with the supplied parameters
    NSDictionary *currPosObj = @{ @"x" : [NSNumber numberWithDouble:(double)self.currPos.origin.x],
                                  @"y" : [NSNumber numberWithDouble:(double)self.currPos.origin.y],
                                  @"width" : [NSNumber numberWithDouble:(double)self.currPos.size.width],
                                  @"height" : [NSNumber numberWithDouble:(double)self.currPos.size.height] };
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setCurrentPositionWithX:Y:W:H: %@", currPosObj]];
    
    // serialize the current position dictionary object into JSON
    NSError *err = nil;
    NSData *currPosData = [NSJSONSerialization dataWithJSONObject:currPosObj options:0 error:&err];
    if(err != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setCurrentPositionWithX:Y:W:H: - JSON SERIALIZATION ERROR: %@", err.localizedDescription]];
    } else {
        
        // if there are no errors during serialization, set the current position object inside the MRAID ad
        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:kASMRAIDSetPropertiesCurrentPosition, [currPosData stringEncoded]]];
    }
}

- (void)setDefaultPositionWithFrame:(CGRect)frame {
    
    // define default position object from given input frame
    NSDictionary *defPosObj = @{ @"x" : [NSNumber numberWithDouble:(double)frame.origin.x],
                                 @"y" : [NSNumber numberWithDouble:(double)frame.origin.y],
                                 @"width" : [NSNumber numberWithDouble:(double)frame.size.width],
                                 @"height" : [NSNumber numberWithDouble:(double)frame.size.height] };
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setDefaultPositionWithFrame: %@", defPosObj]];
    [self defaultPosWithObj:defPosObj];
    
}

- (void)setDefaultPositionWithX:(CGFloat)xPos Y:(CGFloat)yPos W:(CGFloat)width H:(CGFloat)height {
    
    // define default position object from given input parameters
    NSDictionary *defPosObj = @{ @"x" : [NSNumber numberWithDouble:(double)xPos],
                                 @"y" : [NSNumber numberWithDouble:(double)yPos],
                                 @"width" : [NSNumber numberWithDouble:(double)width],
                                 @"height" : [NSNumber numberWithDouble:(double)height] };
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setDefaultPositionWithX:Y:W:H: %@", defPosObj]];
    [self defaultPosWithObj:defPosObj];
    
}

- (void)defaultPosWithObj:(NSDictionary *)defPosObj {
    
    // serialize the default position dictionary object into JSON
    NSError *err = nil;
    NSData *defPosData = [NSJSONSerialization dataWithJSONObject:defPosObj options:0 error:&err];
    if(err != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, defaultPosWithObj: - JSON SERIALIZATION ERROR: %@", err.localizedDescription]];
    } else {
        
        // if there are no errors during serialization, set the default position object inside the MRAID ad
        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:kASMRAIDSetPropertiesDefaultPosition, [defPosData stringEncoded]]];
        
    }
    
    // if the placement is inline, store the default banner position to facilitate restoring the default position later
    if(self.mPlcType == kASMRAIDPlacementInline) {
        self.defaultBannerPos = [self.delegate adViewForMRAIDView:self].frame;
    }
}

- (void)setScreenAndMaxSize {
    [ASSDKLogger logStatement:@"ASMRAIDView, setScreenAndMaxSize"];
    
    // define screen size dictionary object dependent on iOS version and status bar orientation
    NSError *err = nil;
    CGRect screenSize = [UIScreen mainScreen].bounds;
    CGFloat width = -1.0f;
    CGFloat height = -1.0f;
    if(kIS_iOS_8_OR_LATER || kIS_PORTRAIT) {
        width = screenSize.size.width;
        height = screenSize.size.height;
    } else if(kIS_LANDSCAPE) {
        width = screenSize.size.height;
        height = screenSize.size.width;
    }
    if(kIS_iOS_6) height-=kStatusBarHeight;
    NSDictionary *sizeObj = @{ kWidthKey : [NSNumber numberWithDouble:(double)width],
                               kHeightKey : [NSNumber numberWithDouble:(double)height] };
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRADIView, setScreenAndMaxSize - sizeObj: %@", sizeObj]];
    
    // serialize the size dictionary object into JSON
    err = nil;
    NSData *sizeObjData = [NSJSONSerialization dataWithJSONObject:sizeObj options:0 error:&err];
    if(err != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setScreenAndMaxSize - JSON SERIALIZATION ERROR: %@", err.localizedDescription]];
    } else {
        // if there are no serialization errors set the screen size and max size inside the MRAID ad
        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:kASMRAIDSetPropertiesScreenSize, [sizeObjData stringEncoded]]];
        self.screenSize = CGSizeMake(width, height);
        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:kASMRAIDSetPropertiesMaxSize, [sizeObjData stringEncoded]]];
    }
    
    // forcing the status bar to be hidden for all expanded content interstitail or banner
    //    // max size dependent on statusbar hidden or not
    //    err = nil;
    //    if(![UIApplication sharedApplication].statusBarHidden) {
    //        height -= [UIApplication sharedApplication].statusBarFrame.size.height;
    //        sizeObj = @{ @"width" : [NSNumber numberWithDouble:width],
    //                    @"height" : [NSNumber numberWithDouble:height] };
    //        sizeObjData = [NSJSONSerialization dataWithJSONObject:sizeObj options:0 error:&err];
    //    }
    //
    //    if(err != nil) {
    //        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setScreenAndMaxSize - JSON SERIALIZATION ERROR(2): %@", err.localizedDescription]];
    //    } else {
    //        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"mraid.setMaxSize(%@);", [sizeObjData stringEncoded]]];
    //        self.maxSize = CGSizeMake(width, height);
    //    }
}

#pragma mark - External Player

- (void)externalVideoPlayerFromMraidBuiltWith:(AVPlayer *)player {
    if(!self.parentOfExpandedMRAIDView) {
        [self.delegate loadExternalPlayerForMRAIDView:self withPlayer:player];
    } else {
        // since there is a parent mraid view, we know that this must be an expanded mraid view
        ASMRAIDViewController *mraidVC = self.parentOfExpandedMRAIDView.mraidVC;
        
        [self.supportUtil initPlayerLayer];
        self.supportUtil.mraidVideoPlayerLayer.frame = CGRectMake(mraidVC.view.frame.origin.x, mraidVC.view.frame.origin.y,
                                                                  mraidVC.view.frame.size.width, mraidVC.view.frame.size.height);
        
        [mraidVC.view.layer addSublayer:self.supportUtil.mraidVideoPlayerLayer];
        [player play];
    }
    
    self.isCloseRegionHiddenBeforeVideoPlay = [self.closeRegion isCloseBtnHidden];
    [self.closeRegion hideCloseBtn:YES];
    [self bridgeCommandProcessed];
}

- (void)externalVideoPlayerFromMraidDidComplete:(AVPlayer *)player {
    [self.delegate removeExternalPlayerForMRAIDView:self forPlayer:player];
    
    [self.closeRegion hideCloseBtn:self.isCloseRegionHiddenBeforeVideoPlay];
}

#pragma mark - Events

- (void)handleEventFor:(NSString *)eventName withArgArray:(NSArray *)args {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, handleEventFor:%@ withArgArray:%@", eventName, args]];
    
    // define the javascript command to handle events for event name with supplied arguments
    NSString *jsCmd = [NSString stringWithFormat:kASMRAIDHandleEventCommand, eventName];
    if(args != nil) {
        for(NSString *arg in args) {
            jsCmd = [jsCmd stringByAppendingString:[NSString stringWithFormat:@", %@", arg]];
        }
    }
    jsCmd = [jsCmd stringByAppendingString:@");"];
    
    // fire the handle event command
    [self.webView stringByEvaluatingJavaScriptFromString:jsCmd];
    
}

#pragma mark - Ready Event

- (void)callReady {
    [ASSDKLogger logStatement:@"ASMRAIDView, callReady"];
    
    [self mraidIsReady];
}

- (void)mraidIsReady {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, mraidIsReady - id: %@", self.restorationIdentifier]];
    
    // set state to default first so mraid functions can be utilized
    [self setState:([self.restorationIdentifier isEqualToString:kASMRAIDPrimary] ? kASMRAIDStateDefault : kASMRAIDStateExpanded)];
    
    // check placement type, and set the default position accordingly
    switch(self.mPlcType) {
        case kASMRAIDPlacementInline:
            [self setDefaultPositionWithFrame:[self.delegate adViewForMRAIDView:self].frame];
            break;
        case kASMRAIDPlacementInterstitial:
            [self setDefaultPositionWithFrame:self.frame];
            break;
        default:
            break;
    }
    
    // define the supported features
    [self setMraidSupportedFeatures];
    
    // should probably fire impression here
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
    
    // fire event handlers for ready
    [self handleEventFor:kASMRAIDEventReady withArgArray:nil];
}

- (void)finishReady {
    [ASSDKLogger logStatement:@"ASMRAIDView, finishReady"];
    
    // set viewable to YES, viewableChange handled inside javascript
    [self setViewable:YES];
    
    // finish bridge task
    [self bridgeCommandProcessed];
    
    // giving the ad the actual size of container
    NSString *jsCmd = [NSString stringWithFormat:@"var style = document.createElement('style');style.setAttribute('type', 'text/css')style.appendChild(document.createTextNode(' body{ height:%0.0fpx; width:%0.0fpx;}'));", self.frame.size.height, self.frame.size.width];
    [self.webView stringByEvaluatingJavaScriptFromString:jsCmd];
}

- (void)setMraidSupportFor:(NSString *)featureStr as:(NSString *)supportedStr {
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:kASMRAIDSetSupportWithTokens, featureStr, supportedStr]];
}

- (void)setMraidSupportedFeatures {
    [ASSDKLogger logStatement:@"ASMRAIDView, setMraidSupportedFeature"];

    // check if device has the capabilities to make telephone calls or send SMS messages
    NSString *canMakeTelCall = [self.supportUtil canMakeTelephoneCalls] ? kASMRAIDSupportsTrue : kASMRAIDSupportsFalse;
    NSString *canSendSMS = [self.supportUtil canSendSMSTexts] ? kASMRAIDSupportsTrue : kASMRAIDSupportsFalse;
    
    // set the supported fefatures for MRAID
    [self setMraidSupportFor:kASMRAIDSupportsSMSFeature as:canSendSMS];
    [self setMraidSupportFor:kASMRAIDSupportsTelFeature as:canMakeTelCall];
    [self setMraidSupportFor:kASMRAIDSupportsCalFeature as:kASMRAIDSupportsTrue];
    [self setMraidSupportFor:kASMRAIDSupportsStorePicFeature as:kASMRAIDSupportsTrue];
    [self setMraidSupportFor:kASMRAIDSupportsInlineVideoFeature as:kASMRAIDSupportsTrue];
    [self setMraidSupportFor:kASMRAIDSupportsVPAIDFeature as:kASMRAIDSupportsTrue];
}

#pragma mark - MRAID Bridge Functions

- (NSString *)getVersion {
    return [self.webView stringByEvaluatingJavaScriptFromString:kASMRAIDGetVersion];
}

- (NSString *)getState {
    return [self.webView stringByEvaluatingJavaScriptFromString:kASMRAIDGetState];
}

- (void)setState:(ASMRAIDState)newState {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setState: %ld - id: %@", (long)newState, self.restorationIdentifier]];
    
    // set state locally and in the MRAID ad, handles stateChange event inside javascript
    self.mState = newState;
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:kASMRAIDSetState, (long)newState]];
}

- (void)setPlacementType:(ASMRAIDPlacementType)plcType {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setPlacementType: %ld", (long)plcType]];
    
    // set placement type locally and in the MRAID ad
    self.mPlcType = plcType;
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:kASMRAIDSetPlacementType, (long)plcType]];
}

- (void)setViewable:(BOOL)yesOrNo {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setViewable: %d", yesOrNo]];
    
    // setup setViewable javascript command according to the input BOOL
    NSString *jsCmd = @"";
    if(yesOrNo) {
        jsCmd = @"mraid.setViewable(true)";
        self.hidden = NO;
    } else {
        jsCmd = @"mraid.setViewable(false)";
        self.hidden = YES;
    }
    
    // fire javascript command, viewableChange event is handled in the javascript
    [self.webView stringByEvaluatingJavaScriptFromString:jsCmd];
    
    if(!self.hidden) {
        [self mraidViewWillAttemptOrientationChange];
    }
    
    
    if([self.delegate respondsToSelector:@selector(contentViewForMRAIDView:)]) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setViewable - mraidView.hidden: %d, contentView.hidden: %d", self.hidden, [self.delegate contentViewForMRAIDView:self].hidden]];
    } else if([self.delegate respondsToSelector:@selector(viewControllerForMRAIDView:)]) {
        UIViewController *tempVc = [self.delegate viewControllerForMRAIDView:self];
        
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setViewable - mraidView.hidden: %d, mraidView.frame: %@, mraidView.webView.hidden: %d, mraidView.webView.frame: %@, vc.hidden: %d, vc.frame: %@", self.hidden, NSStringFromCGRect(self.frame), self.webView.hidden, NSStringFromCGRect(self.webView.frame), tempVc.view.hidden, NSStringFromCGRect(tempVc.view.frame)]];
    }
}

- (void)open:(NSString *)urlStr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, open - url: %@", urlStr]];
    
    // set flag for processing bridge command when app returns from background
    self.didLeaveAppCauseOfMRAIDBridge = YES;
    
    // attempt to open the request externally in safari
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
}

- (void)expand {
    [ASSDKLogger logStatement:@"ASMRAIDView, expand"];
    
    // pull the expand properties object
    NSDictionary *expandPropertiesObj = [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesExpand];
    
    if(expandPropertiesObj != nil) {
        // need to set the state to expanded such that the repositioning won't reset the default position
        [self setState:kASMRAIDStateExpanded];
    
        // adjust width & height for screen size
        CGFloat width = [expandPropertiesObj[kWidthKey] floatValue];
        if(width > self.screenSize.width || width == 0.0f) width = self.screenSize.width;
        CGFloat height = [expandPropertiesObj[kHeightKey] floatValue];
        if(height > self.screenSize.height || height == 0.0f) height = self.screenSize.height;
        
        // center ad
        CGFloat xPos = (self.screenSize.width - width)/2;
        CGFloat yPos = (self.screenSize.height - height)/2;
        
        // animate positioning of mraid view
        __weak ASMRAIDView *mraidView = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                [UIView animateWithDuration:kMRAIDAnimationDuration animations:^{
                    @try {
                        if([mraidView.delegate respondsToSelector:@selector(repositionParentViewOfMRAID:toNewFrame:)]) {
                            [mraidView.delegate repositionParentViewOfMRAID:mraidView
                                                                 toNewFrame:CGRectMake(0.0f, 0.0f, mraidView.screenSize.width, mraidView.screenSize.height)];
                        }
                        [mraidView.delegate repositionMRAIDView:mraidView toNewFrame:CGRectMake(xPos, yPos, width, height)];
                    }
                    @catch (NSException *exception) {
                        [ASSDKLogger onException:exception];
                    }
                } completion:^(BOOL fin){
                    @try {
                        // finish up expand by checking custom close, setting current position plus state, and handling events
                        [mraidView setCustomCloseWithBOOL:[(NSNumber *)expandPropertiesObj[kUseCustomClose] boolValue]];
                        [mraidView setCurrentPositionWithX:xPos Y:yPos W:width H:height];
                        [mraidView handleEventFor:kASMRAIDEventSizeChange
                                     withArgArray:@[[mraidView stringForFloat:width], [mraidView stringForFloat:height]]];
                        
                        // transfer mraidView over to mraid view controller
                        dispatch_async(dispatch_get_main_queue(), ^{
                            @try {
                                // initialize ASMRAIDViewController, transfer this MRAIDView object to the new view controller, and present it
                                mraidView.movingBetweenMraidViewAndVC = YES;
                                mraidView.mraidVC = [[ASMRAIDViewController alloc] initWithMRAIDView:mraidView];
                                [mraidView.mraidVC.origRootVC presentViewController:mraidView.mraidVC animated:NO completion:nil];
                            }
                            @catch (NSException *exception) {
                                [ASSDKLogger onException:exception];
                            }
                        });
                        
                    }
                    @catch (NSException *exception) {
                        [ASSDKLogger onException:exception];
                    }
                }];
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        });
    } else {
        [ASSDKLogger logStatement:@"ASMRAIDView, expand failed with bad properties object"];
    }
    
    // finish the bridge task
    [self bridgeCommandProcessed];
    
}

- (void)addMRAIDBackToBannerView {
    [ASSDKLogger logStatement:@"ASMRAIDView, addMRAIDBackToBannerView"];
    
    UIView *contentView = nil;
    
    // remove the current contentView from the ASAdView banner view
    if([self.delegate respondsToSelector:@selector(contentViewForMRAIDView:)]) {
        contentView = [self.delegate contentViewForMRAIDView:self];
        [contentView removeFromSuperview];
    }
    
    // set the current MRAID view to be the new content view in ASAdView
    if([self.delegate respondsToSelector:@selector(setContentViewToMRAIDView:)]) {
        [self.delegate setContentViewToMRAIDView:self];
    }
    contentView = self;
    
    if([self.delegate respondsToSelector:@selector(adViewForMRAIDView:)]) {
        // add mraid view to the parent banner
        UIView *parentBanner = [self.delegate adViewForMRAIDView:self];
        [parentBanner addSubview:contentView];
        [parentBanner bringSubviewToFront:contentView];
        [parentBanner bringSubviewToFront:self.closeRegion];
    }
    
    // cleanup mraidVC
    self.mraidVC = nil;
}

- (void)expandWithURL:(NSString *)urlStr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, expandWithURL - urlStr: %@", urlStr]];
    
    // set state and finish bridge command for original webview
    [self setState:kASMRAIDStateExpanded];
    [self bridgeCommandProcessed];
    
    // initialize the expanded mraid view
    ASMRAIDView *expandedMraidView = [[ASMRAIDView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.screenSize.width, self.screenSize.height)
                                                        withURL:[NSURL URLWithString:urlStr]
                                                      forRootVC:self.rootVC
                                                      asPlcType:self.mPlcType
                                                    andDelegate:self.delegate];
    
    // setup pointer back to original MRAID view
    expandedMraidView.parentOfExpandedMRAIDView = self;
    
    // create a new mraid vc as a property of the expanded mraid view
    __block dispatch_semaphore_t animateSema = dispatch_semaphore_create(0);
    __weak ASMRAIDView *mraidView = self;
    [UIView animateWithDuration:kMRAIDAnimationDuration animations:^{
        mraidView.alpha = 0.0f;
    } completion:^(BOOL fin){
        dispatch_semaphore_signal(animateSema);
    }];
    dispatch_semaphore_wait(animateSema, dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)));
    self.movingBetweenMraidViewAndVC = YES;
    self.mraidVC = [[ASMRAIDViewController alloc] initWithMRAIDView:expandedMraidView];
    self.mraidVC.origRootVC = self.rootVC;
}

- (void)foundMRAIDInitialSetup {
    [ASSDKLogger logStatement:@"ASMRAIDView, foundMRAIDParseHTMLAndInitialSetup"];
    
    // init the support feature class
    self.supportUtil = [[ASMRAIDSupportsUtil alloc] initWithMRAID:self];
    
    // set placement type
    [self setPlacementType:self.mPlcType];
    
    // set current position, default position, and orientation properties
    [self mraidViewWillAttemptOrientationChange];
    
    // initially allow orientation change
    self.allowOrienationChange = YES;
    self.webView.hidden = YES;
}

- (void)finishLoadIntoMRAIDVC {
    [ASSDKLogger logStatement:@"ASMRAIDView, finishMRAIDSetupAndLoadIntoMRAIDVC"];
    
    // set custom close to yes
    [self setCustomCloseWithBOOL:self.mraidFoundFromURL];
    
    // load the mraid view into the mraid vc and present it
    __weak ASMRAIDView *mraidView = self;
    [self.parentOfExpandedMRAIDView.mraidVC.origRootVC presentViewController:self.parentOfExpandedMRAIDView.mraidVC animated:NO completion:^{
        [mraidView finishExpandedMRAID];
    }];
}

- (void)finishExpandedMRAID {
    [ASSDKLogger logStatement:@"ASMRAIDView, finishExpandedMRAID"];

    @try {
        self.webView.hidden = NO;
        [self mraidIsReady];
        
        // change state to expanded
        [self setState:kASMRAIDStateExpanded];
        
        // reposition the webview to the expanded props position for MRAID, or to full screen for non-MRAID urls
        if(self.mraidFoundFromURL) {
            
            self.frame = self.currPos;
            self.webView.frame = CGRectMake(0.0f, 0.0f, self.currPos.size.width, self.currPos.size.height);
            [self.closeRegion hideCloseBtn:YES];
            
        } else {
            
            self.frame = CGRectMake(0.0f, 0.0f, self.mraidVC.view.frame.size.width, self.mraidVC.view.frame.size.height);
            self.webView.frame = CGRectMake(0.0f, 0.0f, self.mraidVC.view.frame.size.width, self.mraidVC.view.frame.size.height);
            self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
            
        }
        
        // reset flags
        self.initialFromURLLoad = NO;
        self.mraidFoundFromURL = NO;
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onClose {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, onClose - id: %@", self.restorationIdentifier]];
    
    @try {
        // attempt to dismiss the expanded VC even if it doesn't exist
        [self.mraidVC onClose];
        
        if(self.mState == kASMRAIDStateDefault) {
            
            // if state is default, change state to hidden and clean up the MRAID ad
            [self setState:kASMRAIDStateHidden];
            [self cleanup];
            [self.delegate closeMRAIDView:self];
            
        } else if(self.mState == kASMRAIDStateExpanded || self.mState == kASMRAIDStateResized) {
            
            if(self.mPlcType == kASMRAIDPlacementInline) {
                // if mraid view controller exists, move mraid view back to the banner, and reset the orientation to
                // the original pre expand oreientation
                if(self.mraidVC != nil && !self.parentOfExpandedMRAIDView) {
                    [self addMRAIDBackToBannerView];
                    [self forceOrientationWith:self.origDefaultOrientationStr];
                }
            }
            
            // when returning from resize, re-enable custom close button
            if(self.mState == kASMRAIDStateResized) {
                [self.closeRegion hideCloseBtn:NO];
            }
            
            // invoke restore default to reposition mraid view accordingly
            [self restoreDefault];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)restoreDefault {
    [ASSDKLogger logStatement:@"ASMRAIDView, restoreDefault"];
    
    if(self.alpha == 0.0f) {
        __block dispatch_semaphore_t animateSema = dispatch_semaphore_create(0);
        __weak ASMRAIDView *mraidView = self;
        [UIView animateWithDuration:kMRAIDAnimationDuration animations:^{
            mraidView.alpha = 1.0f;
        } completion:^(BOOL fin){
            dispatch_semaphore_signal(animateSema);
        }];
        dispatch_semaphore_wait(animateSema, dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)));
    }
    
    // check to see if mraid view controller exists, if so dismiss it and move mraid view back to the banner container
    if(self.mraidVC != nil) {
        if(self.mPlcType == kASMRAIDPlacementInline &&
           self.mState == kASMRAIDStateExpanded) {
            
            __weak ASMRAIDView *mraidView = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                @try {
                    [mraidView.mraidVC dismissMRAIDInterVCWithBlock:^{
                        @try {
                            mraidView.movingBetweenMraidViewAndVC = NO;
                            [mraidView addMRAIDBackToBannerView];
                        }
                        @catch (NSException *exception) {
                            [ASSDKLogger onException:exception];
                        }
                    }];
                }
                @catch (NSException *exception) {
                    [ASSDKLogger onException:exception];
                }
            });
        } else if(self.mPlcType == kASMRAIDPlacementInterstitial && self.mState == kASMRAIDStateExpanded) {
            self.movingBetweenMraidViewAndVC = YES;
            __weak ASMRAIDView *mraidView = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [mraidView.mraidVC.origRootVC dismissViewControllerAnimated:NO completion:^{
                    [mraidView removeFromSuperview];
                    UIViewController *parentVC = [mraidView.delegate viewControllerForMRAIDView:mraidView];
                    [parentVC.view addSubview:mraidView];
                    [parentVC.view bringSubviewToFront:mraidView.closeRegion];
                    mraidView.mraidVC = nil;
                    mraidView.movingBetweenMraidViewAndVC = NO;
                }];
            });
        }
    }
    
    // if a parent of expanded mraid view exists, dismiss the VC and call the parent's restore default
    if(self.parentOfExpandedMRAIDView !=  nil) {
        __weak ASMRAIDView *mraidView = self;
        [self.parentOfExpandedMRAIDView.mraidVC dismissViewControllerAnimated:YES completion:^{
            [mraidView.closeRegion removeFromSuperview];
            [mraidView.parentOfExpandedMRAIDView restoreDefault];
        }];
        return;
    }
    
    // get the default position from mraid ad
    self.webView.hidden = NO;
    NSDictionary *defaultPosObj = [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesDefaultPos];
    CGRect defaultRect = CGRectMake([defaultPosObj[kXKey] floatValue], [defaultPosObj[kYKey] floatValue],
                                    [defaultPosObj[kWidthKey] floatValue], [defaultPosObj[kHeightKey] floatValue]);
    
    // set state back to default
    [self setState:kASMRAIDStateDefault];
    
    // reposition the mraid view to it's default position
    __weak ASMRAIDView *mraidView = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            [UIView animateWithDuration:kMRAIDAnimationDuration animations:^{
                @try {
                    if([mraidView.delegate respondsToSelector:@selector(repositionParentViewOfMRAID:toNewFrame:)]) {
                        [mraidView.delegate repositionParentViewOfMRAID:mraidView toNewFrame:mraidView.defaultBannerPos];
                    }
                    [mraidView.delegate repositionMRAIDView:mraidView toNewFrame:defaultRect];
                }
                @catch (NSException *exception) {
                    [ASSDKLogger onException:exception];
                }
            } completion:^(BOOL fin){
                @try {
                    // finish with setting the new current position and handling the size change event
                    [mraidView setCurrentPositionWithX:defaultRect.origin.x Y:defaultRect.origin.y W:defaultRect.size.width H:defaultRect.size.height];
                    [mraidView handleEventFor:kASMRAIDEventSizeChange
                                 withArgArray:@[[mraidView stringForFloat:defaultRect.size.width], [mraidView stringForFloat:defaultRect.size.height]]];
                }
                @catch (NSException *exception) {
                    [ASSDKLogger onException:exception];
                }
                @finally {
                    // finish the bridge task
                    [mraidView bridgeCommandProcessed];
                }
            }];
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
    
}

- (void)setCustomCloseWithBOOL:(BOOL)yesOrNo {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setCustomCloseWithBOOL: %d", yesOrNo]];
    
    [self.closeRegion hideCloseBtn:yesOrNo];
    self.useCustomClose = yesOrNo;
    
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:kASMRAIDUseCustomClose, (yesOrNo) ? @"true" : @"false"]];
}

- (void)setCustomClose:(NSString *)arg {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, setCustomClose: %@", arg]];
    
    // check the string argument for true/false and set custom close accordingly
    if([arg isEqualToString:@"true"]) {
        [self.closeRegion hideCloseBtn:YES];
        self.useCustomClose = YES;
    } else if([arg isEqualToString:@"false"]) {
        [self.closeRegion hideCloseBtn:NO];
        self.useCustomClose = NO;
    }
    
    // finish the bridge task
    [self bridgeCommandProcessed];
}

- (void)resize {
    [ASSDKLogger logStatement:@"ASMRAIDView, resize"];
    
    // grab the resize properties
    NSDictionary *resizePropertiesObj = [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesResize];
    if(resizePropertiesObj != nil) {
        
        // pull out the position values from resize properties
        CGFloat width = [resizePropertiesObj[kWidthKey] floatValue];
        CGFloat height = [resizePropertiesObj[kHeightKey] floatValue];
        CGFloat offsetX = [resizePropertiesObj[kOffsetXKey] floatValue];
        CGFloat offsetY = [resizePropertiesObj[kOffsetYKey] floatValue];
        
        // check to see if there's a valid custom close position
        NSString *closePos = nil;
        if(resizePropertiesObj[kCustomClosePositionKey]) {
            closePos = resizePropertiesObj[kCustomClosePositionKey];
            NSArray *possPos = @[kCustomClosePosTopLeft, kCustomClosePosTopRight, kCustomClosePosCenter, kCustomClosePosBottomLeft,
                                 kCustomClosePosBottomRight,kCustomClosePosTopCenter, kCustomClosePosBottomCenter];
            closePos = [possPos containsObject:closePos] ? closePos : kCustomClosePosBottomRight;
            possPos = nil;
        }
        
        // check to ssee if there's a valid allow offscreen value
        NSNumber *allowOffscreenNum = resizePropertiesObj[kAllowOffscreenKey];
        BOOL allowOffscreen = !allowOffscreenNum ? YES : [allowOffscreenNum boolValue];
        
        // ensure that the width and height is at least as big as the close region
        CGRect parentFrame = self.rootVC.view.frame;
        if(width < 50.0f) width = 50.0f;
        if(height < 50.0f) height = 50.0f;
        
        // if the creative is not allowed offscreen, reposition the ad accordingly
        if(!allowOffscreen) {
            if(offsetX < 0.0f) offsetX = 0.0f;
            if(offsetY < 0.0f) offsetY = 0.0f;
            if(offsetX + width > parentFrame.size.width) width = parentFrame.size.width - offsetX;
            if(offsetY + height > parentFrame.size.height) height = parentFrame.size.height - offsetY;
            
            if(offsetX == 0.0f && width < parentFrame.size.width) offsetX = (parentFrame.size.width - width)/2;
        }
        
        // move the MRAID ad and close region to the resized position
        if([self.delegate respondsToSelector:@selector(repositionParentViewOfMRAID:toNewFrame:)]) {
            [self.delegate repositionParentViewOfMRAID:self toNewFrame:CGRectMake(offsetX, offsetY, width, height)];
            [self.delegate repositionMRAIDView:self toNewFrame:CGRectMake(0.0f, 0.0f, width, height)];
        } else {
            [self.delegate repositionMRAIDView:self toNewFrame:CGRectMake(offsetX, offsetY, width, height)];
        }
        [self moveCloseRegionTo:closePos];
        
        // set state to resized, set thenew current position, and handle sizechange event
        [self setState:kASMRAIDStateResized];
        [self setCurrentPositionWithX:offsetX Y:offsetY W:width H:height];
        [self handleEventFor:kASMRAIDEventSizeChange
                withArgArray:@[[self stringForFloat:width], [self stringForFloat:height]]];
        
        // all resized ads should use custom close
        [self.closeRegion hideCloseBtn:YES];
        
    } else {
        
        [ASSDKLogger logStatement:@"ASMRAIDView, resize failed with bad properties object"];
        
    }
    
    // finish bridge task
    [self bridgeCommandProcessed];
}

- (void)forceOrientationWith:(NSString *)orientationStr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, forceOrientationWith: - orientationStr: %@", orientationStr]];
    
    if(!orientationStr) return;
    
    // translate the forced orientation to an orientation value understood by the iOS device
    if([orientationStr isEqualToString:kOrientationPortrait]) {
        self.preferredOrientation = kIS_IPHONE ? UIInterfaceOrientationMaskPortrait : UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown;
    } else if([orientationStr isEqualToString:kOrientationLandscape]) {
        self.preferredOrientation = UIInterfaceOrientationMaskLandscape;
    } else if([orientationStr isEqualToString:kOrientationNone]) {
        self.preferredOrientation = kIS_IPHONE ? UIInterfaceOrientationMaskAllButUpsideDown : UIInterfaceOrientationMaskAll;
    }
    
    UIViewController *presentingVC = self.rootVC.presentingViewController;
    if(presentingVC != nil) {
        self.didForceOrientaiton = YES;
        __weak ASMRAIDView *mraidView = self;
        [presentingVC dismissViewControllerAnimated:NO completion:^{
            @try {
                [presentingVC presentViewController:mraidView.rootVC animated:NO completion:^{
                    [ASSDKLogger logStatement:@"ASMRAIDView, forceOrientaitonWIth: - presentViewController, completion block"];
                    @try {
                        // reset the screen and max size for the new orientation
                        [mraidView mraidViewWillAttemptOrientationChange];
                        mraidView.didForceOrientaiton = NO;
                    }
                    @catch (NSException *exception) {
                        [ASSDKLogger onException:exception];
                    }
                    @finally {
                        [mraidView bridgeCommandProcessed];
                    }
                }];
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        }];
    } else {
        [self bridgeCommandProcessed];
    }
}

- (void)forceOrientaiton {
    [ASSDKLogger logStatement:@"ASMRAIDView, forceOrientation"];
    
    // pull the orientation properties from MRAID ad and get the orientation to force
    NSDictionary *orientationProps = [self getPropertiesObjectFromJavascriptQuery:kASMRAIDGetPropertiesOrientation];
    NSString *orientation = orientationProps[kForceOrientationKey];
    
    [self forceOrientationWith:orientation];
}

- (void)fireVpaidEvent:(NSString *)eventStr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, fireVpaidEvent: - event: %@", eventStr]];
    
    // create event
    if(eventStr != nil) {
        if([eventStr isEqualToString:kMRAIDVPAIDEventImpression]) {
            if(self.vastImpressionURL != nil) {
                ASVASTEvent *vastEvent = [ASVASTEvent new];
                
                vastEvent.canBeSent = YES;
                vastEvent.oneTime = YES;
                vastEvent.type = ASVASTEventType_AdImpression;
                [vastEvent addURL:self.vastImpressionURL];
                
                // fire event
                [ASCommManager sendAsynchronousRequestsForEvent:vastEvent];
                [self.delegate mraidView:self didFireAdvertiserEventWithMessage:@"MRAID View Fires Impression Event"];
                
                // set vast URL to nil to prevent it from firing more than once
                self.vastImpressionURL = nil;
            }
        } else if([eventStr isEqualToString:kMRAIDVPAIDEventVideoComplete]) {
            // FIRE VC REWARDED -- AE-5248
            if([self.delegate respondsToSelector:@selector(attemptToRewardVC:)])
                [self.delegate attemptToRewardVC:self];
        } else {
            [ASSDKLogger logStatement:@"ASMRAIDView, fireVpaidEvent: - Nothing To Do"];
        }
    }
    
    [self bridgeCommandProcessed];
}

- (void)bridgeCommandProcessed {
    [ASSDKLogger logStatement:@"ASMRAIDView, bridgeCommandProcessed"];
    
    @try {
        // finish bridge command so the next javascript command can be processed
        [self.webView stringByEvaluatingJavaScriptFromString:@"mraidBridge.finishConnection()"];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - URL Redirects

- (void)actionTaskFor:(NSString *)actionStr withArg:(NSString *)argStr {
//    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, actionTaskFor:withArg: - %@ : %@", actionStr, argStr]];
    
    // check actionStr for corresponding action, for now it's only debug logging
    if([actionStr isEqualToString:kASMRAIDLoggerTaskLog]) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, id: %@, js.log: %@", self.restorationIdentifier, argStr]];
        return;
    }
}

- (void)bridgeTaskFor:(NSString *)funcStr withArg:(NSString *)argStr {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, bridgeTaskFor:withArg: - %@ : %@", funcStr, argStr]];
    
    // check funcStr for the bridge task called for from the MRAID ad javascript
    if([funcStr isEqualToString:kASMRAIDBridgeFinishReady]) {
        [self finishReady];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskOpen]) {
        [self open:argStr];
        [self.delegate didReceiveTouchForMRAIDView:self];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskExpand]) {
        [self expand];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskExpandWithURL]) {
        [self expandWithURL:argStr];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskClose]) {
        [self onClose];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskRestoreDefault]) {
        [self restoreDefault];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskUseCustomClose]) {
        [self setCustomClose:argStr];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskResize]) {
        [self resize];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskForceOrientation]) {
        [self forceOrientaiton];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskStorePicture]) {
        [self.supportUtil startPhotoDownloadFrom:argStr];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskCreateCalendarEvent]) {
        [self.supportUtil createCalendarEventWith:argStr];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskPlayVideo]) {
        [self.supportUtil playVideoWith:argStr];
    } else if([funcStr isEqualToString:kASMRAIDBridgeTaskVPAIDEvent]) {
        [self fireVpaidEvent:argStr];
    }
}

#pragma mark - UIWebViewDelelgate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, webView:shouldStartLoadWithRequest:navigationType: - request: %@", request.URL]];
    
    @try {
        // ignore legit webview requests so they load normally
        if (![request.URL.scheme isEqualToString:kASMRAIDBridgeTaskScheme]) {
            
            // for 2 part ads, allow the second webview to load without question
            if(self.initialFromURLLoad) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, webView:shouldStartLoadWithRequest:navigationType: - mraidFromURL, request: %@", request.URL.absoluteString]];
                return YES;
            }
            
            // check if this request is sent during the loading phase or if it's re-directing out of app. if the ad is still loading let it load.
            // if it's re-directing out of app, stop the load and open with external browser
            if(self.mState > kASMRAIDStateLoading && self.didLeaveAppCauseOfMRAIDBridge && [[UIApplication sharedApplication] canOpenURL:request.URL]) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, webView:shouldStartLoadWithRequest:navigationType: - open external request in safari with url: %@", request.URL.absoluteString]];
                [[UIApplication sharedApplication] openURL:request.URL];
                if([request.URL.absoluteString containsStr:@"sms://"] || [request.URL.absoluteString containsStr:@"tel://"]) {
                    [self bridgeCommandProcessed];
                }
                return NO;
            } else {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, webView:shouldStartLoadWithRequest:navigationType: - external request will load in webview with url: %@", request.URL.absoluteString]];
                return YES;
            }
        }
        
        // the request is an internal mraid bridge task or action
        
        // choose the task and action
        __weak ASMRAIDView *mraidView = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            @try {
                // parse the task and action from the request URL
                NSString *taskAction = request.URL.host;
                NSString *taskType = nil;
                NSString *actionType = nil;
                
                // get the task-action
                if([taskAction rangeOfString:@"-"].location < taskAction.length) {
                    NSArray *taskActionArr = [taskAction componentsSeparatedByString:@"-"];
                    taskType = taskActionArr[0];
                    actionType = taskActionArr[1];
                }
                
                // get argument(s)
                NSRange actionRange = [request.URL.absoluteString rangeOfString:actionType];
                NSString *argument = [[request.URL absoluteString] substringFromIndex:(actionRange.location + actionRange.length + 1)];
                argument = [argument stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                // dispatch onto main queue, to handle any UI action that has been requested from MRAID ad
                dispatch_async(dispatch_get_main_queue(), ^{
                    @try {
                        if(mraidView != nil) {
                            if([taskType isEqualToString:@"action"]) {
                                [mraidView actionTaskFor:actionType withArg:argument];
                            } else if([taskType isEqualToString:@"bridge"]) {
                                [mraidView bridgeTaskFor:actionType withArg:argument];
                            }
                        }
                    }
                    @catch (NSException *exception) {
                        [ASSDKLogger onException:exception];
                    }
                });
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        });
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
    return NO;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, webViewDidStartLoad: - id: %@, request: %@", self.restorationIdentifier, webView.request.URL]];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, webViewDidFinishLoad: - id: %@, request: %@", self.restorationIdentifier, webView.request.URL]];
    
    @try {
        if(self.initialFromURLLoad) {
            
            // for the secondary webview, if the initial load has not been done and the MRAID.js tag is found
            // then inject MRAID into the secondary webview, else finish the load
            if((self.mraidFoundFromURL = [ASMRAIDParser checkLoadedHTMLForMRAIDInWebView:webView])) {
                [self foundMRAIDInitialSetup];
            }
            [self finishLoadIntoMRAIDVC];
            
        } else {
            
            // if the primary webview finishes a load while in the loading state, the MRAID ad is ready
            if(self.mState == kASMRAIDStateLoading) {
                [self mraidIsReady];
            }
            
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, webView:didFailLoadWithError: - id: %@, error: %@", self.restorationIdentifier, error.localizedDescription]];
    
    @try {
        // there was a failure, finish up any on-going bridge task
        [self bridgeCommandProcessed];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end
//
//  ASMRAIDSupportsUtil.m
//  AerServSDK
//
//  Created by Albert Zhu on 9/10/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASMRAIDSupportsUtil.h"
#import "ASAlertManager.h"
#import "ASMRAIDView.h"

#import <EventKit/EventKit.h>

@interface ASMRAIDSupportsUtil () <NSURLSessionDelegate, NSURLSessionDownloadDelegate>

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) AVPlayer *mraidExtPlayer;
@property (nonatomic, strong) AVPlayerLayer *mraidVideoPlayerLayer;

@end

@implementation ASMRAIDSupportsUtil

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASMRAIDSupportsUtil, dealloc"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemFailedToPlayToEndTimeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    
    self.mainView = nil;
    self.dateFormatter = nil;
    self.mraidVideoPlayerLayer = nil;
    self.mraidExtPlayer = nil;
}

- (instancetype)initWithMRAID:(ASMRAIDView *)mraidView {
    if(self = [super init]) {
        self.mainView = mraidView;
        self.dateFormatter = [[NSDateFormatter alloc] init];
    }
    
    return self;
}

#pragma mark - Support

- (BOOL)canMakeTelephoneCalls {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]];
}

- (BOOL)canSendSMSTexts {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"sms://"]];
}

#pragma mark - Saving Pictures

- (void)startPhotoDownloadFrom:(NSString *)uri {
    [ASSDKLogger logStatement:@"ASMRAIDSupportUtil, sessionDownload"];
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = kASMRAIDSupportUtilTimeout;
    sessionConfig.timeoutIntervalForResource = kASMRAIDSupportUtilTimeout;
    sessionConfig.requestCachePolicy = NSURLRequestReturnCacheDataElseLoad;
    NSURLSession *dlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    
    NSURLSessionDownloadTask *task = [dlSession downloadTaskWithURL:[NSURL URLWithString:uri]];
    [task resume];
}

- (void)storePhotoFromPath:(NSString *)path {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDSupportUtil, storePhotoFromPath - path: %@", path]];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(photoSaved:didFinishSavingWithError:contextInfo:), nil);
}
                                   
- (void)photoSaved:(UIImage *)image didFinishSavingWithError:(NSError *)err contextInfo:(void *)contextInfo {
    [ASSDKLogger logStatement:@"ASMRAIDSupportUtil, photoSaved"];
    
    // report error if there was issue saving image to photos library
    if(err != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDSupportUtil, photoSaved - image saving error: %@", err.localizedDescription]];
    }
    
    [self.mainView bridgeCommandProcessed];
}

+ (NSString *)cacheDirFilePath {
    NSArray *cachePaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return cachePaths[0];
}

#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    [ASSDKLogger logStatement:@"ASMRAIDSupportsUtil, URLSession:downloadTask:didResumeAtOffset:expectedTotalBytes:"];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDSupportsUtil, URLSession:downloadTask:didWriteData:totalBytesWritten:totalBytesExpectedToWrite: - bytesWritten: %lld, totalBytesWritten: %lld, totalBytesExpectedToWrite: %lld", bytesWritten, totalBytesWritten, totalBytesExpectedToWrite]];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    [ASSDKLogger logStatement:@"ASMRAIDSupportsUtil, URLSession:downloadTask:didFinishDownloadingToURL: -- COMPLETE"];
    
    @try {
        // figure out file name from the response
        NSURLResponse *resp = downloadTask.response;
        NSString *tempFileName = resp.suggestedFilename;
        
        // check for existing photo in cache directory
        NSError *err = nil;
        NSString *tempFilePath = [[ASMRAIDSupportsUtil cacheDirFilePath] stringByAppendingPathComponent:tempFileName];
        if([[NSFileManager defaultManager] fileExistsAtPath:tempFilePath] && ![[NSFileManager defaultManager] removeItemAtPath:tempFilePath error:&err]) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDSupportsUtil, URLSession:downloadTask:didFinishDownloadingToURL: - REMOVEITEMATPATH: %@, ERROR: %@", tempFilePath, err.localizedDescription]];
            [self.mainView bridgeCommandProcessed];
        }
        
        // copy downloaded item into cache and then store into photos library
        if([[NSFileManager defaultManager] copyItemAtPath:location.path toPath:tempFilePath error:&err]) {
            [self storePhotoFromPath:tempFilePath];
        } else {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDSupportsUtil, URLSession:downloadTask:didFinishDownloadingToURL: - COPYITEM ERROR, location.path: %@, tempFilePath: %@, err: %@", location.path, tempFilePath, err.localizedDescription]];
            [self.mainView bridgeCommandProcessed];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
    @finally {
        [self.mainView bridgeCommandProcessed];
    }
}

#pragma mark - Calendar Events

- (void)createCalendarEventWith:(NSString *)jsonCalProperties {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, createCalendarEventWith: jsonProperties: %@", jsonCalProperties]];
    
    NSError *err = nil;
    NSDictionary *calendarProperties = [NSJSONSerialization JSONObjectWithData:[jsonCalProperties dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&err];
    if(err != nil) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDView, createCalendarEventWith: - JSON SERIALIATION ERROR: %@", err.localizedDescription]];
        return;
    }
    
    __weak ASMRAIDSupportsUtil *support = self;
    EKEventStore *store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *err) {
        @try {
            if(err != nil) {
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDSupportUtil, createCalendarEventWIth: - REQUEST ACCESS TO ENTITY ERROR: %@", err.localizedDescription]];
            } else {
                if(granted) {
                    EKEvent *newEvent = [EKEvent eventWithEventStore:store];
                    newEvent.calendar = store.defaultCalendarForNewEvents;
                    
                    if(calendarProperties[kASMRAIDCalendarEventDescKey] != nil) {
                        newEvent.title = calendarProperties[kASMRAIDCalendarEventDescKey];
                    }
                    
                    if(calendarProperties[kASMRAIDCalendarEventLocKey] != nil) {
                        newEvent.location = calendarProperties[kASMRAIDCalendarEventLocKey];
                    }
                    
                    if(calendarProperties[kASMRAIDCalendarEventStatusKey] != nil) {
                        NSString *status = calendarProperties[kASMRAIDCalendarEventStatusKey];
                        if([status isEqualToString:@"pending"]) {
                            newEvent.availability = EKEventAvailabilityFree;
                        } else if([status isEqualToString:@"tentative"]) {
                            newEvent.availability = EKEventAvailabilityTentative;
                        } else if([status isEqualToString:@"confirmed"]) {
                            newEvent.availability = EKEventAvailabilityBusy;
                        } else if([status isEqualToString:@"cancelled"]) {
                            newEvent.availability = EKEventAvailabilityUnavailable;
                        }
                    } else {
                        newEvent.availability = EKEventAvailabilityNotSupported;
                    }
                    
                    if(calendarProperties[kASMRAIDCalendarEventStartKey] != nil) {
                        newEvent.startDate = [support dateFromJSONString:calendarProperties[kASMRAIDCalendarEventStartKey]];
                    }
                    
                    if(calendarProperties[KASMRAIDCalendarEventEndKey] != nil) {
                        newEvent.endDate = [support dateFromJSONString:calendarProperties[KASMRAIDCalendarEventEndKey]];
                    }
                    
                    NSDictionary *recurrenceObj = calendarProperties[kASMRAIDCalendarEventRecurrenceKey];
                    if([recurrenceObj isEqual:[NSNull null]])
                        recurrenceObj = nil;
                    BOOL futureSpan = NO;
                    if (recurrenceObj[kASMRAIDCalendarRecurrenceFreqKey] != nil &&
                        recurrenceObj[kASMRAIDCalendarRecurrenceIntervalKey] != nil &&
                        recurrenceObj[kASMRAIDCalendarRecurrenceExpiresKey] != nil) {
                        NSString *freqStr = recurrenceObj[kASMRAIDCalendarRecurrenceFreqKey];
                        NSInteger interval = [recurrenceObj[kASMRAIDCalendarRecurrenceIntervalKey] integerValue];
                        NSDate *expiresDate = [support dateFromJSONString:recurrenceObj[kASMRAIDCalendarRecurrenceExpiresKey]];
                        
                        if(freqStr != nil && expiresDate != nil) {
                            EKRecurrenceRule *recurRule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:[support recurrenceFrequencyFrom:freqStr]
                                                                                                       interval:interval
                                                                                                  daysOfTheWeek:recurrenceObj[kASMRAIDCalendarRecurrenceDaysInWeekKey]
                                                                                                 daysOfTheMonth:recurrenceObj[kASMRAIDCalendarRecurrenceDaysInMonthKey]
                                                                                                monthsOfTheYear:recurrenceObj[kASMRAIDCalendarRecurrenceMonthsInYearKey]
                                                                                                 weeksOfTheYear:nil
                                                                                                  daysOfTheYear:recurrenceObj[kASMRAIDCalendarRecurrenceDaysInYearKey]
                                                                                                   setPositions:nil
                                                                                                            end:[EKRecurrenceEnd recurrenceEndWithEndDate:expiresDate]];
                            
                            if(recurRule != nil) {
                                [newEvent addRecurrenceRule:recurRule];
                                futureSpan = YES;
                            }
                        }
                    }
                    
                    if(calendarProperties[kASMRAIDCalendarEventReminderKey] != nil) {
                        id reminder = calendarProperties[kASMRAIDCalendarEventReminderKey];
                        
                        EKAlarm *alarm = nil;
                        if([reminder isKindOfClass:[NSString class]]) {
                            NSDate *reminderDate = [support dateFromJSONString:(NSString *)reminder];
                            if(reminderDate != nil)
                                alarm = [EKAlarm alarmWithAbsoluteDate:reminder];
                        } else if([reminder isKindOfClass:[NSNumber class]]) {
                            NSNumber *reminderNum = (NSNumber *)reminder;
                            alarm = [EKAlarm alarmWithRelativeOffset:[reminderNum doubleValue]];
                        }
                        
                        if(alarm != nil)
                            [newEvent addAlarm:alarm];
                    }
                    
                    err = nil;
                    if(![store saveEvent:newEvent span:futureSpan ? EKSpanFutureEvents : EKSpanThisEvent commit:YES error:&err]) {
                        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASMRAIDSupportsUtil, createCalendarEventWith - SAVE EVENT ERROR: %@", err.localizedDescription]];
                    }
                } else {
                    [ASSDKLogger logStatement:@"ASMRAIDSupportsUtil, createCalendarEventWith - Create Calendar Permission Denied"];
                }
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
    [self.mainView bridgeCommandProcessed];
}

- (NSDate *)dateFromJSONString:(NSString*)inStr {
    if(inStr.length == kASMRAIDDateFormatNoTimeZoneLen) {
        self.dateFormatter.dateFormat = kASMRAIDDateFormatNoTimeZone;
    } else if(inStr.length == kASMRAIDDateFormatNoSecondsLen) {
        self.dateFormatter.dateFormat = kASMRAIDDateFormatNoSeconds;
    } else if(inStr.length == kASMRAIDDateFormatWithSecondsLen) {
        self.dateFormatter.dateFormat = kASMRAIDDateFormatWithSeconds;
    } else {
        return nil;
    }
    
    return [self.dateFormatter dateFromString:inStr];
}

- (EKRecurrenceFrequency)recurrenceFrequencyFrom:(NSString *)inputStr {
    EKRecurrenceFrequency freq = EKRecurrenceFrequencyWeekly;
    inputStr = [inputStr lowercaseString];
    if([inputStr isEqualToString:@"daily"]) {
        freq = EKRecurrenceFrequencyDaily;
    } else if([inputStr isEqualToString:@"weekly"]) {
        freq = EKRecurrenceFrequencyWeekly;
    } else if([inputStr isEqualToString:@"monthly"]) {
        freq = EKRecurrenceFrequencyMonthly;
    } else if([inputStr isEqualToString:@"yearly"]) {
        freq = EKRecurrenceFrequencyYearly;
    }
    return freq;
}

#pragma mark - Play Video

- (void)playVideoWith:(NSString *)urlStr {
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:urlStr]];
    
    // registering notifiations
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoCompleted:)
                                                 name:AVPlayerItemFailedToPlayToEndTimeNotification
                                               object:playerItem];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoCompleted:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:playerItem];
    
    self.mraidExtPlayer = [AVPlayer playerWithPlayerItem:playerItem];
    
    [self.mainView externalVideoPlayerFromMraidBuiltWith:self.mraidExtPlayer];
}

- (void)videoCompleted:(NSNotification *)notification {
    [self.mainView externalVideoPlayerFromMraidDidComplete:self.mraidExtPlayer];
    self.mraidExtPlayer = nil;
}

- (void)initPlayerLayer {
    self.mraidVideoPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.mraidExtPlayer];
    self.mraidVideoPlayerLayer.backgroundColor = [UIColor blackColor].CGColor;
    self.mraidVideoPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
}

@end

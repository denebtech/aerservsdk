//
//  ViewController.m
//  frameworktest
//
//  Created by Arash Sharif on 11/5/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ViewController.h"

#import <AerServSDK/AerServSDK.h>
#import <Tapjoy/Tapjoy.h>
#import <Tapjoy/TJPlacement.h>

@interface ViewController ()<ASInterstitialViewControllerDelegate, ASAdViewDelegate, TJPlacementDelegate>

@property (weak, nonatomic) IBOutlet UITextField *text_placement;
@property (weak, nonatomic) IBOutlet UIButton *interstitial_button;
@property (weak, nonatomic) IBOutlet UIButton *show_button;
@property (weak, nonatomic) IBOutlet UIButton *banner_button;
@property (weak, nonatomic) IBOutlet UIButton *kill_banner_button;
@property (weak, nonatomic) IBOutlet UIButton *pause_button;
@property (weak, nonatomic) IBOutlet UIButton *play_button;
@property (weak, nonatomic) IBOutlet UISwitch *preload_switch;
@property (weak, nonatomic) IBOutlet UISwitch *sizeAdToFit_switch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *env_segCtrl;
@property (weak, nonatomic) IBOutlet UIButton *tapjoy_button;
@property (nonatomic, assign) ASEnvironmentType selEnv;
@property (nonatomic, assign) BOOL load_banner;

@property (nonatomic, weak) IBOutlet UILabel *versionLbl;

@property (strong, nonatomic) ASInterstitialViewController* adController;
@property (strong, nonatomic) ASAdView *banner_view;

@property (nonatomic, strong) TJPlacement *tjp;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.interstitial_button addTarget:self action:@selector(on_click_interstitial) forControlEvents:UIControlEventTouchUpInside];
    [self.show_button addTarget:self action:@selector(on_click_show) forControlEvents:UIControlEventTouchUpInside];
    self.show_button.enabled = NO;
    [self.banner_button addTarget:self action:@selector(on_click_banner) forControlEvents:UIControlEventTouchUpInside];
    [self.kill_banner_button addTarget:self action:@selector(on_click_kill_banner) forControlEvents:UIControlEventTouchUpInside];
    [self.pause_button addTarget:self action:@selector(on_click_pause) forControlEvents:UIControlEventTouchUpInside];
    [self.play_button addTarget:self action:@selector(on_click_play) forControlEvents:UIControlEventTouchUpInside];
    [self.tapjoy_button addTarget:self action:@selector(on_click_tapjoy) forControlEvents:UIControlEventTouchUpInside];
    [self.versionLbl setText:[ASFileManager readVersionFromBundle]];
    self.selEnv = kASEnvProduction;
    
    #if kUsePredefinedPLC
    self.text_placement.text = kPredefinedPLC;
    #endif
    
    #if kTogglePreload
    [self.preload_switch setOn:YES animated:NO];
    #endif
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    #if kLoadInterstitialOnStart
    __weak ViewController *vc = self;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [vc on_click_interstitial];
    });
    #endif
    
    #if kLoadBannerOnStart
    __weak ViewController *vc = self;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [vc on_click_banner];
    });
    #endif
    
    #if kToggleStagingOnStart
    self.selEnv = kASEnvStaging;
    self.env_segCtrl.selectedSegmentIndex = 1;
    #endif
    
    // This method requests the tapjoy server for current virtual currency of the user.
    //Get currency
    [Tapjoy getCurrencyBalanceWithCompletion: ^ (NSDictionary * parameters, NSError * error) {
        if (error) {
            //Show error message
            NSLog(@"getCurrencyBalance error: %@", [error localizedDescription]);
        } else {
            //Update currency value of your app
            NSLog(@"getCurrencyBalance returned %@: %d", parameters[@"currencyName"], [parameters[@"amount"] intValue]);
        }
    }];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.versionLbl.hidden = kIS_LANDSCAPE;
    
    if(self.banner_view != nil) {
        CGFloat viewWidth, viewHeight = 0.0f;
        if(kIS_LANDSCAPE) {
            viewWidth = kIS_iOS_8_OR_LATER ? self.view.frame.size.width : self.view.frame.size.height;
            viewHeight = kIS_iOS_8_OR_LATER ? self.view.frame.size.height : self.view.frame.size.width;
        } else {
            viewWidth = self.view.frame.size.width;
            viewHeight = self.view.frame.size.height;
        }
        [self.banner_view rotateToOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
        
        CGFloat bannerContentWidth = self.banner_view.adContentSize.width;
        CGFloat xPos = (bannerContentWidth > viewWidth) ? 0.0f : (viewWidth - bannerContentWidth)/2;
        CGFloat yPos = viewHeight - kPredefinedBannerAdSize;
        CGFloat width = (bannerContentWidth > viewWidth) ? viewWidth : bannerContentWidth;
        self.banner_view.frame = CGRectMake(xPos, yPos, width, kPredefinedBannerAdSize);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper

- (void)preloadReadyAlert {
    NSString *message = @"Preload Ready";
    [ASAlertManager showAlertWithMessage:message forTimeInt:kPopUpDisplayTime];
}

#pragma mark - IBAction Methods

- (IBAction)envChange:(id)sender {
    NSLog(@"ViewController, envChange - selEnv: %d", (int)self.env_segCtrl.selectedSegmentIndex);
    switch (self.env_segCtrl.selectedSegmentIndex) {
        case 0:
            self.selEnv = kASEnvProduction;
            break;
        case 1:
            self.selEnv = kASEnvStaging;
            break;
        case 2:
            self.selEnv = kASEnvDevelopment;
            break;
        default:
            break;
    }
}

- (void)on_click_show {
    if(self.preload_switch.on) {
        if(self.adController != nil)
            [self.adController showFromViewController:self];
        
        if(self.banner_view != nil) {
            [self.banner_view showPreloadedBanner];
            self.kill_banner_button.hidden = NO;
        }
        
        self.show_button.enabled = NO;
    }
}

#pragma mark - Interstitial Methods

- (void)interstitialCleanUp {
    if(self.banner_view != nil) {
        self.kill_banner_button.hidden = YES;
        [self.banner_view removeFromSuperview];
        self.banner_view = nil;
    }
    self.adController = nil;
}

- (void)on_click_interstitial {
    [self interstitialCleanUp];
    self.adController = [ASInterstitialViewController viewControllerForPlacementID:self.text_placement.text withDelegate:self];
	self.adController.isPreload = self.preload_switch.on;
    self.adController.env = self.selEnv;
    self.show_button.enabled = NO;
		
	self.load_banner = NO;
    self.adController.keyWords = @[@"key", @"word"];
//    self.adController.pubKeys = @{ @"animal" : @"dog", @"mood" : @"Very Happy!" };
    self.adController.userId = kTestUserID;
//    self.adController.showOutline = YES;
//    self.adController.timeoutInterval = 500;
    self.adController.locationServicesEnabled = YES;
    [self.adController loadAd];
}

#pragma mark - ASInterstitialViewControllerDelegate Methods

- (void)interstitialViewControllerAdLoadedSuccessfully:(ASInterstitialViewController *)viewController {
	NSLog(@"Interstitial ad loaded");
    [self.adController showFromViewController:self];
}

- (void)interstitialViewControllerAdFailedToLoad:(ASInterstitialViewController*)viewController withError:(NSError*)error {
    NSLog(@"Interstitial ad failed to load with error: %@", error);
    
    #if kContinousLoad
    [ASAlertManager showAlertWithMessage:error.localizedDescription forTimeInt:kPopUpDisplayTime/4];
    [self.adController loadAd];
    #else
    [ASAlertManager showAlertWithMessage:error.localizedDescription forTimeInt:kPopUpDisplayTime];
    #endif
}

- (void)interstitialViewControllerDidPreloadAd:(ASInterstitialViewController *)viewController {
    NSLog(@"Interstitial ad has preloaded");
    self.show_button.enabled = YES;
    [self preloadReadyAlert];
}

- (void)interstitialViewControllerAdDidComplete:(ASInterstitialViewController *)viewController {
    NSLog(@"Interstitial ad did complete");
}

- (void)interstitialViewControllerAdWasTouched:(ASInterstitialViewController *)viewController {
    NSLog(@"Interstitial ad was touched");
}

- (void)interstitialViewControllerDidAppear:(ASInterstitialViewController *)viewController {
    NSLog(@"Interstitial ad did appear");
}

- (void)interstitialViewControllerDidDisappear:(ASInterstitialViewController *)viewController {
	NSLog(@"Interstitial ad did disappear");
    self.adController = nil;
    self.show_button.enabled = NO;
    
    #if kContinousLoad
    [self on_click_interstitial];
    #endif
}

- (void)interstitialViewControllerDidVirtualCurrencyLoad:(ASInterstitialViewController *)viewController vcData:(NSDictionary *)vcData {
    NSLog(@"Interstitial ad did load virtual currency: %@", vcData);
}

- (void)interstitialViewControllerDidVirtualCurrencyReward:(ASInterstitialViewController *)viewController vcData:(NSDictionary *)vcData {
    NSLog(@"Interstitial ad did reward virtual currency: %@", vcData);
}

- (void)interstitialViewController:(ASInterstitialViewController *)viewController didFireAdvertiserEventWithMessage:(NSString *)msg {
    NSLog(@"Interstitial ad did fire advertiser event - msg: %@", msg);
}

- (void)interstitialViewController:(ASInterstitialViewController *)viewController didShowAdWithTransactionInfo:(NSDictionary *)transcationData {
    NSLog(@"Interstitial ad did show with transaction info: %@", transcationData);
}

#pragma mark - Banner Methods

- (void)on_click_banner {
    if (self.banner_view != nil) {
        self.kill_banner_button.hidden = YES;
        [self.banner_view removeFromSuperview];
        self.banner_view = nil;
    }
    self.banner_view = [ASAdView viewWithPlacementID:self.text_placement.text andAdSize:ASBannerSize];
    self.show_button.enabled = NO;
	self.banner_view.isPreload = self.preload_switch.on;
    self.banner_view.env = self.selEnv;
    self.adController = nil;
	
	self.load_banner = YES;
    self.banner_view.keyWords =  @[@"key", @"word"];
    self.banner_view.userId = kTestUserID;
    self.banner_view.delegate = self;
//    self.banner_view.timeoutInterval = 500;
    self.banner_view.bannerRefreshTimeInterval = kBannerRefreshInterval;
    self.banner_view.locationServicesEnabled = YES;
    self.banner_view.sizeAdToFit = self.sizeAdToFit_switch.on;
//    self.banner_view.outlineAd = YES;
    
    self.banner_view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.banner_view];
    [self.banner_view loadAd];
    
    if(!self.preload_switch.on) {
        self.kill_banner_button.hidden = NO;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)on_click_kill_banner {
    if(self.banner_view != nil) {
        [self.banner_view cancel];
        [self.banner_view removeFromSuperview];
        self.banner_view = nil;
        self.kill_banner_button.hidden = YES;
    }
}

- (void)on_click_pause {
    if(self.banner_view != nil) {
        [self.banner_view pause];
    }
}

- (void)on_click_play {
    if(self.banner_view != nil) {
        [self.banner_view play];
    }
}

- (void)adSizedChanged:(ASAdView *)banner_view {
    [self viewDidLayoutSubviews];
}

#pragma mark - Tapjoy

- (void)on_click_tapjoy {
    self.tjp = [TJPlacement placementWithName:kTapjoyPlacementName delegate:self];
    [self.tjp requestContent];
}

#pragma mark - TJPlacementDelegate

- (void)requestDidSucceed:(TJPlacement *)placement {
    NSLog(@"-- requestDidSucceed");
    
    if(self.tjp.isContentReady) {
        [self.tjp showContentWithViewController:self];
    } else {
        NSLog(@"-- content not ready");
    }
}

- (void)requestDidFail:(TJPlacement *)placement error:(NSError *)error {
    NSLog(@"-- requestDidFail:error: - err: %@", error.localizedDescription);
}

- (void)contentIsReady:(TJPlacement *)placement {
    NSLog(@"-- contentIsReady");
    [self.tjp showContentWithViewController:self];
}

#pragma mark - ASAdViewDelegate

- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}

- (void)adViewDidFailToLoadAd:(ASAdView *)adView withError:(NSError *)error {
	NSLog(@"Failed to load banner ad with error: %@", error.localizedDescription);
    
    [self.banner_view cancel];
    self.kill_banner_button.hidden = YES;
    [self.banner_view removeFromSuperview];
    self.banner_view = nil;
    
    #if kContinousLoad
    [ASAlertManager showAlertWithMessage:error.localizedDescription forTimeInt:kPopUpDisplayTime/4];
    
    __weak ViewController *vc = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [vc on_click_banner];
    });
    #else
    [ASAlertManager showAlertWithMessage:error.localizedDescription forTimeInt:kPopUpDisplayTime];
    #endif
}

- (void)adViewDidLoadAd:(ASAdView *)adView {
    NSLog(@"Banner ad did load");
    [self viewDidLayoutSubviews];
}

- (void)adViewDidPreloadAd:(ASAdView *)adView {
    NSLog(@"Banner ad has preloaded");
    [self preloadReadyAlert];
    self.show_button.enabled = YES;
}

- (void)adViewDidVirtualCurrencyLoad:(ASAdView *)adView vcData:(NSDictionary *)vcData {
	NSLog(@"Banner ad with virtual currency has been loaded with data: %@", vcData);
}

- (void)adViewDidVirtualCurrencyReward:(ASAdView *)adView vcData:(NSDictionary *)vcData {
	NSLog(@"Banner ad with virtual currency has been rewarded with data: %@", vcData);
}

- (void)adWasClicked:(ASAdView *)adView {
    NSLog(@"Banner ad was clicked");
}

- (void)adView:(ASAdView *)adVidew didShowAdWithTransactionInfo:(NSDictionary *)transcationData {
    NSLog(@"Banner ad did show ad with transaction info: %@", transcationData);
}

- (void)adView:(ASAdView *)adView didFireAdvertiserEventWithMessage:(NSString *)msg {
    NSLog(@"Banner ad did fire advertiser event - msg: %@", msg);
}

@end

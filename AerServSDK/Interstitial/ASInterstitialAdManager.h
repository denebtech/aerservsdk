//
//  ASInterstitialAdFetcher.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/24/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASAdManager.h"
#import <CoreLocation/CoreLocation.h>

#define kTestMediationJson 0
#define kTestMediationFileName @"TremorMediation"
#define kTestMediationFileType @"json"

@protocol ASInterstitialAdManagerDelegate;
@class ASInterstitialAdViewController;
@class ASTransactionInfo;

@interface ASInterstitialAdManager : ASAdManager

@property (nonatomic, weak) id<ASInterstitialAdManagerDelegate> delegate;
@property (nonatomic, assign) BOOL outlineAd;

- (void)fetchAdForPlacementID:(NSString *)placementID inEnv:(ASEnvironmentType)env usingkeyWords:(NSArray*)keyWords andPubKeys:(NSDictionary*)pubKeys withUserID:(NSString *)userId onPlatform:(ASPlatformType)platform;
- (void)presentInterstitialFromViewController:(UIViewController *)controller;
- (void)cancel;
- (void)reportErrorForProvider;

@end

@protocol ASInterstitialAdManagerDelegate<NSObject>

- (void)interstitialAdManager:(ASInterstitialAdManager *)manager didFailToFetchAdwithError:(NSError *)error;
- (void)interstitialAdManagerCreatedAdController:(ASInterstitialAdManager *)manager;
- (void)interstitialAdManagerDidPreload:(ASInterstitialAdManager *)manager;
- (void)interstitialAdManagerAdDidExpire:(ASInterstitialAdManager *)manager;
- (void)interstitialAdManagerAdWillAppear:(ASInterstitialAdManager *)manager;
- (void)interstitialAdManagerAdDidAppear:(ASInterstitialAdManager *)manager;
- (void)interstitialAdManagerAdCompleted:(ASInterstitialAdManager *)manager;
- (void)interstitialAdManagerAdWillDisappear:(ASInterstitialAdManager *)manager;
- (void)interstitialAdManagerAdDidDisappear:(ASInterstitialAdManager *)manager;
- (void)inerstititalAdManagerAdWasTouched:(ASInterstitialAdManager *)manager;
- (void)didVirtualCurrencyLoad:(NSDictionary *)vcData;
- (void)didVirtualCurrencyReward:(NSDictionary *)vcData;
- (void)AVPlayerCreated:(AVPlayer* )avPlayer;
- (void)interstitialAdManager:(ASInterstitialAdManager *)manager didFireAdvertiserEventWithMessage:(NSString *)msg;
- (void)interstitialAdManager:(ASInterstitialAdManager *)manager didFindAdTransactionInfo:(ASTransactionInfo *)transactionInfo;

@end

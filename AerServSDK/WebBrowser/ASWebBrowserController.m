//
//  ASWebBrowserController.m
//  AerServSDK
//
//  Created by Scott A Andrew on 4/16/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASWebBrowserController.h"

const NSInteger toolbarHeight = 44;

typedef NS_ENUM(NSUInteger, ASWebBrowserControlIndex) {
    ASWebBrowserControlBack,
    ASWebBrowserControlForward,
    ASWebBrowserControlRefresh,
    ASWebBrowserControlDone = ASWebBrowserControlRefresh + 2
};

@interface ASWebBrowserController ()<UIWebViewDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) UIToolbar *toolbar;
@property (nonatomic, strong) UIWebView *webView;

@property (nonatomic, strong) UIBarButtonItem *backButton;
@property (nonatomic, strong) UIBarButtonItem *forwardButton;
@property (nonatomic, strong) UIBarButtonItem *refreshButton;
@property (nonatomic, strong) UIBarButtonItem *actionButton;

@property (nonatomic, strong) UIActivityIndicatorView* activityIndicator;

@property (nonatomic, strong) UIActionSheet* actionSheet;
@property (nonatomic, strong) NSURL* URL;

// a workaround to know when we are really loaded.
@property (nonatomic, assign) NSInteger loadCount;

@end

@implementation ASWebBrowserController

- (void)dealloc {
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
    self.activityIndicator = nil;
    
    [self.webView removeFromSuperview];
    self.webView.delegate = nil;
    [self.webView loadHTMLString:@"" baseURL:nil];
    [self.webView stopLoading];
    
    [self.toolbar removeFromSuperview];
    self.toolbar.items = nil;
    self.toolbar = nil;
    
    self.backButton = nil;
    self.forwardButton = nil;
    self.refreshButton = nil;
    self.actionButton = nil;
}

- (void)viewDidLoad {
    [ASSDKLogger logStatement:@"ASWebBrowserController, viewDidLoad"];
    [super viewDidLoad];
    
    // self.toolbar.translatesAutoresizingMaskIntoConstraints = NO;
    
    // lets generate our toolbar.
    self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds) - toolbarHeight, CGRectGetWidth(self.view.bounds), toolbarHeight)];
    self.toolbar.items = [self createToolbarItems];
    self.toolbar.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.toolbar];

    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,  CGRectGetWidth(self.view.bounds), CGRectGetMinY(self.toolbar.frame))];
    self.webView.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.webView.delegate = self;
  
    [self.view addSubview:self.webView];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.center = self.view.center;
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.activityIndicator];
    
    NSDictionary* views = @{@"toolbar" : self.toolbar,
                            @"webView" : self.webView,
                            @"activityIndicator" : self.activityIndicator};
    
    // setup our constraints for rotation.
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[webView]-0-[toolbar]-0-|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[toolbar]-0-|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[webView]-0-|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-[activityIndicator]-|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[activityIndicator]-|" options:0 metrics:nil views:views]];
}

- (void)loadRequest:(NSURLRequest *)request {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASWebBrowserController, loadRequest: - request url: %@", request.URL]];
    [self.webView loadRequest:request];
}

- (NSArray *)createToolbarItems {
    NSMutableArray *items = [NSMutableArray new];
    
    self.backButton = [[UIBarButtonItem alloc] initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
    self.forwardButton = [[UIBarButtonItem alloc] initWithTitle:@">" style:UIBarButtonItemStylePlain target:self action:@selector(onForward)];
    self.refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:nil action:@selector(onRefresh)];
    self.actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(onAction)];
    
    [items addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [items addObject:self.backButton];
    [items addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];

    [items addObject:self.forwardButton];
    [items addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];

    [items addObject:self.refreshButton];
    [items addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
 
    [items addObject:self.actionButton];
    [items addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];

    [items addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onDone)]];
    [items addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
   
    
    self.backButton.enabled = NO;
    self.forwardButton.enabled = NO;
    self.refreshButton.enabled = YES;
    
    return items;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (void)onDone {
    [ASSDKLogger logStatement:@"ASWebBrowserController - onDone"];
    @try {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        [self.delegate webBrowserControllerWasDismissed:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onBack {
    [ASSDKLogger logStatement:@"ASWebBrowserController - onBack"];
    @try {
        [self dismissActionSheet];
        [self.webView goBack];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onForward {
    [ASSDKLogger logStatement:@"ASWebBrowserController - onForward"];
    @try {
        [self dismissActionSheet];
        [self.webView goForward];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onRefresh {
    [ASSDKLogger logStatement:@"ASWebBrowserController - onRefresh"];
    @try {
        [self dismissActionSheet];
        [self.webView reload];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)onAction {
    if (self.actionSheet != nil)
        return;
    
    [ASSDKLogger logStatement:@"ASWebBrowserController, onAction"];
    @try {
        self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Open in Safari", nil];
        
        if([self.actionSheet respondsToSelector:@selector(showFromBarButtonItem:animated:)]) {
            [self.actionSheet showFromBarButtonItem:self.actionButton animated:YES];
        }
        else {
            [self.actionSheet showFromToolbar:self.toolbar];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - UIActionSheetDelegate Methods

- (void)dismissActionSheet {
    [self.actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    self.actionSheet.delegate = nil;
    self.actionSheet = nil;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    @try {
        self.actionSheet = nil;
        if (buttonIndex == 0) {
            // Open in Safari.
            [ASSDKLogger logStatement:@"ASWebBroswer, actionSherr:clickedButtonAtIndex:0 - Open in Safari"];
            [[UIApplication sharedApplication] openURL:self.URL];
        }
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - UIViewController Default Methods

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.webView stopLoading];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIWebViewDelegate Methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    self.URL = request.URL;
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASWebBrowserController, webView:shouldStartLoadWithRequest:navigationType: - url: %@", self.URL]];
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    self.loadCount++;
    
    self.refreshButton.enabled = YES;
    [self.activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.loadCount--;
    if (self.loadCount > 0) return;

    [self.activityIndicator stopAnimating];
    self.backButton.enabled = self.webView.canGoBack;
    self.forwardButton.enabled = self.webView.canGoForward;
    self.refreshButton.enabled = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASWebBrowserController, webView:didFailLoadWithError: - error: %@", error.localizedDescription]];
    self.loadCount--;
    
    [self.activityIndicator stopAnimating];
    self.backButton.enabled = self.webView.canGoBack;
    self.forwardButton.enabled = self.webView.canGoForward;
    self.refreshButton.enabled = YES;
}

@end

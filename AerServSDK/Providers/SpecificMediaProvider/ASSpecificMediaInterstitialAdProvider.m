//
//  SpecificMediaProvider.m
//  SpecificMediaProvider
//
//  Created by Gauthier on 7/28/14.
//  Copyright (c) 2014 AerServ. All rights reserved.
//

#import "ASSpecificMediaInterstitialAdProvider.h"
#import "Vindico.h"
#import "VindicoAdView.h"
#import "VindicoAdProperties.h"
#import "VindicoAdRequest.h"


@interface ASSpecificMediaInterstitialAdProvider()
@property (nonatomic, strong) VindicoAdRequest *request;
@property (nonatomic, strong) VindicoAdView *adView;
@end

@implementation ASSpecificMediaInterstitialAdProvider

-(void) requestInterstitialAdWithPropeties:(NSDictionary*)info {
    
    NSDictionary* params = info[ASInterstitialParameter_Parameters];
    
    NSError* error;
    
    if (params[@"locationId"] == nil) {
        error = [NSError errorWithDomain:NSStringFromClass([self class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Invalid locationId"}];
    }
    
    if (error != nil) {
        [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
        return;
    }

    
    // Request an ad from Vindico with a Location ID
    self.request = [[VindicoAdRequest alloc] initWithLocationID:params[@"locationId"] adType:VindicoVideoAd];
    self.adView = [[Vindico shared] adViewWithType:VindicoVideoAd];
    
    [self.delegate interstitialAdProvider:self didLoadAd:nil];
}


-(void) showInterstitialFromRootViewController:(UIViewController*)rootViewController {
    [self.adView getAdWithRequest:self.request onCompletion:^(NSObject<VindicoAdRequestProtocol> *request, VindicoAdView *adView, NSError *error) {
        if(!error) {
            
            adView.adCompletion = ^(VindicoAdView *adView, BOOL success, NSError *error) {
                /*@TODO: the below if/else never gets called but the documentation specifies that it needs to be here.  Ad also doesn't load unless this block is registered */
                if(success) {
                    
                    [rootViewController.navigationController popViewControllerAnimated:YES];
                    [self.delegate interstitialAdProviderDidAppear:self];
                }
                else {
                    [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
                }
            };
            
            
            [self.adView presentFromViewController:rootViewController animated:YES];
            
        }
        else {
            [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
        }
    }];
}
@end



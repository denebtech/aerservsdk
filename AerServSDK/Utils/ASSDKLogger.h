//
//  ASSDKLogger.h
//  AerServSDK
//
//  Created by Vasyl Savka on 7/15/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASBaseLogger.h"

#define kASDebugPOSTURL @"https://debug.aerserv.com/sybok/"

#define kASSDKLoggerOSKey @"os"
#define kASSDKLoggerOSVKey @"osv"
#define kASSDKLoggerADIDKey @"adid"
#define kASSDKLoggerOIDKey @"oid"
#define kASSDKLoggerBundleIDKey @"bundleid"

#define kASSDKLoggerPLCKey @"plc"
#define kASSDKLoggerKeywordsKey @"keywords"
#define kASSDKLoggerPubKeysKey @"pub_keys"

#define kASSDKLoggerDNTKey @"dnt"
#define kASSDKLoggerCarrierKey @"carrier"
#define kASSDKLoggerNetworkKey @"network"
#define kASSDKLoggerMCCKey @"mcc"
#define kASSDKLoggerLatKey @"lat"
#define kASSDKLoggerLongKey @"long"

#define kASSDKLoggerSDKVKey @"sdkv"
#define kASSDKLoggerUAKey @"ua"

#define kASSDKLoggerLogsKey @"logs"

#define kASSDKLoggerExKey @"exception"
#define kASSDKLoggerExNameKey @"name"
#define kASSDKLoggerExReasonKey @"reason"
#define kASSDKLoggerExTraceKey @"stackTrace"

#define kASSDKLoggerQueueLimit 500

#define kASSDKLoggerPLCLimit 5

@interface ASSDKLogger : ASBaseLogger

// stored on singleton creation
//@property (nonatomic, copy) NSString *os;
////@property (nonatomic, copy) NSString *osv;
//@property (nonatomic, copy) NSString *adid;
//@property (nonatomic, copy) NSString *oid;
//@property (nonatomic, copy) NSString *bundleid;
@property (nonatomic, copy) NSString *cb; //cache-buster

// stored on request start
//@property (nonatomic, copy) NSString *plc;
@property (nonatomic, copy) NSString *keywords;
@property (nonatomic, copy) NSString *pubKeys;

// from NSURL+ASUtils
@property (nonatomic, assign) NSInteger dnt;
@property (nonatomic, copy) NSString *carrier;
@property (nonatomic, copy) NSString *network;
@property (nonatomic, copy) NSString *mcc; // mobile country code
@property (nonatomic, copy) NSString *coordLat;
@property (nonatomic, copy) NSString *coordLong;

// from ASMutableURLRequest Singleton
//@property (nonatomic, copy) NSString *sdkv;
//@property (nonatomic, copy) NSString *ua;

@property (atomic, strong) dispatch_queue_t logQ;
@property (nonatomic, strong) NSMutableArray *logs;

@property (nonatomic, assign) NSInteger plcCount;

+ (ASSDKLogger *)logInstance;
+ (void)startLogWithPLC:(NSString *)plcStr andKeywords:(NSArray *)keywordsArray andPubKeys:(NSDictionary*)pubKeys;
+ (void)showLogs:(BOOL)yesOrNo;
+ (void)logStatement:(NSString *)logStr;
+ (void)onException:(NSException *)ex;

@end

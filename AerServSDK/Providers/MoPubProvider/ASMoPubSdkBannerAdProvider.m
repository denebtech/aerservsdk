//
//  ASMoPubSdkBannerAdProvider.m
//  AerServSDK
//
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASMoPubSdkBannerAdProvider.h"
#import "MPAdView.h"

@interface ASMoPubSdkBannerAdProvider()<MPAdViewDelegate>

@property (nonatomic, strong) id mpAdView;
@property (nonatomic, assign) BOOL adLoaded;
@property (nonatomic, assign) BOOL adFailed;

@end

@implementation ASMoPubSdkBannerAdProvider

- (instancetype)init {
    if (self = [super initWithAdClassName:@"MPAdView" timeout:kCustomBannerAdTimeout]) {
        self.automaticallyRefreshAds = YES;
    }
    return self;
}

- (void)initializePartnerAd:(NSDictionary *)properties {
    // Do nothing.  There is nothing to be initialized.
}

- (BOOL)hasPartnerAdInitialized {
    // Since there is nothing to be initialized, always return YES
    return YES;
}

- (BOOL)hasPartnerAdFailedToInitialize {
    // Since there is nothing to be initialized, always assume initialization didn't fail and return NO
    return NO;
}

- (void)loadPartnerAd:(NSDictionary *)properties {
    self.adLoaded = NO;
    self.adFailed = NO;
    
    Class mpAdViewClass = NSClassFromString(@"MPAdView");
    NSString *adUnitId = [self valueInProperty:properties forKey:@"MoPubAdUnit"];
    
    // Set ad size based on whether we have a phone or tablet
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.mpAdView = [[mpAdViewClass alloc] initWithAdUnitId:adUnitId size:CGSizeMake(728, 90)];
    } else {
        self.mpAdView = [[mpAdViewClass alloc] initWithAdUnitId:adUnitId size:CGSizeMake(320, 50)];
    }
    
    [self.mpAdView setDelegate:self];
    [self.mpAdView loadAd];
}

- (BOOL)hasPartnerAdLoaded {
    return self.adLoaded;
}

- (BOOL)hasPartnerAdFailedToLoad {
    return self.adFailed;
}

- (void)showPartnerAd {
    // MoPub shows the ad when loadAd is called, so we don't need another call to show ad.  (Some partners split this into two calls.)
}

- (UIView *)partnerAdView {
    // MoPub provides its own ad view.  Return that here.
    return self.mpAdView;
}

- (void)terminatePartnerAd {
    [self cleanupAd];
}

- (void)cleanupAd {
    [self killRefreshTimer];
    [self.mpAdView setDelegate:nil];
    [self.mpAdView removeFromSuperview];
}

#pragma mark - MPAdViewDelegate

- (UIViewController *)viewControllerForPresentingModalView {
    return [self asBannerViewController];
}

- (void)adViewDidLoadAd:(MPAdView *)view {
    // No need to let AerServ SDK know that ad has loaded.  This is communicated in hasPartnerAdLoaded.
    [ASSDKLogger logStatement:@"ASMoPubSdkBannerAdProvider, adViewDidLoadAd"];
    self.adLoaded = YES;
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
    [self scheduleRefresh];
}

- (void)adViewDidFailToLoadAd:(MPAdView *)view {
    // No need to let AerServ SDK know that ad has failed to load.  This is communicated in hasPartnerAdFailedToLoad.
    [ASSDKLogger logStatement:@"ASMoPubSdkBannerAdProvider, adViewDidFailToLoadAd"];
    self.adFailed = YES;
    [self cleanupAd];
}

- (void)willPresentModalViewForAd:(MPAdView *)view {
    [ASSDKLogger logStatement:@"ASMoPubSdkBannerAdProvider, willPresentModalViewForAd"];
    [self asPartnerBannerAdWasClicked];
}

- (void)willLeaveApplicationFromAd:(MPAdView *)view {
    [ASSDKLogger logStatement:@"ASMoPubSdkBannerAdProvider, willLeaveApplicationFromAd"];
    [self asPartnerBannerWillLeaveApplication];
    [self cleanupAd];
}

@end

//
//  ASPubSettingUtils.m
//  AerServSDK
//
//  Created by Arash Sharif on 3/3/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASPubSettingUtils.h"

@interface ASPubSettingUtils()

@end

@implementation ASPubSettingUtils

+ (NSNumber *)getOffsetAsPercent:(NSString *)offset videoDuration:(double)duration {
    
    //init offset to -1 for does not apply
    NSNumber *offsetPercent = [[NSNumber alloc] initWithInt:-1];
    
    //make sure there is an offset passed
    if(offset) {
        
        //if it's already a percent parse it out and return it as a decimal
        if([offset rangeOfString:@"%"].location != NSNotFound) {
            
            double skipOffsetPercentDouble = [[offset stringByReplacingOccurrencesOfString:@"%" withString:@""] doubleValue] / 100;
            offsetPercent = [[NSNumber alloc] initWithDouble:skipOffsetPercentDouble];
            
        } else if( duration > 0 ) { //otherwise make sure duration is greater than 0 and convert offset string to percent
            
            NSArray *parts = [offset componentsSeparatedByString:@":"];
            if( [parts count] == 3) {
                
                NSNumber *hours = [[NSNumber alloc] initWithDouble:[parts[0] doubleValue]];
                NSNumber *minutes = [[NSNumber alloc] initWithDouble:[parts[1] doubleValue]];
                NSNumber *seconds = [[NSNumber alloc] initWithDouble:[parts[2] doubleValue]];
                
                double skipOffsetPercentDouble = ([hours integerValue] * 60 * 60) + ([minutes integerValue] * 60) + [seconds doubleValue];
                offsetPercent = [[NSNumber alloc] initWithDouble:skipOffsetPercentDouble / duration];
            }
            
        }
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASOffsetUtils, getOffsetAsPercent:videoDuration: - offsetPercent: %@", offsetPercent]];
    }
    
    return offsetPercent;
}

@end

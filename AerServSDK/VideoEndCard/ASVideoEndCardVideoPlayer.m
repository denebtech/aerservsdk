//
//  ASVideoEndCardVideoPlayer.m
//  AerServSDK
//
//  Created by Albert Zhu on 4/27/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASVideoEndCardVideoPlayer.h"

#import "ASVideoEndCardAd.h"
#import "ASVideoEndCardVideoPlayerView.h"
#import "ASVideoView.h"

#import "ASVASTEvent.h"
#import "ASPubSettingUtils.h"
#import "ASCountdownLabel.h"
#import "ASCategoryHelper.h"
#import "ASMoatKitUtil.h"
#import "ASCommManager.h"

#import "AVPlayer+StateChange.h"

static void *ASVideoEndCardVideoPlayer_StatusObservation = &ASVideoEndCardVideoPlayer_StatusObservation;
static void *ASVideoEndCardPlayerItem_BufferObservation = &ASVideoEndCardPlayerItem_BufferObservation;

@interface ASVideoEndCardVideoPlayer ()

@property (nonatomic, strong) ASVideoEndCardAd *vecAd;

@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) id periodicTimeObserver;

@property (nonatomic, strong) ASVideoEndCardVideoPlayerView *view;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestRecog;

@property (nonatomic, strong) NSNumber *offsetPercent;
@property (nonatomic, assign) BOOL offsetApplied;
@property (nonatomic, strong) ASCountdownLabel *cdLbl;

@property (nonatomic, assign) BOOL didStart;
@property (nonatomic, assign) BOOL didSendPreloadReady;

@property (nonatomic, assign) CMTime currTime;
@property (nonatomic, strong) UIImageView *seekScreen;

@end

@implementation ASVideoEndCardVideoPlayer

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, dealloc"];
    
    [self finishAdWithCleanUp];
    _offsetPercent = nil;
    
    if(_view != nil) {
        [_view.videoView attachPlayer:nil];
        if(_tapGestRecog != nil) {
            [_view removeGestureRecognizer:_tapGestRecog];
            _tapGestRecog = nil;
        }
        if(_cdLbl != nil) {
            [_cdLbl removeFromSuperview];
            _cdLbl = nil;
        }
        
        [_view removeFromSuperview];
        _view = nil;
    }
    
    if(_periodicTimeObserver != nil) {
        [_player removeTimeObserver:_periodicTimeObserver];
        _periodicTimeObserver = nil;
    }
    _player = nil;
    _playerItem = nil;
    
    _vecAd = nil;
}

- (instancetype)initWithVideoEndCardAd:(ASVideoEndCardAd *)vecAd andDelegate:(id<ASVideoEndCardVideoPlayerDelegate>)delegate {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, initWithVideoEndCardAd:andDelegate:"];
    if(self = [super init]) {
        _delegate = delegate;
        _vecAd = vecAd;
        [self setupPlayerItemWithURL:_vecAd.mediaFileUrl];
        [self setupPlayerWithPlayerItem];
        [self setupPlayerView];
        [self setupNotificaitons];
    }
    
    return self;
}

+ (instancetype)videoPlayerWithVideoEndCardAd:(ASVideoEndCardAd *)vecAd andDelegate:(id<ASVideoEndCardVideoPlayerDelegate>)delegate {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, videoPlayerWithVideoEndCardAd:andDelegate:"];
    return [[ASVideoEndCardVideoPlayer alloc] initWithVideoEndCardAd:vecAd andDelegate:delegate];
}

#pragma mark - Public Methods

- (void)playAd {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, playAd"];
    if(!self.didStart) {
        
        // start MOAT tracking if it's enabled
        if([ASMoatKitUtil checkForUseOfMoat]) {
            [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, startAd - START MOAT TRACKER"];
            
            [ASMoatKitUtil startMoatVideoTrackerWithAVPlayer:self.player
                                              andPlayerLayer:self.view.videoView.playerLayer
                                                  insideView:[self.delegate videoEndCardVideoPlayerContainingView:self]];
        }
        
        [self.player changeStateToPlay];
        self.didStart = YES;
    }
}

- (void)setViewFrame:(CGRect)frame {
    [self.view changeFrame:frame];
}

#pragma mark - Player Item & Status Observations

- (void)setupPlayerItemWithURL:(NSURL *)url {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, setupPlayerItemWithURL:"];
    self.playerItem = [AVPlayerItem playerItemWithURL:url];
    
    if(self.playerItem != nil) {
        [self.playerItem addObserver:self
                          forKeyPath:kAVPlayerItemKeyStatus
                             options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew
                             context:ASVideoEndCardVideoPlayer_StatusObservation];
        
        [self.playerItem addObserver:self
                          forKeyPath:kAVPlayerItemKeyPlaybackBufferFull
                             options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew
                             context:ASVideoEndCardPlayerItem_BufferObservation];
        
        [self.playerItem addObserver:self
                          forKeyPath:kAVPlayerItemKeyPlaybackLikelyToKeepUp
                             options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew
                             context:ASVideoEndCardPlayerItem_BufferObservation];
     }
}

- (void)observeAVPlayerWithStatus:(AVPlayerItemStatus)status {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, observeAVPlayerWithStatus:"];
    switch(status) {
        case AVPlayerItemStatusReadyToPlay: {
            [self.view.videoView attachPlayer:self.player];
            
            if(!self.vecAd.isPreload)
                [self.delegate videoEndCardVideoPlayerReadyToPlay:self];
            break;
        }
        case AVPlayerItemStatusFailed: {
            NSError *err = [NSError errorWithDomain:NSStringFromClass([ASVideoEndCardVideoPlayer class])
                                               code:100
                                           userInfo:@{NSLocalizedDescriptionKey : @"Video could not be accessed correctly"}];
            
            [self.delegate videoEndCardVideoPlayer:self didFailWithError:err];
            break;
        }
        default:
            break;
    }
}

- (void)observerBufferStatus {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, observeBufferStatus"];
    if(self.playerItem.playbackLikelyToKeepUp || self.playerItem.playbackBufferFull) {
        if(self.didStart && ![ASCategoryHelper instance].videoPauseState)
            [self.player play];
        else if(!self.didStart) {
            if(self.vecAd.isPreload && !self.didSendPreloadReady) {
                [self.delegate videoEndCardVideoPlayerReadyToPlay:self];
                self.didSendPreloadReady = YES;
            }
        }
    } else if(self.playerItem.playbackBufferEmpty) {
        [self.player pause];
    }
}

- (void)observeValueForKeyPath:(NSString*)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, observeValueForKeyPath:ofObject:change:context:"];
    @try {
        if(context == ASVideoEndCardVideoPlayer_StatusObservation) {
            AVPlayerItemStatus status = [change[NSKeyValueChangeNewKey] integerValue];
            
            __weak ASVideoEndCardVideoPlayer *vecVidPlayer = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                @try {
                    [vecVidPlayer observeAVPlayerWithStatus:status];
                } @catch(NSException *exception) {
                    [ASSDKLogger logStatement:@"ASVideoEndCard, observeValueForKeyPath:ofObject:change:context:, statusObservation - EXCEPTION CAUGHT"];
                    [ASSDKLogger onException:exception];
                }
            });
        } else if(context == ASVideoEndCardPlayerItem_BufferObservation) {
            [self observerBufferStatus];
        }
    } @catch (NSException *exception) {
        [ASSDKLogger logStatement:@"ASVideoEndCard, observeValueForKeyPath:ofObject:change:context: - EXCEPTION CAUGHT"];
        [ASSDKLogger onException:exception];
    }
}

- (void)removePlayerItemAndObservations {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, removePlayerItemAndObservations"];
    if(!self.playerItem) return;
    
    [self.playerItem removeObserver:self forKeyPath:kAVPlayerItemKeyStatus];
    [self.playerItem removeObserver:self forKeyPath:kAVPlayerItemKeyPlaybackBufferFull];
    [self.playerItem removeObserver:self forKeyPath:kAVPlayerItemKeyPlaybackLikelyToKeepUp];
    self.playerItem = nil;
}

#pragma mark - Player & Time Observations

- (void)observeTime:(CMTime)time {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVideoEndCardVideoPlayer, observeTime: - time: %0.2f", CMTimeGetSeconds(time)]];
    if(CMTIME_IS_INVALID(time) || !self.didStart) {
        self.cdLbl.hidden = YES;
        self.cdLbl.text = @"";
    } else {
        self.cdLbl.hidden = NO;
        self.cdLbl.text = [NSString stringWithFormat:@"%d", (int)(CMTimeGetSeconds(self.playerItem.duration) - CMTimeGetSeconds(time))];
    
        double viewedPercentage = CMTimeGetSeconds(time)/CMTimeGetSeconds(self.playerItem.duration);
        
        if(viewedPercentage >= kVideo0Percentage && self.vecAd.videoStart.canBeSent) {
            [ASCommManager sendAsynchronousRequestsForEvent:self.vecAd.videoStart];
            
            [self.delegate videoEndCardVideoPlayer:self didFireAdvertiserEventWithMessage:@"End Card Video Player Fires Video Start Event"];
        }
        
        if(!self.offsetApplied && viewedPercentage > [self.offsetPercent doubleValue]) {
            [self.delegate videoEndCardVideoPlayerWillShowSkip:self];
            self.offsetApplied = YES;
        }
    }
}

- (void)setupPlayerWithPlayerItem {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, setupPlayerWithPlayerItem"];
    if(self.playerItem != nil)
        self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    
    if(self.player != nil) {
        
        // close offfset setup
        self.offsetPercent = [NSNumber numberWithInt:-1];
        if(self.vecAd.closeOffset != nil) {
            self.offsetPercent = [ASPubSettingUtils getOffsetAsPercent:self.vecAd.closeOffset videoDuration:CMTimeGetSeconds(self.playerItem.duration)];
        }
        
        if([self.offsetPercent intValue] < 0) {
            self.offsetApplied = YES;
        }
        
        // setting up periodic timer check
        __weak ASVideoEndCardVideoPlayer *vecPlayer = self;
        self.periodicTimeObserver = [self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1.0f, 1)
                                                                              queue:NULL
                                                                         usingBlock:^(CMTime time){
                                                                             @try {
                                                                                 [vecPlayer observeTime:time];
                                                                             } @catch (NSException *exception) {
                                                                                 [ASSDKLogger logStatement:@"ASVideoEndCard, setupPlayerWithPlayerItem - player addPeriodicTimeObservForInterval:queue:usingBlock: - EXCEPTION CAUGHT"];
                                                                                 [ASSDKLogger onException:exception];
                                                                             }
                                                                         }];
    }
}

- (void)removePlayerAndTimeObseration {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, removePlayerAndTimeObseration"];
    if(!self.player) return;
    
    if(self.periodicTimeObserver != nil) {
        [self.player removeTimeObserver:self.periodicTimeObserver];
        self.periodicTimeObserver = nil;
    }
    self.offsetPercent = nil;
    
    self.player = nil;
}

#pragma mark - Player View

- (void)recognizedTap:(UIGestureRecognizer *)gesture {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, recognizedTap"];
    @try {
        [ASCommManager sendAsynchronousRequestsForEvent:self.vecAd.videoClick];
        [self.delegate videoEndCardVideoPlayerDidRecordTap:self];
        
        [self.player playPause];
        [[UIApplication sharedApplication] openURL:self.vecAd.redirectUrl];
    } @catch (NSException *exception) {
        [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, recognizedTap - EXCEPTION CAUGHT"];
        [ASSDKLogger onException:exception];
    }
}

- (void)setupPlayerView {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, setupPlayerView"];
    if(!self.player) return;
    
    self.view = [[ASVideoEndCardVideoPlayerView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    self.tapGestRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recognizedTap:)];
    [self.view addGestureRecognizer:self.tapGestRecog];
    
    if(!self.cdLbl) {
        self.cdLbl = [ASCountdownLabel new];
    }
    self.cdLbl.hidden = YES;
    self.cdLbl.text = @"";
    [self.cdLbl changePosToX:0.0f andY:(self.view.frame.size.height - kCountdownLblH)];
    [self.view addSubview:self.cdLbl];
}

- (void)removePlayerView {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, removePlayerView"];
    if(!self.view) return;
    
    [self.view.videoView attachPlayer:nil];
    
    if(self.tapGestRecog != nil) {
        [self.view removeGestureRecognizer:self.tapGestRecog];
        self.tapGestRecog  = nil;
    }
    
    if(self.cdLbl != nil) {
        [self.cdLbl removeFromSuperview];
        self.cdLbl = nil;
    }
    
    [self.view removeFromSuperview];
    self.view = nil;
}

- (UIImage *)screenShot {
    CGRect rect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.view.layer renderInContext:context];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - Notificaiton Setup & Methods

- (void)setupNotificaitons {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, setupNotificaitons"];
    NSNotificationCenter *noteCenter = [NSNotificationCenter defaultCenter];
    
    [noteCenter addObserver:self selector:@selector(appDidGotoBackground)
                       name:UIApplicationWillResignActiveNotification object:nil];
    
    [noteCenter addObserver:self selector:@selector(appDidBecomeActive)
                       name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [noteCenter addObserver:self selector:@selector(adDidFailToFinishPlaying)
                       name:AVPlayerItemFailedToPlayToEndTimeNotification object:self.playerItem];
    
    [noteCenter addObserver:self selector:@selector(adDidFinishPlayingToEnd)
                       name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
}

- (void)removeNotifications {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, removeNotifications"];
    NSNotificationCenter *noteCenter = [NSNotificationCenter defaultCenter];
    [noteCenter removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [noteCenter removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [noteCenter removeObserver:self name:AVPlayerItemFailedToPlayToEndTimeNotification object:self.playerItem];
    [noteCenter removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
}

- (void)appDidGotoBackground {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, appDidGotoBackground"];
    @try {
        self.currTime = self.player.currentTime;
        [self.player pause];
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"---- player.currenTime: %0.2f", CMTimeGetSeconds(self.player.currentTime)]];
        
        self.seekScreen = [[UIImageView alloc] initWithImage:[self screenShot]];
        self.seekScreen.layer.opacity = 0.0f;
        [self.view addSubview:self.seekScreen];
        [self.view bringSubviewToFront:self.seekScreen];
        __weak ASVideoEndCardVideoPlayer *vecVidPlayer = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:kAnimationTime animations:^{
                vecVidPlayer.view.layer.opacity = kOpacityNone;
                vecVidPlayer.cdLbl.layer.opacity = kOpacityNone;
                vecVidPlayer.seekScreen.layer.opacity = kOpacityFull;
            } completion:^(BOOL fin){
                vecVidPlayer.view.hidden = YES;
                vecVidPlayer.cdLbl.hidden = YES;
            }];
        });
    } @catch (NSException *exception) {
        [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, appDidGotoBackground - EXCEPTION CAUGHT"];
        [ASSDKLogger onException:exception];
    }
}

- (void)appDidBecomeActive {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, appDidBecomeActive"];
    @try {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"---- player.currenTime: %0.2f", CMTimeGetSeconds(self.player.currentTime)]];
        __weak ASVideoEndCardVideoPlayer *vecVidPlayer = self;
        [self.player seekToTime:self.currTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL fin){
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"---- player.currenTime: %0.2f", CMTimeGetSeconds(self.player.currentTime)]];
            
            vecVidPlayer.view.hidden = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:kAnimationTime animations:^{
                    vecVidPlayer.view.layer.opacity = kOpacityFull;
                    vecVidPlayer.cdLbl.layer.opacity = kOpacityFull;
                    vecVidPlayer.seekScreen.layer.opacity = kOpacityNone;
                } completion:^(BOOL fin){
                    [vecVidPlayer.seekScreen removeFromSuperview];
                    vecVidPlayer.seekScreen = nil;
                    [vecVidPlayer.player play];
                    vecVidPlayer.cdLbl.hidden = NO;
                }];
            });
        }];
    } @catch (NSException *exception) {
        [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, appDidGotoBackground - EXCEPTION CAUGHT"];
        [ASSDKLogger onException:exception];
    }
}

- (void)adDidFailToFinishPlaying {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, adDidFailToFinishPlaying"];
    @try {
        NSError *err = [NSError errorWithDomain:NSStringFromClass([ASVideoEndCardVideoPlayer class])
                                           code:100
                                       userInfo:@{NSLocalizedDescriptionKey : @"Video encountered issues while playing"}];
        [self.delegate videoEndCardVideoPlayer:self didFailWithError:err];
    } @catch (NSException *exception) {
        [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, adDidFailToFinishPlaying - EXCEPTION CAUGHT"];
        [ASSDKLogger onException:exception];
    }
}

- (void)adDidFinishPlayingToEnd {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, adDidFinishPlayingToEnd"];
    @try {
        if(self.vecAd.videoComplete.canBeSent) {
            [ASCommManager sendAsynchronousRequestsForEvent:self.vecAd.videoComplete];
         
            [self.delegate videoEndCardVideoPlayer:self didFireAdvertiserEventWithMessage:@"End Card Video Player Fires Video Complete"];
        }
        
        [self.delegate videoEndCardVideoPlayerAdDidPlayCompletely:self];
    } @catch(NSException *exception) {
        [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, adDidFinishPlayingToEnd - EXCEPTION CAUGHT"];
        [ASSDKLogger onException:exception];
    }
}

#pragma mark - Clean up

- (void)finishAdWithCleanUp {
    [ASSDKLogger logStatement:@"ASVideoEndCardVideoPlayer, finishAdWithCleanUp"];
    
    // finish moat video tracker
    [ASMoatKitUtil finishMoatVideoTracker];
    
    [self removeNotifications];
    [self removePlayerView];
    [self removePlayerAndTimeObseration];
    [self removePlayerItemAndObservations];
}

@end

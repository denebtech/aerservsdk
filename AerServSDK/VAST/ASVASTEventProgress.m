//
//  ASVASTEventProgress.m
//  AerServSDK
//
//  Created by Vasyl Savka on 6/19/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASVASTEventProgress.h"
#import "NSString+ASUtils.h"

@implementation ASVASTEventProgress

- (void)dealloc {
    self.progressTimeStr = nil;
}

- (instancetype)initWithVASTEvent:(ASVASTEvent *)event {
    self = [super init];
    
    if(self) {
        [ASSDKLogger logStatement:@"ASVASTEventProgress, initWithVASTEvent:"];
        self.oneTime = event.oneTime;
        self.canBeSent = event.canBeSent;
        self.type = event.type;
        [self addURLs:event.urls];
    }
    
    return self;
}

- (NSInteger)calculateTimeInSec {    
    return [self.progressTimeStr timeIntervalValue];
}

@end

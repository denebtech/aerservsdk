//
//  ASSpotXInterstitialAdProvider.h
//  AerServSDK
//
//  Created by Vasyl Savka on 10/14/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASCustomInterstitialAdProvider.h"

#define kSpotXchangeVastSdkChannelIdKey @"SpotXchangeVastSdkChannelId"

@interface ASSpotXchangeVastInterstitialAdProvider : ASCustomInterstitialAdProvider

@end

//
//  ASVASTVideo.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/28/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVASTCreative.h"
#import "ASXMLNode.h"
#import "ASVASTEvent.h"
#import "ASVASTEventError.h"
#import "ASVASTEventProgress.h"
#import "NSString+ASUtils.h"
#import "NSURL+ASUtils.h"
#import "ASCommManager.h"

typedef NS_ENUM(NSInteger, ASVASTVideoDeliveryType) {
    ASVASTVideoDeliveryType_Streaming,
    ASVASTVideoDeliveryType_Progressive
};

@interface ASVASTCreative()

@property (nonatomic, strong) NSURL *clickThroughURL;

@property (nonatomic, strong) ASVASTEvent *clickTracking;
@property (nonatomic, strong) ASVASTEvent *firstQuartile;
@property (nonatomic, strong) ASVASTEvent *thirdQuartile;
@property (nonatomic, strong) ASVASTEvent *midpoint;
@property (nonatomic, strong) ASVASTEvent *start;
@property (nonatomic, strong) ASVASTEvent *complete;
@property (nonatomic, strong) ASVASTEvent *skip;
@property (nonatomic, strong) ASVASTEvent *creativeView;

@property (nonatomic, strong) ASVASTEvent *acceptInvitation;
@property (nonatomic, strong) ASVASTEvent *collapse;
@property (nonatomic, strong) ASVASTEvent *close;
@property (nonatomic, strong) ASVASTEvent *pause;
@property (nonatomic, strong) ASVASTEvent *resume;
@property (nonatomic, strong) ASVASTEventError *error;
@property (nonatomic, strong) ASVASTEvent *mute;
@property (nonatomic, strong) ASVASTEvent *unmute;

@property (nonatomic, strong) NSArray *progress;

// attributes for the video
@property (nonatomic, copy) NSString *type;
@property (nonatomic, assign) NSInteger bitrate;
@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, copy) NSString *skipOffset;

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) NSData *data;
@property (nonatomic, strong) NSHTTPURLResponse *response;

@property (nonatomic, assign) BOOL isVpaid;
@property (nonatomic, copy) NSString *adParams;

@end

@implementation ASVASTCreative

- (void)dealloc
{
    self.clickThroughURL = nil;
    self.clickTracking = nil;
    self.firstQuartile = nil;
    self.thirdQuartile = nil;
    self.midpoint = nil;
    self.start = nil;
    self.complete = nil;
    self.skip = nil;
    self.creativeView = nil;
    self.acceptInvitation = nil;
    self.collapse = nil;
    self.close = nil;
    self.pause = nil;
    self.response = nil;
    self.error = nil;
    self.mute = nil;
    self.unmute = nil;
    
    self.progress = nil;
    self.type = nil;
    self.skipOffset = nil;
    self.data = nil;
    self.response = nil;
    self.adParams = nil;
}

- (instancetype)initWithXMLNode:(ASXMLNode *)node withDelegate:(id<ASVASTCreativeDelegate>)delegate {
    if (self = [super init]) {
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, initWithXMLNode:withDelegate: - Creating video ad from node:\n%@", node]];
        self.delegate = delegate;
        if ([node.name isEqualToString:@"Linear"]) {
            
            //find the skipOffset if it applies
            [self findSkipOffSet:node];
            
            // lets walk down our children looking for what we need to stash.. No reason
            // for XPATH here.
            for (ASXMLNode *videoNode in node.children) {
                // lets have some we'll branch off depending on the name of the node.
                NSString *nodeName = videoNode.name;
                
                if ([nodeName isEqualToString:@"Duration"]) {
                    self.duration = [videoNode.value timeIntervalValue];
                } else if ([nodeName isEqualToString:@"MediaFiles"]) {
                    [self readMediaFile:videoNode];
                } else if ([nodeName isEqualToString:@"VideoClicks"]) {
                    [self readVideoClicks:videoNode];
                } else if ([nodeName isEqualToString:@"TrackingEvents"]) {
                    [self createEventsFromNode:videoNode];
                } else if([nodeName isEqualToString:@"AdParameters"]) {
                    self.adParams = videoNode.value;
                }
            }
            
            // sort progress array if there is one
            if(self.progress != nil) {
                self.progress = [self.progress sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
                    ASVASTEventProgress *asEvProg1 = obj1;
                    ASVASTEventProgress *asEvProg2 = obj2;
                    
                    NSInteger t1 = [asEvProg1 calculateTimeInSec];
                    NSInteger t2 = [asEvProg2 calculateTimeInSec];
                    
                    if(t1 > t2) {
                        return NSOrderedDescending;
                    } else if(t1 < t2) {
                        return NSOrderedAscending;
                    }
                    return NSOrderedSame;
                }];
            }
        }
    }
    
    return self;
}

- (void)findSkipOffSet:(ASXMLNode *)node {
    self.skipOffset = node.attributes[@"skipoffset"];
}


- (id)valueForUndefinedKey:(NSString *)key {
    return nil;
}

- (ASVASTEvent *)eventForEventTypeNamed:(NSString*)type {
    #if kVASTEventShowDebug
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, eventForEventTypeNamed: - type: %@", type]];
    #endif
    ASVASTEvent *resultingEvent = nil;
    
    if([type isEqualToString:@"progress"]) {
        resultingEvent = [ASVASTEvent new];
        resultingEvent.type = ASVASTEventType_Progress;
        resultingEvent.canBeSent = YES;
        resultingEvent.oneTime = YES;
    } else {
        // our event properties are the same name as othe tracking event types (or) click tracking
        // so we just use value for key to see an event already exists.. If not we have to
        // create one based on the request.
        resultingEvent = [self valueForKey:type];
          
        if (resultingEvent == nil) {
            if ([type isEqualToString:@"start"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_VideoStart;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = YES;
                
                self.start = resultingEvent;
            }
            else if ([type isEqualToString:@"firstQuartile"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_VideoFirstQuartile;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = YES;
                
                self.firstQuartile = resultingEvent;
            }
            else if ([type isEqualToString:@"thirdQuartile"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_VideoThirdQuartile;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = YES;
                
                self.thirdQuartile = resultingEvent;
            }
            else if ([type isEqualToString:@"midpoint"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_VideoMidpoint;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = YES;
                
                self.midpoint = resultingEvent;
            }
            else if ([type isEqualToString:@"complete"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_VideoComplete;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = YES;
                
                self.complete = resultingEvent;
            }
            else if ([type isEqualToString:@"creativeView"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_CreativeView;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = YES;
                
                self.creativeView = resultingEvent;
            }
            else if ([type isEqualToString:@"skip"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_Skip;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = YES;
                
                self.skip = resultingEvent;
            }
            else if ([type isEqualToString:@"acceptInvitation"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_AcceptInvitation;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = YES;
                
                self.acceptInvitation = resultingEvent;
            }
            else if ([type isEqualToString:@"collapse"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_Collapse;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = NO;
                
                self.collapse = resultingEvent;
            }
            else if ([type isEqualToString:@"close"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_Close;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = YES;
                
                self.close = resultingEvent;
            }
            else if ([type isEqualToString:@"pause"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_Pause;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = NO;
                
                self.pause = resultingEvent;
            }
            else if ([type isEqualToString:@"resume"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_Resume;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = NO;
                
                self.resume = resultingEvent;
            }
            else if ([type isEqualToString:@"error"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_AdError;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = NO;
                
                self.error = (ASVASTEventError *)resultingEvent;
            }
            else if ([type isEqualToString:@"mute"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_Mute;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = NO;
                
                self.mute = resultingEvent;
            }
            else if ([type isEqualToString:@"unmute"]) {
                resultingEvent = [ASVASTEvent new];
                resultingEvent.type = ASVASTEventType_Unmute;
                resultingEvent.canBeSent = YES;
                resultingEvent.oneTime = NO;
                
                self.unmute = resultingEvent;
            }
        }
    }
    
    return resultingEvent;
}

- (void)createEventsFromNode:(ASXMLNode *)node {
    // lets go through each event node.. stashing away the events we
    // need to perform.
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, createEventsFromNode - Number of events found: %lu", (unsigned long)[node.children count]]];

    for (ASXMLNode *eventNode in node.children) {
        NSString *eventType = eventNode.attributes[@"event"];
        
        // if we don't have a value we don't have an event.
        if (eventNode.value != nil) {
            
            NSURL *url = [NSURL URLWithAddress:eventNode.value];
           
            // if we need a type to be able to an event in.
            if (eventType != nil) {
                ASVASTEvent *event = [self eventForEventTypeNamed:eventType];
            
                // we don't have an entry for an event yet. Create way with an
                // empty array.
                if (event != nil && url != nil) {
                    
                    [event addURL:url];
                    
                    // progress events, set offset attribute and add to array
                    if(event.type == ASVASTEventType_Progress && eventNode.attributes[@"offset"] != nil) {
                        ASVASTEventProgress *pEvent = [[ASVASTEventProgress alloc] initWithVASTEvent:event];
                        pEvent.progressTimeStr = eventNode.attributes[@"offset"];
                        if(!self.progress) {
                            self.progress = [NSArray arrayWithObject:pEvent];
                        } else {
                            self.progress = [self.progress arrayByAddingObject:pEvent];
                        }
                    }
                }
                
            }
        }
    }
}

- (void)removeProgressEvent {
    if(self.progress.count > 0) {
        [ASSDKLogger logStatement:@"ASVASTCreative, removeProgressEvent"];
        NSMutableArray *mutableArr = [NSMutableArray arrayWithArray:self.progress];
        [mutableArr removeObjectAtIndex:0];
        self.progress = [NSArray arrayWithArray:mutableArr];
    }
}

- (void)readVideoClicks:(ASXMLNode *)clicks {
    for (ASXMLNode *node in clicks.children) {
        if ([node.name isEqualToString:@"ClickTracking"]) {
            
            // create an event if we haven't gotten one yet.
            if (self.clickTracking == nil) {
                self.clickTracking = [ASVASTEvent new];
                self.clickTracking.type = ASVASTEventType_VideoClickTracking;
                self.clickTracking.canBeSent = YES;
            }
            
            
            if (node.value != nil) {
                NSURL *url = [NSURL URLWithAddress:node.value];
                
                if (url != nil) {
                    [self.clickTracking addURL:url];
                    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, readVideoClicks: - VAST will track clicks at %@", url]];
                }
            }
            
        }
        else if ([node.name isEqualToString:@"ClickThrough"]) {
            NSURL *url = [NSURL URLWithAddress:node.value];
            
            if (url != nil) {
                self.clickThroughURL = url;
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, readVideoClicks: - VAST click through will go to %@", url]];
            }
        }
    }
}

- (BOOL)isProperVideoType:(NSString *)type {
    // allow null and blank types
    return (type == (id)[NSNull null] || type.length == 0  ||
            [type isEqualToString:@"video/mp4"] ||
            [type isEqualToString:@"video/quicktime"] ||
            [type isEqualToString:@"video/3gpp"]);
}

- (void)setSizeWithAttributes:(NSDictionary *)fileAttr {
    CGSize size = CGSizeZero;
    if(fileAttr[@"width"] != nil) {
        size.width = [fileAttr[@"width"] integerValue];
    }
    if(fileAttr[@"height"] != nil) {
        size.height = [fileAttr[@"height"] integerValue];
    }
    self.size = size;

}

- (void)readMediaFile:(ASXMLNode *)mediaFilesNode {
    // look for VPAID ad first.
    self.isVpaid = NO;
    
    for(ASXMLNode *mediaFileNode in mediaFilesNode.children) {
        if(!mediaFileNode.value) continue;

        NSDictionary *fileAttributes = [mediaFileNode attributes];
        if([fileAttributes[@"apiFramework"] isEqualToString:@"VPAID"] &&
           [fileAttributes[@"type"] containsStr:@"javascript"]) {
            self.type = fileAttributes[@"type"];
            self.url = [NSURL URLWithAddress:[mediaFileNode.value removeCharWhitespace]];
            self.size = CGSizeMake([fileAttributes[@"width"] integerValue], [fileAttributes[@"height"] integerValue]);
            self.isVpaid = YES;
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, readMeadiaFile - VPAID js file at %@", self.url]];
        }
    }
    
    if(!self.isVpaid) {
        // re-run search for VAST when there is no VPAID
        NSInteger maxBitrate = -1;
        NSInteger maxWidth = -1;
        NSInteger maxHeight = -1;
        for (ASXMLNode *mediaFileNode in mediaFilesNode.children) {
            if(!mediaFileNode.value) continue;
            
            NSDictionary *fileAttributes = [mediaFileNode attributes];
            self.type = fileAttributes[@"type"];
        
            if ([self isProperVideoType:fileAttributes[@"type"]]) {
                NSInteger seenBitrate = [fileAttributes[@"bitrate"] integerValue];
                NSInteger seenWidth = [fileAttributes[@"width"] integerValue];
                NSInteger seenHeight = [fileAttributes[@"height"] integerValue];
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, readMeadiaFile - VAST video file seen with bitrate: %ld, width: %ld, height: %ld", seenBitrate, seenWidth, seenHeight]];
                
                if(seenBitrate >= maxBitrate && seenWidth >= maxWidth && seenHeight >= maxHeight) {
                    maxBitrate = seenBitrate;
                    maxWidth = seenWidth;
                    maxHeight = seenHeight;
                    self.bitrate = maxBitrate;
                    self.url = [NSURL URLWithAddress:mediaFileNode.value];
                
                    [self setSizeWithAttributes:fileAttributes];
                }
            }
        }
        
        if(maxBitrate >= 0) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, readMeadiaFile - VAST video file with bitrate: %ld, width: %ld, height: %ld found at url: %@", maxBitrate, maxWidth, maxHeight, self.url]];
        }
    }
    
    // check url for media file, if none report vast error for missing file or no supported media types
    if(!self.url) {
        if(mediaFilesNode.children.count > 0) {
            [self.delegate vastCreative:self encountersVASTError:kVASTErrorCodeNoSupportedMediaFile];
        }
    }
}

- (void)loadAd:(ASVastCreativeLoadResultHandler)resultHandler {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, loadAd: - Load VAST creative with url: %@", self.url]];

    self.isLoading = YES;
    
    __weak ASVASTCreative *creative = self;
    [ASCommManager sendGetRequestToEndPoint:self.url forTime:kVASTCreativeTimeout endWithCompletionBlock:^(NSURLResponse *resp, NSData *data, NSError *err) {
        @try {
            BOOL success = NO;
            
            // we getting data from an HTTP server so our response should be a URL response.
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)resp;
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTCreative, loadAd: - Banner URL response: %d", (int)[httpResponse statusCode]]];
            
            // if we got an OK response we need to go ahead and copy the headers out.
            if ([httpResponse statusCode] == 200) {
                creative.data = data;
                creative.response = httpResponse;
                
                success = YES;
            }
            
            if (resultHandler != nil) {
                resultHandler(success);
            }
            
            creative.isLoading = NO;
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
}

- (void)mergeIntoCreative:(ASVASTCreative *)creative {
    [ASSDKLogger logStatement:@"ASVASTCreative, mergeIntoCreative:"];
    
    creative.size = self.size;
    creative.type = self.type;
    creative.bitrate = self.bitrate;
    creative.url = self.url;
    creative.duration = self.duration;
    creative.playOrder = self.playOrder;
    creative.clickThroughURL = self.clickThroughURL;
    creative.isVpaid = self.isVpaid;
    creative.adParams = self.adParams;
    
    if (self.clickTracking != nil) {
        if (creative.clickTracking == nil) {
            creative.clickTracking = [self.clickTracking copy];
        }
        else {
            [creative.clickTracking addURLs:self.clickTracking.urls];
        }
    }
    
    if (self.firstQuartile != nil) {
        if (creative.firstQuartile == nil) {
            creative.firstQuartile = [self.firstQuartile copy];
        }
        else {
            [creative.firstQuartile addURLs:self.firstQuartile.urls];
        }
    }
    
    if (self.thirdQuartile != nil) {
        if (creative.thirdQuartile == nil) {
            creative.thirdQuartile = [self.thirdQuartile copy];
        }
        else {
            [creative.thirdQuartile addURLs:self.thirdQuartile.urls];
        }
    }
    
    if (self.midpoint != nil) {
        if (creative.midpoint == nil) {
            creative.midpoint = [self.midpoint copy];
        }
        else {
            [creative.midpoint addURLs:self.midpoint.urls];
        }
    }
    
    if (self.start != nil) {
        if (creative.start == nil) {
            creative.start = [self.start copy];
        }
        else {
            [creative.start addURLs:self.start.urls];
        }
    }
    
    if (self.complete != nil) {
        if (creative.complete == nil) {
            creative.complete = [self.complete copy];
        }
        else {
            [creative.complete addURLs:self.complete.urls];
        }
    }
    
    if (self.skip != nil) {
        if (creative.skip == nil) {
            creative.skip = [self.skip copy];
        }
        else {
            [creative.skip addURLs:self.skip.urls];
        }
    }
    
    if(self.creativeView != nil) {
        if(creative.creativeView == nil) {
            creative.creativeView = [self.creativeView copy];
        }
        else {
            [creative.creativeView addURLs:self.creativeView.urls];
        }
    }
    
    if(self.acceptInvitation != nil) {
        if(!creative.acceptInvitation) {
            creative.acceptInvitation = [self.creativeView copy];
        }
        else {
            [creative.acceptInvitation addURLs:self.acceptInvitation.urls];
        }
    }
    
    if(self.collapse != nil) {
        if(!creative.collapse) {
            creative.collapse = [self.collapse copy];
        }
        else {
            [creative.collapse addURLs:self.collapse.urls];
        }
    }
    
    if(self.close != nil) {
        if(!creative.close) {
            creative.close = [self.close copy];
        }
        else {
            [creative.close addURLs:self.close.urls];
        }
    }
    
    if(self.pause != nil) {
        if(!creative.pause) {
            creative.pause = [self.pause copy];
        }
        else {
            [creative.pause addURLs:self.pause.urls];
        }
    }
    
    if(self.response != nil) {
        if(!creative.response) {
            creative.resume = [self.resume copy];
        }
        else {
            [creative.resume addURLs:self.resume.urls];
        }
    }
    
    if(self.error != nil) {
        if(!creative.error) {
            creative.error = [self.error copy];
        }
        else {
            [creative.error addURLs:self.error.urls];
        }
    }
    
    if(self.mute != nil) {
        if(!creative.mute) {
            creative.mute = [self.mute copy];
        }
        else {
            [creative.mute addURLs:self.mute.urls];
        }
    }
    
    if(self.unmute != nil) {
        if(!creative.unmute) {
            creative.unmute = [self.unmute copy];
        }
        else {
            [creative.unmute addURLs:self.unmute.urls];
        }
    }
    
    if (self.progress != nil) {
        if (creative.progress == nil) {
            creative.progress = [self.progress copy];
        }
        else {
            creative.progress = [creative.progress arrayByAddingObjectsFromArray:self.progress];
        }
    }
}

@end

//
//  ASAdMob.m
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/23/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "GoogleMobileAds.h"

#import "ASAdMobInterstitialAdProvider.h"
#import "ASAdMobTestDevices.h"

@interface ASAdMobInterstitialAdProvider() <GADInterstitialDelegate>

@property (nonatomic, strong) id gadInterstitial;
@property (nonatomic, assign) BOOL isPreload;
@end

@implementation ASAdMobInterstitialAdProvider

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, dealloc"];
    self.gadInterstitial = nil;
}

- (void)requestInterstitialAdWithProperties:(NSDictionary *)info isPreload:(BOOL)preload  {
    NSDictionary *parameters = info[ASInterstitialParameter_Parameters];
    self.isPreload = preload;
    
    // we need to build our request.
    self.gadInterstitial = [[[NSClassFromString(@"GADInterstitial") class] alloc] initWithAdUnitID:parameters[@"AdMobAdUnitID"]];
    
    if(self.gadInterstitial != nil && [self.gadInterstitial respondsToSelector:@selector(setDelegate:)]) {
        [self.gadInterstitial setDelegate:(id)self];
    } else {
        [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, requestInterstitialAdWithProperties:isPreload: - Could not create AdMob Interstitial class object, failing over"];
        NSError* error = [NSError errorWithDomain:NSStringFromClass([ASAdMobInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Could not link against AdMob.  Failing over"}];
        [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
	}
    
    CLLocation *location = parameters[ASInterstitialParameter_Location];
    
    [self.gadInterstitial loadRequest:(NSURLRequest*)[self requestWithLocation:location]];
}

- (id)requestWithLocation:(CLLocation *)location {
    id request = [NSClassFromString(@"GADRequest") request];
    
    if (location != nil) {
        [request setLocationWithLatitude:location.coordinate.longitude
                               longitude:location.coordinate.longitude
                                accuracy:location.horizontalAccuracy];
        [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, requestWithLocation: - Location Set"];
    } else {
        [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, requestWithLocation: - Invalid Location"];
    }
    [request setTestDevices:kASAdMobTestDevices];
    
    return request;
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, showInterstitialFromRootViewController:"];
    
    [self.gadInterstitial presentFromRootViewController:rootViewController];
}

#pragma mark - GADInterstitialDelegate handler

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, interstitialDidReceiveAd - Google AdMob Interstitial Ad did load"];
    @try {
        if (self.isPreload) {
            [self.delegate interstitialAdProvider:self didPreloadAd:ad];
        } else {
            [self.delegate interstitialAdProvider:self didLoadAd:ad];
        }
        
        [ASAdResponseLogger endAdResponseAndSendEvent];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAdMobInterstitialAdProvider, interstitial:didFailToReceiveAdWithError - Google AdMob Interstitial Ad did fail with error: %@", error.localizedDescription]];
    @try {
        [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, interstitialWillPresentScreen: - Google AdMob Interstitial Ad will present"];
    @try {
        [self.delegate interstitialAdProviderWillAppear:self];
        [self.delegate interstitialAdProviderDidAppear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, interstitialWillDismissScreen: - Google AdMob Interstitial Ad will dismiss"];
    @try {
        [self.delegate interstitialAdProviderWillDisappear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, interstitialDidDismissScreen: - Google AdMob Interstitial Ad did dismiss"];
    @try {
        [self.delegate interstitialAdProviderDidDisappear:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    [ASSDKLogger logStatement:@"ASAdMobInterstitialAdProvider, interstitialWillLeaveApplication: - Google AdMob Interstitial Ad will leave application"];
    @try {
        [self.delegate interstitialAdProviderAdWasTouched:self];
    }
    @catch (NSException *exception) {
        [ASSDKLogger onException:exception];
    }
}

@end

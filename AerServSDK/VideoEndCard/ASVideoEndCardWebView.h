//
//  ASVideoEndCardWebView.h
//  AerServSDK
//
//  Created by Albert Zhu on 4/29/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kVideoEndCardMIMEType @"text/html"
#define kVideoEndCardEncodingName @"charset=UTF-8"
#define kVideoEndCardBaseURL @"endcard.aerserv.com"

@class ASVideoEndCardAd;
@protocol ASVideoEndCardWebViewDelegate;

@interface ASVideoEndCardWebView : UIWebView

@property (nonatomic, assign) id<ASVideoEndCardWebViewDelegate> vecWebViewDeleagte;

+ (instancetype)webViewForVideoEndCardAd:(ASVideoEndCardAd *)vecAd andWebViewDelegate:(id<ASVideoEndCardWebViewDelegate>)vecWebViewDelegate;
- (void)loadEndCard;

@end

@protocol ASVideoEndCardWebViewDelegate <NSObject>

- (void)videoEndCardWebViewWasClicked:(ASVideoEndCardWebView *)vecWebView;
- (void)videoEndCardWebView:(ASVideoEndCardWebView *)vecWebView didFailWithError:(NSError *)err;
- (void)videoEndCadWebViewDidFinishLoad:(ASVideoEndCardWebView *)vecWebView;

@end

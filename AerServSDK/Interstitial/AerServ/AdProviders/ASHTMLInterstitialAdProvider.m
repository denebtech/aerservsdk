//
//  ASHTMLInterstialAdProvider.m
//  AerServSDK
//
//  Created by Scott A Andrew on 4/9/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASHTMLInterstitialAdProvider.h"
#import "ASHTMLInterstitialViewController.h"
#import "NSURL+ASUtils.h"

@interface ASHTMLInterstitialAdProvider()

@end



@implementation ASHTMLInterstitialAdProvider

// don't think this is ever hit -- AZ
- (void)processURLReponse:(NSURLResponse*)response withData:(NSData*)data {
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    
    __weak ASHTMLInterstitialAdProvider* adProvider = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // lets process our data. If we do this succesfully we are out of here. If not
        // fall out to report an error.
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASHTMLInterstitialAdProvider, processURLResponse:withData: - response.statusCode: %ld", (long)httpResponse.statusCode]];
        @try {
            if ([httpResponse statusCode] == 200) {
                if ([adProvider processData:data withResponseHeaders:[httpResponse allHeaderFields]]) {
                    return;
                }
            }
            
            // if we got here there is an error.
            [adProvider.delegate interstitialAdProvider:adProvider
                               didFailToLoadAdWithError:[NSError errorWithDomain:NSStringFromClass([ASHTMLInterstitialAdProvider class])
                                                                            code:100
                                                                        userInfo:@{NSLocalizedDescriptionKey : @"Failed to load HTML ad because the response data could not be processed"}]];
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    });
}

- (BOOL)processData:(NSData*)data withResponseHeaders:(NSDictionary *)headers {
    if ([data length] <= 1) {
        [ASSDKLogger logStatement:@"ASHTMLInterstitialAdProvider, processData:withResponseHeaders - No data to process."];
        return NO;
    }
    
    // create ASHTMLIntestitialViewController with data, headers, and properties
    NSString* urlString = self.properties[ASInterstitialParameter_Parameters][ASInterstitialParameter_Parameter_URL];
    NSURL* url = [NSURL URLWithAddress:urlString];
    self.viewControllerToPresent = [[ASHTMLInterstitialViewController alloc] initWithData:data headers:headers baseURL:url delegate:self];

    if (self.viewControllerToPresent == nil) {
        [ASSDKLogger logStatement:@"ASHTMLInterstitialAdProvider, processData:withResponseHeaders: - Init of ASHTMLInterstitialViewController failed."];
            [self.delegate interstitialAdProvider:self
                               didFailToLoadAdWithError:[NSError errorWithDomain:NSStringFromClass([ASHTMLInterstitialAdProvider class])
                                                                            code:100
                                                                        userInfo:@{NSLocalizedDescriptionKey : @"Failed to load ad"}]];
        
        return NO;
    }
    [ASSDKLogger logStatement:@"ASHTMLInterstitialAdProvider, processData:withResponseHeaders: - Init of ASHTMLInterstitialViewController success."];

    return YES;
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASHTMLInterstitialAdProvider, cancel"];
    [super cancel];
}


@end

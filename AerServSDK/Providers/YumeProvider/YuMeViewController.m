//
//  YuMeViewController.m
//  YuMeiOSSampleApp
//
//  Created by Bharath Ramakrishnan on 4/5/13.
//  Copyright (c) 2014 YuMe Advertising Pvt. Ltd.,. All rights reserved.
//

#import "YuMeViewController.h"
#import "YuMeInterface.h"

#define YUME_CONFIG_ID @"1736BgfkLcsh"

@interface YuMeViewController ()



@end //@interface YuMeViewController

@implementation YuMeViewController

static YuMeInterface *pYuMeInterface = nil;

#pragma mark -
#pragma mark View Controller


- (YuMeViewController *)initWithDelegate:(id) delegate withDomain:(NSString*) domain withServer:(NSString*) server {
    
    self.domain = domain;
    self.server = server;
    
    self = [self initWithDelegate:delegate];
    return self;

    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //instance of YuMeInterface
    pYuMeInterface = [[YuMeInterface alloc] init];
    pYuMeInterface.viewController = self;
    
    [[UIDevice currentDevice]setProximityMonitoringEnabled:NO];
    
    
    
    
}

- (void)dealloc {
    [self deInitYuMeSDK];
	pYuMeInterface = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Orientation Supports
#pragma mark -

- (BOOL)shouldAutorotate {
    [self resizeAdView];
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    [self resizeAdView];
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self resizeAdView];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self resizeAdView];
    
}

#pragma Internal methods
#pragma mark -

/*
 * This function displays a pre-fetched Ad.
 */
- (void)showAdAction {
    if ([pYuMeInterface yumeIsAdAvailable]) {
        [self createAdView];
        [self displayAd];
    }
}

/*
 * Initializes the YuMe SDK.
 */
- (BOOL)initYuMeSDK {
    BOOL bResult = FALSE;

    //if user wants to enable proximity state Enabled
	[[UIDevice currentDevice]setProximityMonitoringEnabled:YES];
    
    if(pYuMeInterface) {
        
        YuMeAdParams *params = [[YuMeAdParams alloc] init];
        
        //give AdServer URL.
        params.pAdServerUrl = self.server;//@"http://shadow01.yumenetworks.com/";
        
		//give publisher domain name.
        params.pDomainId =  self.domain;//@"1736BgfkLcsh";
        
		//Disk quota in MB. This should have a reasonable value for Ad pre-fetch to function properly.
		//The recommended value is 5 MB. If this value is <= 0, caching cannot be done even if params.bEnableCaching is set to TRUE.
        params.storageSize = 5.0f; //MB
        
        //The SDK Usage Mode.
        params.eSdkUsageMode = YuMeSdkUsageModePrefetch;
        
        //The ad type - Preroll
        params.eAdType = YuMeAdTypePreroll;
        
        //Initialize the YuMe SDK
        bResult = [pYuMeInterface yumeInit:YUME_CONFIG_ID adParams:params];
        
        //release the params object
        params = nil;
    }
    return bResult;
}

/*
 * De-Initializes the YuMe SDK.
 */
- (void)deInitYuMeSDK {
    if(pYuMeInterface)
        [pYuMeInterface yumeDeInit];
}

/*
 * Displays an Ad that was pre-fetched.
 */
- (void)displayAd {
    BOOL bResult = FALSE;
    if(pYuMeInterface) {
        bResult = [pYuMeInterface yumeShowAd:_adView viewController:self];
    }
    if (bResult)
        NSLog(@"showAd returned successfully");
}

/*
 * Creates a UIView for displaying ad.
 */
- (void)createAdView {
    if(!_adView)
        _adView = [[UIView alloc] initWithFrame:CGRectZero];
    [self resizeAdView];
    [self.view addSubview:_adView];
    _adView.backgroundColor = [UIColor clearColor];
}

/*
 * Removes the ad view.
 */
- (void)removeAdView {
    if (_adView) {
        [_adView removeFromSuperview];
        _adView = nil;
    }
}

/*
 * Displays an alert message.
 * @param msg The message to be displayed.
 * @param caption The 'Cancel' button title.
 */
- (void)displayBox:(NSString *)msg withButtonText:(NSString *)caption {
	UIWindow *parentWindow = [[UIApplication sharedApplication] keyWindow];
	
	// open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:msg
                                                   delegate:parentWindow
                                          cancelButtonTitle:caption
                                          otherButtonTitles:nil];
	[alert show];	
	alert = nil;
}

/*
 * Gets the error message from NSError object.
 * @param pError A pointer to an NSError object.
 */
- (NSString *)getErrDesc:(NSError *)pError {
	NSString *errStr = [[pError userInfo] valueForKey:NSLocalizedDescriptionKey];
	return [errStr copy];
}

/*
 * Resizes the Ad View depending on the orientation.
 */
- (void)resizeAdView {
    if (_adView)
        _adView.frame = self.view.bounds;
}

















/*

- (void)interstitialAdViewControllerViewWillAppear:(ASInterstitialAdViewController *)viewController;
- (void)interstitialAdViewControllerViewDidAppear:(ASInterstitialAdViewController *)viewController;

- (void)interstitialAdViewControllerViewWillDisappear:(ASInterstitialAdViewController *)viewController;
- (void)interstitialAdViewControllerViewDidDisappear:(ASInterstitialAdViewController *)viewController;


- (void)interstitialAdViewControllerCreatedAdSuccessfully:(ASInterstitialAdViewController *)controller;
- (void)interstitialAdViewControllerFailed:(ASInterstitialAdViewController *)controller withError:(NSError *)error;

- (void)interstitialAdViewControllerAdLoadingWasCancelled:(ASInterstitialAdViewController *)controller;
- (void)interstitialAdViewControllerAdFinished:(ASInterstitialAdViewController *)controller;
- (void)interstitialAdViewControllerAdWasTouched:(ASInterstitialAdViewController *)controller;

- (void) AVPlayerCreated:(AVPlayer *)avPlayer;
 */

@end //@implementation YuMeViewController

//
//  ASAdResponseLogger.h
//  AerServSDK
//
//  Created by Vasyl Savka on 10/28/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASBaseLogger.h"

#define kAdResponseDebug 0

#define kDurationMillisKey @"durationMillis"

#define kCategoryAdRequestStr @"adRequest"
#define kActionRequestAndRenderStr @"requestAndRender"

@interface ASAdResponseLogger : ASBaseLogger

+ (ASAdResponseLogger *)logInstance;
+ (void)startAdResponseLogWithPLC:(NSString *)plc;
+ (void)endAdResponseAndSendEvent;

@end

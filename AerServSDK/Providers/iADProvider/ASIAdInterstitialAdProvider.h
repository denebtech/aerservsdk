//
//  ASIAdInterstitialAdProvider.h
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/27/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdProvider.h"
#import <Foundation/Foundation.h>
@interface ASIAdInterstitialAdProvider : ASInterstitialAdProvider

@end

//
//  ASAdColonyDelegateProxy.h
//  AerServSDK
//
//  Created by Scott A Andrew on 4/11/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AdColony.h"

@class ASAdColonyInterstitialAdProvider;

@interface ASAdColonyDelegateProxy : NSObject<AdColonyDelegate, AdColonyAdDelegate>

+ (instancetype)sharedProxy;
- (instancetype)init;

- (void)addProvider:(ASAdColonyInterstitialAdProvider*)provider;

@end

//
//  ASUnityDelegateProxy.h
//  AerServSDK
//
//  Created by Albert Zhu on 8/17/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASUnityInstance.h"

@class ASUnityInterstitialAdProvider;

@interface ASUnityDelegateProxy : NSObject <UnityAdsDelegate>

+ (ASUnityDelegateProxy *)sharedDelegate;

@end

//
//  ASInternalBaseInterstitialAdProvider.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/9/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInternalBaseInterstitialAdProvider.h"
#import "ASInterstitialAdViewController.h"
#import "NSURL+ASUtils.h"
#import "ASMutableURLRequest.h"
#import "ASCommManager.h"
#import "ASVirtualCurrency.h"

//#import "ASHTMLInterstitialViewController.h"
//#import "ASVPAIDInterstitialViewController.h"

@interface ASInternalBaseInterstitialAdProvider()

@property (nonatomic, strong) NSDictionary* properties;
@property (nonatomic, assign) BOOL isPreload;
@property (nonatomic, strong) UIViewController *rootVC;

@end



@implementation ASInternalBaseInterstitialAdProvider

#pragma mark - ASInterstitialAdProvider Methods

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, dealloc"];
    self.viewControllerToPresent = nil;
    self.properties = nil;
    self.rootVC = nil;
}

- (void)requestInterstitialAdWithProperties:(NSDictionary*)info isPreload:(BOOL)preload {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInternalBaseInterstitialAdProvider, requestInterstitialAdWithProperties:isPreload: - preload: %d", preload]];
    
    self.properties = info;
    self.isPreload = preload;
    
    if (info[ASInterstitialParameter_HTMLData] != nil && info[ASInterstitialParameter_HTMLHeaders]) {
		
		// If VC is enabled (present in header data), fire an event to notify app that a PLC with VC has loaded
		NSDictionary *vcData = [ASVirtualCurrency getVirtualCurrencyData:info[ASInterstitialParameter_HTMLHeaders]];
		if(vcData) {
			[self.delegate interstitialAdProvider:self didVirtualCurrencyLoad:vcData];
		}
		
        if (![self processData:info[ASInterstitialParameter_HTMLData] withResponseHeaders:info[ASInterstitialParameter_HTMLHeaders]]) {
            [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, requestInterstitialAdWithProperties:isPreload: - Could not process data from properties"];
            NSError* error = [NSError errorWithDomain:NSStringFromClass([ASInternalBaseInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"No ad in response."}];
            
            [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
		}
    }
    else if (info[ASInterstitialParameter_Parameters] != nil) {
        NSString* urlString = info[ASInterstitialParameter_Parameters][ASInterstitialParameter_Parameter_URL];
        
        NSURL* url = [NSURL URLWithAddress:urlString];
        
        if (url == nil) {
            NSError* error = [NSError errorWithDomain:NSStringFromClass([ASInternalBaseInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Unable to create URL to retrieve ad from."}];
            
            [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
        }
        else {
            [self fetchAdFromURL:url];
        }
        
    }
}

- (BOOL)enableAutomaticImpressionAndClickTracking {
    // Subclasses may override this method to return NO to perform impression and click tracking
    // manually.
    return YES;
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    if (self.viewControllerToPresent) {
        self.rootVC = rootViewController;
        
        [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInternalBaseInterstitialAdProvider, showInterstitialFromRootViewControlller - Presenting %@", NSStringFromClass([self.viewControllerToPresent class])]];
        self.viewControllerToPresent.outlineAd = self.outlineAd;
        
        __weak ASInternalBaseInterstitialAdProvider *provider = self;
        void (^presentBlock)() = ^{
            @try {
                [rootViewController presentViewController:provider.viewControllerToPresent animated:YES completion:nil];
                if(provider.isPreload) {
                    [provider.viewControllerToPresent startAd];
                }
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        };
        
        if(rootViewController.presentedViewController != nil) {
            [rootViewController dismissViewControllerAnimated:NO completion:^{
                presentBlock();
            }];
        } else {
            presentBlock();
        }
        
    }
}

#pragma mark - ASInternalBaseInterstitialAdProvider Methods

- (void)fetchAdFromURL:(NSURL*)url {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInternalBaseInterstitialAdProvider, fetchAdFromURL - url: %@", url.absoluteString]];
    
    __weak ASInternalBaseInterstitialAdProvider *adProvider = self;
    [ASCommManager sendGetRequestToEndPoint:url forTime:self.timeoutInt endWithCompletionBlock:^(NSURLResponse *resp, NSData *data, NSError *err) {
        @try {
            if (err != nil) {
                [adProvider.delegate interstitialAdProvider:adProvider didFailToLoadAdWithError:err];
            }
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)resp;
            NSDictionary *headers = [httpResponse allHeaderFields];
            dispatch_async(dispatch_get_main_queue(), ^{
                @try {
                    [adProvider.delegate interstitialAdProvider:adProvider willUpdateForFailoverWithResponseHeaders:headers];
                    BOOL pass = [adProvider processData:data withResponseHeaders:headers]; // changed this to response header...
                    
                    if(!pass && adProvider != nil) {
                        NSError *err = [NSError errorWithDomain:NSStringFromClass([ASInternalBaseInterstitialAdProvider class]) code:100 userInfo:@{NSLocalizedDescriptionKey:@"Empty Response"}];
                        [adProvider.delegate interstitialAdProvider:adProvider didFailToLoadAdWithError:err];
                    }
                }
                @catch (NSException *exception) {
                    [ASSDKLogger onException:exception];
                }
            });
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
        }
    }];
}

- (BOOL)processData:(NSData*)data withResponseHeaders:(NSDictionary *)headers {
    // override me to process the data retrieved from the web.
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, processData:withResponseHeaders: - should be overridden, this returns NO"];
    return NO;
}

- (void)cancel {
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, cancel"];
    
    [self.viewControllerToPresent cancel];
    self.viewControllerToPresent = nil;
    self.properties = nil;
}

#pragma mark - ASInterstitialAdViewControllerDelegate Methods from ASInterstitialAdViewController.h

- (BOOL)shouldShowInterstitialAd {
    return self.shouldShowAd;
}

- (void)interstitialAdViewControllerViewWillAppear:(ASInterstitialAdViewController*)viewController {
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerViewWillAppear:"];
    [self.delegate interstitialAdProviderWillAppear:self];
}

- (void)interstitialAdViewControllerViewDidAppear:(ASInterstitialAdViewController*)viewController {
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerViewDidAppear:"];
    [self.delegate interstitialAdProviderDidAppear:self];
}

- (void)interstitialAdViewControllerViewWillDisappear:(ASInterstitialAdViewController*)viewController {
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerViewWillDisappear:"];
    [self.delegate interstitialAdProviderWillDisappear:self];
}

- (void)interstitialAdViewControllerViewDidDisappear:(ASInterstitialAdViewController*)viewController {
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerViewDidDisappear:"];
    [self.delegate interstitialAdProviderDidDisappear:self];
    
    // release the presented view controller
    self.viewControllerToPresent = nil;
}

- (void)interstitialAdViewControllerCreatedAdSuccessfully:(ASInterstitialAdViewController*)controller {
    if (self.isPreload) {
        [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerCreatedAdSuccessfully: - as preload"];
        [self.delegate interstitialAdProvider:self didPreloadAd:controller];
    } else {
        [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerCreatedAdSuccessfully:"];
        [self.delegate interstitialAdProvider:self didLoadAd:controller];
    }
}

- (void)interstitialAdViewControllerFailed:(ASInterstitialAdViewController *)controller withError:(NSError*)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerFailed:withError: - error: %@", error.localizedDescription]];
    [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
}

- (void)interstitialAdViewControllerAdLoadingWasCancelled:(ASInterstitialAdViewController*)controller {
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerAdLoadingWasCancelled:"];
}

- (void)interstitialAdViewControllerAdFinished:(ASInterstitialAdViewController*)controller {
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerAdFinished:"];

	// Fire the VC rewarded event when the ad has completed to notify the app that VC should be rewarded
	NSDictionary *vcData = [ASVirtualCurrency getVirtualCurrencyData:self.properties[ASInterstitialParameter_HTMLHeaders]];
    if(vcData) {
        [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerAdFinished: - Fire VC Reward"];
		[self.delegate interstitialAdProvider:self didVirtualCurrencyReward:vcData];
        [ASVirtualCurrency vcServerCallbackWithVCData:vcData];
    } else {
        [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerAdFinished:"];
    }

    [self.delegate interstitialAdProviderDidAdComplete:self];
}

- (void)interstitialAdViewControllerAdWasTouched:(ASInterstitialAdViewController *)controller {
    [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerAdWasTouched:"];
    [self.delegate interstitialAdProviderAdWasTouched:self];
}

- (void)interstitialAdViewControllerDidRewardVC:(ASInterstitialAdViewController *)controller {
    
    // Fire the VC rewarded event when the ad has completed to notify the app that VC should be rewarded
    NSDictionary *vcData = [ASVirtualCurrency getVirtualCurrencyData:self.properties[ASInterstitialParameter_HTMLHeaders]];
    if(vcData) {
        [ASSDKLogger logStatement:@"ASInternalBaseInterstitialAdProvider, interstitialAdViewControllerAdFinished: - Fire VC Reward"];
        [self.delegate interstitialAdProvider:self didVirtualCurrencyReward:vcData];
        [ASVirtualCurrency vcServerCallbackWithVCData:vcData];
    }
}


- (void)AVPlayerCreated:(AVPlayer *)avPlayer {
    [self.delegate AVPlayerCreated:avPlayer];
}

- (BOOL)interstitialAdViewControllerIsPreload:(ASInterstitialAdViewController *)controller {
    return self.isPreload;
}

- (void)interstitialAdViewController:(ASInterstitialAdViewController *)controller didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate interstitialAdProvider:self didFireAdvertiserEventWithMessage:msg];
}

@end

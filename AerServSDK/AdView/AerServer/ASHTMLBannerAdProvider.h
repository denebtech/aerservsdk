//
//  ASHTTPBanner.h
//  AerServSDK
//
//  Created by Scott A Andrew on 4/14/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASBannerAdProvider.h"

#define kShowHTML 0
#define kUsePredefinedHTML 0
#define kPredefinedHTML @"BannerSizingIssue"
#define kPredefinedHTMLFileType @"html"

@interface ASHTMLBannerAdProvider : ASBannerAdProvider

@end

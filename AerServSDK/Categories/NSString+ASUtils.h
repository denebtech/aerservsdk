//
//  NSString+ASUtils.h
//  AerServSDK
//
//  Created by Vasyl Savka on 3/25/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ASUtils)

- (BOOL)checkForEmpty;
- (id)JSONObjectValue;
- (NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
- (NSTimeInterval)timeIntervalValue;
- (NSString *)removeCharWhitespace;
- (NSString *)urlencode;
- (BOOL)containsStr:(NSString *)inStr;

@end

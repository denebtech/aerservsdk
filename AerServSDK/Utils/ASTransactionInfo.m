//
//  ASTransactionInfo.m
//  AerServSDK
//
//  Created by Vasyl Savka on 4/18/16.
//  Copyright © 2016 AerServ, LLC. All rights reserved.
//

#import "ASTransactionInfo.h"

@interface ASTransactionInfo()

@property (nonatomic, copy) NSString *buyer;
@property (nonatomic, strong) NSNumber *price;

@end

@implementation ASTransactionInfo

- (void)dealloc {
    _buyer = nil;
    _price = nil;
}

- (instancetype)initWithTransactionObj:(NSDictionary *)transactionObj {
    if(self = [super init]) {
        if(transactionObj != nil) {
            _buyer = transactionObj[kTransactionBuyerNameKey];
            _price = transactionObj[kTransactionBuyerPriceKey];
            [self checkBuyer];
            [self formatPrice];
        }
    }
    
    return self;
}

+ (instancetype)infoWithTransactionObj:(NSDictionary *)transactionObj {
    return [[ASTransactionInfo alloc] initWithTransactionObj:transactionObj];
}

- (NSString *)getBuyer {
    return self.buyer;
}

- (double)getPrice {
    return (!self.price) ? -1.0f : [self.price doubleValue];
}

- (void)checkBuyer {
    if(!self.buyer || [self.buyer isKindOfClass:[NSNull class]]) {
        self.buyer = @"N/A";
    }
}

- (void)formatPrice {
    if(!self.price || [self.price isKindOfClass:[NSNull class]]) {
        self.price = [NSNumber numberWithInteger:0];
        return;
    }
    
    double priceDbl = [self.price doubleValue];
    NSString *priceStr = [NSString stringWithFormat:@"%0.2f", priceDbl];
    self.price = [NSNumber numberWithDouble:[priceStr doubleValue]];
}

- (NSDictionary *)asTransactionInfoObj {
    return @{ kTransactionBuyerNameKey : self.buyer, kTransactionBuyerPriceKey : self.price };
}

- (NSString *)asTransactionInfoJSONStr {
    NSDictionary *jsonObj = [self asTransactionInfoObj];
    
    NSError *err;
    NSData *jsonObjData = [NSJSONSerialization dataWithJSONObject:jsonObj options:0 error:&err];
    
    return (!err) ? [[NSString alloc] initWithData:jsonObjData encoding:NSUTF8StringEncoding] : nil;
}

- (NSDictionary *)createDictionaryContainingBothTransactionAndVCData:(NSDictionary*)vcData {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASTransactionInfo, createDictionaryContainingBothTransactionAndVCData: - vcData: %@", vcData]];
    NSMutableDictionary *tempObj = [NSMutableDictionary dictionaryWithDictionary:vcData];
    [tempObj addEntriesFromDictionary:[self asTransactionInfoObj]];
    return [NSDictionary dictionaryWithDictionary:tempObj];
}

- (void)updateTransactionInfoWithTransactionObj:(NSDictionary *)transObj {
    if(transObj != nil) {
        self.buyer = [transObj objectForKey:kTransactionBuyerNameKey];
        self.price = [transObj objectForKey:kTransactionBuyerPriceKey];
        [self checkBuyer];
        [self formatPrice];
    }
}

- (void)updatedTransactionInfoWithResponseHeadersData:(NSData *)asRespHeaderData {
    if(asRespHeaderData != nil) {
        NSError *err = nil;
        id obj = [NSJSONSerialization JSONObjectWithData:asRespHeaderData options:0 error:&err];
        if(!err && [obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *asHeaders = (NSDictionary *)obj;
            [self updateTransactionInfoWithTransactionObj:asHeaders[kTransactionKey]];
        }
    }
}

@end

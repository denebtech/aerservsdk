//
//  ASVASTVideoPlayerView.h
//  AerServSDK
//
//  Created by Vasyl Savka on 4/30/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ASVideoView;
//@class ASHTTPAdView;

@interface ASVASTVideoPlayerView : UIView

@property (readonly, nonatomic, strong) ASVideoView* videoView;
//@property (nonatomic, weak) ASHTTPAdView* webView;
//@property (nonatomic, weak) ASHTTPAdView* HTTPAdView;

@property (nonatomic, assign) CGRect originalBannerFrame;
@property (nonatomic, assign) CGSize originalAdSize;
@property (nonatomic, assign) CGSize originalHTMLAdSize;

-(CGSize) contentSize;

@end

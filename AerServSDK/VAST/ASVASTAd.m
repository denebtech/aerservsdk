//
//  ASVASTAd.m
//  AerServSDK
//
//  Created by Vasyl Savka on 3/28/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASVASTAd.h"
#import "ASXMLQueryEngine.h"
#import "ASXMLNode.h"
#import "ASVASTParser.h"
#import "ASVASTContainer.h"
#import "ASVASTCreative.h"
#import "ASVASTEvent.h"
#import "ASVASTEventError.h"
#import "ASCommManager.h"

#import "NSURL+ASUtils.h"
//#import "ASMoatKitUtil.h"

NSString* const ASVASTAdImpressionEvent = @"impressions";
NSString* const ASVASTAdErrorEvent = @"errors";

@interface ASVASTAd() <ASVASTCreativeDelegate>

@property (nonatomic, strong) ASVASTEvent *impressionEvent;
@property (nonatomic, strong) ASVASTEventError *errorEvent;

@property (nonatomic, strong) NSMutableArray *creatives;
@property (nonatomic, assign) BOOL isWrapper;
@property (nonatomic, strong) NSURL *wrapperURL;
@property (nonatomic, weak) id<ASVASTAdDelegate> delegate;

@property (nonatomic, assign) BOOL isPreload;

@end

@implementation ASVASTAd

- (void)dealloc {
    _impressionEvent = nil;
    _errorEvent = nil;
    [_creatives removeAllObjects];
    _creatives = nil;
    _wrapperURL = nil;
    _delegate = nil;
}

- (void)processImpressionNodes:(NSArray *)impressionNodes {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTAd, processImpressionNodes: - Number of impression events found: %lu", (unsigned long)[impressionNodes count]]];
    
    for (ASXMLNode* impressionNode in impressionNodes) {
        if (self.impressionEvent == nil) {
            self.impressionEvent = [ASVASTEvent new];
            self.impressionEvent.type = ASVASTEventType_AdImpression;
            self.impressionEvent.canBeSent = YES;
            self.impressionEvent.oneTime = YES;
        }
        
        // I know i should need this but we are gong to just make sure when debugging we see what we expect.
        NSString* urlString = impressionNode.value;
        NSURL* url = [NSURL URLWithAddress:urlString];
        if (url != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTAd, processImpressionNodes: - Impression URL: %@", urlString]];
            [self.impressionEvent addURL:url];
            
//            // check for events.aerserv.com domain, if so start the MOAT tracker for this event
//            NSRange domainRange = [[url host] rangeOfString:kASEventDomain];
//            if(domainRange.location < [url host].length) {
//				// init moat video tracker with impression url
//                [ASMoatKitUtil initMoatVideoTrackerWithTrackingEventURL:url.absoluteString];
//            }

        }
    }
}

- (void)processErrorNodes:(NSArray *)errorNodes {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTAd, processErrorNodes: - Number of error events found: %lu", (unsigned long)[errorNodes count]]];
    for (ASXMLNode* errorNode in errorNodes) {
        if(!errorNode.value)
            continue;
        
        if (!self.errorEvent) {
            self.errorEvent = [ASVASTEventError new];
            self.errorEvent.type = ASVASTEventType_AdError;
            self.errorEvent.canBeSent = YES;
            self.errorEvent.oneTime = YES;
        }
        
        NSString* urlString = errorNode.value;
        NSURL* url = [NSURL URLWithAddress:urlString];
        if (url != nil) {
            [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTAd, processErrorNodes: - Error URL:%@", urlString]];
            [self.errorEvent addURL:url];
        }
    }
}

- (void)checkForVASTErrorsFromLinearNode:(ASXMLNode *)linearNode {
    [ASSDKLogger logStatement:@"ASVASTAd, checkForVASTErrorFromLinearNode"];
    
    if(!linearNode) return;
    
    __weak ASVASTAd *vastAd = self;
    
    // checking for media file
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [ASSDKLogger logStatement:@"ASVASTAd, checkForVASTErrorsFromLinearNode: - MEDIA FILE NODE ENTER"];
        if(!vastAd.isWrapper) {
            BOOL missingMediaFile = NO;
            ASXMLNode *mediaFilesNode = [linearNode childNodeNamed:@"MediaFiles"];
            for(ASXMLNode *mediaFileNode in mediaFilesNode.children) {
                if(![mediaFileNode validValue])
                    missingMediaFile = YES;
                else {
                    missingMediaFile = NO;
                    int count = 0;
                    NSDictionary *mediaFileAttr = [mediaFileNode attributes];
                    NSArray *reqMediaFileAttr = @[kVastReqMediaFileAttributeDelivery, kVastReqMediaFileAttributeType];
                    for(NSString *key in mediaFileAttr.allKeys)
                        if([reqMediaFileAttr containsObject:key] && ![mediaFileAttr[key] isEqualToString:kEmptyStr])
                            count++;
                    
                    if(count < reqMediaFileAttr.count) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [vastAd.delegate vastAdDidFailToLoad:vastAd
                                                       withError:[NSError errorWithDomain:NSStringFromClass([ASVASTAd class])
                                                                                     code:kVASTErrorCodeNoSupportedMediaFile
                                                                                 userInfo:@{NSLocalizedDescriptionKey : kParseErrorMissingMediaFileAttributes}]
                                                andVastErrorCode:kVASTErrorCodeNoSupportedMediaFile];
                        });
                    }
                    
                }
            }
            
            if(!mediaFilesNode || mediaFilesNode.children.count == 0 || missingMediaFile) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [vastAd.delegate vastAdDidFailToLoad:vastAd
                                               withError:[NSError errorWithDomain:NSStringFromClass([ASVASTAd class])
                                                                             code:kVASTErrorCodeNoSupportedMediaFile
                                                                         userInfo:@{NSLocalizedDescriptionKey : kParseErrorMissingMediaFile}]
                                        andVastErrorCode:kVASTErrorCodeNoSupportedMediaFile];
                });
            }
            mediaFilesNode = nil;
        }
        [ASSDKLogger logStatement:@"ASVASTAd, checkForVASTErrorsFromLinearNode: - MEDIA FILE NODE EXIT"];
    });
}

- (BOOL)didFindVpaidWhileProcessingCreativeNodes:(NSArray *)creativeNodes {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTAd, processCreativeNodes: - Number of creatives found: %lu", (unsigned long)[creativeNodes count]]];
    NSMutableArray* videos = [NSMutableArray new];
    NSInteger sequence = 0;
    BOOL isVpaid = NO;
    
    for (ASXMLNode* creativeNode in creativeNodes) {
        ASXMLNode *linearNode = [creativeNode childNodeNamed:@"Linear"];
        
        if(!self.isWrapper)
            [self checkForVASTErrorsFromLinearNode:linearNode];
        
        sequence = [creativeNode.attributes[@"sequence"] integerValue];
        
        if (!self.creatives)
            self.creatives = [NSMutableArray new];
        
        if (linearNode != nil) {
            ASVASTCreative* creative = nil;
            creative = [[ASVASTCreative alloc] initWithXMLNode:linearNode withDelegate:self];
            
            if (creative != nil) {
                if(creative.isVpaid) {
                    [ASSDKLogger logStatement:@"ASVASTAd, processCreativeNodes: - Creative Node is VPAID"];
                    isVpaid = YES;
                }
                creative.playOrder = sequence;
                [videos addObject:creative];
            }
        }
    }
    
    // we need to sort our videos based on creative play order
    [videos sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        ASVASTCreative *creative1 = obj1;
        ASVASTCreative *creative2 = obj2;
        
        if (creative1.playOrder > creative2.playOrder)
            return NSOrderedDescending;
        
        if (creative1.playOrder < creative2.playOrder)
            return NSOrderedAscending;
        
        return NSOrderedSame;
        
    }];
    self.creatives = videos;
    
    return isVpaid;
}

- (instancetype)initWithXMLNode:(ASXMLNode *)node withDelegate:(id<ASVASTAdDelegate>)delegate asPreload:(BOOL)preload partOfAdPods:(BOOL)adPods {
    if(self = [super init]) {
        if(!node) return nil;
        
        _delegate = delegate;
        _isPreload = preload;
        
        // checking if we're a wrapper
        ASXMLNode *wrapperNode = [node childNodeNamed:@"Wrapper"];
        if(wrapperNode != nil) {
            [ASSDKLogger logStatement:@"ASVASTAd, initWIthXMLNode:withDelegate:asPreload: - is wrapper"];
            _isWrapper = YES;
            
            // checking if it's a wrapper or if the VASTAdTagURI element exists
            ASXMLNode *vastAdTagURINode = [wrapperNode childNodeNamed:@"VASTAdTagURI"];
            if(vastAdTagURINode != nil) {
                _wrapperURL = [NSURL URLWithAddress:[vastAdTagURINode value]];
            } else {
                [_delegate vastAdDidFailToLoad:self
                                     withError:[NSError errorWithDomain:NSStringFromClass([ASVASTAd class])
                                                                   code:kVASTErrorCodeSchemaValidationError
                                                               userInfo:@{NSLocalizedDescriptionKey : kParseErrorMissingVASTAdTagURI}]
                              andVastErrorCode:kVASTErrorCodeSchemaValidationError];
                return nil;
            }

            
            // parse wrapper impressions
            NSArray *impressionNodes = [wrapperNode childrenNodesNamed:@"Impression"];
            if(impressionNodes != nil && impressionNodes.count > 0)
                [self processImpressionNodes:impressionNodes];
            impressionNodes = nil;
            
            // parse wrapper errors
            NSArray *errorNodes = [wrapperNode childrenNodesNamed:@"Error"];
            if(errorNodes != nil && errorNodes.count > 0)
                [self processErrorNodes:errorNodes];
            errorNodes = nil;
            
            // parse wrapper creative
            ASXMLNode *creativesNode = [wrapperNode childNodeNamed:@"Creatives"];
            if(creativesNode != nil) {
                NSArray *creativeNodeArr = [creativesNode childrenNodesNamed:@"Creative"];
                if(creativeNodeArr != nil && creativeNodeArr.count > 0)
                    [self didFindVpaidWhileProcessingCreativeNodes:creativeNodeArr];
            }
        } else {
            [ASSDKLogger logStatement:@"ASVASTAd, initWIthXMLNode:withDelegate:asPreload: - not a wrapper"];
            
            ASXMLNode *inlineNode = [node childNodeNamed:@"InLine"];
            BOOL isVpaid = NO;
            BOOL goodVideo = NO;
            if(inlineNode != nil) {
                
                // parse inline impressions
                NSArray *impressionNodes = [inlineNode childrenNodesNamed:@"Impression"];
                if(impressionNodes != nil && impressionNodes.count > 0) {
                    [self processImpressionNodes:impressionNodes];
                }
                
                // parse inline errors
                NSArray *errorNodes = [inlineNode childrenNodesNamed:@"Error"];
                if(errorNodes != nil && errorNodes.count > 0)
                    [self processErrorNodes:errorNodes];
                
                
                // parse wrapper creative
                ASXMLNode *creativesNode = [inlineNode childNodeNamed:@"Creatives"];
                NSArray *creativeNodeArr = [creativesNode childrenNodesNamed:@"Creative"];
                if(creativesNode != nil && creativeNodeArr != nil && creativeNodeArr.count > 0) {
                    // look for vpaid while processing the creative nodes
                    isVpaid = [self didFindVpaidWhileProcessingCreativeNodes:creativeNodeArr];
                    
                    // validate that there is at least one good video creative
                    for(ASVASTCreative *tmpCreative in _creatives) {
                        if(tmpCreative.url != nil)
                            goodVideo = YES;
                    }
                }
            }
            
            if(goodVideo == NO) {
                [ASSDKLogger logStatement:@"ASVASTAd, initWithXMLData:withDelegate: - Throwing ad away because a valid video url could not be found"];
                [_delegate vastAdDidFailToLoad:self
                                     withError:[NSError errorWithDomain:NSStringFromClass([ASVASTAd class])
                                                                   code:kVASTErrorCodeNoSupportedMediaFile
                                                               userInfo:@{NSLocalizedDescriptionKey : @"Ad failed to load because the url could not be found."}]
                              andVastErrorCode:kVASTErrorCodeNoSupportedMediaFile];
                
                return nil;
            }
            
            // the VAST video is good, if not part of ad pods trigger creation of the associated view
            [ASSDKLogger logStatement:@"ASVASTAd, initWithXMLData:withDelegate: - Inline VAST ad parsed with no issues, create associated view"];
            if(!adPods) {
                [_delegate vastAdDidLoad:self];
            }
        }
    }
    
    return self;
}

- (void)getWrapperAd:(NSURL*)wrapperURL {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTAd, getWrapperAd - wrapperURL: %@", wrapperURL]];
    
    if(!wrapperURL) {
        wrapperURL = [NSURL URLWithString:@""];
    }
    
    __weak ASVASTAd *ad = self;
    [ASCommManager sendGetRequestToEndPoint:wrapperURL forTime:kVASTWrapperAdTimeout endWithCompletionBlock:^(NSURLResponse *resp, NSData *data, NSError *err) {
        @try {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)resp;
            
            // we got an add lets see what we need to do.
            if (httpResponse.statusCode == 200) {
                [ASSDKLogger logStatement:@"ASVASTAd, getWrapperAd - Response received, init VAST ad from wrapperURL"];
                ASVASTContainer *retrievedAdContainer = [ASVASTParser findAdWithXMLData:data withDelegate:ad.delegate asPreload:ad.isPreload];
                
                // merge the retrieved ad into ours.
                [retrievedAdContainer.vastAd mergeIntoAd:ad];
                NSInteger retrievedAdCreativeCount = [retrievedAdContainer.vastAd.creatives count];
                
                // copy the creatives.
                [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVASTAd, getWrapperAd - Number of creatives to copy over: %ld", (long)retrievedAdCreativeCount]];
                for (int creativeNumber = 0; creativeNumber < retrievedAdCreativeCount; creativeNumber++) {
                    ASVASTCreative *creative = retrievedAdContainer.vastAd.creatives[creativeNumber];
                    
                    // if we have a matching creative. Then merge it into ours. Otherwise
                    // add a copy of the creative.
                    if (creativeNumber < [ad.creatives count]) {
                        ASVASTCreative *ourCreative = self.creatives[creativeNumber];
                        [creative mergeIntoCreative:ourCreative];
                        [retrievedAdContainer.vastAd.creatives replaceObjectAtIndex:creativeNumber withObject:ourCreative];
                    }
                    else {
                        [ad.creatives addObject:creative];
                    }
                }
                
                if(retrievedAdContainer.vastAd.seqAd != nil) {
                    ad.seqAd = retrievedAdContainer.vastAd.seqAd;
                }
                
                // if the ad is wrapper dig deeper. otherwise let our delegate know we are loaded.
                if (retrievedAdContainer.vastAd.isWrapper) {
                    [ASSDKLogger logStatement:@"ASVASTAd, getWrapperAd - Another wrapper level found, proceed to unwrap ad"];
                    [ad getWrapperAd:retrievedAdContainer.vastAd.wrapperURL];
                }
            }
            else {
                [ASSDKLogger logStatement:@"ASVASTAd, getWrapperAd - Bad response received, VAST ad failed to load"];
                [ad.delegate vastAdDidFailToLoad:ad
                                       withError:err
                                andVastErrorCode:kVASTErrorCodeNoInlineVASTAfterWrappers];
            }
        }
        @catch (NSException *exception) {
            [ASSDKLogger onException:exception];
            [ad.delegate vastAdDidFailToLoad:ad
                                   withError:[NSError errorWithDomain:NSStringFromClass([ASVASTAd class])
                                                                 code:kVASTErrorCodeGeneralWrapperError
                                                             userInfo:@{NSLocalizedDescriptionKey : @"Ad Faield to load because there was an issue processing the provided wrapper URI"}]
                            andVastErrorCode:kVASTErrorCodeGeneralWrapperError];
        }
    }];
}

- (void)resolveWrappedAd {
    [ASSDKLogger logStatement:@"ASVASTAd, resolveWrapperAd"];
    [self getWrapperAd:self.wrapperURL];
}

- (void)mergeIntoAd:(ASVASTAd *)adToMergeInto {
    [ASSDKLogger logStatement:@"ASVASTAd, mergeIntoAd"];
    adToMergeInto.isPreload = self.isPreload;
    
    // we need to merge just the events.
    if (adToMergeInto.impressionEvent == nil) {
        adToMergeInto.impressionEvent = [self.impressionEvent copy];
    }
    else {
        [adToMergeInto.impressionEvent addURLs:self.impressionEvent.urls];
    }
    
    if (adToMergeInto.errorEvent == nil) {
        adToMergeInto.errorEvent = [self.errorEvent copy];
    }
    else {
        [adToMergeInto.errorEvent addURLs:self.errorEvent.urls];
    }
}

#pragma mark - ASVASTCreativeDelegate Protocol

- (void)vastCreative:(ASVASTCreative *)creative encountersVASTError:(ASVASTErrorCode)errCode {
    [ASCommManager sendAsynchronousRequestsForEvent:[self.errorEvent replaceErrorMacroWithCode:errCode]];
    [self.delegate vastAd:self didFireAdvertiserEventWithMessage:[NSString stringWithFormat:@"VAST Ad Fires Error Event from Creative with errCode: %ld", (long)errCode]];
}

@end

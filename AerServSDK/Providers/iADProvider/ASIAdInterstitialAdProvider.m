//
//  ASIAdInterstitialAdProvider.m
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/27/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <iAd/iAd.h>
#import "ASIAdInterstitialAdProvider.h"
#import "ASIAdViewController.h"
#import "ASVirtualCurrency.h"

@interface ASIAdInterstitialAdProvider() <ASInterstitialAdViewControllerDelegate>

@property (nonatomic, strong) ASIAdViewController *viewController;
@property (nonatomic, strong) NSDictionary *properties;
@property (nonatomic, assign) BOOL isPreload;

@end



@implementation ASIAdInterstitialAdProvider

- (void)dealloc {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, dealloc"];
    _viewController = nil;
    _properties = nil;
}

- (void)requestInterstitialAdWithProperties:(NSDictionary *)info isPreload:(BOOL)preload {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASIAdInterstitialAdProvider, requestInterstitialAdWithProperties:isPreload: - preload: %d", preload]];
    self.isPreload = preload;
    self.properties = info;
    self.viewController = [[ASIAdViewController alloc] initWithDelegate:self];
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    rootViewController.interstitialPresentationPolicy = ADInterstitialPresentationPolicyManual;
    
    if(rootViewController.presentedViewController != nil) {
        [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, showInterstitialFromRootViewController - There's already a view controller presented, dismiss first then present"];
        __weak ASIAdInterstitialAdProvider *iadProvider = self;
        [rootViewController dismissViewControllerAnimated:NO completion:^{
            @try {
                [rootViewController presentViewController:iadProvider.viewController animated:YES completion:nil];
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        }];
    } else {
        [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, showInterstitialFromRootViewController - Present view controller"];
        [rootViewController presentViewController:self.viewController animated:YES completion:nil];
    }
}

#pragma mark - ASInterstitialAdViewControllerDelegate

- (BOOL)shouldShowInterstitialAd {
    return self.viewController.adToDisplay != nil;
}

- (void)interstitialAdViewControllerViewWillAppear:(ASInterstitialAdViewController*)viewController {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerViewWillAppear:"];
    [self.delegate interstitialAdProviderWillAppear:self];
}

- (void)interstitialAdViewControllerViewDidAppear:(ASInterstitialAdViewController*)viewController {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerViewDidAppear:"];
    [self.delegate interstitialAdProviderDidAppear:self];
}

- (void)interstitialAdViewControllerViewWillDisappear:(ASInterstitialAdViewController*)viewController {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerViewWillDisappear:"];
    [self.delegate interstitialAdProviderWillDisappear:self];
}

- (void)interstitialAdViewControllerViewDidDisappear:(ASInterstitialAdViewController*)viewController {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerViewDidDisappear:"];
    [self.delegate interstitialAdProviderWillDisappear:self];
    
    // release the iAd view controller
    self.viewController = nil;
}

- (void)interstitialAdViewControllerCreatedAdSuccessfully:(ASInterstitialAdViewController*)controller {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerCreatedAdSuccessfully:"];
    
    if(self.isPreload) {
        [self.delegate interstitialAdProvider:self didPreloadAd:self.viewController];
    } else {
        [self.delegate interstitialAdProvider:self didLoadAd:self.viewController];
    }
}

- (void)interstitialAdViewControllerFailed:(ASInterstitialAdViewController *)controller withError:(NSError *)error {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerFailed - error: %@", error.localizedDescription]];
    [self.delegate interstitialAdProvider:self didFailToLoadAdWithError:error];
}

- (void)interstitialAdViewControllerAdLoadingWasCancelled:(ASInterstitialAdViewController*)controller {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerAdLoadingWasCancelled"];
}

- (void)interstitialAdViewControllerAdFinished:(ASInterstitialAdViewController*)controller {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerAdFinished"];
    self.viewController = nil;
}

- (void)interstitialAdViewControllerAdWasTouched:(ASInterstitialAdViewController *)controller {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerAdWasTouched"];
    [self.delegate interstitialAdProviderAdWasTouched:self];
}

- (void)interstitialAdViewControllerDidRewardVC:(ASInterstitialAdViewController *)controller {
    NSDictionary *vcData = [ASVirtualCurrency getVirtualCurrencyData:self.properties[ASInterstitialParameter_HTMLHeaders]];
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerAdWasTouched - vcData; %@", vcData]];
    if(vcData != nil) {
        [self.delegate interstitialAdProvider:self didVirtualCurrencyReward:vcData];
        [ASVirtualCurrency vcServerCallbackWithVCData:vcData];
    }
}

- (void)AVPlayerCreated:(AVPlayer *)avPlayer{
    [self.delegate AVPlayerCreated:avPlayer];
}

- (BOOL)interstitialAdViewControllerIsPreload:(ASInterstitialAdViewController *)controller {
    [ASSDKLogger logStatement:@"ASIAdInterstitialAdProvider, interstitialAdViewControllerViewDidAppear"];
    return self.isPreload;
}

- (void)interstitialAdViewController:(ASInterstitialAdViewController *)controller didFireAdvertiserEventWithMessage:(NSString *)msg {
    [self.delegate interstitialAdProvider:self didFireAdvertiserEventWithMessage:msg];
}

@end

//
//  ASMRAIDCloseButton.m
//  AerServSDK
//
//  Created by Vasyl Savka on 9/9/15.
//  Copyright (c) 2015 AerServ, LLC. All rights reserved.
//

#import "ASMRAIDCloseButton.h"

@implementation ASMRAIDCloseButton

- (instancetype)init {
    self = [super init];
    
    self.contentMode = UIViewContentModeRedraw;
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    // fill the background
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(8.0f, 8.0f, 32.0f, 32.0f)];
    UIColor* bgColor = self.isHighlighted ? [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.4f] : [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.4f];
    [bgColor setFill];
    [ovalPath fill];
    
    // create X
    NSString* closeX = @"X";
    UIFont *closeXFont = [UIFont fontWithName:@"AvenirNext-Medium" size:20];
    UIColor* fontColor = self.isHighlighted ? [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f] : [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.lineBreakMode = NSLineBreakByClipping;
    textStyle.alignment = NSTextAlignmentCenter;
    NSDictionary *closeXAttr = @{NSFontAttributeName : closeXFont,
                                 NSParagraphStyleAttributeName : textStyle,
                                 NSForegroundColorAttributeName : fontColor};
    [closeX drawAtPoint:CGPointMake(17.0f, 11.0f) withAttributes:closeXAttr];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    [self setNeedsDisplay];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    [self setNeedsDisplay];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    
    [self setNeedsDisplay];
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(32.0f, 32.0f);
}

@end

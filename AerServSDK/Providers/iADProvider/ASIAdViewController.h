//
//  ASIAdViewController.h
//  AerServSampleApp
//
//  Created by Scott A Andrew on 5/27/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import "ASInterstitialAdViewController.h"
#import <Foundation/Foundation.h>
@class ADInterstitialAd;

@interface ASIAdViewController : ASInterstitialAdViewController

@property (strong, nonatomic) UIView* adToDisplay;
@property (readonly, nonatomic, strong) UIScrollView *scrollView;

@end

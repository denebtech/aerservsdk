//
//  ASAppLovinDelegateProxy.m
//  AerServSDK
//
//  Created by Albert Zhu on 11/25/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASAppLovinDelegateProxy.h"
#import "ASAppLovinInstance.h"

#import "ASVirtualCurrency.h"

@implementation ASAppLovinDelegateProxy

+ (ASAppLovinDelegateProxy *)sharedProxy {
    static ASAppLovinDelegateProxy *sharedProxy = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedProxy = [ASAppLovinDelegateProxy new];
        
    });
    
    return sharedProxy;
}

+ (void)addAppLovinProvider:(ASAppLovinInterstitialAdProvider *)provider {
    [ASAppLovinDelegateProxy sharedProxy].provider = provider;
}

+ (void)removeAppLovinProvider {
    [ASAppLovinDelegateProxy sharedProxy].provider = nil;
}

#pragma mark - ALAdLoadDelegate Methods

- (void)adService:(nonnull ALAdService *)adService didLoadAd:(nonnull ALAd *)ad {
    [ASSDKLogger logStatement:@"ASAppLovinDelegateProxy, adService:didLoadAd:"];
    
    [[ASAppLovinDelegateProxy sharedProxy].provider asPartnerInterstitialAdDidLoad:ad];
    
    [ASAdResponseLogger endAdResponseAndSendEvent];
}

- (void)adService:(nonnull ALAdService *)adService didFailToLoadAdWithError:(int)code {
    NSString *msg;
    switch (code) {
        case 204:
            msg = @"No fill";
            break;
        case -1001:
            msg = @"Ad request network timeout";
            break;
        case -1:
            msg = @"Ad request unspecified error";
            break;
        case -200:
            msg = @"Unable to precache resources";
            break;
        case -201:
            msg = @"Unable to precache image resources";
            break;
        case -202:
            msg = @"Unable to precache video resources";
            break;
        case -700:
            msg = @"Unable to render native ad";
            break;
        case -701:
            msg = @"Unable to preload native ad";
            break;
        default:
            msg = @"Unknown error";
            break;
    }
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinDelegateProxy, adService:didFailToLoadAdWithError: %@", msg]];
    
    self.provider.failedToReceiveAd = YES;
    [self.provider cleanupAd];
}

#pragma mark - ALAdDisplayDelegate Methods

- (void)ad:(nonnull ALAd *)ad wasDisplayedIn:(nonnull UIView *)view {
    [ASSDKLogger logStatement:@"ASAppLovinDelegateProxy, ad:wasDisplayedIn:"];
    [self.provider asPartnerInterstitialAdWillAppear];
    [self.provider asPartnerInterstitialAdDidAppear];
}

- (void)ad:(nonnull ALAd *)ad wasClickedIn:(nonnull UIView *)view {
    [ASSDKLogger logStatement:@"ASAppLovinDelegateProxy, ad:wasClickedIn:"];
    [self.provider asPartnerInterstitialAdWasTouched];
}

- (void)ad:(nonnull ALAd *)ad wasHiddenIn:(nonnull UIView *)view {
    [ASSDKLogger logStatement:@"ASAppLovinDelegateProxy, ad:wasHiddenIn:"];
    [self.provider asPartnerInterstitialAdWillDisappear];
    [self.provider asPartnerInterstitialAdDidDisappear];
    [self.provider cleanupAd];
}

#pragma mark - ALAdVideoPlaybackDelegate Methods

- (void)videoPlaybackBeganInAd:(nonnull ALAd *)ad {
    [ASSDKLogger logStatement:@"ASAppLovinDelegateProxy, videoPlaybackBeganInAd:"];
}

- (void)videoPlaybackEndedInAd:(nonnull ALAd *)ad atPlaybackPercent:(nonnull NSNumber *)percentPlayed fullyWatched:(BOOL)wasFullyWatched {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinDelegateProxy, videoPlaybackEndedInAd:atPlaybackPercent:fullywatched:- percentPlayed: %@, fullyWatched: %d", percentPlayed, wasFullyWatched]];
    
    if (wasFullyWatched) {
        [self.provider asPartnerInterstitialVideoCompleted];
    }
}

#pragma mark - ALAdRewardDelegate

- (void)rewardValidationRequestForAd:(ALAd *)ad didSucceedWithResponse:(NSDictionary *)response {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinDelegateProxy, rewardValidationRequestForAd:didSuceedWithResponse:"]];
    
//    // Ignoring AL VC amounts for now
//
//    // TODO: Pass through logic will go here
//    
//    respnose[@"currency"]
//    response[@"amount"]
}

- (void)rewardValidationRequestForAd:(ALAd *)ad wasRejectedWithResponse:(NSDictionary *)response {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinDelegateProxy, rewardValidationRequestForAd:wasRejectedWithResponse"]];
}

- (void)rewardValidationRequestForAd:(ALAd *)ad didFailWithError:(NSInteger)responseCode {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinDelegateProxy, rewardValidationRequestForAd:didFailWithError - responseCode: %ld", (long)responseCode]];
}

- (void)userDeclinedToViewAd:(ALAd *)ad {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinDelegateProxy, userDeclinedToViewAd:"]];
    
    // OPTIONAL
}

- (void)rewardValidationRequestForAd:(ALAd *)ad didExceedQuotaWithResponse:(NSDictionary *)response {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASAppLovinDelegateProxy, rewardValidationRequestForAd:didExceedQuotaWithResponse: -- documentation says this is \"No longer used\""]];
}

@end

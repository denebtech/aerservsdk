//
//  ASSpecificMediaBannerAdProvider.m
//  SpecificMediaProvider
//
//  Created by Arash Sharif on 9/8/14.
//  Copyright (c) 2014 AerServ. All rights reserved.
//

#import "ASSpecificMediaBannerAdProvider.h"
#import "Vindico.h"
#import "VindicoAdProperties.h"

@implementation ASSpecificMediaBannerAdProvider


- (void) dealloc {
    
    self.adView = nil;
    self.request = nil;
}

- (void)requestAdWithSize:(CGSize)size withProperties:(NSDictionary *)properties {
    
    NSDictionary *params = properties[ASBannerParameter_Parameters];
    NSError *error;
    
    if (params[@"locationId"] == nil) {
        error = [NSError errorWithDomain:NSStringFromClass([self class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Invalid locationId"}];
    }
    
    if (error != nil) {
        [self.delegate bannerProvider:self didFailToLoadAdWithError:error];
        return;
    }
    
  
    
    // Request an ad from Vindico with a Location ID
    self.request = [[VindicoAdRequest alloc] initWithLocationID:params[@"locationId"] adType:VindicoDisplayAd];
    self.adView = [[Vindico shared] adViewWithType:VindicoDisplayAd];
    
    [self.adView getAdWithRequest:self.request onCompletion:^(NSObject<VindicoAdRequestProtocol> *request, VindicoAdView *adView, NSError *error) {
        if(!error) {
            
            self.adView.frame = CGRectMake(0, 0, size.width, size.height);
            [self.delegate bannerProvider:self didLoadAd:adView];
            
        }
        else {
            [self.delegate bannerProvider:self didFailToLoadAdWithError:error];
        }
    }];

    
}
@end

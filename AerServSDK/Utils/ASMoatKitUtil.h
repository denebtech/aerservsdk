//
//  ASMoatKitUtil.h
//  AerServSDK
//
//  Created by Albert Zhu on 12/8/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#ifndef AerServSDK_ASMoatKitUtil_h
#define AerServSDK_ASMoatKitUtil_h

#define kMoatTrackerClass @"AERMoatTracker"
#define kMoatVideoTrackerClass @"AERMoatVideoTracker"

#define kMoatEnabledKey @"isMoatEnabled"

#define kMoatAerServDisplayPartnerCode @"aerservnative722147260635"
#define kMoatAerServVideoPartnerCode @"aerservnativevideo709050655357"
#define kMoatAerServCampaignID @"AerServ"
#define kMoatBundleDisplayNameKey @"CFBundleName"

#define kEventsAerServDomain @"events.aerserv"
#define kVideoStartEventStr @"ev=2"
#define kBPFEventStr @"ev=22"
#define kQueryStringAdvertiserKey @"buyer"
#define kQueryStringLineItemKey @"iline"
#define kQueryStringCreativeItemKey @"icid"
#define kQueryStringPlacementKey @"plc"
#define kQueryStringAmpersandDelimTokenStr @"&"

#define kMoatVideoAdIdsAdvertiserKey @"level1"
#define kMoatVideoAdIdsCampaignKey @"level2"
#define kMoatVideoAdIdsLineItemKey @"level3"
#define kMoatVideoAdsIdsCreativeKey @"level4"
#define kMoatVideoAdIdsAppKey @"slicer1"
#define kMoatVideoAdIdsPlacmentKey @"slicer2"

#define kMoatDisplayAdIdsAdvertiserKey @"moatClientLevel1"
#define kMoatDisplayAdIdsCampaignKey @"moatClientLevel2"
#define kMoatDisplayAdIdsLineItemKey @"moatClientLevel3"
#define kMoatDisplayAdsIdsCreativeKey @"moatClientLevel4"
#define kMoatDisplayAdIdsAppKey @"moatClientSlicer1"
#define kMoatDisplayAdIdsPlacmentKey @"moatClientSlicer2"

#define kASDefaultBuyer @"AermarketHosted"


@interface ASMoatKitUtil : NSObject

+ (ASMoatKitUtil *)tracker;

// checking for moat enabled
+ (void)setMoatBitWithASProperties:(NSDictionary *)properties;
+ (BOOL)checkForUseOfMoat;

// moat video tracking
+ (void)initMoatVideoTrackerWithTrackingEventURL:(NSString *)eventUrlStr;
+ (void)startMoatVideoTrackerWithAVPlayer:(AVPlayer *)videoPlayer andPlayerLayer:(AVPlayerLayer *)playerLayer insideView:(UIView *)viewContainer;
+ (void)finishMoatVideoTracker;

// moat display tracking
+ (NSString *)findBPFFromHTMLData:(NSData *)htmlData;
+ (void)startMoatDisplayTrackerWithAdView:(UIView *)adView andBPF:(NSString *)bpfUrlStr;
+ (void)cleanUp;

@end

#endif

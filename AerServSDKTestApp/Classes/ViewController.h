//
//  ViewController.h
//  AerServSDKTestApp
//
//  Created by Arash Sharif on 11/5/14.
//  Copyright (c) 2014 AerServ, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kToggleStagingOnStart 0

#define kTogglePreload 0
#define kUsePredefinedPLC 0
#define kPredefinedPLC @"1006088"

#define kLoadInterstitialOnStart 0
#define kLoadBannerOnStart 0
#define kContinousLoad 0

#define kPopUpDisplayTime 1.0f
#define kPredefinedBannerAdSize 60.0f
#define kBannerRefreshInterval 15.0f

#define kTestUserID @"AerServTestID"

#define kTapjoyPlacementName @"End Game Interstitial"

@interface ViewController : UIViewController

@end

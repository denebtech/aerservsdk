//
//  ASVungleInstance.m
//  AerServSDK
//
//  Created by Albert Zhu on 11/24/15.
//  Copyright © 2015 AerServ, LLC. All rights reserved.
//

#import "ASVungleInstance.h"
#import "VungleSDK.h"

@implementation ASVungleInstance

+ (ASVungleInstance *)instance {
    static ASVungleInstance *instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [ASVungleInstance new];
    });
    
    return instance;
}

+ (void)vungleSetupWithConfig:(NSDictionary *)adapterConfig {
    [ASSDKLogger logStatement:[NSString stringWithFormat:@"ASVungleInstance, vungleSetupWithConfig: - adapterConfig: %@", adapterConfig]];
    
    [ASVungleInstance instance].vungleClass = NSClassFromString(@"VungleSDK");
    
    NSError *err = nil;
    if(!adapterConfig[kVungleAppIDKey]) {
        [ASSDKLogger logStatement:@"ASVungleInstance, vungleSetupWithConfig - Invalid vungle app id "];
        err = [NSError errorWithDomain:NSStringFromClass([ASVungleInstance class]) code:100 userInfo:@{NSLocalizedDescriptionKey : @"Vungle Instance, Invalid AppID"}];
    }
    
    if(![ASVungleInstance instance].vungleClass || err != nil || kIS_iOS_6) {
        [ASSDKLogger logStatement:@"ASVungleInstance, vungleSetupWithConfig - Could not create Vungle class object. Will try again later during request"];
        [ASVungleInstance instance].vungleClass = nil;
    } else {
        [ASVungleInstance instance].state = kASProviderSetupStarted;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                [[[ASVungleInstance instance].vungleClass sharedSDK] startWithAppId:adapterConfig[kVungleAppIDKey]];
                [ASVungleInstance instance].state = kASProviderSetupReady;
            }
            @catch (NSException *exception) {
                [ASSDKLogger onException:exception];
            }
        });
    }
}

@end
